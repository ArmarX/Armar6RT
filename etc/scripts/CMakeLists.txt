configure_file("startArmar6.sh.in" "${ARMARX_BIN_DIR}/startArmar6.sh" @ONLY)
configure_file("shutdown-Armar6.sh.in" "${ARMARX_BIN_DIR}/shutdown-Armar6.sh" @ONLY)
configure_file("stopArmar6-remote.sh.in" "${ARMARX_BIN_DIR}/stopArmar6-remote.sh" @ONLY)
configure_file("startArmar6-remote.sh.in" "${ARMARX_BIN_DIR}/startArmar6-remote.sh" @ONLY)
configure_file("startArmar6-demo.sh.in" "${ARMARX_BIN_DIR}/startArmar6-demo.sh" @ONLY)
configure_file("armar6-sync.py.in" "${ARMARX_BIN_DIR}/armar6-sync.py" @ONLY)

exec_program(chmod "${ARMARX_BIN_DIR}"
             ARGS a+x *.sh)
exec_program(chmod "${ARMARX_BIN_DIR}"
            ARGS a+x *.py)

    set(FILES ${FILES}
        "${ARMARX_BIN_DIR}/startArmar6.sh"
        "${ARMARX_BIN_DIR}/shutdown-Armar6.sh"
        "${ARMARX_BIN_DIR}/startArmar6-remote.sh"
        "${ARMARX_BIN_DIR}/stopArmar6-remote.sh"
	"${ARMARX_BIN_DIR}/startArmar6-demo.sh"
        "${ARMARX_BIN_DIR}/armar6-sync.py"
    )


install(PROGRAMS ${FILES}
    COMPONENT binaries
    DESTINATION bin)
