/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_rt::ArmarXObjects::Armar6Unit
 * @author     Pascal Weiner ( pascal dot weiner at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Armar6Unit.h"

#include <chrono>
#include <filesystem>
#include <sstream>
#include <thread>

#include <sched.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/syscall.h>

#include <boost/algorithm/clamp.hpp>

#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/Tools/Gravity.h>

#include <ArmarXCore/core/ArmarXObjectScheduler.h>
#include <ArmarXCore/core/rapidxml/wrapper/MultiNodeRapidXMLReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/time/CycleUtil.h>
#include <ArmarXCore/util/CPPUtility/trace.h>
#include <RobotAPI/components/units/RobotUnit/Units/RobotUnitSubUnit.h>
#include <RobotAPI/components/units/RobotUnit/util/RtTiming.h>
#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointController.h>
#include <armarx/control/ethercat/Bus.h>

#include <devices/ethercat/common/elmo/gold/Slave.h>
#include <devices/ethercat/common/imagine_board/Slave.h>
#include <devices/ethercat/common/sensor_board/armar6/Slave.h>
#include <devices/ethercat/ethercat_hub/Slave.h>
#include <devices/ethercat/ft_sensor_board/armar6/Slave.h>
#include <devices/ethercat/ft_sensor_board/armar6/Device.h>
#include <devices/ethercat/hand/armar6_v1/Device.h>
#include <devices/ethercat/hand/armar6_v2/Device.h>
#include <devices/ethercat/head/armar6/Device.h>
#include <devices/ethercat/platform/armar6_mecanum/Device.h>
#include <devices/ethercat/sensor_actor_unit/armar6/Device.h>
#include <devices/ethercat/torso/armar6_prismatic/Device.h>
#include <devices/ethercat/head_board/armar7de/Device.h>

#include <armar6/rt/units/Armar6Unit/Logging/LogEntry.h>
#include <armar6/rt/units/Armar6Unit/Units/KITHandUnit.h>


namespace armarx
{

    Armar6Unit::Armar6Unit()
    {
        namespace ecat = armarx::control::ethercat;
        namespace dev = devices::ethercat;

        // Slaves.
        using Elmo = dev::common::elmo::gold::Slave;
        using SensorBoard = dev::common::sensor_board::armar6::Slave;
        using EtherCATHub = dev::ethercat_hub::Slave;

        // Devices.
        using SensorActorUnit = dev::sensor_actor_unit::armar6::Device;
        using Platform = dev::platform::armar6_mecanum::Device;
        using Torso = dev::torso::armar6_prismatic::Device;

        // Slaves and devices.
        namespace HandV1 = dev::hand::armar6_v1;
        namespace HandV2 = dev::hand::armar6_v2;
        namespace FTSensorBoard = dev::ft_sensor_board::armar6;
        namespace HeadBoard = dev::head_board::armar7de;

        ecat::Bus& bus = ecat::Bus::getBus();

        // Register slave factories.
        bus.registerSlaveFactory(&ecat::createSlave<Elmo>);
        bus.registerSlaveFactory(&ecat::createSlave<SensorBoard>);
        bus.registerSlaveFactory(&ecat::createSlave<EtherCATHub>);
        bus.registerSlaveFactory(&ecat::createSlave<FTSensorBoard::Slave>);
        bus.registerSlaveFactory(&ecat::createSlave<HandV1::Slave>);
        bus.registerSlaveFactory(&ecat::createSlave<HandV2::Slave>);
        bus.registerSlaveFactory(&ecat::createSlave<HeadBoard::Slave>);

        // Register device factories.
        bus.registerDeviceFactory("Joint", &ecat::createDevice<SensorActorUnit>);
        bus.registerDeviceFactory("HolonomicPlatform", &ecat::createDevice<Platform>);
        bus.registerDeviceFactory("KITHand", &ecat::createDevice<HandV1::Device>);
        bus.registerDeviceFactory("KITHandV2", &ecat::createDevice<HandV2::Device>);
        bus.registerDeviceFactory("HeadBoardArmar7de", &ecat::createDevice<HeadBoard::Device>);
        bus.registerDeviceFactory("ForceTorqueSensor", &ecat::createDevice<FTSensorBoard::Device>);
        bus.registerDeviceFactory("PrismaticJoint", &ecat::createDevice<Torso>);
    }


    void
    Armar6Unit::onInitRobotUnit()
    {
        using armarx::control::ethercat::Bus;
        using armarx::control::ethercat::RTUnit;

        ARMARX_TRACE;

        ARMARX_INFO << "Armar6Unit initializing now.";

        std::string busConfigFilePath = getProperty<std::string>("BusConfigFilePath").getValue();
        ARMARX_INFO << "Using bus config at `" << busConfigFilePath << "`.";
        MultiNodeRapidXMLReader hwconfig = ReadHardwareConfigFile(busConfigFilePath, "Armar6");

        ARMARX_TRACE;

        Bus& bus = Bus::getBus();
        bus.setAndParseHardwareConfig(hwconfig);

        ARMARX_TRACE;

        RTUnit::onInitRobotUnit();
    }


    void
    Armar6Unit::onConnectRobotUnit()
    {
        ARMARX_TRACE;

        ARMARX_INFO << "Armar6Unit connects now.";

        startRTThread();

        while (getRobotUnitState() < RobotUnitState::Running)
        {
            usleep(100000);
        }

        rtLoggingThread = new SimpleRunningTask<>(
            [this]
            {
                try
                {
                    ARMARX_INFO << "Logging thread started";

                    while (!rtLoggingThread->isStopped())
                    {
                        if (log)
                        {
                            log->writeToFile();
                        }

                        usleep(100000);
                    }

                    ARMARX_INFO << "Logging thread stopping, flush";

                    if (log)
                    {
                        log->writeToFile();
                        log->stopLogging();
                    }
                }
                catch (...)
                {
                    armarx::handleExceptions();
                }
            },
            "RTLoggingThread");

        rtLoggingThread->start();
    }


    void
    Armar6Unit::onDisconnectRobotUnit()
    {
        ARMARX_TRACE;

        ARMARX_INFO << "Armar6Unit disconnects now";
        if (rtLoggingThread)
        {
            rtLoggingThread->stop();
        }
    }


    void
    Armar6Unit::onExitRobotUnit()
    {
        ARMARX_TRACE;

        ARMARX_INFO << "Armar6Unit is exiting now ";

        ARMARX_INFO << "Armar6Unit exit complete";
    }


    armarx::PropertyDefinitionsPtr
    Armar6Unit::createPropertyDefinitions()
    {
        auto def = armarx::control::ethercat::RTUnit::createPropertyDefinitions();
        return def;
    }


    Armar6Unit::~Armar6Unit()
    {
        ARMARX_INFO << "DTOR of Armar6Unit";
    }


    void
    Armar6Unit::rtRun()
    {
        ARMARX_TRACE;

        ARMARX_INFO << "Control task running";

        armarx::control::ethercat::RTUnit::rtRun();

        while (getObjectScheduler() && !getObjectScheduler()->isTerminationRequested())
        {
            ARMARX_INFO << deactivateSpam() << "Waiting for shutdown";
            usleep(10000);
        }
    }


    void
    Armar6Unit::icePropertiesInitialized()
    {
        ARMARX_TRACE;

        armarx::control::ethercat::RTUnit::icePropertiesInitialized();
        leftHandUnit =
            Component::create<KITHandUnit>(getIceProperties(), "LeftHandUnit", getConfigDomain());
        addPropertyUser(PropertyUserPtr::dynamicCast(leftHandUnit));
        rightHandUnit =
            Component::create<KITHandUnit>(getIceProperties(), "RightHandUnit", getConfigDomain());
        addPropertyUser(PropertyUserPtr::dynamicCast(rightHandUnit));
    }


    void
    Armar6Unit::startLogging(const std::string& filename,
                             const Ice::StringSeq& devices,
                             const Ice::Current&)
    {
        ARMARX_TRACE;

        log->startLogging(filename);
    }


    void
    Armar6Unit::stopLogging(const Ice::Current&)
    {
        ARMARX_TRACE;

        log->stopLogging();
    }


    void
    Armar6Unit::initializeDefaultUnits()
    {
        ARMARX_TRACE;

        armarx::control::ethercat::RTUnit::initializeDefaultUnits();

        ARMARX_IMPORTANT << "Setting up custom Units";
        auto icePropertiesMap = getIceProperties()->getPropertiesForPrefix("");

        namespace ecat = armarx::control::ethercat;
        namespace dev = devices::ethercat;
        const auto setupHandUnit = [&](auto& handUnit, const std::string& side)
        ///TODO sensorized hand
        {
            if (!icePropertiesMap["ArmarX." + side + "HandUnit.EndeffectorName"].empty())
            {                
                auto getHandByName = [](const std::string& handName) -> dev::hand::common::AbstractHandPtr
                {
                    namespace HandV1 = dev::hand::armar6_v1;
                    namespace HandV2 = dev::hand::armar6_v2;

                    ecat::Bus& bus = ecat::Bus::getBus();
                    for (const auto& dev : bus.getDevices())
                    {
                        auto hand = std::dynamic_pointer_cast<HandV1::Device>(dev);
                        if (hand && hand->getHandDeviceName() == handName)
                        {
                            return hand;
                        }
                        auto handv2 = std::dynamic_pointer_cast<HandV2::Device>(dev);
                        if (handv2 && handv2->getHandDeviceName() == handName)
                        {
                            return handv2;
                        }
//                        auto sensofthandv1 = std::dynamic_pointer_cast<KITSensorizedSoftFingerHand::V1::FunctionalDevice>(dev);
//                        if (sensofthandv1 && sensofthandv1->getHandDeviceName() == handName)
//                        {
//                            return sensofthandv1;
//                        }
//                        auto softvisuhandv1 = std::dynamic_pointer_cast<KITFingerVisionSoftHand::V1::FunctionalDevice>(dev);
//                        if (softvisuhandv1 && softvisuhandv1->getHandDeviceName() == handName)
//                        {
//                            return softvisuhandv1;
//                        }
                    }
                    return nullptr;
                };

                dev::hand::common::AbstractHandPtr hand = getHandByName(side + "Hand");
                if (hand)
                {
                    handUnit->initialize(this, hand);
                    getArmarXManager()->addObject(handUnit, false, std::string(""), false);
                }
                else
                {
                    ARMARX_IMPORTANT
                        << "No " << side
                        << "Hand Device found for creating handunit ";
                }
            }
            else
            {
                ARMARX_INFO << "Property ArmarX." << side
                            << "HandUnit.EndEffectorName is empty -> Creating no " << side << " hand unit ";
            }
        };

        setupHandUnit(leftHandUnit, "Left");
        setupHandUnit(rightHandUnit, "Right");


        ARMARX_IMPORTANT << "Setting up custom Units...DONE";
    }

} // namespace armarx
