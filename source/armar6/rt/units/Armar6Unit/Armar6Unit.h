/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_rt::ArmarXObjects::Armar6Unit
 * @author     Pascal Weiner ( pascal dot weiner at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <array>
#include <fstream>
#include <sstream>
#include <thread>

#include <RobotAPI/components/units/SensorActorUnit.h>
#include <armarx/control/ethercat/RTUnit.h>

#include <armar6/rt/interface/units/Armar6Unit.h>
#include <armar6/rt/units/Armar6Unit/Logging/CircularBuffer.h>
#include <armar6/rt/units/Armar6Unit/Logging/LogEntry.h>
#include <armar6/rt/units/Armar6Unit/Logging/RtLogging.h>


namespace armarx
{

    using KITHandUnitPtr = IceInternal::Handle<class KITHandUnit>;

    /**
     * @defgroup Component-Armar6Unit Armar6Unit
     * @ingroup armar6_rt-Components
     * A description of the component Armar6Unit.
     *
     * @class Armar6Unit
     * @ingroup Component-Armar6Unit
     * @brief Brief description of class Armar6Unit.
     *
     * Detailed description of class Armar6Unit.
     */
    class Armar6Unit :
        virtual public Armar6UnitInterface,
        virtual public armarx::control::ethercat::RTUnit
    {

    public:
        Armar6Unit();
        ~Armar6Unit() override;

        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string
        getDefaultName() const override
        {
            return "Armar6Unit";
        }

    protected:
        void onInitRobotUnit() override;
        void onConnectRobotUnit() override;
        void onDisconnectRobotUnit() override;
        void onExitRobotUnit() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;
        void icePropertiesInitialized() override;

    private:
        // void stopRTThread();

        /** the run method of the rt thread */
        void rtRun() override;

        //EVAL everything to log an print data to the
        std::ofstream dataLogFile;
        std::vector<char> stringbuffer;
        std::stringstream stringstream;

        void set_latency_target(std::int32_t latency_target_value = 0);

        armarx::RtLoggingPtr log;
        SimpleRunningTask<>::pointer_type rtLoggingThread;

        std::atomic<bool> moveHeadPitchToZero{false};

        KITHandUnitPtr leftHandUnit, rightHandUnit;

        // Armar6UnitBase interface

    public:
        void startLogging(const std::string& filename,
                          const Ice::StringSeq& devices,
                          const Ice::Current&) override;
        void stopLogging(const Ice::Current&) override;


        // Units interface
    protected:
        void initializeDefaultUnits() override;
    };

} // namespace armarx
