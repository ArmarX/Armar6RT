#pragma once


#include <RobotAPI/components/units/HandUnit.h>
#include <RobotAPI/components/units/RobotUnit/RobotUnit.h>

#include <devices/ethercat/hand/common/AbstractHand.h>
#include <devices/ethercat/hand/common/AbstractHandUnitControllerWrapper.h>


namespace armarx
{
    class KITHandUnit :
        public HandUnit
    {
    public:
        KITHandUnit();


        // HandUnit interface
    public:
        void onInitHandUnit() override;
        void onStartHandUnit() override;
        void onExitHandUnit() override;

    private:
        RobotUnit* robotUnit = NULL;
        VirtualRobot::EndEffectorPtr eef;
        devices::ethercat::hand::common::AbstractHandPtr hand;

        std::vector<RapidXmlReaderNode> configNodes;
        devices::ethercat::hand::common::AbstractHandUnitControllerWrapperPtr controllerWrapper;


        // HandUnitInterface interface
    public:
        void setShape(const std::string& shape, const Ice::Current&) override;

        std::string getDeviceName() const;
        void initialize(RobotUnit* robotUnit, const devices::ethercat::hand::common::AbstractHandPtr& hand);


        // KITHandUnitInterface interface
    public:
        //bool executeHandCommand(const std::string& command, const Ice::Current&);
        //bool executeHandCommandForced(const std::string& command, const Ice::Current&);
        //Ice::Float getEstimatedHandState(const Ice::Current&);
        //void setEstimatedHandState(Ice::Float handState, const Ice::Current&);

        std::string describeHandState(const Ice::Current&) override;

        // HandUnitInterface interface
    public:
        void setJointAngles(const NameValueMap& targetJointAngles, const Ice::Current&) override;
        NameValueMap getCurrentJointValues(const Ice::Current&) override;
    };
    using KITHandUnitPtr = IceInternal::Handle<KITHandUnit>;

} // namespace armarx
