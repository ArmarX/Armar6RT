#include "KITHandUnit.h"


using namespace armarx;

KITHandUnit::KITHandUnit()
{

}

void KITHandUnit::onInitHandUnit()
{
    usingProxy("Armar6Unit");
}

void KITHandUnit::onStartHandUnit()
{
    ARMARX_TRACE;

    ARMARX_CHECK_EXPRESSION(hand);

    controllerWrapper = hand->createHandUnitControllerWrapper(robotUnit, getHandName(Ice::emptyCurrent));

    ARMARX_CHECK_EXPRESSION(controllerWrapper);
}

void KITHandUnit::onExitHandUnit()
{
}


std::string KITHandUnit::getDeviceName() const
{
    return hand->getHandDeviceName();
}


void KITHandUnit::initialize(RobotUnit* robotUnit, const devices::ethercat::hand::common::AbstractHandPtr& hand)
{
    ARMARX_CHECK_EXPRESSION(hand);
    ARMARX_CHECK_EXPRESSION(robotUnit);
    ARMARX_CHECK_EXPRESSION(!this->robotUnit);
    this->robotUnit = robotUnit;
    this->hand = hand;
}



void KITHandUnit::setShape(const std::string& shape, const Ice::Current&)
{
    ARMARX_CHECK_EXPRESSION(controllerWrapper);
    ARMARX_INFO << "setShape(" << shape << ')';
    controllerWrapper->setShape(shape);
}

/*
bool KITHandUnit::executeHandCommandForced(const std::string& command, const Ice::Current&)
{
    ARMARX_WARNING << deactivateSpam(1) << "Not implemented yet!";
    return true;
}

bool KITHandUnit::executeHandCommand(const std::string& shape, const Ice::Current&)
{
    ARMARX_CHECK_EXPRESSION(controller);
    controller->activateController();
    KITHandCommand command = KITHandCommand::STOP;
    if (armarx::Contains(shape, "open", true))
    {
        command = KITHandCommand::OPEN_HAND;
    }
    else if (armarx::Contains(shape, "close", true))
    {
        command = KITHandCommand::CLOSE_HAND;
    }

    controller->setShapeCommand(command);
    return true;
}

Ice::Float KITHandUnit::getEstimatedHandState(const Ice::Current&)
{
    ARMARX_WARNING << deactivateSpam(1) << "Not implemented yet!";
    return 0;
}

void KITHandUnit::setEstimatedHandState(Ice::Float handState, const Ice::Current&)
{
    ARMARX_WARNING << deactivateSpam(1) << "Not implemented yet!";
}*/


std::string KITHandUnit::describeHandState(const Ice::Current&)
{
    if (controllerWrapper)
    {
        return controllerWrapper->describeHandState();
    }
    ARMARX_ERROR << "ControllerWrapper Ptr NULL!";
    return "";
}


void armarx::KITHandUnit::setJointAngles(const NameValueMap& targetJointAngles, const Ice::Current&)
{
    ARMARX_IMPORTANT << VAROUT(targetJointAngles);
    controllerWrapper->setJointAngles(targetJointAngles);
}

NameValueMap KITHandUnit::getCurrentJointValues(const Ice::Current&)
{
    return controllerWrapper->getActualJointValues();
    //ARMARX_WARNING << deactivateSpam() << "getCurrentJointValues() called, but KITHand does not provide joint angles!";
    //return NameValueMap();
}
