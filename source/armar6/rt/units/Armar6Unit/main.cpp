/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_rt::application::Armar6Unit
 * @author     Pascal Weiner ( pascal dot weiner at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <armar6/rt/units/Armar6Unit/Armar6Unit.h>

#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/logging/Logging.h>

int main(int argc, char* argv[])
{
    if (argc > 1 && argv[1][0] != '-')
    {
        std::string socketFileDescriptorProperty = "--ArmarX.Armar6Unit.SocketFileDescriptor=";
        socketFileDescriptorProperty += argv[1];
        char* propertyArray = new char[socketFileDescriptorProperty.size()];
        strcpy(propertyArray, socketFileDescriptorProperty.c_str());
        argv[1] = propertyArray;
    }
    return armarx::runSimpleComponentApp < armarx::Armar6Unit > (argc, argv, "Armar6Unit", "", "ArmarX", true);
}
