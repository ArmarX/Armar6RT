#pragma once

#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointController.h>

#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>

#include <VirtualRobot/Nodes/RobotNode.h>

#include <RobotAPI/interface/units/RobotUnit/NJointCurrentController.h>

namespace armarx
{


    TYPEDEF_PTRS_HANDLE(NJointCurrentController);

    class NJointCurrentController :
        public armarx::NJointController,
        public NJointCurrentControllerInterface
    {
    public:
        using ConfigPtrT = NJointCurrentControllerConfigPtr;

        // NJointControllerInterface interface
    public:
        std::string getClassName(const Ice::Current&) const override;

        // NJointController interface
    public:
        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;


        static WidgetDescription::WidgetPtr GenerateConfigDescription(const VirtualRobot::RobotPtr& robot,
                const std::map<std::string, ConstControlDevicePtr>&,
                const std::map<std::string, ConstSensorDevicePtr>&);

        static NJointCurrentControllerConfigPtr GenerateConfigFromVariants(const StringVariantBaseMap& values);
        NJointCurrentController(
            RobotUnit* prov,
            NJointCurrentControllerConfigPtr config,
            const VirtualRobot::RobotPtr& r);
        ControlTarget1DoFActuatorCurrent* target;
        NJointCurrentControllerConfigPtr config;
        VirtualRobot::RobotNodePtr rn;
        // NJointControllerInterface interface
    public:
        WidgetDescription::StringWidgetDictionary getFunctionDescriptions(const Ice::Current&) const override;
        void callDescribedFunction(const std::string& name, const StringVariantBaseMap& valueMap, const Ice::Current&) override;
        static ::armarx::WidgetDescription::WidgetSeq createSliders(const VirtualRobot::RobotNodePtr& rn, float initialCurrent);

        // NJointCurrentControllerInterface interface
    public:
        void setControllerTarget(float current, const Ice::Current&) override;
    };
}
