/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "NJointTorqueController.h"
#include <VirtualRobot/Nodes/RobotNode.h>
#include <VirtualRobot/Robot.h>
#include <RobotAPI/components/units/RobotUnit/RobotUnit.h>

#define DEFAULT_TCP_STRING "default TCP"

namespace armarx
{

    NJointControllerRegistration<NJointTorqueController> registrationControllerNJointTorqueController("NJointTorqueController");

    std::string NJointTorqueController::getClassName(const Ice::Current&) const
    {
        return "NJointTorqueController";
    }

    void NJointTorqueController::rtPreActivateController()
    {
        targetTorque = 0.0;
        for (auto& ctrl : torqueCtrls)
        {
            ctrl->reset();
        }
    }

    void NJointTorqueController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        for (size_t i = 0; i < targets.size(); ++i)
        {
            auto& target = targets.at(i);
            auto& ctrl = torqueCtrls.at(i);
            auto& s = sensors.at(i);

            ctrl->getTorqueConfigData()->pid_proportional_gain = Kp;
            ctrl->getTorqueConfigData()->pid_integral_gain = Ki;
            ctrl->getTorqueConfigData()->pid_derivative_gain = Kd;
            ctrl->getTorqueConfigData()->accelerationGain = accelerationGain;
            ctrl->getTorqueConfigData()->deadZone = deadZone;
            ctrl->getTorqueConfigData()->decay = decay;
            ctrl->getTorqueConfigData()->pid_max_value = 3.5;
            ctrl->getTorqueConfigData()->maxAcceleration = maxAcceleration;
            ctrl->getTorqueConfigData()->maxJerk = maxJerk;
            ctrl->getTorqueConfigData()->torqueToCurrent = torqueToCurrent;

            float gravity = s->gravityTorque;

            float actualTorque = s->torque;

            float actualPosition = s->position;

            float targetTorque = math::MathUtils::LimitTo(this->targetTorque, 5);

            //get data from the elmo
            float actualVelocity = s->velocity;
            currentTarget = ctrl->update(sensorValuesTimestamp, timeSinceLastIteration, gravity, actualTorque, targetTorque, actualVelocity, actualPosition);
            velocityTarget = ctrl->getCurrentTargetVelocity();
            currentPIDError = ctrl->getPid().previousError;
            currentPIDIntegral = ctrl->getPid().integral;
            currentAcceleration = ctrl->getLastAcceleration();
            currentJerk = ctrl->getLastJerk();
            // send the current value to Elmo
            target->current = currentTarget;
            //            target->velocity = velocityTarget;


        }
    }




    WidgetDescription::WidgetPtr NJointTorqueController::GenerateConfigDescription(const VirtualRobot::RobotPtr& robot, const std::map<std::string, ConstControlDevicePtr>& controlDevices, const std::map<std::string, ConstSensorDevicePtr>&)
    {
        using namespace armarx::WidgetDescription;
        HBoxLayoutPtr layout = new HBoxLayout;

        LabelPtr label = new Label;
        label->text = "joint name";
        layout->children.emplace_back(label);
        StringComboBoxPtr box = new StringComboBox;
        box->defaultIndex = 0;
        for (auto& c : controlDevices)
        {
            if (c.second->hasJointController(ControlModes::Current1DoF))
            {
                box->options.push_back(c.first);
            }
        }
        box->name = "jointName";
        layout->children.emplace_back(box);



        auto sliders = createSliders(0, 0, 0, 1, 0.1, 0.01, 10.0f, 10.0f, 0.0f);
        layout->children.insert(layout->children.end(),
                                sliders.begin(),
                                sliders.end());
        layout->children.emplace_back(new HSpacer);
        return layout;
    }

    NJointTorqueControllerConfigPtr NJointTorqueController::GenerateConfigFromVariants(const StringVariantBaseMap& values)
    {
        return new NJointTorqueControllerConfig {values.at("jointName")->getString(),
                values.at("Kp")->getFloat(), values.at("Ki")->getFloat(), values.at("Kd")->getFloat(),
                values.at("accelerationGain")->getFloat(),
                values.at("deadZone")->getFloat(),
                values.at("decay")->getFloat(),
                values.at("maxAcceleration")->getFloat(),
                values.at("maxJerk")->getFloat(),
                values.at("torqueToCurrent")->getFloat()
                                                };
    }

    NJointTorqueController::NJointTorqueController(RobotUnitPtr prov, NJointTorqueControllerConfigPtr config, const VirtualRobot::RobotPtr& r)
    {
        Kp = config->Kp;
        Ki = config->Ki;
        Kd = config->Kd;
        accelerationGain = config->accelerationGain;
        deadZone = config->deadZone;
        decay = config->decay;
        maxAcceleration = config->maxAcceleration;
        maxJerk = config->maxJerk;
        torqueToCurrent = config->torqueToCurrent;

        ARMARX_CHECK_EXPRESSION(prov);
        RobotUnitPtr robotUnit = RobotUnitPtr::dynamicCast(prov);
        ARMARX_CHECK_EXPRESSION(robotUnit);
        ARMARX_CHECK_EXPRESSION(!config->jointName.empty());
        auto node = r->getRobotNode(config->jointName);
        ARMARX_CHECK_EXPRESSION(node) << config->jointName;
        auto jointName = config->jointName;
        //        auto jointName = nodeset->getNode(i)->getName();
        //        ControlTargetBase* ct = prov->getControlTarget(jointName, ControlModes::Current1DoF);
        //        targets.push_back(ct->asA<ControlTarget1DoFActuatorCurrent>());
        ControlTargetBase* ct = useControlTarget(jointName, ControlModes::Current1DoF);
        targets.push_back(ct->asA<ControlTarget1DoFActuatorCurrent>());

        const SensorValueBase* sv = useSensorValue(jointName);
        sensors.push_back(sv->asA<SensorValue1DoFActuator>());

        armarx::control::joint_controller::VelocityManipulatingTorqueControllerConfigurationPtr cfg(new armarx::control::joint_controller::VelocityManipulatingTorqueControllerConfiguration());
        cfg->pid_proportional_gain = Kp;
        cfg->pid_integral_gain = Ki;
        cfg->pid_derivative_gain = Kd;
        cfg->accelerationGain = accelerationGain;
        cfg->deadZone = deadZone;
        cfg->decay = decay;
        cfg->maxAcceleration = maxAcceleration;
        cfg->maxJerk = maxJerk;
        cfg->torqueToCurrent = torqueToCurrent;
        torqueCtrls.push_back(std::shared_ptr<armarx::control::joint_controller::VelocityManipulatingTorqueController>(new armarx::control::joint_controller::VelocityManipulatingTorqueController(cfg)));
    }

    void NJointTorqueController::rtPostDeactivateController()
    {

    }

    ::armarx::WidgetDescription::WidgetSeq NJointTorqueController::createSliders(float Kp, float Ki, float Kd, float accelerationGain,
            float deadZone, float decay, float maxAcceleration, float maxJerk, float torqueToCurrent)
    {
        using namespace armarx::WidgetDescription;
        ::armarx::WidgetDescription::WidgetSeq widgets;
        auto addSlider = [&](const std::string & label, float min, float max, float defaultValue)
        {
            widgets.emplace_back(new Label(false, label));
            {
                FloatSliderPtr c_x = new FloatSlider;
                c_x->name = label;
                c_x->min = min;
                c_x->defaultValue = defaultValue;
                c_x->max = max;
                widgets.emplace_back(c_x);
            }
        };

        addSlider("Kp", 0, 20, Kp);
        addSlider("Ki", 0, 10, Ki);
        addSlider("Kd", 0, 4, Kd);
        addSlider("accelerationGain", 0, 10, accelerationGain);
        addSlider("deadZone", 0, 2, deadZone);
        addSlider("decay", 0, 0.05, decay);
        addSlider("maxAcceleration", 0, 40, maxAcceleration);
        addSlider("maxJerk", 0, 400, maxJerk);
        addSlider("torqueToCurrent", 0, 2, torqueToCurrent);
        return widgets;
    }

    void NJointTorqueController::onPublish(const SensorAndControl&, const DebugDrawerInterfacePrx&, const DebugObserverInterfacePrx& dd)
    {
        StringVariantBaseMap map;
        map["currentTarget"] = new Variant((float)currentTarget * 0.001);
        map["velocityTarget"] = new Variant((float)velocityTarget);
        map["currentPIDError"] = new Variant((float)currentPIDError);
        map["currentPIDIntegral"] = new Variant((float)currentPIDIntegral);
        map["currentAcceleration"] = new Variant((float)currentAcceleration);
        map["currentJerk"] = new Variant((float)currentJerk);
        dd->setDebugChannel("NJointTorqueController", map);
    }

    WidgetDescription::StringWidgetDictionary NJointTorqueController::getFunctionDescriptions(const Ice::Current&) const
    {
        using namespace armarx::WidgetDescription;


        HBoxLayoutPtr configLayout = new HBoxLayout;
        auto sliders = createSliders(Kp, Ki, Kd, accelerationGain, deadZone, decay, maxAcceleration, maxJerk, torqueToCurrent);
        configLayout->children.insert(configLayout->children.end(),
                                      sliders.begin(),
                                      sliders.end());



        HBoxLayoutPtr targetsLayout = new HBoxLayout;
        ::armarx::WidgetDescription::WidgetSeq widgets;
        auto addSlider = [&](const std::string & label, float min, float max, float defaultValue)
        {
            widgets.emplace_back(new Label(false, label));
            {
                FloatSliderPtr c_x = new FloatSlider;
                c_x->name = label;
                c_x->min = min;
                c_x->defaultValue = defaultValue;
                c_x->max = max;
                widgets.emplace_back(c_x);
            }
        };

        addSlider("targetTorque", -5, 5, 0);
        targetsLayout->children.insert(targetsLayout->children.end(),
                                       widgets.begin(),
                                       widgets.end());
        return {{"ControllerConfig", configLayout},
            {"ControllerTarget", targetsLayout}
        };
    }

    void NJointTorqueController::callDescribedFunction(const std::string& name, const StringVariantBaseMap& valueMap, const Ice::Current&)
    {
        if (name == "ControllerConfig")
        {
            Kp = valueMap.at("Kp")->getFloat();
            ARMARX_INFO << VAROUT(Kp);
            Ki = valueMap.at("Ki")->getFloat();
            Kd = valueMap.at("Kd")->getFloat();
            accelerationGain = valueMap.at("accelerationGain")->getFloat();
            deadZone = valueMap.at("deadZone")->getFloat();
            decay = valueMap.at("decay")->getFloat();
            maxAcceleration = valueMap.at("maxAcceleration")->getFloat();
            maxJerk = valueMap.at("maxJerk")->getFloat();
            torqueToCurrent = valueMap.at("torqueToCurrent")->getFloat();
            std::stringstream s;
            for (auto& p : valueMap)
            {
                auto var = VariantPtr::dynamicCast(p.second);
                s << p.first << ": " << (var ? var->getOutputValueOnly() : "NULL") << "\n";
            }
            ARMARX_INFO << "Setting new config: " << s.str();
        }
        else if (name == "ControllerTarget")
        {
            targetTorque = valueMap.at("targetTorque")->getFloat();
        }
        else
        {
            ARMARX_WARNING << "Unknown function name called: " << name;
        }
    }



} // namespace armarx




