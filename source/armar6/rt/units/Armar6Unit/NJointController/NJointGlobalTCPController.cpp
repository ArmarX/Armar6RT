/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "NJointGlobalTCPController.h"

#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/MathTools.h>

#include <RobotAPI/components/units/RobotUnit/RobotUnit.h>

#define DEFAULT_TCP_STRING "default TCP"

namespace armarx
{

    NJointControllerRegistration<NJointGlobalTCPController> registrationControllerNJointGlobalTCPController("NJointGlobalTCPController");

    std::string NJointGlobalTCPController::getClassName(const Ice::Current&) const
    {
        return "NJointGlobalTCPController";
    }

    void NJointGlobalTCPController::rtPreActivateController()
    {
        xVel = 0;
        yVel = 0;
        zVel = 0;
        rollVel = 0;
        pitchVel = 0;
        yawVel = 0;
        startPose = Eigen::Matrix4f::Identity();
        Eigen::AngleAxisf aa(platformSensorValues->relativePositionRotation, Eigen::Vector3f(0, 0, 1));
        ARMARX_IMPORTANT << VAROUT(platformSensorValues->relativePositionRotation);
        startPose.block<3, 3>(0, 0) = aa.toRotationMatrix();
        startPose(0, 3) = platformSensorValues->relativePositionX;
        startPose(1, 3) = platformSensorValues->relativePositionY;
        ARMARX_IMPORTANT << VAROUT(startPose);
        startPose = startPose * tcp->getPoseInRootFrame();
        //        startPose(0, 3) -= 50;
        ARMARX_IMPORTANT << VAROUT(startPose);
    }

    void NJointGlobalTCPController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        Eigen::Matrix4f currentPlatformPose = Eigen::Matrix4f::Identity();
        Eigen::AngleAxisf aa(platformSensorValues->relativePositionRotation, Eigen::Vector3f(0, 0, 1));
        currentPlatformPose.block<3, 3>(0, 0) = aa.toRotationMatrix();
        currentPlatformPose(0, 3) = platformSensorValues->relativePositionX;
        currentPlatformPose(1, 3) = platformSensorValues->relativePositionY;
        //        ARMARX_INFO << deactivateSpam(1) << VAROUT(currentPlatformPose) << VAROUT(tcp->getPoseInRootFrame());
        //        Eigen::Matrix4f currentTCPPoseGlobal = currentPlatformPose * tcp->getPoseInRootFrame();
        //        ARMARX_INFO << deactivateSpam(1) << VAROUT(currentTCPPoseGlobal);
        //        Eigen::Matrix4f errorPoseGlobal = startPose * currentTCPPoseGlobal.inverse();
        //        ARMARX_INFO << deactivateSpam(1) << VAROUT(errorPoseGlobal);
        //        Eigen::Matrix4f errorPoseLocal = currentPlatformPose.inverse() * errorPoseGlobal;
        //        ARMARX_INFO << deactivateSpam(1) << VAROUT(errorPoseLocal);
        //        Eigen::Vector3f orientationDelta;
        //        VirtualRobot::MathTools::eigen4f2rpy(errorPoseLocal, orientationDelta);
        Eigen::VectorXf targetVelocity(6);
        //        errorPoseLocal.block<3, 1>(0, 3) *= KpPosition;
        //        orientationDelta *= KpOrientation;
        //        x << errorPoseLocal(0, 3) - platformSensorValues->velocityX, errorPoseLocal(1, 3) - platformSensorValues->velocityY, errorPoseLocal(2, 3);
        ////                ,        orientationDelta(0), orientationDelta(1), orientationDelta(2) - platformSensorValues->velocityRotation;
        ///
        ///
        //        Eigen::Matrix4f errorPoseRoot = startPose * tcp->getPoseInRootFrame().inverse();
        //        Eigen::Matrix4f errorPoseTCPFrame = tcp->transformTo(tcp->getRobot()->getRootNode(), startPose); //tcp->getPoseInRootFrame().inverse() * goaltcpPose;


        Eigen::Matrix4f errorPoseTCPFrame = (tcp->getPoseInRootFrame().inverse() * currentPlatformPose.inverse()) * startPose;
        //        ARMARX_INFO << deactivateSpam(0.3) << VAROUT(startPose) << "\n" << tcp->getPoseInRootFrame();
        Eigen::Vector3f rpyDelta;
        VirtualRobot::MathTools::eigen4f2rpy(errorPoseTCPFrame, rpyDelta);
        targetVelocity << errorPoseTCPFrame(0, 3), errorPoseTCPFrame(1, 3), errorPoseTCPFrame(2, 3), rpyDelta(0), rpyDelta(1), rpyDelta(2);
        targetVelocity.head(3) *= KpPosition;
        targetVelocity.tail(3) *= KpOrientation;
        Eigen::Vector3f velocityPlatform(platformSensorValues->velocityX, platformSensorValues->velocityY, 0);
        Eigen::Vector3f tcpVel;
        Eigen::Rotation2Df rot(platformSensorValues->relativePositionRotation);
        Eigen::Vector3f rotatedVelocity = Eigen::Vector3f::Zero();
        rotatedVelocity.head(2) = rot * velocityPlatform.head(2);

        Eigen::Rotation2Df rotVel(platformSensorValues->velocityRotation);


        //        float x = tcp->getPositionInRootFrame()(0);
        //        float y = tcp->getPositionInRootFrame()(1);
        //        float px = x * -sin(platformSensorValues->velocityRotation) - y * cos(platformSensorValues->velocityRotation);
        //        float py = x * cos(platformSensorValues->velocityRotation) + y * -sin(platformSensorValues->velocityRotation);
        float vx = - platformSensorValues->velocityRotation * tcp->getPositionInRootFrame()(1);
        float vy = platformSensorValues->velocityRotation * tcp->getPositionInRootFrame()(0);
        Eigen::Vector3f rotationVelocityTranslation = Eigen::Vector3f::Zero();
        rotationVelocityTranslation(0) = vx;
        rotationVelocityTranslation(1) = vy;
        rotationVelocityTranslation.head(2) = rotVel * tcp->getPositionInRootFrame().head(2) - tcp->getPositionInRootFrame().head(2);
        Eigen::Vector3f velocityTCPFrame = tcp->transformTo(tcp->getRobot()->getRootNode(), velocityPlatform);
        //        ARMARX_INFO << deactivateSpam(0.3) << VAROUT(velocityPlatform) << VAROUT(velocityTCPFrame);
        targetVelocity.head(3) -= velocityTCPFrame + tcp->transformTo(tcp->getRobot()->getRootNode(), rotationVelocityTranslation);

        targetVelocity.tail(3) -= tcp->transformTo(tcp->getRobot()->getRootNode(), Eigen::Vector3f(0, 0, platformSensorValues->velocityRotation));
        Eigen::MatrixXf jacobiInv = ik->getPseudoInverseJacobianMatrix(tcp, VirtualRobot::IKSolver::All);
        Eigen::MatrixXf jacobi = ik->getJacobianMatrix(tcp);
        Eigen::VectorXf jointTargetVelocities = jacobiInv * targetVelocity;
        //        ARMARX_INFO << deactivateSpam(0.3) << VAROUT(errorPoseTCPFrame) << "\n Cartesian Velocity: " << targetVelocity << "\n Joint Velocities: " << jointTargetVelocities << " recalculated Cartesian Velocity: " << (jacobi * jointTargetVelocities);

        for (size_t i = 0; i < targets.size(); ++i)
        {
            targets.at(i)->velocity = jointTargetVelocities(i);
        }
    }




    WidgetDescription::WidgetPtr NJointGlobalTCPController::GenerateConfigDescription(const VirtualRobot::RobotPtr& robot, const std::map<std::string, ConstControlDevicePtr>& controlDevices, const std::map<std::string, ConstSensorDevicePtr>&)
    {
        using namespace armarx::WidgetDescription;
        HBoxLayoutPtr layout = new HBoxLayout;

        LabelPtr label = new Label;
        label->text = "nodeset name";
        layout->children.emplace_back(label);
        StringComboBoxPtr box = new StringComboBox;
        box->defaultIndex = 0;

        box->options = robot->getRobotNodeSetNames();
        box->name = "nodeSetName";
        layout->children.emplace_back(box);

        LabelPtr labelTCP = new Label;
        labelTCP->text = "tcp name";
        layout->children.emplace_back(labelTCP);
        StringComboBoxPtr boxTCP = new StringComboBox;
        boxTCP->defaultIndex = 0;

        boxTCP->options = robot->getRobotNodeNames();
        boxTCP->options.insert(boxTCP->options.begin(), DEFAULT_TCP_STRING);
        boxTCP->name = "tcpName";
        layout->children.emplace_back(boxTCP);





        auto sliders = createSliders(1, 1);
        layout->children.insert(layout->children.end(),
                                sliders.begin(),
                                sliders.end());
        layout->children.emplace_back(new HSpacer);
        return layout;
    }

    NJointGlobalTCPControllerConfigPtr NJointGlobalTCPController::GenerateConfigFromVariants(const StringVariantBaseMap& values)
    {
        return new NJointGlobalTCPControllerConfig {values.at("nodeSetName")->getString(), values.at("tcpName")->getString(),
                values.at("K p Position")->getFloat(), values.at("K p Orientation")->getFloat()
                                                   };
    }

    NJointGlobalTCPController::NJointGlobalTCPController(const RobotUnitPtr&, NJointGlobalTCPControllerConfigPtr config, const VirtualRobot::RobotPtr& r)
    {
        useSynchronizedRtRobot();
        ARMARX_CHECK_EXPRESSION(!config->nodeSetName.empty());
        auto nodeset = rtGetRobot()->getRobotNodeSet(config->nodeSetName);
        ARMARX_CHECK_EXPRESSION(nodeset) << config->nodeSetName;
        for (size_t i = 0; i < nodeset->getSize(); ++i)
        {
            auto jointName = nodeset->getNode(i)->getName();
            ControlTargetBase* ct = useControlTarget(jointName, ControlModes::Velocity1DoF);
            const SensorValueBase* sv = useSensorValue(jointName);
            targets.push_back(ct->asA<ControlTarget1DoFActuatorVelocity>());
            sensors.push_back(sv->asA<SensorValue1DoFActuatorPosition>());
        };
        const SensorValueBase* platformSensor = useSensorValue("Platform");
        platformSensorValues = platformSensor->asA<SensorValueHolonomicPlatform>();
        ARMARX_CHECK_EXPRESSION(platformSensorValues);
        tcp = (config->tcpName.empty() || config->tcpName == DEFAULT_TCP_STRING) ? nodeset->getTCP() : rtGetRobot()->getRobotNode(config->tcpName);
        ik.reset(new VirtualRobot::DifferentialIK(nodeset, tcp, VirtualRobot::JacobiProvider::eSVD));
        ARMARX_CHECK_EXPRESSION(tcp) << config->tcpName;

        KpPosition = config->KpPosition;
        KpOrientation = config->KpOrientation;
    }

    void NJointGlobalTCPController::rtPostDeactivateController()
    {

    }

    ::armarx::WidgetDescription::WidgetSeq NJointGlobalTCPController::createSliders(float KpPosition, float KpOrientation)
    {
        using namespace armarx::WidgetDescription;
        ::armarx::WidgetDescription::WidgetSeq widgets;
        auto addSlider = [&](const std::string & label, float min, float max, float defaultValue)
        {
            widgets.emplace_back(new Label(false, label));
            {
                FloatSliderPtr c_x = new FloatSlider;
                c_x->name = label;
                c_x->min = min;
                c_x->defaultValue = defaultValue;
                c_x->max = max;
                widgets.emplace_back(c_x);
            }
        };

        addSlider("K p Position", 0, 20, KpPosition);
        addSlider("K p Orientation", 0, 20, KpOrientation);
        return widgets;
    }

    WidgetDescription::StringWidgetDictionary NJointGlobalTCPController::getFunctionDescriptions(const Ice::Current&) const
    {
        using namespace armarx::WidgetDescription;
        HBoxLayoutPtr layout = new HBoxLayout;
        auto sliders = createSliders(KpPosition, KpOrientation);
        layout->children.insert(layout->children.end(),
                                sliders.begin(),
                                sliders.end());
        return {{"ControllerConfig", layout}};
    }

    void NJointGlobalTCPController::callDescribedFunction(const std::string& name, const StringVariantBaseMap& valueMap, const Ice::Current&)
    {
        if (name == "ControllerConfig")
        {
            KpPosition = valueMap.at("K p Position")->getFloat();
            KpOrientation = valueMap.at("K p Orientation")->getFloat();
        }
        else
        {
            ARMARX_WARNING << "Unknown function name called: " << name;
        }
    }



} // namespace armarx




