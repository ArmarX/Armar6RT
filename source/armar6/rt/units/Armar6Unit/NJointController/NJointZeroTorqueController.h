/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// Simox
#include <VirtualRobot/VirtualRobot.h>

// ArmarX
#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointController.h>

#include <devices/ethercat/sensor_actor_unit/armar6/joint_controller/ZeroTorque.h>

#include <RobotAPI/interface/units/RobotUnit/NjointZeroTorqueController.h>


namespace armarx
{
    struct NJointZeroTorqueControllerTarget
    {
        Ice::FloatSeq targetTorques;
    };

    TYPEDEF_PTRS_HANDLE(NJointZeroTorqueController);

    class NJointZeroTorqueController :
        public NJointControllerWithTripleBuffer<NJointZeroTorqueControllerTarget>,
        public NJointZeroTorqueControllerInterface
    {
    public:
        using ConfigPtrT = NJointZeroTorqueControllerConfigPtr;
        NJointZeroTorqueController(RobotUnitPtr prov, const NJointZeroTorqueControllerConfigPtr& config, const VirtualRobot::RobotPtr&);

        // NJointControllerInterface interface
        std::string getClassName(const Ice::Current&) const override;
        //        WidgetDescription::StringWidgetDictionary getFunctionDescriptions(const Ice::Current&) const override;
        //        void callDescribedFunction(const std::string& name, const StringVariantBaseMap& valueMap, const Ice::Current&) override;

        // NJointController interface
        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        static WidgetDescription::WidgetPtr GenerateConfigDescription(
            const VirtualRobot::RobotPtr&,
            const std::map<std::string, ConstControlDevicePtr>&,
            const std::map<std::string, ConstSensorDevicePtr>&);

        static NJointZeroTorqueControllerConfigPtr GenerateConfigFromVariants(const StringVariantBaseMap& values);
    protected:
        void rtPreActivateController() override;
        void rtPostDeactivateController() override;

        std::vector<ControlTarget1DoFActuatorZeroTorque*> targets;
        float maxTorque = std::numeric_limits<float>::max();

        // NJointZeroTorqueControllerInterface interface
    public:
        void setControllerTarget(const Ice::FloatSeq&, const Ice::Current&) override;
    };

} // namespace armarx
