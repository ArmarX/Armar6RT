#include "NJointCurrentController.h"



#include <RobotAPI/components/units/RobotUnit/RobotUnit.h>
#include <devices/ethercat/sensor_actor_unit/armar6/Data.h>

#include <VirtualRobot/Robot.h>


namespace armarx
{

    NJointControllerRegistration<NJointCurrentController> registrationControllerNJointCurrentController("NJointCurrentController");


    NJointCurrentController::NJointCurrentController(RobotUnit* prov, NJointCurrentControllerConfigPtr config, const VirtualRobot::RobotPtr& r)
    {
        {
            ARMARX_CHECK_EXPRESSION(prov);
            ControlTargetBase* ct = useControlTarget(config->deviceName, ControlModes::Current1DoF);
            ARMARX_CHECK_EXPRESSION(ct->asA<ControlTarget1DoFActuatorCurrent>());
            target = ct->asA<ControlTarget1DoFActuatorCurrent>();

            rn = r->getRobotNode(config->deviceName);
        }

        config->current = 0;
        this->config = config;

    }

    std::string NJointCurrentController::getClassName(const Ice::Current&) const
    {
        return "NJointCurrentController";
    }

    void NJointCurrentController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        target->current = config->current;
    }

    ::armarx::WidgetDescription::WidgetSeq NJointCurrentController::createSliders(const VirtualRobot::RobotNodePtr& rn, float initialCurrent)
    {
        using namespace armarx::WidgetDescription;
        ::armarx::WidgetDescription::WidgetSeq widgets;

        if (rn)
        {
            widgets.emplace_back(new Label(false, rn->getName() + " target current"));
            FloatSliderPtr c_position = new FloatSlider;
            c_position->name = "current";
            c_position->min = -3;
            c_position->defaultValue = initialCurrent;
            c_position->max = 3;
            widgets.emplace_back(c_position);
        }
        return widgets;
    }

    WidgetDescription::WidgetPtr NJointCurrentController::GenerateConfigDescription(const VirtualRobot::RobotPtr& robot, const std::map<std::string, ConstControlDevicePtr>& controlDevices, const std::map<std::string, ConstSensorDevicePtr>&)
    {
        using namespace armarx::WidgetDescription;
        HBoxLayoutPtr layout = new HBoxLayout;

        LabelPtr label = new Label;
        label->text = "control device name";
        layout->children.emplace_back(label);
        StringComboBoxPtr box = new StringComboBox;
        box->defaultIndex = 0;
        //filter control devices
        for (const auto& name2dev : controlDevices)
        {
            const ConstControlDevicePtr& dev = name2dev.second;
            const auto& name = name2dev.first;
            ARMARX_CHECK_EXPRESSION(dev) << name;
            if (
                dev->hasJointController(ControlModes::Current1DoF) &&
                dev->getJointController(ControlModes::Current1DoF)->getControlTarget()->isA<ControlTarget1DoFActuatorCurrent>()
            )
            {
                box->options.emplace_back(name);
            }
        }
        box->name = "name";
        layout->children.emplace_back(box);
        auto sliders = createSliders(nullptr, 0);
        layout->children.insert(layout->children.end(),
                                sliders.begin(),
                                sliders.end());
        layout->children.emplace_back(new HSpacer);
        return layout;
    }

    NJointCurrentControllerConfigPtr NJointCurrentController::GenerateConfigFromVariants(const StringVariantBaseMap& values)
    {
        return new NJointCurrentControllerConfig
        {
            0, // current is set on controller activation
            values.at("name")->getString()
        };
    }



    WidgetDescription::StringWidgetDictionary NJointCurrentController::getFunctionDescriptions(const Ice::Current&) const
    {
        using namespace armarx::WidgetDescription;
        HBoxLayoutPtr layout = new HBoxLayout;
        auto sliders = createSliders(rn, 0);
        layout->children.insert(layout->children.end(),
                                sliders.begin(),
                                sliders.end());
        return {{"ControllerConfig", layout}};
    }

    void NJointCurrentController::callDescribedFunction(const std::string& name, const StringVariantBaseMap& valueMap, const Ice::Current&)
    {
        if (name == "ControllerConfig")
        {
            config->current = valueMap.at("current")->getFloat();
        }
        else
        {
            ARMARX_WARNING << "Unknown function name called: " << name;
        }
    }
}


void armarx::NJointCurrentController::setControllerTarget(float current, const Ice::Current&)
{
    config->current = current;
}
