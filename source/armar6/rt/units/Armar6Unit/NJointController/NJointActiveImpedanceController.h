#pragma once


#include <VirtualRobot/Nodes/RobotNode.h>

#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointController.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>

#include <devices/ethercat/sensor_actor_unit/armar6/Data.h>

#include <RobotAPI/interface/units/RobotUnit/NJointActiveImpedanceController.h>
//#include <armar6/rt/units/Armar6Unit/BusData/JointData.h>


namespace armarx
{

    /*TYPEDEF_PTRS_HANDLE(NJointActiveImpedanceControllerConfig);

    class NJointActiveImpedanceControllerConfig : virtual public NJointControllerConfig
    {
    public:
        NJointActiveImpedanceControllerConfig(float position, float kp, float kd, std::string const& deviceName):
            position(position),
            kp(kp),
            kd(kd),
            deviceName(deviceName)
        {}
        float position = 0;
        float kp = 0;
        float kd = 0;
        const std::string deviceName;
    };*/


    TYPEDEF_PTRS_HANDLE(NJointActiveImpedanceController);


    class NJointActiveImpedanceController :
        public armarx::NJointController,
        public NJointActiveImpedanceControllerInterface
    {
    public:
        using ConfigPtrT = NJointActiveImpedanceControllerConfigPtr;

        // NJointControllerInterface interface
    public:
        std::string getClassName(const Ice::Current&) const override;

        // NJointController interface
    public:
        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;


        static WidgetDescription::WidgetPtr GenerateConfigDescription(const VirtualRobot::RobotPtr& robot,
                const std::map<std::string, ConstControlDevicePtr>&,
                const std::map<std::string, ConstSensorDevicePtr>&);

        static NJointActiveImpedanceControllerConfigPtr GenerateConfigFromVariants(const StringVariantBaseMap& values);
        NJointActiveImpedanceController(
            RobotUnitPtr prov,
            NJointActiveImpedanceControllerConfigPtr config,
            const VirtualRobot::RobotPtr& r);
        ActiveImpedanceControlTarget* target;
        const devices::ethercat::sensor_actor_unit::armar6::SensorValueArmar6Actuator* sensorValues;
        NJointActiveImpedanceControllerConfigPtr config;
        VirtualRobot::RobotNodePtr rn;
        // NJointControllerInterface interface
    public:
        WidgetDescription::StringWidgetDictionary getFunctionDescriptions(const Ice::Current&) const override;
        void callDescribedFunction(const std::string& name, const StringVariantBaseMap& valueMap, const Ice::Current&) override;
        static ::armarx::WidgetDescription::WidgetSeq createSliders(const VirtualRobot::RobotNodePtr& rn, float initialPosition);

        // NJointActiveImpedanceControllerInterface interface
    public:
        void setControllerTarget(float position, float kp, float kd, const Ice::Current&) override;
    };

}
