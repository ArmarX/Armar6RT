#include "NJointActiveImpedanceController.h"

#include <RobotAPI/components/units/RobotUnit/RobotUnit.h>
#include <devices/ethercat/sensor_actor_unit/armar6/Data.h>

#include <VirtualRobot/Robot.h>

namespace armarx
{

    NJointControllerRegistration<NJointActiveImpedanceController> registrationControllerNJointActiveImpedanceController("NJointActiveImpedanceController");


    NJointActiveImpedanceController::NJointActiveImpedanceController(RobotUnitPtr prov, NJointActiveImpedanceControllerConfigPtr config, const VirtualRobot::RobotPtr& r)
    {
        {
            ARMARX_CHECK_EXPRESSION(prov);
            ControlTargetBase* ct = useControlTarget(config->deviceName, ControlModes::Current1DoF);
            ARMARX_CHECK_EXPRESSION(ct->asA<ActiveImpedanceControlTarget>());
            target = ct->asA<ActiveImpedanceControlTarget>();

            ConstSensorDevicePtr sensor = prov->getSensorDevice(config->deviceName);
            ARMARX_CHECK_EXPRESSION(sensor) << "device name: " << config->deviceName;
            sensorValues = sensor->getSensorValue()->asA<devices::ethercat::sensor_actor_unit::armar6::SensorValueArmar6Actuator>();
            ARMARX_CHECK_EXPRESSION(sensorValues);
            rn = r->getRobotNode(config->deviceName);
        }

        config->position = sensorValues->position;
        this->config = config;

    }

    std::string NJointActiveImpedanceController::getClassName(const Ice::Current&) const
    {
        return "NJointActiveImpedanceController";
    }

    void NJointActiveImpedanceController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        target->position = config->position;
        target->kp = config->kp;
        target->kd = config->kd;
    }

    ::armarx::WidgetDescription::WidgetSeq NJointActiveImpedanceController::createSliders(const VirtualRobot::RobotNodePtr& rn, float initialPosition)
    {
        using namespace armarx::WidgetDescription;
        ::armarx::WidgetDescription::WidgetSeq widgets;

        if (rn)
        {
            widgets.emplace_back(new Label(false, rn->getName() + " position"));
            FloatSliderPtr c_position = new FloatSlider;
            c_position->name = "position";
            c_position->min = rn->getJointLimitLow();
            c_position->defaultValue = initialPosition; // @@@ rn->getJointValue() is always == 0???
            c_position->max = rn->getJointLimitHigh();
            widgets.emplace_back(c_position);
        }
        widgets.emplace_back(new Label(false, "kp"));
        {
            FloatSliderPtr c_kp = new FloatSlider;
            c_kp->name = "kp";
            c_kp->min = 0;
            c_kp->defaultValue = 50;
            c_kp->max = 200;
            widgets.emplace_back(c_kp);
        }
        widgets.emplace_back(new Label(false, "kd"));
        {
            FloatSliderPtr c_kd = new FloatSlider;
            c_kd->name = "kd";
            c_kd->min = 0;
            c_kd->defaultValue = 0;
            c_kd->max = 10;
            widgets.emplace_back(c_kd);
        }
        return widgets;
    }

    WidgetDescription::WidgetPtr NJointActiveImpedanceController::GenerateConfigDescription(const VirtualRobot::RobotPtr& robot, const std::map<std::string, ConstControlDevicePtr>& controlDevices, const std::map<std::string, ConstSensorDevicePtr>&)
    {
        using namespace armarx::WidgetDescription;
        HBoxLayoutPtr layout = new HBoxLayout;

        LabelPtr label = new Label;
        label->text = "control device name";
        layout->children.emplace_back(label);
        StringComboBoxPtr box = new StringComboBox;
        box->defaultIndex = 0;
        //filter control devices
        for (const auto& name2dev : controlDevices)
        {
            const ConstControlDevicePtr& dev = name2dev.second;
            const auto& name = name2dev.first;
            ARMARX_CHECK_EXPRESSION(dev) << name;
            if (
                dev->hasJointController(ControlModes::ActiveImpedance) &&
                dev->getJointController(ControlModes::ActiveImpedance)->getControlTarget()->isA<ActiveImpedanceControlTarget>()
            )
            {
                box->options.emplace_back(name);
            }
        }
        box->name = "name";
        layout->children.emplace_back(box);
        auto sliders = createSliders(nullptr, 0);
        layout->children.insert(layout->children.end(),
                                sliders.begin(),
                                sliders.end());
        layout->children.emplace_back(new HSpacer);
        return layout;
    }

    NJointActiveImpedanceControllerConfigPtr NJointActiveImpedanceController::GenerateConfigFromVariants(const StringVariantBaseMap& values)
    {
        return new NJointActiveImpedanceControllerConfig
        {
            0, // position is set on controller activation
            values.at("kp")->getFloat(),
            values.at("kd")->getFloat(),
            values.at("name")->getString()
        };
    }



    WidgetDescription::StringWidgetDictionary NJointActiveImpedanceController::getFunctionDescriptions(const Ice::Current&) const
    {
        using namespace armarx::WidgetDescription;
        HBoxLayoutPtr layout = new HBoxLayout;
        auto sliders = createSliders(rn, sensorValues->position);
        ARMARX_IMPORTANT << rn->getName() << ", JointValue= " << rn->getJointValue();
        ARMARX_IMPORTANT << VAROUT(sensorValues->position);
        layout->children.insert(layout->children.end(),
                                sliders.begin(),
                                sliders.end());
        return {{"ControllerConfig", layout}};
    }

    void NJointActiveImpedanceController::callDescribedFunction(const std::string& name, const StringVariantBaseMap& valueMap, const Ice::Current&)
    {
        if (name == "ControllerConfig")
        {
            config->position = valueMap.at("position")->getFloat();
            config->kp = valueMap.at("kp")->getFloat();
            config->kd = valueMap.at("kd")->getFloat();
        }
        else
        {
            ARMARX_WARNING << "Unknown function name called: " << name;
        }
    }
}


void armarx::NJointActiveImpedanceController::setControllerTarget(float position, float kp, float kd, const Ice::Current&)
{
    config->position = position;
    config->kp = kp;
    config->kd = kd;
}
