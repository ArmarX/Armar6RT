/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <VirtualRobot/Robot.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValueHolonomicPlatform.h>
#include <VirtualRobot/IK/DifferentialIK.h>
#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointController.h>
#include <RobotAPI/libraries/core/PIDController.h>

namespace armarx
{

    TYPEDEF_PTRS_HANDLE(NJointGlobalTCPController);
    TYPEDEF_PTRS_HANDLE(NJointGlobalTCPControllerConfig);

    class NJointGlobalTCPControllerConfig : virtual public NJointControllerConfig
    {
    public:
        NJointGlobalTCPControllerConfig(std::string const& nodeSetName, const std::string& tcpName, float KpPosition, float KpOrientation):
            nodeSetName(nodeSetName),
            tcpName(tcpName),
            KpPosition(KpPosition),
            KpOrientation(KpOrientation)
        {}

        const std::string nodeSetName;
        const std::string tcpName;
        const float KpPosition;
        const float KpOrientation;
    };


    class NJointGlobalTCPController : public NJointController
    {
    public:
        using ConfigPtrT = NJointGlobalTCPControllerConfigPtr;
        NJointGlobalTCPController(RobotUnitPtr prov, const NJointControllerConfigPtr& config, const VirtualRobot::RobotPtr&);

        // NJointControllerInterface interface
        std::string getClassName(const Ice::Current&) const override;
        WidgetDescription::StringWidgetDictionary getFunctionDescriptions(const Ice::Current&) const override;
        void callDescribedFunction(const std::string& name, const StringVariantBaseMap& valueMap, const Ice::Current&) override;

        // NJointController interface
        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        static WidgetDescription::WidgetPtr GenerateConfigDescription(
            const VirtualRobot::RobotPtr&,
            const std::map<std::string, ConstControlDevicePtr>&,
            const std::map<std::string, ConstSensorDevicePtr>&);

        static NJointGlobalTCPControllerConfigPtr GenerateConfigFromVariants(const StringVariantBaseMap& values);
        NJointGlobalTCPController(const RobotUnitPtr& robotUnit,
                                  NJointGlobalTCPControllerConfigPtr config,
                                  const VirtualRobot::RobotPtr& r);
    protected:
        void rtPreActivateController() override;
        void rtPostDeactivateController() override;
    private:
        static ::armarx::WidgetDescription::WidgetSeq createSliders(float KpPosition, float KpOrientation);
        std::vector<ControlTarget1DoFActuatorVelocity*> targets;
        std::vector<const SensorValue1DoFActuatorPosition*> sensors;
        const SensorValueHolonomicPlatform* platformSensorValues;
        Eigen::Matrix4f startPose;

        std::atomic<float> xVel;
        std::atomic<float> yVel;
        std::atomic<float> zVel;
        std::atomic<float> rollVel;
        std::atomic<float> pitchVel;
        std::atomic<float> yawVel;
        VirtualRobot::RobotNodePtr tcp;
        VirtualRobot::DifferentialIKPtr ik;
        std::atomic<float> KpPosition;
        std::atomic<float> KpOrientation;




    };

} // namespace armarx

