#pragma once

// Simox
#include <VirtualRobot/VirtualRobot.h>

// ArmarX
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointController.h>

#include <armarx/control/joint_controller/Torque.h>
#include <armarx/control/joint_controller/VelocityManipulatingTorque.h>

namespace armarx
{

    TYPEDEF_PTRS_HANDLE(NJointTorqueController);
    TYPEDEF_PTRS_HANDLE(NJointTorqueControllerConfig);

    class NJointTorqueControllerConfig : virtual public NJointControllerConfig
    {
    public:
        NJointTorqueControllerConfig(std::string const& jointName, float Kp, float Ki, float Kd, float accelerationGain, float deadZone, float decay, float maxAcceleration, float maxJerk, float torqueToCurrent):
            jointName(jointName),
            Kp(Kp),
            Ki(Ki),
            Kd(Kd),
            accelerationGain(accelerationGain),
            deadZone(deadZone),
            decay(decay),
            maxAcceleration(maxAcceleration),
            maxJerk(maxJerk),
            torqueToCurrent(torqueToCurrent)
        {}

        const std::string jointName;
        const float Kp;
        const float Ki;
        const float Kd;
        const float accelerationGain;
        const float deadZone;
        const float decay;
        const float maxAcceleration;
        const float maxJerk;
        const float torqueToCurrent;
    };


    class NJointTorqueController : public NJointController
    {
    public:
        using ConfigPtrT = NJointTorqueControllerConfigPtr;
        NJointTorqueController(RobotUnitPtr prov, const NJointControllerConfigPtr& config, const VirtualRobot::RobotPtr&);

        // NJointControllerInterface interface
        std::string getClassName(const Ice::Current&) const override;
        WidgetDescription::StringWidgetDictionary getFunctionDescriptions(const Ice::Current&) const override;
        void callDescribedFunction(const std::string& name, const StringVariantBaseMap& valueMap, const Ice::Current&) override;

        // NJointController interface
        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        static WidgetDescription::WidgetPtr GenerateConfigDescription(
            const VirtualRobot::RobotPtr&,
            const std::map<std::string, ConstControlDevicePtr>&,
            const std::map<std::string, ConstSensorDevicePtr>&);

        static NJointTorqueControllerConfigPtr GenerateConfigFromVariants(const StringVariantBaseMap& values);
        NJointTorqueController(
            RobotUnitPtr prov,
            NJointTorqueControllerConfigPtr config,
            const VirtualRobot::RobotPtr& r);
    protected:
        void rtPreActivateController() override;
        void rtPostDeactivateController() override;
        void onPublish(const SensorAndControl&, const DebugDrawerInterfacePrx&, const DebugObserverInterfacePrx&) override;

    private:
        static ::armarx::WidgetDescription::WidgetSeq createSliders(float Kp, float Ki, float Kd, float accelerationGain, float deadZone, float decay, float maxAcceleration, float maxJerk, float torqueToCurrent);
        std::vector<ControlTarget1DoFActuatorCurrent*> targets;
        std::vector<const SensorValue1DoFActuator*> sensors;
        std::vector<std::shared_ptr<armarx::control::joint_controller::VelocityManipulatingTorqueController>> torqueCtrls;

        std::atomic<float> targetTorque, Kp, Ki, Kd, deadZone, decay, accelerationGain;

        std::atomic<float> currentTarget, currentPIDError, currentPIDIntegral, torqueToCurrent, maxAcceleration, maxJerk;
        std::atomic<float> velocityTarget, currentJerk, currentAcceleration;
    };

} // namespace armarx
