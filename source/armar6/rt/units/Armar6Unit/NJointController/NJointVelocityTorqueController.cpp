#include "NJointVelocityTorqueController.h"

#include <devices/ethercat/sensor_actor_unit/armar6/Data.h>

#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/RobotUnit.h>


namespace armarx
{

    NJointControllerRegistration<NJointVelocityTorqueController> registrationControllerNJointVelocityTorqueController("NJointVelocityTorqueController");


    NJointVelocityTorqueController::NJointVelocityTorqueController(RobotUnit* prov, NJointVelocityTorqueControllerConfigPtr config, const VirtualRobot::RobotPtr& r)
    {
        {
            ARMARX_CHECK_EXPRESSION(prov);
            ControlTargetBase* ct = useControlTarget(config->deviceName, ControlModes::VelocityTorque);
            auto sensor = useSensorValue(config->deviceName);
            ARMARX_CHECK_EXPRESSION(sensor) << "device name: " << config->deviceName;
            sensorValues = sensor->asA<devices::ethercat::sensor_actor_unit::armar6::SensorValueArmar6Actuator>();
            ARMARX_CHECK_EXPRESSION(sensorValues);
            ARMARX_CHECK_EXPRESSION(ct->asA<ControlTarget1DoFActuatorTorqueVelocity>());
            target = ct->asA<ControlTarget1DoFActuatorTorqueVelocity>();
        }

        this->config = config;
    }


    std::string NJointVelocityTorqueController::getClassName(const Ice::Current&) const
    {
        return "NJointVelocityTorqueController";
    }


    void NJointVelocityTorqueController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        target->velocity = config->desiredVelocity;
        target->maxTorque = config->maxTorque;

        //        ARMARX_INFO << deactivateSpam(1) << "Setting current value to " << target->current << " with " << VAROUT(config->desiredTorque) << VAROUT(config->desiredVelocity);
        //        sensorValues->torque;
        //        sensorValues->position;
        //        sensorValues->relativePosition;
        //        sensorValues->velocity;
    }


    ::armarx::WidgetDescription::WidgetSeq NJointVelocityTorqueController::createSliders()
    {
        using namespace armarx::WidgetDescription;
        ::armarx::WidgetDescription::WidgetSeq widgets;
        auto addSlider = [&](const std::string & label, float min, float max, float defaultValue)
        {
            widgets.emplace_back(new Label(false, label));
            {
                FloatSliderPtr c_x = new FloatSlider;
                c_x->name = label;
                c_x->min = min;
                c_x->defaultValue = defaultValue;
                c_x->max = max;
                widgets.emplace_back(c_x);
            }
        };
        addSlider("desiredTorque", 0, 50, 0);
        addSlider("desiredVelocity", -2, 2, 0);
        //        addSlider("Kp", 0, 10, 1);
        //        addSlider("Ki", 0, 3, 0);
        //        addSlider("Kd", 0, 3, 0);
        return widgets;
    }


    WidgetDescription::WidgetPtr NJointVelocityTorqueController::GenerateConfigDescription(const VirtualRobot::RobotPtr&, const std::map<std::string, ConstControlDevicePtr>& controlDevices, const std::map<std::string, ConstSensorDevicePtr>&)
    {
        using namespace armarx::WidgetDescription;
        HBoxLayoutPtr layout = new HBoxLayout;

        LabelPtr label = new Label;
        label->text = "control device name";
        layout->children.emplace_back(label);
        StringComboBoxPtr box = new StringComboBox;
        box->defaultIndex = 0;
        //filter control devices
        for (const auto& name2dev : controlDevices)
        {
            const ConstControlDevicePtr& dev = name2dev.second;
            const auto& name = name2dev.first;
            ARMARX_CHECK_EXPRESSION(dev) << name;
            if (

                dev->hasJointController(ControlModes::VelocityTorque) &&
                dev->getJointController(ControlModes::VelocityTorque)->getControlTarget()->isA<ControlTarget1DoFActuatorTorqueVelocity>()
            )
            {
                box->options.emplace_back(name);
            }
        }
        box->name = "name";
        layout->children.emplace_back(box);
        auto sliders = createSliders();
        layout->children.insert(layout->children.end(),
                                sliders.begin(),
                                sliders.end());
        layout->children.emplace_back(new HSpacer);
        return layout;
    }


    NJointVelocityTorqueControllerConfigPtr NJointVelocityTorqueController::GenerateConfigFromVariants(const StringVariantBaseMap& values)
    {
        return new NJointVelocityTorqueControllerConfig
        {
            values.at("desiredTorque")->getFloat(),
            values.at("desiredVelocity")->getFloat(),
            values.at("name")->getString()
        };
    }


    WidgetDescription::StringWidgetDictionary NJointVelocityTorqueController::getFunctionDescriptions(const Ice::Current&) const
    {
        using namespace armarx::WidgetDescription;
        HBoxLayoutPtr layout = new HBoxLayout;
        auto sliders = createSliders();
        layout->children.insert(layout->children.end(),
                                sliders.begin(),
                                sliders.end());
        return {{"ControllerConfig", layout}};
    }


    void NJointVelocityTorqueController::callDescribedFunction(const std::string& name, const StringVariantBaseMap& valueMap, const Ice::Current&)
    {
        if (name == "ControllerConfig")
        {
            config->maxTorque = valueMap.at("desiredTorque")->getFloat();
            config->desiredVelocity = valueMap.at("desiredVelocity")->getFloat();
            //            config->Kp = valueMap.at("Kp")->getFloat();
            //            config->Ki = valueMap.at("Ki")->getFloat();
            //            config->Kd = valueMap.at("Kd")->getFloat();
        }
        else
        {
            ARMARX_WARNING << "Unknown function name called: " << name;
        }
    }

}
