#pragma once

#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointController.h>

#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>

#include <devices/ethercat/sensor_actor_unit/armar6/Data.h>


namespace armarx
{


    TYPEDEF_PTRS_HANDLE(NJointVelocityTorqueController);
    TYPEDEF_PTRS_HANDLE(NJointVelocityTorqueControllerConfig);

    class NJointVelocityTorqueControllerConfig : virtual public NJointControllerConfig
    {
    public:
        NJointVelocityTorqueControllerConfig(float desiredTorque, float desiredVelocity, std::string const& deviceName):
            maxTorque {desiredTorque},
            desiredVelocity {desiredVelocity},
            deviceName(deviceName)
        {}
        std::atomic<float> maxTorque {0.0};
        std::atomic<float> desiredVelocity {0.1};
        //        std::atomic<float> Kp {1.0};
        //        std::atomic<float> Ki {0};
        //        std::atomic<float> Kd {0};
        const std::string deviceName;
    };


    class NJointVelocityTorqueController : public armarx::NJointController
    {
    public:
        using ConfigPtrT = NJointVelocityTorqueControllerConfigPtr;

        // NJointControllerInterface interface
    public:
        std::string getClassName(const Ice::Current&) const override;

        // NJointController interface
    public:
        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;

        static WidgetDescription::WidgetPtr GenerateConfigDescription(
            const VirtualRobot::RobotPtr&,
            const std::map<std::string, ConstControlDevicePtr>&,
            const std::map<std::string, ConstSensorDevicePtr>&);

        static NJointVelocityTorqueControllerConfigPtr GenerateConfigFromVariants(const StringVariantBaseMap& values);
        NJointVelocityTorqueController(
            RobotUnit* prov,
            NJointVelocityTorqueControllerConfigPtr config,
            const VirtualRobot::RobotPtr& r);
        ControlTarget1DoFActuatorTorqueVelocity* target;
        const devices::ethercat::sensor_actor_unit::armar6::SensorValueArmar6Actuator* sensorValues;
        NJointVelocityTorqueControllerConfigPtr config;
        // NJointControllerInterface interface
    public:
        WidgetDescription::StringWidgetDictionary getFunctionDescriptions(const Ice::Current&) const override;
        void callDescribedFunction(const std::string& name, const StringVariantBaseMap& valueMap, const Ice::Current&) override;
        static ::armarx::WidgetDescription::WidgetSeq createSliders();
    };
}
