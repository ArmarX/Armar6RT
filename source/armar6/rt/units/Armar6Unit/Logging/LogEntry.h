/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <IceUtil/Time.h>

#include <Eigen/Core>


namespace devices::ethercat::sensor_actor_unit::armar6
{
    class Device;
}


namespace devices::ethercat::ft_sensor_board::armar6
{
    class Device;
}


namespace armarx
{

    struct FtLogEntry
    {
        Eigen::Vector3f forces;
        Eigen::Vector3f torques;
        Eigen::Matrix<int, 6, 1> adcTicks;
        std::vector<std::string> getColumns()
        {
            return {"fx", "fy", "fz", "tx", "ty", "tz", "adc1", "adc2", "adc3", "adc4", "adc5", "adc6"};
        }
    };


    struct JointLogEntry
    {
        float position;
        float velocity;
        float torque;
        std::int32_t torqueTicks;

        std::vector<std::string> getColumns()
        {
            return {"position", "velocity", "torque", "torqueTicks"};
        }
    };


    struct LogEntry
    {
        long timestamp;
        std::vector<FtLogEntry> fts;
        std::vector<JointLogEntry> joints;

        std::string toString();
        std::string getHeader(const std::vector<devices::ethercat::sensor_actor_unit::armar6::Device*>& joints,
                              const std::vector<devices::ethercat::ft_sensor_board::armar6::Device*>& ftSensors);
    };

}

