/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <fstream>
#include <memory>

#include <IceUtil/Time.h>

#include "CircularBuffer.h"
#include "LogEntry.h"


namespace devices::ethercat::sensor_actor_unit::armar6
{
    class Device;
}


namespace devices::ethercat::ft_sensor_board::armar6
{
    class Device;
}


namespace armarx
{

    class RtLogging;
    using RtLoggingPtr = std::shared_ptr<RtLogging>;


    class RtLogging
    {
    public:
        RtLogging(const std::vector<devices::ethercat::sensor_actor_unit::armar6::Device*>& joints, const std::vector<devices::ethercat::ft_sensor_board::armar6::Device*>& ftSensors, double maxFrequency = std::numeric_limits<double>::max());
        void rtLog(const IceUtil::Time& timestamp);
        void startLogging(const std::string& filename);
        void stopLogging();
        void writeToFile();

    private:
        CircularBuffer<LogEntry> logBuffer;
        const std::vector<devices::ethercat::sensor_actor_unit::armar6::Device*> joints;
        const std::vector<devices::ethercat::ft_sensor_board::armar6::Device*> ftSensors;
        bool loggingActivate = false;
        bool loggingWasActive = false;
        std::string logFilename;
        std::ofstream logfile;
        IceUtil::Time startTime, lastTimestamp;
        double maxFrequency;
    };

}
