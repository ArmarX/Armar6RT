/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "RtLogging.h"

#include <filesystem>

#include <devices/ethercat/ft_sensor_board/armar6/Device.h>
#include <devices/ethercat/sensor_actor_unit/armar6/Device.h>


using namespace armarx;

using FTSensorBoardDevice = devices::ethercat::ft_sensor_board::armar6::Device;
using SensorActorUnitDevice = devices::ethercat::sensor_actor_unit::armar6::Device;


RtLogging::RtLogging(const std::vector<SensorActorUnitDevice*>& joints, const std::vector<FTSensorBoardDevice*>& ftSensors, double maxFrequency)
    : joints(joints), ftSensors(ftSensors), maxFrequency(maxFrequency)
{
    LogEntry initLogEntry;
    initLogEntry.fts.resize(ftSensors.size());
    initLogEntry.joints.resize(joints.size());
    logBuffer.init(1000, initLogEntry);
}


void RtLogging::rtLog(const IceUtil::Time& timestamp)
{
    if (!loggingActivate)
    {
        return;
    }
    double freq = 1.0 / (timestamp - lastTimestamp).toSecondsDouble();
    if (freq > maxFrequency)
    {
        return;
    }
    lastTimestamp = timestamp;

    logBuffer.getWriteRef().timestamp = (timestamp - startTime).toMicroSeconds();
    for (size_t i = 0; i < joints.size(); i++)
    {
        JointLogEntry& e = logBuffer.getWriteRef().joints.at(i);
        const SensorActorUnitDevice* joint = joints.at(i);
        e.position = joint->getData()->getActualPosition();
        e.torque = joint->getData()->getActualTorque();
        e.velocity = joint->getData()->getActualVelocity();
        e.torqueTicks = joint->getData()->getTorqueTicks();

    }
    for (size_t i = 0; i < ftSensors.size(); i++)
    {
        FtLogEntry& e = logBuffer.getWriteRef().fts.at(i);
        const FTSensorBoardDevice* ftSensor = ftSensors.at(i);
        e.forces = ftSensor->getData()->getForces();
        e.torques = ftSensor->getData()->getTorques();
        e.adcTicks = ftSensor->getData()->getRawValues();
    }
    logBuffer.push();
}


void RtLogging::startLogging(const std::string& filename)
{
    if (!loggingActivate)
    {
        startTime = armarx::rtNow();
        logFilename = filename;
        loggingActivate = true;
        ARMARX_IMPORTANT_S << "start logging to: " << filename;
    }
}


void RtLogging::stopLogging()
{
    loggingActivate = false;
    ARMARX_IMPORTANT_S << "stop logging";
}


void RtLogging::writeToFile()
{
    if (!loggingActivate)
    {
        if (loggingWasActive && logfile.is_open())
        {
            logfile.close();
        }
        loggingWasActive = false;
        return;
    }
    if (!loggingWasActive)
    {
        ARMARX_IMPORTANT_S << "open logfile: " << logFilename;
        bool writeHeader = !std::filesystem::exists(logFilename);
        logfile.open(logFilename, std::ofstream::out | std::ofstream::app);
        if (writeHeader)
        {
            logfile << logBuffer.getReadRef().getHeader(joints, ftSensors) << "\n";
        }
    }
    while (logBuffer.hasData())
    {
        logfile << logBuffer.getReadRef().toString() << "\n";
        logBuffer.pop();
    }
    logfile.flush();
    loggingWasActive = true;
}
