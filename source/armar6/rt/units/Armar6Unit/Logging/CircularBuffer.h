/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <vector>
#include <atomic>

namespace armarx
{

    template<class T>
    class CircularBuffer
    {
    public:
        CircularBuffer()
        {
            readIndex = 0;
            writeIndex = 0;
        }

        void init(size_t size, T initValue)
        {
            buffer.resize(size, initValue);
        }

        T& getWriteRef()
        {
            return buffer.at(writeIndex % buffer.size());
        }
        T& getReadRef()
        {
            return buffer.at(readIndex % buffer.size());
        }

        void push()
        {
            writeIndex++;
        }

        void pop()
        {
            if (hasData())
            {
                readIndex++;
            }
        }

        bool hasData()
        {
            return readIndex != writeIndex;
        }


        size_t length()
        {
            return writeIndex - readIndex;
        }

    private:
        std::vector<T> buffer;
        std::atomic<size_t> readIndex;
        std::atomic<size_t> writeIndex;
    };
}

