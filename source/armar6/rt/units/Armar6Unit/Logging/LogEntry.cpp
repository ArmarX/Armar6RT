/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "LogEntry.h"


#include <devices/ethercat/ft_sensor_board/armar6/Device.h>
#include <devices/ethercat/sensor_actor_unit/armar6/Device.h>


using namespace armarx;

using FTSensorBoardDevice = devices::ethercat::ft_sensor_board::armar6::Device;
using SensorActorUnitDevice = devices::ethercat::sensor_actor_unit::armar6::Device;


std::string LogEntry::toString()
{
    std::stringstream ss;
    ss << timestamp;
    Eigen::IOFormat CommaInitFmt(Eigen::StreamPrecision, Eigen::DontAlignCols, ", ", ",", "", "", "", "");
    for (const FtLogEntry& e : fts)
    {
        ss << "," << e.forces(0) << "," << e.forces(1) << "," << e.forces(2) << "," << e.torques(0) << "," << e.torques(1) << "," << e.torques(2) << ","
           << e.adcTicks.format(CommaInitFmt);
    }
    for (const JointLogEntry& e : joints)
    {
        ss << "," << e.position << "," << e.velocity << "," << e.torque << "," << e.torqueTicks;
    }

    return ss.str();
}


std::string LogEntry::getHeader(const std::vector<SensorActorUnitDevice*>& joints,
                                const std::vector<FTSensorBoardDevice*>& ftSensors)
{
    std::stringstream ss;
    ss << "timestamp ms";
    for (const FTSensorBoardDevice* ft : ftSensors)
    {
        for (const std::string& col : fts.at(0).getColumns())
        {
            ss << "," << ft->getDeviceName() << "." << col;
        }
    }
    for (const SensorActorUnitDevice* joint : joints)
    {
        for (const std::string& col : this->joints.at(0).getColumns())
        {
            ss << "," << joint->getDeviceName() << "." << col;
        }
    }
    return ss.str();
}
