/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <memory>
#include <Ice/Handle.h>

#define H2T_VENDOR_ID 0x7bc
#define H2T_SENSOBOARD_PRODUCT_CODE 0x9252

namespace armarx
{


    using RtRobotContainerPtr = std::shared_ptr<class RtRobotContainer>;
    using SensorBoardPtr = std::shared_ptr<class SensorBoard>;
    using JointPtr = std::shared_ptr<class Joint>;
    using JointCurrentControllerPtr = std::shared_ptr<class JointCurrentController>;

    using JointVelocityControllerPtr = std::shared_ptr<class JointVelocityController >;
    using JointPositionControllerPtr = std::shared_ptr<class JointPositionController > ;
    using JointTorqueControllerPtr = std::shared_ptr<class JointTorqueController   > ;
    using JointZeroTorqueControllerPtr = std::shared_ptr<class JointZeroTorqueController   > ;
    using JointCurrentPassThroughControllerPtr = std::shared_ptr<class JointCurrentPassThroughController>;
    using JointActiveImpedanceControllerPtr = std::shared_ptr<class JointActiveImpedanceController>;
    using JointVelocityTorqueControllerPtr = std::shared_ptr<class JointVelocityTorqueController>;
    template <class Type> class JointStopMovementController;
    template <class Type> class JointElmoEmergencyController;

    using TorqueControllerConfigurationPtr = std::shared_ptr<class TorqueControllerConfiguration >;
    using VelocityManipulatingTorqueControllerConfigurationPtr = std::shared_ptr<class VelocityManipulatingTorqueControllerConfiguration>;
    using PositionControllerConfigurationPtr = std::shared_ptr<class PositionControllerConfiguration >;
    using VelocityControllerConfigurationPtr = std::shared_ptr<class VelocityControllerConfiguration >;


    using KITHandUnitPtr = IceInternal::Handle<class KITHandUnit>;
}
