/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_rt::ArmarXObjects::KITSensorizedSoftFingerHandV1NJointController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointController.h>

#include <armar6/rt/units/Armar6Unit/Devices/KITSensorizedSoftFingerHand/V1/Controllers/CloseAndHold.h>
#include <armar6/rt/units/Armar6Unit/Devices/KITSensorizedSoftFingerHand/V1/utility/Sensors.h>

namespace armarx::KITSensorizedSoftFingerHand::V1
{
    TYPEDEF_PTRS_HANDLE(KITSensorizedSoftFingerHandV1NJointControllerConfig);
    class KITSensorizedSoftFingerHandV1NJointControllerConfig : public NJointControllerConfig
    {
    public:
        using CAHCtrl = Controllers::CloseAndHold;

        std::string          hand_name;

        float                thumb_p = 0;
        float                thumb_i = 0;
        float                thumb_d = 0;
        float                thumb_holding_start_force_threshold = 0;
        float                thumb_holding_stop_force_threshold  = 0;
        std::int16_t         thumb_max_pwm = 512;
        float                thumb_velocity = 0;
        CAHCtrl::HoldingMode thumb_holding_mode = CAHCtrl::HoldingMode::velocity;

        float                index_p = 0;
        float                index_i = 0;
        float                index_d = 0;
        float                index_holding_start_force_threshold = 0;
        float                index_holding_stop_force_threshold  = 0;
        std::int16_t         index_max_pwm = 512;
        float                index_velocity = 0;
        CAHCtrl::HoldingMode index_holding_mode = CAHCtrl::HoldingMode::velocity;

        float                other_p = 0;
        float                other_i = 0;
        float                other_d = 0;
        float                other_holding_start_force_threshold = 0;
        float                other_holding_stop_force_threshold  = 0;
        std::int16_t         other_max_pwm = 512;
        float                other_velocity = 0;
        CAHCtrl::HoldingMode other_holding_mode = CAHCtrl::HoldingMode::velocity;
    };

    /**
    * @defgroup Library-KITSensorizedSoftFingerHandV1NJointController KITSensorizedSoftFingerHandV1NJointController
    * @ingroup armar6_rt
    * A description of the library KITSensorizedSoftFingerHandV1NJointController.
    *
    * @class KITSensorizedSoftFingerHandV1NJointController
    * @ingroup Library-KITSensorizedSoftFingerHandV1NJointController
    * @brief Brief description of class KITSensorizedSoftFingerHandV1NJointController.
    *
    * Detailed description of class KITSensorizedSoftFingerHandV1NJointController.
    */
    class KITSensorizedSoftFingerHandV1NJointController :
        public NJointController
    {
    public:
        using ConfigPtrT = KITSensorizedSoftFingerHandV1NJointControllerConfigPtr;
        KITSensorizedSoftFingerHandV1NJointController(
            RobotUnitPtr robotUnit,
            const KITSensorizedSoftFingerHandV1NJointControllerConfigPtr& config,
            const VirtualRobot::RobotPtr&);

        std::string getClassName(const Ice::Current& = Ice::emptyCurrent) const override;
        WidgetDescription::StringWidgetDictionary getFunctionDescriptions(const Ice::Current& = Ice::emptyCurrent) const override;
        void callDescribedFunction(const std::string& name, const StringVariantBaseMap& valueMap, const Ice::Current& = Ice::emptyCurrent) override;

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;

        static WidgetDescription::WidgetPtr GenerateConfigDescription(
            const VirtualRobot::RobotPtr&,
            const std::map<std::string, ConstControlDevicePtr>&,
            const std::map<std::string, ConstSensorDevicePtr>&);

        static KITSensorizedSoftFingerHandV1NJointControllerConfigPtr
        GenerateConfigFromVariants(const StringVariantBaseMap& values);
    protected:
        void rtPreActivateController() override;
        void rtPostDeactivateController() override;

        using CAHCtrl = Controllers::CloseAndHold;

        std::atomic<float>                   _force_thresh_hold_start_thumb{0};
        std::atomic<float>                   _force_thresh_hold_start_index{0};
        std::atomic<float>                   _force_thresh_hold_start_other{0};

        std::atomic<float>                   _force_thresh_hold_stop_thumb{0};
        std::atomic<float>                   _force_thresh_hold_stop_index{0};
        std::atomic<float>                   _force_thresh_hold_stop_other{0};

        float                                _finger_vel_thumb_trg{0};
        float                                _finger_vel_index_trg{0};
        float                                _finger_vel_other_trg{0};

        std::atomic<float>                   _finger_vel_thumb{0};
        std::atomic<float>                   _finger_vel_index{0};
        std::atomic<float>                   _finger_vel_other{0};

        std::atomic_int16_t                  _max_pwm_thumb{0};
        std::atomic_int16_t                  _max_pwm_index{0};
        std::atomic_int16_t                  _max_pwm_other{0};

        std::atomic<CAHCtrl::HoldingMode>    _holding_mode_thumb{CAHCtrl::HoldingMode::velocity};
        std::atomic<CAHCtrl::HoldingMode>    _holding_mode_index{CAHCtrl::HoldingMode::velocity};
        std::atomic<CAHCtrl::HoldingMode>    _holding_mode_other{CAHCtrl::HoldingMode::velocity};

        CAHCtrl                              _ctrl_thumb;
        CAHCtrl                              _ctrl_index;
        CAHCtrl                              _ctrl_other;

        ControlTarget1DoFActuatorPWM*        _target_pwm_thumb = nullptr;
        ControlTarget1DoFActuatorPWM*        _target_pwm_index = nullptr;
        ControlTarget1DoFActuatorPWM*        _target_pwm_other = nullptr;

        Sensors _sensors;
    };
}
