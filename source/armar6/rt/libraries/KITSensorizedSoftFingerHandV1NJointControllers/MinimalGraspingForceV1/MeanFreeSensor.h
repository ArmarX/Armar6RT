#pragma once

#include <cstdint>
#include <array>

namespace armarx::KITSensorizedSoftFingerHand::V1
{
    template<class ValueT>
    struct MeanFreeValue
    {
        MeanFreeValue()
        {
            for (auto& v : continuous_mean_buffer)
            {
                v = 0;
            }
        }

        static constexpr uint32_t NUM_AVERAGING_SAMPLES_NORMAL_FORCE_SENSOR = 1000;
        std::array<float, NUM_AVERAGING_SAMPLES_NORMAL_FORCE_SENSOR> continuous_mean_buffer;
        std::size_t continuous_mean_index = 0;
        float continuous_mean = 0;
        float continuous_peak_to_peak_noise = 0;
        float current_continuous_mean_free_scaled = 0;

        int64_t current                  = 0;
        int64_t mean_offset              = 0;
        int64_t peak_to_peak_noise       = 0;
        int64_t current_mean_free        = 0;
        //internal state
        int64_t accumulator              = 0;
        int64_t noise_minimum            = 0;
        int64_t noise_maximum            = 0;
        int64_t number_of_updates        = 0;
        float   current_scaled           = 0;
        float   current_mean_free_scaled = 0;

        //do not reset
        int64_t one_percent               = 0;
        float    scaling                  = 1;

        void reset_calibration()
        {
            current                  = 0;
            mean_offset              = 0;
            peak_to_peak_noise       = 0;
            current_mean_free        = 0;
            accumulator              = 0;
            noise_minimum            = 0;
            noise_maximum            = 0;
            number_of_updates        = 0;
            current_scaled           = 0;
            current_mean_free_scaled = 0;
        }

        void visit(auto f) const
        {
            ARMARX_TRACE;
            f("current",               1e-5 +  current);
            f("mean_offset",           1e-5 +  mean_offset);
            f("peak_to_peak_noise",    1e-5 +  peak_to_peak_noise);
            f("current_mean_free",     1e-5 +  current_mean_free);

            f("continuous_mean",     1e-5 +   continuous_mean);
            f("current_continuous_mean_free_scaled",      1e-5 +  current_continuous_mean_free_scaled);
            //f("accumulator",              accumulator);
            //f("noise_minimum",            noise_minimum);
            //f("noise_maximum",            noise_maximum);
            //f("number_of_updates",        number_of_updates);
            //
            f("scaling",                1e-5 +  scaling);
            //f("current_scaled",           current_scaled);
            f("current_mean_free_scaled", 1e-5 + current_mean_free_scaled);
        }

        bool test_current_over_one_percent_plus_peak_noise() const
        {
            ARMARX_TRACE;
            //Offset is 1% of sensor range (effective or data sheet)
            if (current > continuous_mean + (peak_to_peak_noise + one_percent))
            {
                return true;
            }
            return false;
        }

        void update(ValueT value, bool update_continuous)
        {
            const int64_t i64val = static_cast<int64_t>(value);
            ARMARX_TRACE;
            current = i64val;
            current_mean_free = static_cast<int64_t>(current) - static_cast<int64_t>(mean_offset);
            current_scaled = current * scaling;
            current_mean_free_scaled = current_mean_free * scaling;

            if (update_continuous || number_of_updates <= NUM_AVERAGING_SAMPLES_NORMAL_FORCE_SENSOR)
            {
                const auto idx = (++continuous_mean_index) % NUM_AVERAGING_SAMPLES_NORMAL_FORCE_SENSOR;
                continuous_mean += (-continuous_mean_buffer.at(idx) + current) / NUM_AVERAGING_SAMPLES_NORMAL_FORCE_SENSOR;
                continuous_mean_buffer.at(idx) = current;
                float min = std::numeric_limits<float>::infinity();
                float max = -min;
                for (const auto v : continuous_mean_buffer)
                {
                    min = std::min(min, v);
                    max = std::max(max, v);
                }
                continuous_peak_to_peak_noise = max - min;
            }
            current_continuous_mean_free_scaled = (current - continuous_mean) * scaling;

            if (number_of_updates > NUM_AVERAGING_SAMPLES_NORMAL_FORCE_SENSOR)
            {
                return;
            }

            accumulator += i64val;

            if (number_of_updates == 0)
            {
                noise_minimum = i64val;
                noise_maximum = i64val;
            }

            if (number_of_updates < NUM_AVERAGING_SAMPLES_NORMAL_FORCE_SENSOR)
            {
                if (noise_minimum > i64val)
                {
                    noise_minimum = i64val;
                }
                if (noise_maximum < i64val)
                {
                    noise_maximum = i64val;
                }
            }

            if (number_of_updates == NUM_AVERAGING_SAMPLES_NORMAL_FORCE_SENSOR)
            {
                peak_to_peak_noise = noise_maximum - noise_minimum;
                mean_offset = accumulator / NUM_AVERAGING_SAMPLES_NORMAL_FORCE_SENSOR;
            }

            number_of_updates++;
        }
    };

}
