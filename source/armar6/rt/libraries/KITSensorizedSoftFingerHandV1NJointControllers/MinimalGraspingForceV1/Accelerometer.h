#pragma once

#include <array>
#include <cmath>
#include <complex>

#include <unsupported/Eigen/FFT>

#include <armar6/rt/units/Armar6Unit/Devices/KITSensorizedSoftFingerHand/V1/utility/Sensors.h>


namespace armarx::KITSensorizedSoftFingerHand::V1
{
    template<std::uint16_t N>
    struct Accelerometer
    {
        std::array<float, N> x;
        std::array<float, N> y;
        std::array<float, N> xy;
        std::array < std::complex<float>, N + 1 > fft_tmp;
        std::array < float, N / 2 > fft_absolute_x;
        std::array < float, N / 2 > fft_absolute_y;
        std::array < float, N / 2 > fft_absolute_xy;

        double shear_force_criterium = 0.0f;
        double fft_contact_detect_energy = 0.0f;

        Accelerometer()
        {
            ARMARX_TRACE;
            for (std::size_t i = 0; i < N; ++i)
            {
                x.at(i) = 0;
                y.at(i) = 0;
                xy.at(i) = 0;
                if (i < fft_tmp.size())
                {
                    fft_tmp.at(i) = 0;
                }
                if (i < fft_absolute_x.size())
                {
                    fft_absolute_x.at(i) = 0;
                    fft_absolute_y.at(i) = 0;
                    fft_absolute_xy.at(i) = 0;
                }
            }
        }

        void update(const Sensors::Finger& finger)
        {
            ARMARX_TRACE;
            static constexpr std::uint16_t buffer_size = N;
            const std::size_t num_new = std::min(buffer_size, finger->accelerometer_fifo_length);
            if (num_new > 0)
            {
                const std::size_t num_old = N - num_new;
                std::copy(x.begin() + num_new, x.end(), x.begin());
                std::copy(y.begin() + num_new, y.end(), y.begin());
                std::copy(xy.begin() + num_new, xy.end(), xy.begin());
                for (std::size_t i = 0 ; i < num_new; ++i)
                {
                    x.at(num_old + i) = finger->raw_accelerometer_fifo.at(i).at(0);
                    y.at(num_old + i) = finger->raw_accelerometer_fifo.at(i).at(1);
                    xy.at(num_old + i) = std::hypot(x.at(num_old + i), y.at(num_old + i));
                }
            }
            Eigen::FFT<float> fft;
            fft.SetFlag(fft.HalfSpectrum);
            const auto update = [&](const auto & in, auto & out)
            {
                fft.fwd(fft_tmp.data(), in.data(), N);
                for (std::size_t i = 0; i < out.size(); ++i)
                {
                    out.at(i) = std::abs(fft_tmp.at(i));
                }
            };
            update(x,  fft_absolute_x);
            update(y,  fft_absolute_y);
            update(xy, fft_absolute_xy);

            //calculate sum of buckets between 100 and 200hz
            //per bucket: 25hz -> use buckets 4-7
            //+4 = 100hz
            //+8 = 200hz
            //+12 = 300hz
            //+16 = 400hz
            //+20 = 500hz
            //+24 = 600hz
            //+28 = 700hz
            {
                shear_force_criterium = 0.0f;
                for (std::size_t i = 4; i < 7; i++)
                {
                    shear_force_criterium += fft_absolute_xy.at(i);
                }
            }
            //fft_contact_detect_energy
            {
                fft_contact_detect_energy = 0.0f;
                for (std::size_t i = 16; i < 32; i++)
                {
                    fft_contact_detect_energy += fft_absolute_xy.at(i);
                }
            }
        }
    };
}
