#pragma once

#include <array>

#include <armar6/rt/units/Armar6Unit/Devices/KITSensorizedSoftFingerHand/V1/utility/Sensors.h>

#include "NormalForceSensor.h"
#include "ShearForceSensor.h"
#include "Accelerometer.h"

namespace armarx::KITSensorizedSoftFingerHand::V1
{
    struct FingerSensor
    {
        std::array<NormalForceSensor, 4> normal_force_sensors;
        ShearForceSensor                 shear_force_dist_joint;
        ShearForceSensor                 shear_force_dist_tip;
        Accelerometer<64>                accelerometer;

        FingerSensor()
        {
            ARMARX_TRACE;
            for (auto& fs : normal_force_sensors)
            {
                fs.one_percent = 21;
            }
        }

        void reset_calibration()
        {
            for (auto& s : normal_force_sensors)
            {
                s.reset_calibration();
            }
            for (auto& s : shear_force_dist_joint.sensors)
            {
                s.reset_calibration();
            }
            for (auto& s : shear_force_dist_tip.sensors)
            {
                s.reset_calibration();
            }
        }

        void update(const Sensors::Finger& finger, bool update_continuous)
        {
            ARMARX_TRACE;
            normal_force_sensors.at(0).update(finger->raw_pressure_prox_l, update_continuous);
            normal_force_sensors.at(1).update(finger->raw_pressure_prox_r, update_continuous);
            normal_force_sensors.at(2).update(finger->raw_pressure_dist_joint, update_continuous);
            normal_force_sensors.at(3).update(finger->raw_pressure_dist_tip, update_continuous);

            shear_force_dist_joint.update(finger->raw_shear_force_dist_joint, update_continuous);
            shear_force_dist_tip.update(finger->raw_shear_force_dist_tip, update_continuous);

            accelerometer.update(finger);

        }

        bool test_for_object_touched() const
        {
            ARMARX_TRACE;
            for (auto& fs : normal_force_sensors)
            {
                if (fs.test_current_over_one_percent_plus_peak_noise())
                {
                    return true;
                }
            }
            return false;
        }
    };
}
