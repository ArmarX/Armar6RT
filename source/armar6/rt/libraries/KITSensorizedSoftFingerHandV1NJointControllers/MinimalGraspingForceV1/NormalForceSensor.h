#pragma once

#include "MeanFreeSensor.h"

namespace armarx::KITSensorizedSoftFingerHand::V1
{
    using NormalForceSensor = MeanFreeValue<uint32_t>;
}
