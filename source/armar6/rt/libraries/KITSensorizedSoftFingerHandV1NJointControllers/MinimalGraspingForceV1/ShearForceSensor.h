#pragma once

#include <array>
#include <cmath>

#include <armar6/rt/units/Armar6Unit/Devices/KITSensorizedSoftFingerHand/V1/utility/Sensors.h>

#include "MeanFreeSensor.h"

namespace armarx::KITSensorizedSoftFingerHand::V1
{
    struct ShearForceSensor
    {
        std::array<MeanFreeValue<int16_t>, 3> sensors;
        float len_xy;
        float len_z;

        ShearForceSensor()
        {
            ARMARX_TRACE;
            sensors.at(0).scaling = 0.751f;
            sensors.at(1).scaling = 0.751f;
            sensors.at(2).scaling = 1.21f;
        }

        void update(const std::array<std::int16_t, 3>& raw_shear_force, bool update_continuous)
        {
            ARMARX_TRACE;
            for (std::size_t i = 0; i < 3; ++i)
            {
                sensors.at(i).update(raw_shear_force.at(i), update_continuous);
            }
            len_xy = std::hypot(sensors.at(0).current_mean_free_scaled,
                                sensors.at(1).current_mean_free_scaled);
            len_z = std::abs(sensors.at(2).current_mean_free_scaled);
        }
    };
}
