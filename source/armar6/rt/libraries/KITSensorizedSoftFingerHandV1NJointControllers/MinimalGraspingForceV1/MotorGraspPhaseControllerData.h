#pragma once
#include <iostream>

#include <boost/algorithm/clamp.hpp>

#include <armar6/rt/units/Armar6Unit/Devices/KITSensorizedSoftFingerHand/V1/Controllers/CloseAndHold.h>
#include <armar6/rt/units/Armar6Unit/Devices/KITSensorizedSoftFingerHand/V1/utility/Sensors.h>
#include <armar6/rt/units/Armar6Unit/Devices/KITSensorizedSoftFingerHand/V1/utility/SensorFit.h>

#include "FingerSensor.h"
#include "Config.h"

//declare
namespace armarx::KITSensorizedSoftFingerHand::V1
{
    struct controller_run_data
    {
        Sensors& sensors;
        double dt_sec;
        bool fpga_updated;
        double dt_sec_fpga;
    };

    struct MotorGraspPhaseControllerData
    {
        using config_t = NJointMinimalGraspingForceV1ControllerConfig;
        using MotorIdentity = Sensors::MotorIdentity;

        using GraspPhase = GraspPhase::Phase;
        MotorIdentity motor_identity;
        std::array<FingerSensor*, 3> fingers = {nullptr, nullptr, nullptr};

        float position_of_initial_contact;
        float position_of_unload_complete;

        Controllers::PositionToPWM pos_pwm_controller;
        std::int16_t max_pwm;

        PIDController pid_shear;
        PIDController pid_unload;

        PIDController pid_force;
        float         pid_force_limit = 42;
        float         pid_force_forward_p = 42;

        struct controller_state_data
        {
            float shear_force_used_ratio = 0.0f;
            std::size_t shear_force_used_ratio_num = 0;
            float maximum_pressure = 0;
            float pressure_on_start_unload = 0;

            float target_shear = 0.0f;
            float target_pressure = 0.0f;

            bool contact_detection_flag = false;
            //bool slip_detection_flag = false;
            GraspPhase grasp_phase = GraspPhase::FREELY_MOVING;
            bool finger_did_close       = false;

            float monotonic_shear_pressure_control_last_pwm          = 0;
            float monotonic_shear_pressure_control_last_pwm_pressure = 0;
            float monotonic_shear_pressure_control_last_pwm_shear    = 0;
            float monotonic_shear_pressure_control_last_delta_pressure = 0;
            float monotonic_shear_pressure_control_last_delta_shear    = 0;

            float target_pressure_rate_of_change = 0;
            float pressure_rate_of_change = 0;
            float unload_phase_controller_normal_force_via_pwm_last_pwm = 0;
        };
        bool contact_detection_flag_noisy_last = false;
        controller_state_data controller_state;
        float last_encoder_pos = 0;
        float encoder_delta_for_movent = 0.05;

        void update_meassures(const Sensors& sensors, const controller_run_data& ctrl_run_data);

        MotorGraspPhaseControllerData(MotorIdentity m_id,
                                      const config_t& cfg,
                                      FingerSensor* ptr);

        MotorGraspPhaseControllerData(MotorIdentity m_id,
                                      const config_t& cfg,
                                      FingerSensor* ptr1,
                                      FingerSensor* ptr2,
                                      FingerSensor* ptr3);

        std::int16_t pos_control_pwm(const controller_run_data& ctrl_run_data, float target_pos);

        template<std::size_t N>
        struct AvgFilter
        {
            std::array<float, N> last_values;
            std::size_t last_i = 0;
            float avg_previous = 0;
            float avg = 0;

            AvgFilter()
            {
                reset();
            }

            void reset()
            {
                for (auto& v : last_values)
                {
                    v = 0;
                }
                last_i = 0;
                avg = 0;
                avg_previous = 0;
            }
            float update(float v)
            {
                if (!std::isfinite(v))
                {
                    return avg;
                }
                const auto i = (last_i++ % last_values.size());
                v /= last_values.size();
                avg_previous = avg;
                avg += v - last_values.at(i);
                last_values.at(i) = v;
                return avg;
            }
        };
        AvgFilter<10> shear_avg_filter;

        std::array<float, 5> last_pressure_values;
        std::size_t last_pressure_values_i = 0;
        float unload_phase_controller_normal_force_via_pwm(
            const controller_run_data& ctrl_run_data);
    public:
        float force_ctrl_pressure_factor = 1;

        std::int16_t monotonic_shear_pressure_control_pwm_force(const controller_run_data& ctrl_run_data,
                const float target_pressure,
                const float target_shear, bool enable_shear);

        void reset();

        float minimum_distance(const Sensors& sensors) const;
    };
}
//define
namespace armarx::KITSensorizedSoftFingerHand::V1
{
    inline void
    MotorGraspPhaseControllerData::update_meassures(const Sensors& sensors, const controller_run_data& ctrl_run_data)
    {
        ARMARX_TRACE;
        //pressure
        {
            controller_state.maximum_pressure = 0;
            for (const auto finger : fingers)
            {
                if (finger != nullptr)
                {
                    for (int i = 2; i < 4; i++)
                    {
                        controller_state.maximum_pressure = std::max(finger->normal_force_sensors.at(i).current_continuous_mean_free_scaled,
                                                            controller_state.maximum_pressure);
                    }
                }
            }
        }
        //contact_detection_flag
        {
            controller_state.contact_detection_flag = false;
            bool contact_detection_flag_noisy_curr = false;
            for (const auto f : fingers)
            {
                if (f != nullptr && f->test_for_object_touched())
                {
                    contact_detection_flag_noisy_curr = true;
                    break;
                }
            }
            controller_state.contact_detection_flag = (contact_detection_flag_noisy_last && contact_detection_flag_noisy_curr);
            contact_detection_flag_noisy_last = contact_detection_flag_noisy_curr;
        }
        //closing detection
        {
            const auto current_encoder_pos = sensors.motor(motor_identity)->position;
            controller_state.finger_did_close = (current_encoder_pos - last_encoder_pos) > encoder_delta_for_movent;
            last_encoder_pos = current_encoder_pos;
        }
        //pressure rate of change
        if (ctrl_run_data.fpga_updated)
        {
            const float pval = static_cast<float>(controller_state.maximum_pressure);
            float avg_old = 0;
            for (const auto v : last_pressure_values)
            {
                avg_old += v / last_pressure_values.size();
            }
            const auto i = (last_pressure_values_i++ % last_pressure_values.size());
            const float avg_new = avg_old - last_pressure_values.at(i) / last_pressure_values.size() + pval / last_pressure_values.size();
            last_pressure_values.at(i) = pval;
            controller_state.pressure_rate_of_change = (avg_new - avg_old) / 0.007f; //ctrl_run_data.dt_sec_fpga;
        }
    }

    inline
    MotorGraspPhaseControllerData::MotorGraspPhaseControllerData(MotorIdentity m_id,
            const config_t& cfg,
            FingerSensor* ptr) :
        MotorGraspPhaseControllerData(m_id, cfg, ptr, nullptr, nullptr)
    {}

    inline
    MotorGraspPhaseControllerData::MotorGraspPhaseControllerData(MotorIdentity m_id,
            const config_t& cfg,
            FingerSensor* ptr1, FingerSensor* ptr2, FingerSensor* ptr3) :
        motor_identity{m_id},
        fingers{ptr1, ptr2, ptr3},
        pos_pwm_controller(
            motor_cfg(cfg, m_id).pid_pos.p,
            motor_cfg(cfg, m_id).pid_pos.i,
            motor_cfg(cfg, m_id).pid_pos.d),
        max_pwm{motor_cfg(cfg, m_id).max_pwm},
        pid_shear(
            motor_cfg(cfg, m_id).pid_shear.p,
            motor_cfg(cfg, m_id).pid_shear.i,
            motor_cfg(cfg, m_id).pid_shear.d),
        pid_unload(
            motor_cfg(cfg, m_id).pid_unload.p,
            motor_cfg(cfg, m_id).pid_unload.i,
            motor_cfg(cfg, m_id).pid_unload.d),
        pid_force(
            motor_cfg(cfg, m_id).pid_force.p,
            motor_cfg(cfg, m_id).pid_force.i,
            motor_cfg(cfg, m_id).pid_force.d),
        force_ctrl_pressure_factor{motor_cfg(cfg, m_id).force_ctrl_pressure_factor}
    {
        ARMARX_TRACE;
        pid_unload.threadSafe = false;
        pid_shear.threadSafe = false;
        pid_force.threadSafe = false;
    }

    inline std::int16_t
    MotorGraspPhaseControllerData::pos_control_pwm(
        const controller_run_data& ctrl_run_data,
        float target_pos
    )
    {
        ARMARX_TRACE;
        return pos_pwm_controller.calculate(
                   ctrl_run_data.sensors.motor(motor_identity)->position,
                   target_pos,
                   ctrl_run_data.dt_sec,
                   max_pwm
               );
    }

    inline float
    MotorGraspPhaseControllerData::unload_phase_controller_normal_force_via_pwm(
        const controller_run_data& ctrl_run_data)
    {
        if (ctrl_run_data.fpga_updated)
        {
            pid_unload.update(
                ctrl_run_data.dt_sec_fpga,
                controller_state.pressure_rate_of_change,
                controller_state.target_pressure_rate_of_change);

            controller_state.unload_phase_controller_normal_force_via_pwm_last_pwm = pid_unload.getControlValue();
        }
        return std::min(0.f, controller_state.unload_phase_controller_normal_force_via_pwm_last_pwm)/* - 40*/;
    }

    inline std::int16_t
    MotorGraspPhaseControllerData::monotonic_shear_pressure_control_pwm_force(
        const controller_run_data& ctrl_run_data,
        const float targ_pressure,
        const float targ_shear,
        bool enable_shear
    )
    {
        ARMARX_TRACE;
        if (ctrl_run_data.fpga_updated)
        {
            controller_state.monotonic_shear_pressure_control_last_pwm_pressure = 0;
            controller_state.monotonic_shear_pressure_control_last_pwm_shear    = 0;
            controller_state.monotonic_shear_pressure_control_last_delta_pressure = 0;
            controller_state.monotonic_shear_pressure_control_last_delta_shear    = 0;
            //normal force
            {
                controller_state.target_pressure = targ_pressure;

                static const auto pos_hold_pwm = [](float pos)
                {
                    piecewise_linear_function<float, float> pwlf
                    {
                        std::map<float, float>{
                            {0.00f, 140.f},
                            {0.71f, 175.f},
                            {0.94f, 236.f}
                        }
                    };
                    return pwlf.calc_lin_search(pos);
                };

                //shear
                if (std::isfinite(controller_state.shear_force_used_ratio) && enable_shear)
                {
                    controller_state.target_shear = targ_shear;
                    pid_shear.update(
                        ctrl_run_data.dt_sec_fpga,
                        controller_state.shear_force_used_ratio,
                        controller_state.target_shear);
                    controller_state.monotonic_shear_pressure_control_last_delta_shear = pid_shear.getControlValue();
                    controller_state.target_pressure += std::max(0.f, controller_state.monotonic_shear_pressure_control_last_delta_shear);
                }

                controller_state.target_pressure *= force_ctrl_pressure_factor;

                pid_force.update(
                    ctrl_run_data.dt_sec_fpga,
                    static_cast<float>(controller_state.maximum_pressure),
                    controller_state.target_pressure);

                controller_state.monotonic_shear_pressure_control_last_delta_pressure =
                    boost::algorithm::clamp(pid_force.getControlValue(), -pid_force_limit, pid_force_limit);
                controller_state.monotonic_shear_pressure_control_last_pwm_pressure =
                    controller_state.monotonic_shear_pressure_control_last_delta_pressure +
                    pid_force_forward_p * controller_state.target_pressure +
                    pos_hold_pwm(ctrl_run_data.sensors.motor(motor_identity)->position);
            }

            controller_state.monotonic_shear_pressure_control_last_pwm =
                controller_state.monotonic_shear_pressure_control_last_pwm_pressure;
        }
        return controller_state.monotonic_shear_pressure_control_last_pwm;
    }
    inline void
    MotorGraspPhaseControllerData::reset()
    {
        ARMARX_TRACE;
        controller_state.grasp_phase = GraspPhase::FREELY_MOVING;
        pos_pwm_controller.reset();
        pid_shear.reset();
        pid_unload.reset();
        pid_force.reset();

        controller_state.monotonic_shear_pressure_control_last_pwm          = 0;
        controller_state.monotonic_shear_pressure_control_last_pwm_pressure = 0;
        controller_state.monotonic_shear_pressure_control_last_pwm_shear    = 0;
        for (auto& v : last_pressure_values)
        {
            v = 0;
        }
    }

    inline float
    MotorGraspPhaseControllerData::minimum_distance(const Sensors& sensors) const
    {
        switch (motor_identity)
        {
            case MotorIdentity::other:
                return std::min({sensors.finger.little()->proximity,
                                 sensors.finger.ring()->proximity,
                                 sensors.finger.middle()->proximity});
            case MotorIdentity::index:
                return sensors.finger.index()->proximity;
            case MotorIdentity::thumb:
                return sensors.finger.thumb()->proximity;
            default:
                ARMARX_CHECK_EXPRESSION(false) << "Unreachable code reached!";
        }
    }
}
