
#pragma once

#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointController.h>

#include <armar6/rt/units/Armar6Unit/Devices/KITSensorizedSoftFingerHand/V1/utility/Identities.h>
#include <armar6/rt/interface/units/KITSensorizedSoftFingerHand/V1/NJointMinimalGraspingForceV1Controller.h>

namespace armarx::KITSensorizedSoftFingerHand::V1
{
    inline const auto& motor_cfg(const NJointMinimalGraspingForceV1ControllerConfig& cfg,
                                 identity::MotorIdentity id)
    {
        return identity::Select(
                   id, cfg.motor_other, cfg.motor_index, cfg.motor_thumb);
    }

    inline void make_default_cfg(NJointMinimalGraspingForceV1ControllerConfig& c)
    {
        using T = NJointMinimalGraspingForceV1ControllerConfigElements::MotorCfg;
        c.place_fft_energy_threshold = 40000;
        c.place_finger_trigger_count = 3;
        c.hand_name = "RightHand";
        const auto set = [&](auto m, float o, float i, float t)
        {
            (c.motor_other.*m) = o;
            (c.motor_index.*m) = i;
            (c.motor_thumb.*m) = t;
        };
        const auto set_pid = [&](auto m,
                                 float op, float ip, float tp,
                                 float oi, float ii, float ti,
                                 float od, float id, float td)
        {
            (c.motor_other.*m).p = op;
            (c.motor_index.*m).p = ip;
            (c.motor_thumb.*m).p = tp;

            (c.motor_other.*m).i = oi;
            (c.motor_index.*m).i = ii;
            (c.motor_thumb.*m).i = ti;

            (c.motor_other.*m).d = od;
            (c.motor_index.*m).d = id;
            (c.motor_thumb.*m).d = td;
        };
        set_pid(&T::pid_pos,
                21.0f, 7.0f, 7.0f,
                0.0f, 0.0f, 0.0f,
                0.0f, 0.0f, 0.0f);

        set_pid(&T::pid_pressure,
                0.004f, 0.004f, 0.004f,
                0.0f, 0.0f, 0.0f,
                0, 0, 0);

        set_pid(&T::pid_shear,
                5000, 5000, 5000,
                0, 0, 0,
                0, 0, 0);

        set_pid(&T::pid_unload,
                -15, -15, -15,
                -2, -2, -2,
                0, 0, 0);

        set(&T::pid_force_forward_p,   0.4,   0.4,  0.4);
        set(&T::pid_force_limit,   75,   75,  75);
        set_pid(&T::pid_force,
                1, 1, 1,
                0, 0, 0,
                0.2, 0.2, 0.2);


        set(&T::max_pwm,                          1'000,   1'000,  1'000);
        set(&T::pwm_open,                          -500,    -500,   -500);
        set(&T::pwm_freely_moving,                1'000,   1'000,  1'000);
        set(&T::min_pwm_close_to_contact,           300,     220,    220);
        set(&T::target_pressure,                    120,     120,    120);
        set(&T::target_shear,                       0.4,     0.4,    0.4);
        set(&T::encoder_delta_for_movent,          0.05,    0.05,   0.05);
        set(&T::force_ctrl_pressure_factor,                 1.0f / 3.f,      1.0f / 3.f,     1);
    }
}
