#pragma once

#include <array>

#include "MotorGraspPhaseControllerData.h"

namespace armarx::KITSensorizedSoftFingerHand::V1
{
    struct GraspPhaseControllerData
    {
        using config_t = NJointMinimalGraspingForceV1ControllerConfig;
        using MotorIdentity = Sensors::MotorIdentity;

        template<class T>
        using FingerArray = identity::FingerArray<T>;
        template<class T>
        using MotorArray = identity::MotorArray<T>;

        bool run_controller = false;
        MotorArray<MotorGraspPhaseControllerData> motor_controller_data;
        uint32_t fpga_iteration = 0;
        float time_last_fpga_update = 0;
        float last_fpga_dt = 0;
        bool fpga_update = false;

        GraspPhaseControllerData(FingerArray<FingerSensor>& fingers,
                                 const config_t& cfg):
            motor_controller_data
        {
            MotorGraspPhaseControllerData(MotorIdentity::other, cfg, &fingers.at(0), &fingers.at(1), &fingers.at(2)),
            MotorGraspPhaseControllerData(MotorIdentity::index, cfg, &fingers.at(3)),
            MotorGraspPhaseControllerData(MotorIdentity::thumb, cfg, &fingers.at(4))
        }
        {
            ARMARX_TRACE;
        }

        void reset()
        {
            ARMARX_TRACE;
            for (auto& data : motor_controller_data)
            {
                data.reset();
            }
        }

    };
}
