#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <ArmarXCore/libraries/DebugObserverHelper/DebugObserverHelper.h>
#include <armar6/rt/units/Armar6Unit/Devices/KITSensorizedSoftFingerHand/V1/utility/SensorFit.h>

#include "MinimalGraspingForceV1.h"

//register
namespace armarx::KITSensorizedSoftFingerHand::V1
{
    NJointControllerRegistration<MinimalGraspingForceV1_NJointController>
    registrationControllerMinimalGraspingForceV1_NJointController("KITSensorizedSoftFingerHandV1_GraspPhaseControllerV1_NJointController");
}
//create
namespace armarx::KITSensorizedSoftFingerHand::V1
{
    WidgetDescription::WidgetPtr
    MinimalGraspingForceV1_NJointController::GenerateConfigDescription(
        const VirtualRobot::RobotPtr&,
        const std::map<std::string, ConstControlDevicePtr>&,
        const std::map<std::string, ConstSensorDevicePtr>&)
    {
        using T = KITSensorizedSoftFingerHand::V1::NJointMinimalGraspingForceV1ControllerConfigElements::MotorCfg;
        NJointMinimalGraspingForceV1ControllerConfig c;
        make_default_cfg(c);
        ARMARX_TRACE;
        using namespace armarx::WidgetDescription;

        VBoxLayoutPtr set_params = new VBoxLayout;
        //hand name
        {
            HBoxLayoutPtr line = new HBoxLayout;
            set_params->children.emplace_back(line);

            line->children.emplace_back(new Label(false, "hand name"));
            LineEditPtr e = new LineEdit;
            e->name = "hand";
            e->defaultValue = c.hand_name;
            line->children.emplace_back(e);
        }
        //place_fft_energy_threshold
        {
            HBoxLayoutPtr line = new HBoxLayout;
            set_params->children.emplace_back(line);
            line->children.emplace_back(new Label(false, "energy per finger for place"));
            FloatSpinBoxPtr e = new FloatSpinBox;
            e->name = "place_fft_energy_threshold";
            e->min = -2'000'000;
            e->max   = 2'000'000;
            e->defaultValue = c.place_fft_energy_threshold;
            e->steps = 4'001;
            line->children.emplace_back(e);
        }
        const auto add_table_line_f = [&](const std::string & suff, float o, float i, float t)
        {
            HBoxLayoutPtr line = new HBoxLayout;
            set_params->children.emplace_back(line);

            line->children.emplace_back(new Label(false, suff));
            const auto fbox = [&line](const std::string & name, float val)
            {
                FloatSpinBoxPtr e = new FloatSpinBox;
                e->name = name;
                e->min = -5'000'000;
                e->defaultValue = val;
                e->max      = 5'000'000;
                e->steps    = e->max - e->min + 1;
                e->decimals = 8;
                line->children.emplace_back(e);
            };
            fbox("motor_other." + suff, o);
            fbox("motor_index." + suff, i);
            fbox("motor_thumb." + suff, t);
        };
        const auto add_table_line = [&](const std::string & suff, auto m)
        {
            add_table_line_f(suff, c.motor_other.*m, c.motor_index.*m, c.motor_thumb.*m);
        };
        const auto add_pid_table_line = [&](const std::string & pre, auto m)
        {
            add_table_line_f(pre + ".p", (c.motor_other.*m).p, (c.motor_index.*m).p, (c.motor_thumb.*m).p);
            add_table_line_f(pre + ".i", (c.motor_other.*m).i, (c.motor_index.*m).i, (c.motor_thumb.*m).i);
            add_table_line_f(pre + ".d", (c.motor_other.*m).d, (c.motor_index.*m).d, (c.motor_thumb.*m).d);
        };

        add_pid_table_line("pid_pos", &T::pid_pos);
        set_params->children.emplace_back(new HLine);

        add_pid_table_line("pid_pressure", &T::pid_pressure);
        set_params->children.emplace_back(new HLine);

        add_pid_table_line("pid_shear", &T::pid_shear);
        set_params->children.emplace_back(new HLine);

        add_pid_table_line("pid_unload", &T::pid_unload);
        set_params->children.emplace_back(new HLine);

        add_pid_table_line("pid_force", &T::pid_force);
        add_table_line("pid_force_limit",                           &T::pid_force_limit);
        add_table_line("pid_force_forward_p",                           &T::pid_force_forward_p);
        set_params->children.emplace_back(new HLine);

        add_table_line("max_pwm",                           &T::max_pwm);
        add_table_line("pwm_open",                          &T::pwm_open);
        add_table_line("pwm_freely_moving",                 &T::pwm_freely_moving);
        add_table_line("min_pwm_close_to_contact",          &T::min_pwm_close_to_contact);
        add_table_line("target_pressure",                   &T::target_pressure);
        add_table_line("target_shear",                      &T::target_shear);
        add_table_line("force_ctrl_pressure_factor",              &T::force_ctrl_pressure_factor);
        return set_params;
    }
    typename MinimalGraspingForceV1_NJointController::ConfigPtrT
    MinimalGraspingForceV1_NJointController::GenerateConfigFromVariants(const StringVariantBaseMap& valueMap)
    {
        ARMARX_TRACE;
        KITSensorizedSoftFingerHand::V1::NJointMinimalGraspingForceV1ControllerConfigPtr cfg = new KITSensorizedSoftFingerHand::V1::NJointMinimalGraspingForceV1ControllerConfig;

        cfg->hand_name = valueMap.at("hand")->getString();
        cfg->place_fft_energy_threshold    = valueMap.at("place_fft_energy_threshold")->getFloat();

        const auto read_motor_cfg = [&](const std::string & name, auto & targ)
        {
            targ.pid_pos.p      =  valueMap.at("motor_" + name + ".pid_pos.p")->getFloat();
            targ.pid_pos.i      =  valueMap.at("motor_" + name + ".pid_pos.i")->getFloat();
            targ.pid_pos.d      =  valueMap.at("motor_" + name + ".pid_pos.d")->getFloat();

            targ.pid_pressure.p =  valueMap.at("motor_" + name + ".pid_pressure.p")->getFloat();
            targ.pid_pressure.i =  valueMap.at("motor_" + name + ".pid_pressure.i")->getFloat();
            targ.pid_pressure.d =  valueMap.at("motor_" + name + ".pid_pressure.d")->getFloat();

            targ.pid_shear.p    =  valueMap.at("motor_" + name + ".pid_shear.p")->getFloat();
            targ.pid_shear.i    =  valueMap.at("motor_" + name + ".pid_shear.i")->getFloat();
            targ.pid_shear.d    =  valueMap.at("motor_" + name + ".pid_shear.d")->getFloat();

            targ.pid_unload.p    =  valueMap.at("motor_" + name + ".pid_unload.p")->getFloat();
            targ.pid_unload.i    =  valueMap.at("motor_" + name + ".pid_unload.i")->getFloat();
            targ.pid_unload.d    =  valueMap.at("motor_" + name + ".pid_unload.d")->getFloat();

            targ.pid_force.p    =  valueMap.at("motor_" + name + ".pid_force.p")->getFloat();
            targ.pid_force.i    =  valueMap.at("motor_" + name + ".pid_force.i")->getFloat();
            targ.pid_force.d    =  valueMap.at("motor_" + name + ".pid_force.d")->getFloat();
            targ.pid_force_limit    =  valueMap.at("motor_" + name + ".pid_force_limit")->getFloat();
            targ.pid_force_forward_p    =  valueMap.at("motor_" + name + ".pid_force_forward_p")->getFloat();

            targ.max_pwm    =  valueMap.at("motor_" + name + ".max_pwm")->getFloat();
            targ.pwm_open    =  valueMap.at("motor_" + name + ".pwm_open")->getFloat();
            targ.pwm_freely_moving    =  valueMap.at("motor_" + name + ".pwm_freely_moving")->getFloat();
            targ.min_pwm_close_to_contact    =  valueMap.at("motor_" + name + ".min_pwm_close_to_contact")->getFloat();
            targ.target_pressure    =  valueMap.at("motor_" + name + ".target_pressure")->getFloat();
            targ.target_shear    =  valueMap.at("motor_" + name + ".target_shear")->getFloat();

            targ.force_ctrl_pressure_factor    = valueMap.at("motor_" + name + ".force_ctrl_pressure_factor")->getFloat();
        };

        read_motor_cfg("index", cfg->motor_index);
        read_motor_cfg("other", cfg->motor_other);
        read_motor_cfg("thumb", cfg->motor_thumb);

        return cfg;
    }

    MinimalGraspingForceV1_NJointController::MinimalGraspingForceV1_NJointController(RobotUnitPtr robotUnit,
            const ConfigPtrT& config,
            const VirtualRobot::RobotPtr&) :
        _grasp_phase_controller_data{_finger_sensors, [ & ]{ ARMARX_CHECK_NOT_NULL(config); return * config; }() }
    {
        ARMARX_TRACE;
        ARMARX_CHECK_NOT_NULL(config);

        ARMARX_INFO << "setup params in ice to rt buffer";
        {
            std::lock_guard g{_ice_to_rt_buffer_write_mutex};
            auto& buf = _ice_to_rt_buffer.getWriteBuffer();
            buf.cfg = *config;
            _ice_to_rt_buffer.commitWrite();
        }
        ARMARX_INFO << "get ctrl";
        {
            auto get = [this](auto & targ, auto name)
            {
                ARMARX_INFO << "    get ctrl for " << name;
                ControlTargetBase* ct = useControlTarget(name, ControlModes::PWM1DoF);
                ARMARX_CHECK_EXPRESSION(ct->isA<ControlTarget1DoFActuatorPWM>()) << VAROUT(name);
                targ = ct->asA<ControlTarget1DoFActuatorPWM>();
            };

            get(_target_pwm_thumb, config->hand_name + ".motor.thumb");
            get(_target_pwm_index, config->hand_name + ".motor.index");
            get(_target_pwm_other, config->hand_name + ".motor.other");
        }
        ARMARX_INFO << "get sens";
        _sensors = KITSensorizedSoftFingerHand::V1::Sensors(config->hand_name, *this);
    }
}
//name
namespace armarx::KITSensorizedSoftFingerHand::V1
{
    std::string MinimalGraspingForceV1_NJointController::getClassName(const Ice::Current&) const
    {
        return "MinimalGraspingForceV1_NJointController";
    }
}
//remote function calls
namespace armarx::KITSensorizedSoftFingerHand::V1
{
    WidgetDescription::StringWidgetDictionary MinimalGraspingForceV1_NJointController::getFunctionDescriptions(const Ice::Current&) const
    {
        using namespace armarx::WidgetDescription;
        const auto make_ctrl_seetings = [](auto default_val, auto min, auto max)
        {
            HBoxLayoutPtr lay = new HBoxLayout;
            CheckBoxPtr active = new CheckBox;
            active->name = "active";
            active->label = "active";
            active->defaultValue = false;
            lay->children.emplace_back(active);
            FloatSpinBoxPtr o = new FloatSpinBox;
            FloatSpinBoxPtr i = new FloatSpinBox;
            FloatSpinBoxPtr f = new FloatSpinBox;

            lay->children.emplace_back(o);
            lay->children.emplace_back(i);
            lay->children.emplace_back(f);

            o->name = "o";
            i->name = "i";
            f->name = "t";

            o->min = min;
            i->min = min;
            f->min = min;

            o->max = max;
            i->max = max;
            f->max = max;

            o->steps = (max - min) * 100 + 1;
            i->steps = (max - min) * 100 + 1;
            f->steps = (max - min) * 100 + 1;

            o->defaultValue = default_val;
            i->defaultValue = default_val;
            f->defaultValue = default_val;

            return lay;
        };

        const auto make_pid_seetings = []
        {
            VBoxLayoutPtr settings = new VBoxLayout;
            const auto add_line = [&](const std::string & k, float val)
            {
                HBoxLayoutPtr line = new HBoxLayout;
                settings->children.emplace_back(line);
                line->children.emplace_back(new Label(false, k));

                FloatSpinBoxPtr o = new FloatSpinBox;
                FloatSpinBoxPtr i = new FloatSpinBox;
                FloatSpinBoxPtr f = new FloatSpinBox;

                line->children.emplace_back(o);
                line->children.emplace_back(i);
                line->children.emplace_back(f);

                o->name = "o" + k;
                i->name = "i" + k;
                f->name = "t" + k;

                o->min = 0;
                i->min = 0;
                f->min = 0;

                o->max = 1000;
                i->max = 1000;
                f->max = 1000;

                o->steps = 1001;
                i->steps = 1001;
                f->steps = 1001;

                o->defaultValue = val;
                i->defaultValue = val;
                f->defaultValue = val;

                o->decimals = 8;
                i->decimals = 8;
                f->decimals = 8;
            };
            add_line("p", 5);
            add_line("i", 0);
            add_line("d", 0);
            return settings;
        };
        return
        {
            {"pos_control", make_ctrl_seetings(0, 0, 1)},
            {"pwm_control", make_ctrl_seetings(0, -1000, 1000)},
            {"pos_control_pid", make_pid_seetings()},
            {"pressure_control_pid", make_pid_seetings()},
            {"shear_control_pid", make_pid_seetings()},
            {"unload_control_pid", make_pid_seetings()},
            {"Open", nullptr},
            {"Close", nullptr},
            {"Recalibrate", nullptr},
            {"StartPlacing", nullptr}
        };
    }

    void MinimalGraspingForceV1_NJointController::callDescribedFunction(
        const std::string& name, const StringVariantBaseMap& valueMap, const Ice::Current&)
    {
        const auto read_pid_settings = [&](auto & o, auto & i, auto & t)
        {
            o.p = valueMap.at("op")->getFloat();
            i.p = valueMap.at("ip")->getFloat();
            t.p = valueMap.at("tp")->getFloat();
            o.i = valueMap.at("oi")->getFloat();
            i.i = valueMap.at("ii")->getFloat();
            t.i = valueMap.at("ti")->getFloat();
            o.d = valueMap.at("od")->getFloat();
            i.d = valueMap.at("id")->getFloat();
            t.d = valueMap.at("td")->getFloat();
        };
        ARMARX_TRACE;
        if (name == "pos_control")
        {
            std::lock_guard g{_ice_to_rt_buffer_write_mutex};
            auto& buf = _ice_to_rt_buffer.getWriteBuffer();
            buf.pos_control.active = valueMap.at("active")->getBool();
            buf.pos_control.o = valueMap.at("o")->getFloat();
            buf.pos_control.i = valueMap.at("i")->getFloat();
            buf.pos_control.t = valueMap.at("t")->getFloat();
            _ice_to_rt_buffer.commitWrite();
        }
        else if (name == "pwm_control")
        {
            overridePWM(valueMap.at("active")->getBool(),
                        valueMap.at("o")->getFloat(),
                        valueMap.at("i")->getFloat(),
                        valueMap.at("t")->getFloat());

        }
        else if (name == "pos_control_pid")
        {
            std::lock_guard g{_ice_to_rt_buffer_write_mutex};
            auto& buf = _ice_to_rt_buffer.getWriteBuffer();
            read_pid_settings(buf.cfg.motor_other.pid_pos,
                              buf.cfg.motor_index.pid_pos,
                              buf.cfg.motor_thumb.pid_pos);
            _ice_to_rt_buffer.commitWrite();
        }
        else if (name == "pressure_control_pid")
        {
            std::lock_guard g{_ice_to_rt_buffer_write_mutex};
            auto& buf = _ice_to_rt_buffer.getWriteBuffer();
            read_pid_settings(buf.cfg.motor_other.pid_pressure,
                              buf.cfg.motor_index.pid_pressure,
                              buf.cfg.motor_thumb.pid_pressure);
            _ice_to_rt_buffer.commitWrite();
        }
        else if (name == "shear_control_pid")
        {
            std::lock_guard g{_ice_to_rt_buffer_write_mutex};
            auto& buf = _ice_to_rt_buffer.getWriteBuffer();
            read_pid_settings(buf.cfg.motor_other.pid_shear,
                              buf.cfg.motor_index.pid_shear,
                              buf.cfg.motor_thumb.pid_shear);
            _ice_to_rt_buffer.commitWrite();
        }
        else if (name == "unload_control_pid")
        {
            std::lock_guard g{_ice_to_rt_buffer_write_mutex};
            auto& buf = _ice_to_rt_buffer.getWriteBuffer();
            read_pid_settings(buf.cfg.motor_other.pid_unload,
                              buf.cfg.motor_index.pid_unload,
                              buf.cfg.motor_thumb.pid_unload);
            _ice_to_rt_buffer.commitWrite();
        }
        else if (name == "Open")
        {
            openMGF();
        }
        else if (name == "Close")
        {
            closeMGF();
        }
        else if (name == "Stop")
        {
            _clicked_stop = true;
        }
        else if (name == "Recalibrate")
        {
            recalibrate();
        }
        else if (name == "StartPlacing")
        {
            startPlacing();
        }
        else
        {
            ARMARX_WARNING << "Unknown function name called: " << name;
        }
    }

    void
    MinimalGraspingForceV1_NJointController::closeMGF(const Ice::Current&)
    {
        _clicked_close = true;
    }
    void
    MinimalGraspingForceV1_NJointController::recalibrate(const Ice::Current&)
    {
        _clicked_recalibrate = true;
    }
    NJointMinimalGraspingForceV1ControllerDebugData
    MinimalGraspingForceV1_NJointController::getDebugData(const Ice::Current&)
    {
        ARMARX_TRACE;
        NJointMinimalGraspingForceV1ControllerDebugData r;
        std::lock_guard g{_rt_to_ice_read_mutex};
        const auto& buf = _rt_to_ice.getUpToDateReadBuffer();
        r.grasp_phase = std::min({buf.motor_controller_data.other().grasp_phase,
                                  buf.motor_controller_data.index().grasp_phase,
                                  buf.motor_controller_data.thumb().grasp_phase});

        r.updating_sensors                     = buf.updating_sensors                     ;
        r.resetting_controller                 = buf.resetting_controller                 ;
        r.run_controller                       = buf.run_controller                       ;

        r.last_fpga_dt                         = buf.last_fpga_dt                         ;

        r.active_shear_avg_len_xy              = buf.active_shear_avg_len_xy              ;
        r.active_shear_avg_len_z               = buf.active_shear_avg_len_z               ;
        r.active_shear_avg_ratio               = buf.active_shear_avg_ratio               ;
        r.active_shear_num                     = buf.active_shear_num                     ;
        r.used_shear_avg_ratio                 = buf.used_shear_avg_ratio                 ;

        r.open_signal_finger_count             = buf.open_signal_finger_count             ;
        r.open_signal                          = buf.open_signal                          ;


        const auto motor = [](auto & trg, const auto & src)
        {
            trg.grasp_phase = src.grasp_phase;
            trg.contact_detection_flag                                = src.contact_detection_flag                                ;

            trg.shear_force_used_ratio                                = src.shear_force_used_ratio                                ;
            trg.shear_force_used_ratio_num                            = src.shear_force_used_ratio_num                            ;
            trg.maximum_pressure                                      = src.maximum_pressure                                      ;
            trg.pressure_on_start_unload                              = src.pressure_on_start_unload                              ;

            trg.target_shear                                          = src.target_shear                                          ;
            trg.target_pressure                                       = src.target_pressure                                       ;

            trg.contact_detection_flag                                = src.contact_detection_flag                                ;
            trg.finger_did_close                                      = src.finger_did_close                                      ;

            trg.monotonic_shear_pressure_control_last_pwm             = src.monotonic_shear_pressure_control_last_pwm             ;
            trg.monotonic_shear_pressure_control_last_pwm_pressure    = src.monotonic_shear_pressure_control_last_pwm_pressure    ;
            trg.monotonic_shear_pressure_control_last_pwm_shear       = src.monotonic_shear_pressure_control_last_pwm_shear       ;
            trg.monotonic_shear_pressure_control_last_delta_pressure  = src.monotonic_shear_pressure_control_last_delta_pressure  ;
            trg.monotonic_shear_pressure_control_last_delta_shear     = src.monotonic_shear_pressure_control_last_delta_shear     ;

            trg.target_pressure_rate_of_change                        = src.target_pressure_rate_of_change                        ;
            trg.pressure_rate_of_change                               = src.pressure_rate_of_change                               ;
            trg.unload_phase_controller_normal_force_via_pwm_last_pwm = src.unload_phase_controller_normal_force_via_pwm_last_pwm ;
        };
        motor(r.motor_other, buf.motor_controller_data.other());
        motor(r.motor_index, buf.motor_controller_data.index());
        motor(r.motor_thumb, buf.motor_controller_data.thumb());
        const auto finger = [](auto & trg, const auto & sens)
        {
            const auto f_shear = [](auto & out, const auto & in)
            {
                out.len_xy = in.len_xy;
                out.len_z  = in.len_z ;

                out.x_current                             = in.sensors.at(0).current                            ;
                out.x_continuous_mean                     = in.sensors.at(0).continuous_mean                    ;
                out.x_current_mean_free_scaled            = in.sensors.at(0).current_mean_free_scaled           ;
                out.x_current_continuous_mean_free_scaled = in.sensors.at(0).current_continuous_mean_free_scaled;

                out.y_current                             = in.sensors.at(1).current                            ;
                out.y_continuous_mean                     = in.sensors.at(1).continuous_mean                    ;
                out.y_current_mean_free_scaled            = in.sensors.at(1).current_mean_free_scaled           ;
                out.y_current_continuous_mean_free_scaled = in.sensors.at(1).current_continuous_mean_free_scaled;

                out.z_current                             = in.sensors.at(2).current                            ;
                out.z_continuous_mean                     = in.sensors.at(2).continuous_mean                    ;
                out.z_current_mean_free_scaled            = in.sensors.at(2).current_mean_free_scaled           ;
                out.z_current_continuous_mean_free_scaled = in.sensors.at(2).current_continuous_mean_free_scaled;
            };
            f_shear(trg.shear_force_dist_joint, sens.shear_force_dist_joint);
            f_shear(trg.shear_force_dist_tip,   sens.shear_force_dist_tip);

            trg.accelerometer.shear_force_criterium     = sens.accelerometer.shear_force_criterium     ;
            trg.accelerometer.fft_contact_detect_energy = sens.accelerometer.fft_contact_detect_energy ;

            const auto f_nf = [](auto & out, const auto & in)
            {
                out.current                             = in.current                            ;
                out.continuous_mean                     = in.continuous_mean                    ;
                out.current_mean_free_scaled            = in.current_mean_free_scaled           ;
                out.current_continuous_mean_free_scaled = in.current_continuous_mean_free_scaled;
            };
            f_nf(trg.nf_prox_l,     sens.normal_force_sensors.at(0));
            f_nf(trg.nf_prox_r,     sens.normal_force_sensors.at(1));
            f_nf(trg.nf_dist_joint, sens.normal_force_sensors.at(2));
            f_nf(trg.nf_dist_tip,   sens.normal_force_sensors.at(3));
        };
        finger(r.finger_little, buf.normalized_finger_sensors.little());
        finger(r.finger_ring,   buf.normalized_finger_sensors.ring());
        finger(r.finger_middle, buf.normalized_finger_sensors.middle());
        finger(r.finger_index,  buf.normalized_finger_sensors.index());
        finger(r.finger_thumb,  buf.normalized_finger_sensors.thumb());
        return r;
    }
    void
    MinimalGraspingForceV1_NJointController::openMGF(const Ice::Current&)
    {
        _clicked_open = true;
    }
    void
    MinimalGraspingForceV1_NJointController::startPlacing(const Ice::Current&)
    {
        _clicked_start_placing = true;
    }
    void
    MinimalGraspingForceV1_NJointController::overridePWM(
        bool active, Ice::Float o, Ice::Float i, Ice::Float t, const Ice::Current&)
    {
        std::lock_guard g{_ice_to_rt_buffer_write_mutex};
        auto& buf = _ice_to_rt_buffer.getWriteBuffer();
        buf.pwm_control.active = active;
        buf.pwm_control.o =      o     ;
        buf.pwm_control.i =      i     ;
        buf.pwm_control.t =      t     ;
        _ice_to_rt_buffer.commitWrite();
    }

    KITSensorizedSoftFingerHand::V1::GraspPhase::Phase
    MinimalGraspingForceV1_NJointController::currentGraspPhase(const Ice::Current&)
    {
        std::lock_guard g{_rt_to_ice_read_mutex};
        const auto& buf = _rt_to_ice.getUpToDateReadBuffer();
        return std::min({buf.motor_controller_data.other().grasp_phase,
                         buf.motor_controller_data.index().grasp_phase,
                         buf.motor_controller_data.thumb().grasp_phase});
    }
    void MinimalGraspingForceV1_NJointController::setConfig(const NJointMinimalGraspingForceV1ControllerConfigPtr& cfg, const Ice::Current&)
    {
        ARMARX_CHECK_NOT_NULL(cfg);
        std::lock_guard g{_ice_to_rt_buffer_write_mutex};
        _ice_to_rt_buffer.getWriteBuffer().cfg = *cfg;
        _ice_to_rt_buffer.commitWrite();
    }
}
//rt control
namespace armarx::KITSensorizedSoftFingerHand::V1
{
    void MinimalGraspingForceV1_NJointController::rtRun(
        const IceUtil::Time& sensorValuesTimestamp,
        const IceUtil::Time& timeSinceLastIteration)
    {
        static const auto now_sec = []()->float {return std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::steady_clock::now().time_since_epoch()).count() * 1e-9f;};

        ARMARX_TRACE;
        auto& obuf = _rt_to_ice.getWriteBuffer();
        const auto& ibuf = _ice_to_rt_buffer.getUpToDateReadBuffer();

        const double time = now_sec();

        const auto open = [&]
        {
            _grasp_phase_controller_data.run_controller = false;
            _grasp_phase_controller_data.reset();
            obuf.resetting_controller = true;
        };
        //Handle gui signals
        {
            if (_clicked_open.exchange(false))
            {
                open();
            }
            obuf.resetting_controller = false;

            if (_clicked_close.exchange(false))
            {
                _grasp_phase_controller_data.run_controller = true;
                _clicked_start_placing = false;
            }
            if (_clicked_recalibrate.exchange(false))
            {
                _finger_sensors.little().reset_calibration();
                _finger_sensors.ring().reset_calibration();
                _finger_sensors.middle().reset_calibration();
                _finger_sensors.index().reset_calibration();
                _finger_sensors.thumb().reset_calibration();
            }
        }
        //configure motors
        {
            const auto recfg_motor = [&](auto & m, auto & mcfg)
            {
                const auto set_pid = [&](auto & pid, const auto & params)
                {
                    pid.Kp = params.p;
                    pid.Ki = params.i;
                    pid.Kd = params.d;
                };
                set_pid(m.pos_pwm_controller.PID, mcfg.pid_pos);
                set_pid(m.pid_shear,              mcfg.pid_shear);
                set_pid(m.pid_unload,             mcfg.pid_unload);

                set_pid(m.pid_force,             mcfg.pid_force);
                m.pid_force_forward_p  = mcfg.pid_force_forward_p;
                m.pid_force_limit  = std::abs(mcfg.pid_force_limit);

                m.force_ctrl_pressure_factor = mcfg.force_ctrl_pressure_factor;
                m.controller_state.target_shear = mcfg.target_shear;
                m.controller_state.target_pressure = mcfg.target_pressure;
                m.max_pwm = mcfg.max_pwm;
                m.encoder_delta_for_movent = mcfg.encoder_delta_for_movent;
            };
            recfg_motor(_grasp_phase_controller_data.motor_controller_data.other(), ibuf.cfg.motor_other);
            recfg_motor(_grasp_phase_controller_data.motor_controller_data.index(), ibuf.cfg.motor_index);
            recfg_motor(_grasp_phase_controller_data.motor_controller_data.thumb(), ibuf.cfg.motor_thumb);
        }

        // calc stuff
        {
            obuf.run_controller = _grasp_phase_controller_data.run_controller;
            obuf.updating_sensors = _sensors.hand->frame_counter > _grasp_phase_controller_data.fpga_iteration;
            if (obuf.updating_sensors)
            {

                _finger_sensors.little().update(_sensors.finger.little(), !_grasp_phase_controller_data.run_controller);
                _finger_sensors.ring()  .update(_sensors.finger.ring(),   !_grasp_phase_controller_data.run_controller);
                _finger_sensors.middle().update(_sensors.finger.middle(), !_grasp_phase_controller_data.run_controller);
                _finger_sensors.index() .update(_sensors.finger.index(),  !_grasp_phase_controller_data.run_controller);
                _finger_sensors.thumb() .update(_sensors.finger.thumb(),  !_grasp_phase_controller_data.run_controller);

                _grasp_phase_controller_data.fpga_iteration = _sensors.hand->frame_counter;

                _grasp_phase_controller_data.last_fpga_dt = time - _grasp_phase_controller_data.time_last_fpga_update;
                obuf.last_fpga_dt = _grasp_phase_controller_data.last_fpga_dt;
                _grasp_phase_controller_data.fpga_update = true;
                _grasp_phase_controller_data.time_last_fpga_update = time;
            }
        }
        controller_run_data ctrl_run_data
        {
            _sensors,
            timeSinceLastIteration.toSecondsDouble(),
            _grasp_phase_controller_data.fpga_update,
            _grasp_phase_controller_data.last_fpga_dt
        };
        {
            // update criteria for controller state changes
            for (auto& m : _grasp_phase_controller_data.motor_controller_data)
            {
                m.update_meassures(_sensors, ctrl_run_data);
            }
            //shear over hand
            {
                obuf.active_shear_avg_len_xy = 0;
                obuf.active_shear_avg_len_z  = 0;
                obuf.active_shear_num        = 0;

                for (const auto& f : _finger_sensors)
                {
                    if (f.test_for_object_touched())
                    {
                    }
                    if (f.normal_force_sensors.at(2).test_current_over_one_percent_plus_peak_noise())//dist joint
                    {
                        obuf.active_shear_avg_len_xy += f.shear_force_dist_joint.len_xy;
                        obuf.active_shear_avg_len_z  += f.shear_force_dist_joint.len_z;
                        ++obuf.active_shear_num;

                        obuf.active_shear_avg_len_xy += f.shear_force_dist_tip.len_xy;
                        obuf.active_shear_avg_len_z  += f.shear_force_dist_tip.len_z;
                        ++obuf.active_shear_num;
                    }
                }

                if (obuf.active_shear_num)
                {
                    obuf.active_shear_avg_len_xy /= obuf.active_shear_num;
                    obuf.active_shear_avg_len_z  /= obuf.active_shear_num;
                    obuf.active_shear_avg_ratio   = obuf.active_shear_avg_len_z / obuf.active_shear_avg_len_xy;
                }
                else
                {
                    obuf.active_shear_avg_ratio = std::nanf("");
                }
            }

            for (auto& m : _grasp_phase_controller_data.motor_controller_data)
            {
                if (std::isfinite(obuf.active_shear_avg_ratio))
                {
                    obuf.used_shear_avg_ratio = m.shear_avg_filter.update(obuf.active_shear_avg_ratio);
                }
                else
                {
                    //signal to not use shear
                    obuf.used_shear_avg_ratio = obuf.active_shear_avg_ratio;
                }
                m.controller_state.shear_force_used_ratio = obuf.used_shear_avg_ratio;
                m.controller_state.shear_force_used_ratio_num = obuf.active_shear_num;
            }

            //open signal
            {
                obuf.open_signal_finger_count = 0;
                for (const auto& f : _finger_sensors)
                {
                    if (f.accelerometer.fft_contact_detect_energy >= ibuf.cfg.place_fft_energy_threshold)
                    {
                        ++obuf.open_signal_finger_count;
                    }
                }
                obuf.open_signal = (obuf.open_signal_finger_count >= static_cast<std::size_t>(ibuf.cfg.place_finger_trigger_count));
            }
        }


        //execute controller
        if (ibuf.pos_control.active)
        {
            const auto ctrl = [&](auto & targ, auto & ctrl, auto ptarg)
            {
                targ->pwm = ctrl.pos_control_pwm(ctrl_run_data, ptarg);
            };
            ctrl(_target_pwm_other, _grasp_phase_controller_data.motor_controller_data.other(), ibuf.pos_control.o);
            ctrl(_target_pwm_index, _grasp_phase_controller_data.motor_controller_data.index(), ibuf.pos_control.i);
            ctrl(_target_pwm_thumb, _grasp_phase_controller_data.motor_controller_data.thumb(), ibuf.pos_control.t);
        }
        else if (ibuf.pwm_control.active)
        {
            _target_pwm_other->pwm = ibuf.pwm_control.o;
            _target_pwm_index->pwm = ibuf.pwm_control.i;
            _target_pwm_thumb->pwm = ibuf.pwm_control.t;
        }
        else if (!_grasp_phase_controller_data.run_controller)
        {
            _target_pwm_thumb->pwm = motor_cfg(ibuf.cfg, MotorIdentity::thumb).pwm_open;
            _target_pwm_index->pwm = motor_cfg(ibuf.cfg, MotorIdentity::index).pwm_open;
            _target_pwm_other->pwm = motor_cfg(ibuf.cfg, MotorIdentity::other).pwm_open;
        }
        else
        {
            for (auto& motor_ctrl_data : _grasp_phase_controller_data.motor_controller_data)
            {
                const auto motor_id = motor_ctrl_data.motor_identity;
                const auto& subcfg = motor_cfg(ibuf.cfg, motor_id);
                auto& pwm_target = target_pwm(motor_id)->pwm;
                switch (motor_ctrl_data.controller_state.grasp_phase)
                {
                    case GraspPhase::FREELY_MOVING:
                    {
                        const float minimum_distance = motor_ctrl_data.minimum_distance(_sensors);
                        //close with maximum velocity, until the distance sensors sense something
                        pwm_target = std::max(static_cast<float>(subcfg.min_pwm_close_to_contact),
                                              static_cast<float>(subcfg.pwm_freely_moving * minimum_distance));
                        std::cout << static_cast<int>(motor_id) << "  " << VAROUT(pwm_target) << "\n";
                        //check if we have reached the object
                        if (motor_ctrl_data.controller_state.contact_detection_flag
                            //    || motor_ctrl_data.controller_state.finger_did_close
                           )
                        {
                            motor_ctrl_data.controller_state.grasp_phase = GraspPhase::BARRIER_PRE_LLH;
                            motor_ctrl_data.position_of_initial_contact = _sensors.motor(motor_id)->position;
                        }
                    }
                    break;
                    case GraspPhase::BARRIER_PRE_LLH:
                    {
                        //targ.at(i)->pwm = pwm_cont.at(i);
                        pwm_target = motor_ctrl_data.pos_control_pwm(ctrl_run_data, motor_ctrl_data.position_of_initial_contact);
                    }
                    break;
                    case GraspPhase::LOAD_LIFT_HOLD:
                    {
                        pwm_target = motor_ctrl_data.monotonic_shear_pressure_control_pwm_force(
                                         ctrl_run_data,
                                         subcfg.target_pressure,
                                         subcfg.target_shear,
                                         (motor_ctrl_data.controller_state.shear_force_used_ratio_num != 0)
                                     );
                        if (obuf.open_signal && _clicked_start_placing)
                        {
                            motor_ctrl_data.controller_state.grasp_phase = GraspPhase::UNLOAD;
                            motor_ctrl_data.controller_state.pressure_on_start_unload =
                                motor_ctrl_data.controller_state.maximum_pressure;
                            motor_ctrl_data.controller_state.target_pressure_rate_of_change =
                                static_cast<float>(motor_ctrl_data.controller_state.pressure_on_start_unload) * ctrl_run_data.dt_sec_fpga;
                        }
                    }
                    break;
                    case GraspPhase::UNLOAD:
                    {
                        pwm_target = motor_ctrl_data.unload_phase_controller_normal_force_via_pwm(ctrl_run_data);
                        //check if we have lost contact to the object
                        if (!motor_ctrl_data.controller_state.contact_detection_flag || (_sensors.motor(motor_id)->position < 0.15))
                        {
                            motor_ctrl_data.controller_state.grasp_phase = GraspPhase::BARRIER_PRE_OPEN;
                            motor_ctrl_data.position_of_unload_complete = motor_ctrl_data.last_encoder_pos;
                        }
                    }
                    break;
                    case GraspPhase::BARRIER_PRE_OPEN:
                    {
                        pwm_target = motor_ctrl_data.pos_control_pwm(
                                         ctrl_run_data,
                                         motor_ctrl_data.position_of_unload_complete);
                    }
                    break;
                    case GraspPhase::OPEN:
                    {
                        _target_pwm_thumb->pwm = motor_cfg(ibuf.cfg, MotorIdentity::thumb).pwm_open;
                        _target_pwm_index->pwm = motor_cfg(ibuf.cfg, MotorIdentity::index).pwm_open;
                        _target_pwm_other->pwm = motor_cfg(ibuf.cfg, MotorIdentity::other).pwm_open;
                        open();
                    }
                    break;
                }
            }
            //transition from freely_closing to load via barrier
            if (_grasp_phase_controller_data.motor_controller_data.other().controller_state.grasp_phase == GraspPhase::BARRIER_PRE_LLH &&
                _grasp_phase_controller_data.motor_controller_data.index().controller_state.grasp_phase == GraspPhase::BARRIER_PRE_LLH &&
                _grasp_phase_controller_data.motor_controller_data.thumb().controller_state.grasp_phase == GraspPhase::BARRIER_PRE_LLH)
            {
                _grasp_phase_controller_data.motor_controller_data.other().controller_state.grasp_phase = GraspPhase::LOAD_LIFT_HOLD;
                _grasp_phase_controller_data.motor_controller_data.index().controller_state.grasp_phase = GraspPhase::LOAD_LIFT_HOLD;
                _grasp_phase_controller_data.motor_controller_data.thumb().controller_state.grasp_phase = GraspPhase::LOAD_LIFT_HOLD;
            }
            //transition from unload to freely opening via barrier
            if (_grasp_phase_controller_data.motor_controller_data.other().controller_state.grasp_phase == GraspPhase::BARRIER_PRE_OPEN &&
                _grasp_phase_controller_data.motor_controller_data.index().controller_state.grasp_phase == GraspPhase::BARRIER_PRE_OPEN &&
                _grasp_phase_controller_data.motor_controller_data.thumb().controller_state.grasp_phase == GraspPhase::BARRIER_PRE_OPEN)
            {
                auto set = [&](auto & data)
                {
                    data.controller_state.grasp_phase = GraspPhase::OPEN;
                    data.position_of_unload_complete = data.last_encoder_pos;
                };
                set(_grasp_phase_controller_data.motor_controller_data.other());
                set(_grasp_phase_controller_data.motor_controller_data.index());
                set(_grasp_phase_controller_data.motor_controller_data.thumb());
            }
        }

        //write results
        for (int i = 0; i < 3; i++)
        {
            obuf.motor_controller_data.at(i) = _grasp_phase_controller_data.motor_controller_data.at(i).controller_state;
        }

        obuf.normalized_finger_sensors = _finger_sensors;

        _rt_to_ice.commitWrite();
    }

    void MinimalGraspingForceV1_NJointController::rtPreActivateController()
    {
        ARMARX_TRACE;
        _clicked_open = false;
        _clicked_close = false;
        _clicked_stop = false;

        const auto make_negative = [](auto & v)
        {
            v = -std::abs(v);
        };
        make_negative(_finger_sensors.index().shear_force_dist_joint.sensors.at(0).scaling);
        make_negative(_finger_sensors.index().shear_force_dist_joint.sensors.at(1).scaling);
        make_negative(_finger_sensors.index().shear_force_dist_joint.sensors.at(2).scaling);
    }

    void MinimalGraspingForceV1_NJointController::rtPostDeactivateController()
    {
    }
}
//publish
namespace armarx::KITSensorizedSoftFingerHand::V1
{
    void MinimalGraspingForceV1_NJointController::onPublishActivation(const DebugDrawerInterfacePrx&, const DebugObserverInterfacePrx&)
    {
        //maybe do some setup when the controller starts running (for visu / logging)
    }
    void MinimalGraspingForceV1_NJointController::onPublishDeactivation(const DebugDrawerInterfacePrx&, const DebugObserverInterfacePrx&)
    {
        //maybe do some cleanup when the controller starts running (for visu / logging)
    }
    void MinimalGraspingForceV1_NJointController::onPublish(const SensorAndControl&, const DebugDrawerInterfacePrx&, const DebugObserverInterfacePrx& obs)
    {
        ARMARX_TRACE;
        std::lock_guard g{_rt_to_ice_read_mutex};
        const auto& buf = _rt_to_ice.getUpToDateReadBuffer();
        DebugObserverHelper oh{this, obs, true};
        //publish_controller
        {
            auto publish_controller = [&](const std::string name, int idx)
            {
                const auto& mcdata = buf.motor_controller_data.at(idx);
                oh.setDebugObserverDatafield("Contact Flag "                                                  + name, 1e-5f + mcdata.contact_detection_flag);
                oh.setDebugObserverDatafield("Closing Flag "                                                  + name, 1e-5f + mcdata.finger_did_close);
                oh.setDebugObserverDatafield("Controller Grasp Phase "                                        + name, 1e-5f + static_cast<int>(mcdata.grasp_phase));
                oh.setDebugObserverDatafield("Controller current maximum normal force (phase load) "          + name, 1e-5f + static_cast<int>(mcdata.maximum_pressure));

                oh.setDebugObserverDatafield("Controller current monotonic_shear_pressure_control_last_pwm "          + name, 1e-5f + mcdata.monotonic_shear_pressure_control_last_pwm);
                oh.setDebugObserverDatafield("Controller current monotonic_shear_pressure_control_last_pwm_pressure " + name, 1e-5f + mcdata.monotonic_shear_pressure_control_last_pwm_pressure);
                oh.setDebugObserverDatafield("Controller current monotonic_shear_pressure_control_last_pwm_shear "    + name, 1e-5f + mcdata.monotonic_shear_pressure_control_last_pwm_shear);
                oh.setDebugObserverDatafield("Controller current monotonic_shear_pressure_control_last_delta_pressure " + name, 1e-5f + mcdata.monotonic_shear_pressure_control_last_delta_pressure);
                oh.setDebugObserverDatafield("Controller current monotonic_shear_pressure_control_last_delta_shear "    + name, 1e-5f + mcdata.monotonic_shear_pressure_control_last_delta_shear);

                oh.setDebugObserverDatafield("Controller current target_shear "                               + name, 1e-5f + mcdata.target_shear);
                oh.setDebugObserverDatafield("Controller current target_pressure "                            + name, 1e-5f + mcdata.target_pressure);

                oh.setDebugObserverDatafield("uload target_pressure_rate_of_change "                         + name, 1e-5f + mcdata.target_pressure_rate_of_change);
                oh.setDebugObserverDatafield("uload pressure_rate_of_change "                                + name, 1e-5f + mcdata.pressure_rate_of_change);
                oh.setDebugObserverDatafield("uload unload_phase_controller_normal_force_via_pwm_last_pwm "  + name, 1e-5f + mcdata.unload_phase_controller_normal_force_via_pwm_last_pwm);
            };
            publish_controller("other", 0);
            publish_controller("index", 1);
            publish_controller("thumb", 2);
        }

        //publish_finger
        {
            auto publish_finger = [&](const std::string name, int idx)
            {
                const auto& fdata = buf.normalized_finger_sensors.at(idx);

                //normal_force
                {
                    const auto mk_plot_nfor = [&](const std::string & pos)
                    {
                        return [&, pos](const std::string & valn, auto val)
                        {
                            oh.setDebugObserverDatafield("Normal Force " +
                                                         valn + " " +
                                                         pos + " " +
                                                         name,
                                                         1e-5f + static_cast<float>(val));
                        };
                    };

                    fdata.normal_force_sensors.at(0).visit(mk_plot_nfor("proximal left "));
                    fdata.normal_force_sensors.at(1).visit(mk_plot_nfor("proximal right "));
                    fdata.normal_force_sensors.at(2).visit(mk_plot_nfor("distal joint "));
                    fdata.normal_force_sensors.at(3).visit(mk_plot_nfor("distal tip "));
                }

                //shear
                {
                    const auto plot_shear = [&](const std::string & pos, const auto & sens)
                    {
                        const auto mk_plot_sfor = [&](const std::string & xyz)
                        {
                            return [&, xyz](const std::string & valn, auto val)
                            {
                                oh.setDebugObserverDatafield("Shear Force " +
                                                             pos + " " +
                                                             valn + " " +
                                                             xyz + " "  +
                                                             name, 1e-5f +  static_cast<float>(val));
                            };
                        };
                        sens.sensors.at(0).visit(mk_plot_sfor("x"));
                        sens.sensors.at(1).visit(mk_plot_sfor("y"));
                        sens.sensors.at(2).visit(mk_plot_sfor("z"));

                        oh.setDebugObserverDatafield("Shear Force len_xy " + pos + " " + name, 1e-5f + sens.len_xy);
                        oh.setDebugObserverDatafield("Shear Force len_z "  + pos + " " + name, 1e-5f + sens.len_z);
                    };
                    plot_shear("joint", fdata.shear_force_dist_joint);
                    plot_shear("tip", fdata.shear_force_dist_tip);
                }
                //accelerometer fft
                {
                    const auto plot_fft = [&](const std::string & coords, const auto & fft)
                    {
                        for (std::size_t i = 0; i < fft.size(); ++i)
                        {

                            oh.setDebugObserverDatafield("accelerometer fft " +
                                                         coords + " magnitude " +
                                                         std::to_string(i) + " "  +
                                                         name, 1e-5f + fft.at(i));
                        }
                    };
                    plot_fft("x", fdata.accelerometer.fft_absolute_x);
                    plot_fft("y", fdata.accelerometer.fft_absolute_y);
                    plot_fft("xy", fdata.accelerometer.fft_absolute_xy);

                    oh.setDebugObserverDatafield("accelerometer fft energy " + name, 1e-5f + fdata.accelerometer.fft_contact_detect_energy);
                }
            };
            publish_finger("little", 0);
            publish_finger("ring", 1);
            publish_finger("middle", 2);
            publish_finger("index", 3);
            publish_finger("thumb", 4);
            oh.setDebugObserverDatafield("accelerometer fft energy min",
                                         1e-5f + std::min({buf.normalized_finger_sensors.at(0).accelerometer.fft_contact_detect_energy,
                                                 buf.normalized_finger_sensors.at(1).accelerometer.fft_contact_detect_energy,
                                                 buf.normalized_finger_sensors.at(2).accelerometer.fft_contact_detect_energy,
                                                 buf.normalized_finger_sensors.at(3).accelerometer.fft_contact_detect_energy,
                                                 buf.normalized_finger_sensors.at(4).accelerometer.fft_contact_detect_energy}));
            oh.setDebugObserverDatafield("accelerometer fft energy max",
                                         1e-5f + std::max({buf.normalized_finger_sensors.at(0).accelerometer.fft_contact_detect_energy,
                                                 buf.normalized_finger_sensors.at(1).accelerometer.fft_contact_detect_energy,
                                                 buf.normalized_finger_sensors.at(2).accelerometer.fft_contact_detect_energy,
                                                 buf.normalized_finger_sensors.at(3).accelerometer.fft_contact_detect_energy,
                                                 buf.normalized_finger_sensors.at(4).accelerometer.fft_contact_detect_energy}));
            oh.setDebugObserverDatafield("accelerometer fft energy avg",
                                         1e-5f + (buf.normalized_finger_sensors.at(0).accelerometer.fft_contact_detect_energy +
                                                  buf.normalized_finger_sensors.at(1).accelerometer.fft_contact_detect_energy +
                                                  buf.normalized_finger_sensors.at(2).accelerometer.fft_contact_detect_energy +
                                                  buf.normalized_finger_sensors.at(3).accelerometer.fft_contact_detect_energy +
                                                  buf.normalized_finger_sensors.at(4).accelerometer.fft_contact_detect_energy) / 5);
        }

        oh.setDebugObserverDatafield("updating_sensors",    1e-5f +  buf.updating_sensors);
        oh.setDebugObserverDatafield("resetting_controller", 1e-5f +  buf.resetting_controller);
        oh.setDebugObserverDatafield("run_controller",      1e-5f +  buf.run_controller);

        oh.setDebugObserverDatafield("active_shear_avg_len_xy",              1e-5f +    buf.active_shear_avg_len_xy);
        oh.setDebugObserverDatafield("active_shear_avg_len_z",               1e-5f +    buf.active_shear_avg_len_z);
        oh.setDebugObserverDatafield("active_shear_avg_ratio",               1e-5f +    buf.active_shear_avg_ratio);
        oh.setDebugObserverDatafield("active_shear_num",                     1e-5f +    buf.active_shear_num);
        oh.setDebugObserverDatafield("used_shear_avg_ratio",               1e-5f +    buf.used_shear_avg_ratio);

        oh.setDebugObserverDatafield("open_signal",                   1e-5f +           buf.open_signal);
        oh.setDebugObserverDatafield("open_signal_finger_count",      1e-5f +           buf.open_signal_finger_count);

        oh.setDebugObserverDatafield("last_fpga_dt",                   buf.last_fpga_dt);
        oh.sendDebugObserverBatch();
    }
}
