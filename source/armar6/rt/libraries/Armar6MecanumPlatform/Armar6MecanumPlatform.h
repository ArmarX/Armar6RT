/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_rt::ArmarXObjects::Armar6MecanumPlatform
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <Eigen/Dense>

namespace armarx
{

    struct PlatformGeometryInfo
    {
        float platformHalfWidth;
        float platformHalfHeight;
        float radius;
        PlatformGeometryInfo() : radius(0)
        { }
    };

    class Armar6MecanumPlatform
    {
    public:

        Armar6MecanumPlatform();

        Eigen::Vector4f calcWheelVelocity(float vx, float vy, float vAngle);
        Eigen::Vector3f calcCartesianVelocity(const Eigen::Vector4f& wheelVelocities);

        const PlatformGeometryInfo& getInfo() const;

    private:
        PlatformGeometryInfo info;
    };

}

