#pragma once

#include <mutex>

#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointController.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>

#include <armar6/rt/units/Armar6Unit/Devices/KITFingerVisionSoftHand/V1/utility/Sensors.h>
#include <armar6/rt/units/Armar6Unit/Devices/KITFingerVisionSoftHand/V1/Controllers/PositionToPWM.h>
#include <armar6/rt/units/Armar6Unit/Devices/KITFingerVisionSoftHand/V1/ControlTarget/CNNObjectId.h>


#include <armar6/rt/interface/units/KITFingerVisionSoftHand/V1/NJointGraspOnDetectionController.h>

namespace armarx::KITFingerVisionSoftHand::V1
{
    /**
    * @defgroup Library-GraspOnDetection_NJointController GraspOnDetection_NJointController
    * @ingroup armar6_rt
    * A description of the library GraspOnDetection_NJointController.
    *
    * @class GraspOnDetection_NJointController
    * @ingroup Library-GraspOnDetection_NJointController
    * @brief Brief description of class GraspOnDetection_NJointController.
    *
    * Detailed description of class GraspOnDetection_NJointController.
    */
    class GraspOnDetection_NJointController :
        virtual public NJointController,
        virtual public NJointGraspOnDetectionControllerInterface
    {
    public:
        static NJointGraspOnDetectionControllerConfig make_default_cfg();
    public:
        using ConfigPtrT = NJointGraspOnDetectionControllerConfigPtr;
        GraspOnDetection_NJointController(
            RobotUnitPtr robotUnit,
            const ConfigPtrT& config,
            const VirtualRobot::RobotPtr&);

        std::string getClassName(const Ice::Current& = Ice::emptyCurrent) const override;
        WidgetDescription::StringWidgetDictionary getFunctionDescriptions(const Ice::Current& = Ice::emptyCurrent) const override;
        void callDescribedFunction(const std::string& name, const StringVariantBaseMap& valueMap, const Ice::Current& = Ice::emptyCurrent) override;

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;

        static WidgetDescription::WidgetPtr GenerateConfigDescription(
            const VirtualRobot::RobotPtr&,
            const std::map<std::string, ConstControlDevicePtr>&,
            const std::map<std::string, ConstSensorDevicePtr>&);

        static ConfigPtrT
        GenerateConfigFromVariants(const StringVariantBaseMap& values);
    private:
        void start(const Ice::Current& = Ice::emptyCurrent) override;
        void reset(const Ice::Current& = Ice::emptyCurrent) override;
        void configure(const ConfigPtrT& cfg, const Ice::Current& = Ice::emptyCurrent) override;
        void setAutofire(bool fire, const Ice::Current& = Ice::emptyCurrent) override;
        bool detected(const Ice::Current& = Ice::emptyCurrent) override;
    protected:
        void rtPreActivateController() override;
        void rtPostDeactivateController() override;

        std::atomic_bool                     _clicked_reset = false;
        std::atomic_bool                     _clicked_start = false;
        std::atomic_bool                     _autofire      = true;
        std::atomic_bool                     _detected      = false;

        KITFingerVisionSoftHand::V1::Sensors _sensors;

        struct IceToRT
        {
            NJointGraspOnDetectionControllerConfig cfg;

        };
        WriteBufferedTripleBuffer<IceToRT> _ice_to_rt;
        mutable std::recursive_mutex       _ice_to_rt_write_mutex;

        struct ControlData
        {
            struct MotorData
            {
                MotorData(float p, float i, float d);
                KITFingerVisionSoftHand::V1::Controllers::PositionToPWM pos_ctrl;
                ControlTarget1DoFActuatorPWM* target_pwm = nullptr;
                void run(float curr, float targ, float dt, const auto& cfg);
            };
            MotorData other;
            MotorData index;
            MotorData thumb;
            const MotorData& motor_data(KITFingerVisionSoftHand::V1::Sensors::MotorIdentity id) const;
            ControlData(const NJointGraspOnDetectionControllerConfig& cfg);
            bool close = false;
            bool running = false;
        };
        ControlData _ctrl_data;
        ControlTargetCNNObjectId* _object;
    };
}
