#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>

#include "GraspOnDetection.h"

//register
namespace armarx::KITFingerVisionSoftHand::V1
{
    NJointControllerRegistration<GraspOnDetection_NJointController>
    registrationControllerGraspOnDetection_NJointControllers("KITFingerVisionSoftHandV1_GraspOnDetection_NJointController");

    GraspOnDetection_NJointController::ControlData::MotorData::MotorData(float p, float i, float d) :
        pos_ctrl(p, i, d)
    {}

    void GraspOnDetection_NJointController::ControlData::MotorData::run(float curr, float targ, float dt, const auto& cfg)
    {
        target_pwm->pwm = pos_ctrl.calculate(curr, targ, dt, cfg.pwm_max);
    }

    const GraspOnDetection_NJointController::ControlData::MotorData& GraspOnDetection_NJointController::ControlData::motor_data(Sensors::MotorIdentity id) const
    {
        return KITFingerVisionSoftHand::V1::identity::Select(
                   id, other, index, thumb);
    }

    GraspOnDetection_NJointController::ControlData::ControlData(const NJointGraspOnDetectionControllerConfig& cfg) :
        other(cfg.motor_other.pos_pid.p, cfg.motor_other.pos_pid.i, cfg.motor_other.pos_pid.d),
        index(cfg.motor_index.pos_pid.p, cfg.motor_index.pos_pid.i, cfg.motor_index.pos_pid.d),
        thumb(cfg.motor_thumb.pos_pid.p, cfg.motor_thumb.pos_pid.i, cfg.motor_thumb.pos_pid.d)
    {}
}
//create
namespace armarx::KITFingerVisionSoftHand::V1
{
    WidgetDescription::WidgetPtr
    GraspOnDetection_NJointController::GenerateConfigDescription(
        const VirtualRobot::RobotPtr&,
        const std::map<std::string, ConstControlDevicePtr>&,
        const std::map<std::string, ConstSensorDevicePtr>&)
    {
        using namespace armarx::WidgetDescription;

        VBoxLayoutPtr set_params = new VBoxLayout;
        //hand name
        {
            HBoxLayoutPtr line = new HBoxLayout;
            set_params->children.emplace_back(line);

            line->children.emplace_back(new Label(false, "hand name"));
            LineEditPtr e = new LineEdit;
            e->name = "hand";
            e->defaultValue = "RightHand";
            line->children.emplace_back(e);
        }
        const auto add_line = [&](const std::string & name, float val)
        {
            HBoxLayoutPtr line = new HBoxLayout;
            set_params->children.emplace_back(line);

            line->children.emplace_back(new Label(false, name));
            FloatSpinBoxPtr e = new FloatSpinBox;
            e->name = name;
            e->min = -200000;
            e->defaultValue = val;
            e->max   = 200000;
            e->steps = 4000001;
            line->children.emplace_back(e);
        };
        const auto add_check_line = [&](const std::string & name, bool val)
        {
            HBoxLayoutPtr line = new HBoxLayout;
            set_params->children.emplace_back(line);

            line->children.emplace_back(new Label(false, name));
            CheckBoxPtr e = new CheckBox;
            e->name = name;
            e->defaultValue = val;
            line->children.emplace_back(e);
        };
        add_line("detection_threshold_per_finger", 1000.f);
        add_line("detection_threshold_total", 6400.f);
        add_line("finger_count_threahold", 1.f);

        add_check_line("close_full_force", true);
        add_check_line("detect_on_little", true);
        add_check_line("detect_on_ring",   true);
        add_check_line("detect_on_middle", true);
        add_check_line("detect_on_index",  true);
        add_check_line("detect_on_thumb",  true);

        add_line("motor_index.pos_pid.p",      4.0f);
        add_line("motor_index.pos_pid.i",      0.4);
        add_line("motor_index.pos_pid.d",      1);
        add_line("motor_index.pwm_max", 1000.f);
        add_line("motor_index.pos_open", 0.48f);
        add_line("motor_index.pos_close", 0.63f);

        add_line("motor_other.pos_pid.p",      12.0f);
        add_line("motor_other.pos_pid.i",      1.2);
        add_line("motor_other.pos_pid.d",      1);
        add_line("motor_other.pwm_max", 1000.f);
        add_line("motor_other.pos_open", 0.f);
        add_line("motor_other.pos_close", 0.f);

        add_line("motor_thumb.pos_pid.p",      4.0f);
        add_line("motor_thumb.pos_pid.i",      0.4);
        add_line("motor_thumb.pos_pid.d",      1);
        add_line("motor_thumb.pwm_max", 1000.f);
        add_line("motor_thumb.pos_open", 0.34f);
        add_line("motor_thumb.pos_close", 0.51f);


        return set_params;
    }
    NJointGraspOnDetectionControllerConfigPtr
    GraspOnDetection_NJointController::GenerateConfigFromVariants(const StringVariantBaseMap& valueMap)
    {
        NJointGraspOnDetectionControllerConfigPtr cfg = new NJointGraspOnDetectionControllerConfig;

        cfg->hand_name = valueMap.at("hand")->getString();

        cfg->detection_threshold_per_finger =  valueMap.at("detection_threshold_per_finger")->getFloat();
        cfg->detection_threshold_total =  valueMap.at("detection_threshold_total")->getFloat();
        cfg->finger_count_threahold =  valueMap.at("finger_count_threahold")->getFloat();

        cfg->close_full_force =  valueMap.at("close_full_force")->getBool();
        cfg->detect_on_little =  valueMap.at("detect_on_little")->getBool();
        cfg->detect_on_ring   =  valueMap.at("detect_on_ring")->getBool();
        cfg->detect_on_middle =  valueMap.at("detect_on_middle")->getBool();
        cfg->detect_on_index  =  valueMap.at("detect_on_index")->getBool();
        cfg->detect_on_thumb  =  valueMap.at("detect_on_thumb")->getBool();

        cfg->motor_index.pos_pid.p      =  valueMap.at("motor_index.pos_pid.p")->getFloat();
        cfg->motor_index.pos_pid.i      =  valueMap.at("motor_index.pos_pid.i")->getFloat();
        cfg->motor_index.pos_pid.d      =  valueMap.at("motor_index.pos_pid.d")->getFloat();
        cfg->motor_index.pwm_max        =  valueMap.at("motor_index.pwm_max")->getFloat();
        cfg->motor_index.pos_open       =  valueMap.at("motor_index.pos_open")->getFloat();
        cfg->motor_index.pos_close     =  valueMap.at("motor_index.pos_close")->getFloat();

        cfg->motor_other.pos_pid.p      =  valueMap.at("motor_other.pos_pid.p")->getFloat();
        cfg->motor_other.pos_pid.i      =  valueMap.at("motor_other.pos_pid.i")->getFloat();
        cfg->motor_other.pos_pid.d      =  valueMap.at("motor_other.pos_pid.d")->getFloat();
        cfg->motor_other.pwm_max        =  valueMap.at("motor_other.pwm_max")->getFloat();
        cfg->motor_other.pos_open       =  valueMap.at("motor_other.pos_open")->getFloat();
        cfg->motor_other.pos_close     =  valueMap.at("motor_other.pos_close")->getFloat();

        cfg->motor_thumb.pos_pid.p      =  valueMap.at("motor_thumb.pos_pid.p")->getFloat();
        cfg->motor_thumb.pos_pid.i      =  valueMap.at("motor_thumb.pos_pid.i")->getFloat();
        cfg->motor_thumb.pos_pid.d      =  valueMap.at("motor_thumb.pos_pid.d")->getFloat();
        cfg->motor_thumb.pwm_max        =  valueMap.at("motor_thumb.pwm_max")->getFloat();
        cfg->motor_thumb.pos_open       =  valueMap.at("motor_thumb.pos_open")->getFloat();
        cfg->motor_thumb.pos_close     =  valueMap.at("motor_thumb.pos_close")->getFloat();

        return cfg;
    }

    NJointGraspOnDetectionControllerConfig GraspOnDetection_NJointController::make_default_cfg()
    {
        NJointGraspOnDetectionControllerConfig c;

        c.detection_threshold_per_finger = 1000.f;
        c.detection_threshold_total = 6400.f;
        c.finger_count_threahold = 1.f;

        c.close_full_force = true;
        c.detect_on_little = true;
        c.detect_on_ring   = true;
        c.detect_on_middle = true;
        c.detect_on_index  = true;
        c.detect_on_thumb  = true;

        c.motor_index.pos_pid.p = 4.0f;
        c.motor_index.pos_pid.i = 0.4;
        c.motor_index.pos_pid.d = 1;
        c.motor_index.pwm_max   = 1000.f;
        c.motor_index.pos_open  = 0.48f;
        c.motor_index.pos_close = 0.63f;

        c.motor_other.pos_pid.p = 12.0f;
        c.motor_other.pos_pid.i = 1.2;
        c.motor_other.pos_pid.d = 1;
        c.motor_other.pwm_max   = 1000.f;
        c.motor_other.pos_open  = 0.f;
        c.motor_other.pos_close = 0.f;

        c.motor_thumb.pos_pid.p = 4.0f;
        c.motor_thumb.pos_pid.i = 0.4;
        c.motor_thumb.pos_pid.d = 1;
        c.motor_thumb.pwm_max   = 1000.f;
        c.motor_thumb.pos_open  = 0.34f;
        c.motor_thumb.pos_close = 0.51f;
        return c;
    }

    GraspOnDetection_NJointController::GraspOnDetection_NJointController(
        RobotUnitPtr robotUnit,
        const NJointGraspOnDetectionControllerConfigPtr& config,
        const VirtualRobot::RobotPtr&
    ) :
        _ctrl_data([ & ]
    {
        ARMARX_CHECK_NOT_NULL(config);
        return * config;
    }())
    {
        ARMARX_CHECK_NOT_NULL(config);
        configure(config);
        //get ctrl
        {
            ControlTargetBase* ct_thumb = useControlTarget(config->hand_name + ".motor.thumb", armarx::ControlModes::PWM1DoF);
            ControlTargetBase* ct_index = useControlTarget(config->hand_name + ".motor.index", armarx::ControlModes::PWM1DoF);
            ControlTargetBase* ct_other = useControlTarget(config->hand_name + ".motor.other", armarx::ControlModes::PWM1DoF);
            ControlTargetBase* ct_cnn   = useControlTarget(config->hand_name + ".cnn", ControlModes::CNNObjectId);
            ARMARX_CHECK_EXPRESSION(ct_thumb->isA<ControlTarget1DoFActuatorPWM>());
            ARMARX_CHECK_EXPRESSION(ct_index->isA<ControlTarget1DoFActuatorPWM>());
            ARMARX_CHECK_EXPRESSION(ct_other->isA<ControlTarget1DoFActuatorPWM>());
            ARMARX_CHECK_EXPRESSION(ct_cnn->isA<ControlTargetCNNObjectId>());

            _ctrl_data.other.target_pwm = ct_other->asA<ControlTarget1DoFActuatorPWM>();
            _ctrl_data.index.target_pwm = ct_index->asA<ControlTarget1DoFActuatorPWM>();
            _ctrl_data.thumb.target_pwm = ct_thumb->asA<ControlTarget1DoFActuatorPWM>();

            _object = ct_cnn->asA<ControlTargetCNNObjectId>();
        }
        //get sens
        _sensors = KITFingerVisionSoftHand::V1::Sensors(config->hand_name, *this);
    }
}
//name
namespace armarx::KITFingerVisionSoftHand::V1
{
    std::string GraspOnDetection_NJointController::getClassName(const Ice::Current&) const
    {
        return "GraspOnDetection_NJointController";
    }
}
//remote function calls
namespace armarx::KITFingerVisionSoftHand::V1
{
    WidgetDescription::StringWidgetDictionary GraspOnDetection_NJointController::getFunctionDescriptions(const Ice::Current&) const
    {
        using namespace armarx::WidgetDescription;

        return {{"Start", nullptr}, {"Release", nullptr}};
    }

    void GraspOnDetection_NJointController::callDescribedFunction(const std::string& name, const StringVariantBaseMap& valueMap, const Ice::Current&)
    {
        if (name == "Start")
        {
            start();
        }
        else if (name == "Release")
        {
            reset();
        }
        else
        {
            ARMARX_WARNING << "Unknown function name called: " << name;
        }
    }
}
//iface
namespace armarx::KITFingerVisionSoftHand::V1
{
    void GraspOnDetection_NJointController::start(const Ice::Current&)
    {
        _clicked_start = true;
    }
    void GraspOnDetection_NJointController::reset(const Ice::Current&)
    {
        _clicked_reset = true;
    }
    void GraspOnDetection_NJointController::configure(const ConfigPtrT& cfg, const Ice::Current&)
    {
        ARMARX_CHECK_NOT_NULL(cfg);
        std::lock_guard g{_ice_to_rt_write_mutex};
        _ice_to_rt.getWriteBuffer().cfg = *cfg;
        _ice_to_rt.commitWrite();
    }
    void GraspOnDetection_NJointController::setAutofire(bool fire, const Ice::Current&)
    {
        _autofire = fire;
    }
    bool GraspOnDetection_NJointController::detected(const Ice::Current&)
    {
        return _detected;
    }
}
//rt control
namespace armarx::KITFingerVisionSoftHand::V1
{
    void GraspOnDetection_NJointController::rtRun(
        const IceUtil::Time& sensorValuesTimestamp,
        const IceUtil::Time& timeSinceLastIteration)
    {
        const auto& buf = _ice_to_rt.getUpToDateReadBuffer();
        const auto dt = timeSinceLastIteration.toSecondsDouble();

        _object->object_id = buf.cfg.object_id;

        //gui events
        {
            if (_clicked_reset.exchange(false))
            {
                _ctrl_data.close   = false;
                _ctrl_data.running = false;
                _ctrl_data.other.pos_ctrl.reset();
                _ctrl_data.index.pos_ctrl.reset();
                _ctrl_data.thumb.pos_ctrl.reset();
            }
            if (_clicked_start.exchange(false))
            {
                _ctrl_data.running = true;
            }
        }
        //config
        {
            auto cfg_pos_pid = [&](auto & ctrl, const auto & c)
            {
                ctrl.PID.Kp = c.p;
                ctrl.PID.Ki = c.i;
                ctrl.PID.Kd = c.d;
            };
            cfg_pos_pid(_ctrl_data.other.pos_ctrl, buf.cfg.motor_other.pos_pid);
            cfg_pos_pid(_ctrl_data.index.pos_ctrl, buf.cfg.motor_index.pos_pid);
            cfg_pos_pid(_ctrl_data.thumb.pos_ctrl, buf.cfg.motor_thumb.pos_pid);
        }

        if (!_ctrl_data.running)
        {
            _ctrl_data.other.target_pwm->pwm = buf.cfg.motor_other.pwm_open;
            _ctrl_data.index.target_pwm->pwm = buf.cfg.motor_index.pwm_open;
            _ctrl_data.thumb.target_pwm->pwm = buf.cfg.motor_thumb.pwm_open;
        }
        else
        {
            if (!_ctrl_data.close)
            {
                long n_over_thresh = 0;
                long n_detections  = 0;
                const auto thresh = static_cast<std::uint32_t>(buf.cfg.detection_threshold_per_finger);
                const auto test = [&](bool active, auto pop)
                {
                    if (active && (pop >= thresh))
                    {
                        ++n_over_thresh;
                        n_detections += pop;
                    }
                };
                test(buf.cfg.detect_on_little, _sensors.hand->cnn_data_popcount_little);
                test(buf.cfg.detect_on_ring,   _sensors.hand->cnn_data_popcount_ring);
                test(buf.cfg.detect_on_middle, _sensors.hand->cnn_data_popcount_middle);
                test(buf.cfg.detect_on_index,  _sensors.hand->cnn_data_popcount_index);
                test(buf.cfg.detect_on_thumb,  _sensors.hand->cnn_data_popcount_thumb);
                _detected = (n_detections  >= buf.cfg.detection_threshold_total) &&
                            (n_over_thresh >= buf.cfg.finger_count_threahold);
                if (_autofire)
                {
                    _ctrl_data.close = _detected;
                }
            }
            float target_other = buf.cfg.motor_other.pos_open;
            float target_index = buf.cfg.motor_index.pos_open;
            float target_thumb = buf.cfg.motor_thumb.pos_open;
            if (_ctrl_data.close)
            {
                target_other = buf.cfg.motor_other.pos_close;
                target_index = buf.cfg.motor_index.pos_close;
                target_thumb = buf.cfg.motor_thumb.pos_close;
            }
            _ctrl_data.other.run(_sensors.motor.other()->position, target_other, dt, buf.cfg.motor_other);
            _ctrl_data.index.run(_sensors.motor.index()->position, target_index, dt, buf.cfg.motor_index);
            _ctrl_data.thumb.run(_sensors.motor.thumb()->position, target_thumb, dt, buf.cfg.motor_thumb);
        }
    }

    void GraspOnDetection_NJointController::rtPreActivateController()
    {
        _clicked_reset = true;
        _clicked_start = false;
        _detected      = false;
    }
    void GraspOnDetection_NJointController::rtPostDeactivateController()
    {}
}
