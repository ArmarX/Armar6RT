/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    armar6_rt::gui-plugins::SensorizedSoftFingerHandMinimalForceGraspingWidgetController
 * \author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * \date       2021
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include <string>
#include <thread>
#include <chrono>

#include <qwt/qwt_plot_layout.h>
#include <qwt/qwt_scale_widget.h>

#include <SimoxUtility/json.h>
#include <SimoxUtility/math/convert/rpy_to_mat4f.h>

#include <SimoxQtUtility/qwt/SimoxQwtColorMap.h>

#include <ArmarXCore/core/util/OnScopeExit.h>
#include <ArmarXCore/core/util/FileSystemPathBuilder.h>
#include <armar6/rt/libraries/KITSensorizedSoftFingerHandV1NJointControllers/MinimalGraspingForceV1/Config.h>

#include "SensorizedSoftFingerHandMinimalForceGraspingWidgetController.h"

static constexpr float max_pwm = 700;

std::vector<Eigen::Matrix4f> traj_wps_side = simox::json::posquatArray2eigen4fVector(R"([

             {
                 "desc": "mid",
                 "qw": -0.07713359594345093,
                 "qx": 0.7473915219306946,
                 "qy": 0.6585299372673035,
                 "qz": -0.04236489534378052,
                 "x": 697.0657348632813,
                 "y": 674.9738159179688,
                 "z": 1026.3885498046875
             },




             {
                 "desc": "down",
                 "qw": 0.24731890857219696,
                 "qx": 0.661904513835907,
                 "qy": 0.6462811231613159,
                 "qz": -0.2881608307361603,
                 "x": 620.5601196289063,
                 "y": 591.8463134765625,
                 "z": 737.8338623046875
             },
             {
                 "desc": "up",
                 "qw": -0.30653658509254456,
                 "qx": 0.7249116897583008,
                 "qy": 0.5843172073364258,
                 "qz": 0.19776751101016998,
                 "x": 740.4525146484375,
                 "y": 600.8800659179688,
                 "z": 1313.7470703125
             },

             {
                 "desc": "mid turn right",
                 "qw": -0.3569399416446686,
                 "qx": 0.6355316638946533,
                 "qy": 0.5950669646263123,
                 "qz": -0.3385098874568939,
                 "x": 659.5458374023438,
                 "y": 713.1341552734375,
                 "z": 1045.5635986328125
             },
             {
                 "desc": "mid turn left",
                 "qw": 0.13990898430347443,
                 "qx": 0.6970155835151672,
                 "qy": 0.672434389591217,
                 "qz": 0.20597781240940094,
                 "x": 632.5487060546875,
                 "y": 698.0591430664063,
                 "z": 1086.28515625
             },


             {
                 "desc": "mid",
                 "qw": -0.07713359594345093,
                 "qx": 0.7473915219306946,
                 "qy": 0.6585299372673035,
                 "qz": -0.04236489534378052,
                 "x": 697.0657348632813,
                 "y": 674.9738159179688,
                 "z": 1026.3885498046875
             }
             ]
)");

//boilerplate config
namespace armarx
{
    void SensorizedSoftFingerHandMinimalForceGraspingWidgetController::setupNames(const std::string& hand_name,
            const std::string& robot_unit_name,
            const std::string& rsc_name,
            const std::string& static_plotter_name)
    {
        ARMARX_TRACE;
        std::lock_guard g{_all_mutex};
        _static_plotter_name       = static_plotter_name;
        _hand_name                 = hand_name;
        if (robot_unit_name.empty() || _hand_name == "FAKE")
        {
            _fake_mode = true;
            _hand_name = "FAKE";
            getRobotUnitComponentPlugin().deactivate();
            getRobotStateComponentPlugin().deactivate();
        }
        else
        {
            getRobotUnitComponentPlugin().setRobotUnitName(robot_unit_name);
            if (rsc_name.empty())
            {
                getRobotStateComponentPlugin().deactivate();
            }
            else
            {
                getRobotStateComponentPlugin().setRobotStateComponentName(rsc_name);
            }
        }
        ARMARX_INFO << "params "
                    << VAROUT(_hand_name) << " "
                    << VAROUT(robot_unit_name) << " "
                    << VAROUT(rsc_name) << " "
                    << VAROUT(_static_plotter_name);
    }

    void SensorizedSoftFingerHandMinimalForceGraspingWidgetController::loadSettings(QSettings* settings)
    {
        ARMARX_INFO << "loadSettings";
        setupNames(settings->value("hand_name", "RightHand").toString().toStdString(),
                   settings->value("ru", "Armar6Unit").toString().toStdString(),
                   settings->value("rsc", "Armar6StateComponent").toString().toStdString(),
                   settings->value("splotn", "StaticPlotter").toString().toStdString());
    }

    void SensorizedSoftFingerHandMinimalForceGraspingWidgetController::saveSettings(QSettings* settings)
    {
        ARMARX_INFO << "saveSettings";
        ARMARX_TRACE;
        std::lock_guard g{_all_mutex};
        settings->setValue("ru", QString::fromStdString(getRobotUnitComponentPlugin().getRobotUnitName()));
        settings->setValue("hand_name", QString::fromStdString(_hand_name));
        settings->setValue("splotn", QString::fromStdString(_static_plotter_name));
        settings->setValue("rsc", QString::fromStdString(getRobotStateComponentPlugin().getRobotStateComponentName()));
    }

    QPointer<QDialog> SensorizedSoftFingerHandMinimalForceGraspingWidgetController::getConfigDialog(QWidget* parent)
    {
        ARMARX_INFO << "getConfigDialog";
        ARMARX_TRACE;
        std::lock_guard g{_all_mutex};
        if (!_dialog)
        {
            _dialog = new SimpleConfigDialog(parent);
            _dialog->addProxyFinder<RobotUnitInterfacePrx>("ru", "Robot Unit", "*Unit");
            _dialog->addProxyFinder<RobotStateComponentInterfacePrx>("rsc", "Robot State Component", "*Component");
            _dialog->addLineEdit("hand_name", "Hand Name (or 'FAKE')", "RightHand");
_dialog->addLineEdit("splotn", "StaticPlotter Topic", "StaticPlotter");
}
return qobject_cast<SimpleConfigDialog*>(_dialog);
}

void SensorizedSoftFingerHandMinimalForceGraspingWidgetController::configured()
{
    ARMARX_INFO << "configured";
    setupNames(_dialog->get("hand_name"),
               _dialog->get("ru"),
               _dialog->get("rsc"),
               _dialog->get("splotn"));
}
}
//cycle
namespace armarx
{
    SensorizedSoftFingerHandMinimalForceGraspingWidgetController::SensorizedSoftFingerHandMinimalForceGraspingWidgetController() :
        _gen{std::random_device{}()}
    {
        using T = SensorizedSoftFingerHandMinimalForceGraspingWidgetController;
        ARMARX_INFO << "ctor...";
        _widget.setupUi(getWidget());
        ARMARX_INFO << "setting up fft";
        {
            const auto max_n_fft = std::pow(2, _widget.spinBoxFFTInSize->maximum());

            const auto setup_plotter = [&](auto layout, auto & data,
                                           auto nqr,
                                           auto lmifft, auto lmafft,
                                           auto lmiaccx, auto lmaaccx,
                                           auto lmiaccy, auto lmaaccy,
                                           auto lmiaccz, auto lmaaccz)
            {
                data.labelFFTMin = lmifft;
                data.labelFFTMax = lmafft;

                data.labelAccXMin = lmiaccx;
                data.labelAccXMax = lmaaccx;
                data.labelAccYMin = lmiaccy;
                data.labelAccYMax = lmaaccy;
                data.labelAccZMin = lmiaccz;
                data.labelAccZMax = lmaaccz;

                data.labelFFTNyquistBinRe = nqr;

                data.plot = new QwtPlot;
                layout->addWidget(data.plot);

                data.spec = new QwtPlotSpectrogram;
                data.spec->attach(data.plot);

                data.spec->setRenderThreadCount(0);   // use system specific thread count
                data.spec_data = new simox::qt::SimoxQwtSpectrogramData(2048, max_n_fft / 2, _widget.doubleSpinBoxFFTFreq->value());
                data.spec->setData(data.spec_data);

                data.vals.resize(max_n_fft * 2, Eigen::Vector3f::Zero());

                data.spec->setDisplayMode(QwtPlotSpectrogram::ImageMode, true);
                data.spec->setDefaultContourPen(QPen());

                data.plot->plotLayout()->setAlignCanvasToScales(true);
                data.plot->replot();
            };
            setup_plotter(_widget.verticalLayoutLittlePlotter,
                          _fft.finger_datas.at(0),
                          _widget.labelFFTNyquistBinRe_little,
                          _widget.labelFFTMinLi,
                          _widget.labelFFTMaxLi,
                          _widget.labelFFTAccXMinLi,
                          _widget.labelFFTAccXMaxLi,
                          _widget.labelFFTAccYMinLi,
                          _widget.labelFFTAccYMaxLi,
                          _widget.labelFFTAccZMinLi,
                          _widget.labelFFTAccZMaxLi);

            setup_plotter(_widget.verticalLayoutRingPlotter,
                          _fft.finger_datas.at(1),
                          _widget.labelFFTNyquistBinRe_ring,
                          _widget.labelFFTMinRi,
                          _widget.labelFFTMaxRi,
                          _widget.labelFFTAccXMinRi,
                          _widget.labelFFTAccXMaxRi,
                          _widget.labelFFTAccYMinRi,
                          _widget.labelFFTAccYMaxRi,
                          _widget.labelFFTAccZMinRi,
                          _widget.labelFFTAccZMaxRi);

            setup_plotter(_widget.verticalLayoutMiddlePlotter,
                          _fft.finger_datas.at(2),
                          _widget.labelFFTNyquistBinRe_middle,
                          _widget.labelFFTMinMi,
                          _widget.labelFFTMaxMi,
                          _widget.labelFFTAccXMinMi,
                          _widget.labelFFTAccXMaxMi,
                          _widget.labelFFTAccYMinMi,
                          _widget.labelFFTAccYMaxMi,
                          _widget.labelFFTAccZMinMi,
                          _widget.labelFFTAccZMaxMi);

            setup_plotter(_widget.verticalLayoutIndexPlotter,
                          _fft.finger_datas.at(3),
                          _widget.labelFFTNyquistBinRe_index,
                          _widget.labelFFTMinId,
                          _widget.labelFFTMaxId,
                          _widget.labelFFTAccXMinId,
                          _widget.labelFFTAccXMaxId,
                          _widget.labelFFTAccYMinId,
                          _widget.labelFFTAccYMaxId,
                          _widget.labelFFTAccZMinId,
                          _widget.labelFFTAccZMaxId);

            setup_plotter(_widget.verticalLayoutThumbPlotter,
                          _fft.finger_datas.at(4),
                          _widget.labelFFTNyquistBinRe_thumb,
                          _widget.labelFFTMinTh,
                          _widget.labelFFTMaxTh,
                          _widget.labelFFTAccXMinTh,
                          _widget.labelFFTAccXMaxTh,
                          _widget.labelFFTAccYMinTh,
                          _widget.labelFFTAccYMaxTh,
                          _widget.labelFFTAccZMinTh,
                          _widget.labelFFTAccZMaxTh);

            _fft.fft_in.resize(max_n_fft * 2 + 64, 0);
            _fft.fft_out.resize(max_n_fft, 0);
            plotterSettingsChanged();
        }
        //connect fft
        {
            connect(_widget.spinBoxFFTNumIterations,  qOverload<int>(&QSpinBox::valueChanged),          this, &T::plotterSettingsChanged);
            connect(_widget.doubleSpinBoxFFTValFrom,  qOverload<double>(&QDoubleSpinBox::valueChanged), this, &T::plotterSettingsChanged);
            connect(_widget.doubleSpinBoxFFTValTo,    qOverload<double>(&QDoubleSpinBox::valueChanged), this, &T::plotterSettingsChanged);
            connect(_widget.spinBoxFFTInSize,         qOverload<int>(&QSpinBox::valueChanged),          this, &T::plotterSettingsChanged);
            connect(_widget.comboBoxFFTColormap,      qOverload<int>(&QComboBox::currentIndexChanged),  this, &T::plotterSettingsChanged);
            connect(_widget.doubleSpinBoxFFTFreq,     qOverload<double>(&QDoubleSpinBox::valueChanged), this, &T::plotterSettingsChanged);
            connect(_widget.pushButtonFFTResetMinMax, &QPushButton::clicked,                            this, &T::fftResetMinMax);
        }
        //connect test
        {
            connect(_widget.pushButtonTestStaticPlotterWidget, &QPushButton::clicked, this, &T::testStaticPlotterWidget);
        }
        //connect MGF exec
        {
            connect(_widget.pushButtonMGFStop,            &QPushButton::clicked, this, &T::mgfStop);
            connect(_widget.pushButtonMGFAutoCTRL,        &QPushButton::clicked, this, &T::mgfAutoCTRL);
            connect(_widget.pushButtonMGFAutoPWM,         &QPushButton::clicked, this, &T::mgfAutoPWM);
            connect(_widget.pushButtonMGFMoveDown,        &QPushButton::clicked, this, &T::mgfMoveDown);
            connect(_widget.pushButtonMGFMoveTraj,        &QPushButton::clicked, this, &T::mgfMoveTraj);
            connect(_widget.pushButtonMGFMoveUp,          &QPushButton::clicked, this, &T::mgfMoveUp);
            connect(_widget.pushButtonMGFCloseCTRL,       &QPushButton::clicked, this, &T::mgfCloseCTRL);
            connect(_widget.pushButtonMGFClosePWM,        &QPushButton::clicked, this, &T::mgfClosePWM);
            connect(_widget.pushButtonMGFMoveToGrasp,     &QPushButton::clicked, this, &T::mgfMoveToGrasp);
            connect(_widget.pushButtonMGFMoveToInit,      &QPushButton::clicked, this, &T::mgfMoveToInit);
            connect(_widget.pushButtonMGF0TorqueStop,     &QPushButton::clicked, this, &T::mgf0TorqueStop);
            connect(_widget.pushButtonMGFSetGrasp,        &QPushButton::clicked, this, &T::mgfSetGrasp);
            connect(_widget.pushButtonMGFSetPre,          &QPushButton::clicked, this, &T::mgfSetPre);
            connect(_widget.pushButtonMGFSetInit,         &QPushButton::clicked, this, &T::mgfSetInit);
            connect(_widget.pushButtonMGF0TorqueActive,   &QPushButton::clicked, this, &T::mgf0TorqueActive);
            connect(_widget.pushButtonMGFOpenPWM,         &QPushButton::clicked, this, &T::mgfOpenPWM);
            connect(_widget.pushButtonMGFOpenCTRL,        &QPushButton::clicked, this, &T::mgfOpenCTRL);
            connect(_widget.pushButtonMGFMoveRetreat,     &QPushButton::clicked, this, &T::mgfMoveRetreat);
            connect(_widget.pushButtonMGFCalibrateHand,   &QPushButton::clicked, this, &T::mgfCalibrateHand);
            connect(_widget.pushButtonMGFSendCfg,         &QPushButton::clicked, this, &T::mgfSendConfig);
            connect(_widget.pushButtonMGFStartRampingPWM, &QPushButton::clicked, this, &T::mgfRampPWM);
            connect(_widget.pushButtonMGFCtrlReleaseFlag, &QPushButton::clicked, this, &T::mgfCtrlReleaseFlag);
        }
        ARMARX_INFO << "starting timer";
        startTimer(10);
        ARMARX_INFO << "ctor...done!";
        KITSensorizedSoftFingerHand::V1::NJointMinimalGraspingForceV1ControllerConfig c;
        KITSensorizedSoftFingerHand::V1::make_default_cfg(c);
        setMGFCfg(c);
    }

    SensorizedSoftFingerHandMinimalForceGraspingWidgetController::~SensorizedSoftFingerHandMinimalForceGraspingWidgetController()
    {}

    void SensorizedSoftFingerHandMinimalForceGraspingWidgetController::onConnectComponent()
    {
        ARMARX_INFO << "onConnectComponent";
        ARMARX_TRACE;
        std::lock_guard g{_all_mutex};
        getTopic(_static_plotter, _static_plotter_name);
        ARMARX_INFO << VAROUT(_hand_name);
        if (_fake_mode)
        {
            return;
        }
        ARMARX_TRACE;
        ARMARX_INFO << "get robot model";
        {
            if (getRobotStateComponent())
            {
                _mgf.robot = addRobot("mgf robot", VirtualRobot::RobotIO::eStructure);
                _mgf.rnh_robot_arm = getRobotNameHelper().getRobotArm("Right", _mgf.robot);
            }
        }
        ARMARX_INFO << "setup data streaming";
        {
            ARMARX_CHECK_NULL(_streaming_handler);
            RobotUnitDataStreaming::Config cfg;
            if (_mgf.robot)
            {
                for (const auto& n : *_mgf.rnh_robot_arm.getKinematicChain())
                {
                    cfg.loggingNames.emplace_back("sens." + n->getName() + ".position");
                }
            }
            for (const auto& fname :
                 {"little", "ring", "middle", "index", "thumb"
                 })
            {
                const std::string base = "sens." + _hand_name + ".finger." + fname + ".";
                cfg.loggingNames.emplace_back(base + "accelerometer_fifo_length");
                cfg.loggingNames.emplace_back(base + "frame_counter");
                cfg.loggingNames.emplace_back(base + "proximity");
                cfg.loggingNames.emplace_back(base + "raw_");
            }
            cfg.loggingNames.emplace_back("sens." + _hand_name + ".motor");
            cfg.loggingNames.emplace_back("sens." + _hand_name + ".raw_position");
            cfg.loggingNames.emplace_back("sens." + _hand_name + ".raw_velocity");
            cfg.loggingNames.emplace_back("sens." + _hand_name + ".frame_counter");
            cfg.loggingNames.emplace_back("ctrl." + _hand_name + ".motor.other.ControlMode_PWM1DoF.pwm_motor");
            cfg.loggingNames.emplace_back("ctrl." + _hand_name + ".motor.index.ControlMode_PWM1DoF.pwm_motor");
            cfg.loggingNames.emplace_back("ctrl." + _hand_name + ".motor.thumb.ControlMode_PWM1DoF.pwm_motor");
            _streaming_handler = getRobotUnitComponentPlugin().startDataSatreming(cfg);
            //get idx
            ARMARX_TRACE;
            //dump entries
            ARMARX_INFO << _streaming_handler->getDataDescriptionString();

            const auto init_finger = [&](const std::string & fn, auto & keys)
            {
                ARMARX_TRACE;
                _streaming_handler->getDataEntryReader(keys.acc_fifo_len, fn + ".accelerometer_fifo_length");
                for (std::size_t i = 0; i < 16; ++i)
                {
                    const auto ffpre = fn + ".raw_accelerometer_fifo.element_" + std::to_string(i);
                    for (std::size_t j = 0; j < 3; ++j)
                    {
                        _streaming_handler->getDataEntryReader(keys.acc_fifo.at(i).at(j), ffpre + ".element_" + std::to_string(j));
                    }
                }
            };
            ARMARX_TRACE;
            init_finger("sens." + _hand_name + ".finger.little", _keys.little);
            init_finger("sens." + _hand_name + ".finger.ring",   _keys.ring);
            init_finger("sens." + _hand_name + ".finger.middle", _keys.middle);
            init_finger("sens." + _hand_name + ".finger.index",  _keys.index);
            init_finger("sens." + _hand_name + ".finger.thumb",  _keys.thumb);
        }
        ARMARX_INFO << "init controllers";
        {
            ARMARX_INFO << "init wp controller";
            if (_mgf.robot)
            {
                NJointCartesianWaypointControllerConfigPtr cfg_wp = new NJointCartesianWaypointControllerConfig;
                cfg_wp->rns                = _mgf.rnh_robot_arm.getArm().getKinematicChain();
                cfg_wp->ftName             = "FT R";
                cfg_wp->referenceFrameName = "root";
                getRobotUnitComponentPlugin().createNJointController(
                    _mgf.ctrl_wp,
                    "NJointCartesianWaypointController",
                    "SensorizedSoftFingerHandMinimalForceGraspingWidget_wp",
                    cfg_wp
                );
            }
            ARMARX_INFO << "init MGF controller";
            {
                namespace MGF = KITSensorizedSoftFingerHand::V1;
                MGF::NJointMinimalGraspingForceV1ControllerConfigPtr cfg_mgf =
                    new MGF::NJointMinimalGraspingForceV1ControllerConfig;
                *cfg_mgf = getMGFCfg();
                getRobotUnitComponentPlugin().createNJointController(
                    _mgf.ctrl_mgf,
                    "KITSensorizedSoftFingerHandV1_GraspPhaseControllerV1_NJointController",
                    "SensorizedSoftFingerHandMinimalForceGraspingWidget_mgf",
                    cfg_mgf
                );
            }
            //csv
            {
                std::stringstream str;
                str << "iteration;timestep";
                for (const auto& [k, v] : _streaming_handler->getDataDescription().entries)
                {
                    str << ';' << k;
                    _mgf.csv_fields.emplace_back(v);
                }
                str << ";mgf_ctrl_data.updated"
                    << ";mgf_ctrl_data.grasp_phase"
                    << ";sensor_time_stamp_us_ice"
                    << ";time_stamp_us_std"
                    << ";updating_sensors"
                    << ";resetting_controller"
                    << ";run_controller"
                    << ";all_shear_avg_len_xy"
                    << ";all_shear_avg_len_z"
                    << ";all_shear_avg_ratio"
                    << ";last_fpga_dt"
                    << ";finger_active_shear_avg_len_xy"
                    << ";finger_active_shear_avg_len_z"
                    << ";finger_active_shear_avg_ratio"
                    << ";finger_active_shear_num"
                    << ";active_shear_avg_len_xy"
                    << ";active_shear_avg_len_z"
                    << ";active_shear_avg_ratio"
                    << ";active_shear_num "
                    << ";used_shear_avg_ratio"
                    << ";negative_z_shear_avg_len_xy"
                    << ";negative_z_shear_avg_len_z"
                    << ";negative_z_shear_avg_ratio"
                    << ";negative_z_shear_num"
                    << ";all_shear_avg_len_z_pressure_sensors"
                    << ";all_shear_avg_ratio_pressure_sensors"
                    << ";open_signal_finger_count"
                    << ";open_signal";

                const auto add_motor = [&](const std::string & m)
                {
                    str << ';' << m << ".grasp_phase"
                        << ';' << m << ".squish_distance"
                        << ';' << m << ".last_shear_pressure_position"
                        << ';' << m << ".pos_ctrl_target"
                        << ';' << m << ".normal_force_controller_position_delta"
                        << ';' << m << ".shear_force_controller_position_delta"
                        << ';' << m << ".shear_force_min_xy_len"
                        << ';' << m << ".shear_force_min_z_len"
                        << ';' << m << ".shear_force_min_ratio"
                        << ';' << m << ".shear_force_avg_ratio"
                        << ';' << m << ".shear_force_used_ratio"
                        << ';' << m << ".shear_force_used_ratio_num"
                        << ';' << m << ".slip_detection_value"
                        << ';' << m << ".shear_force_ratio_number_of_active_sensors"
                        << ';' << m << ".maximum_pressure"
                        << ';' << m << ".pressure_on_start_unload"
                        << ';' << m << ".target_shear"
                        << ';' << m << ".target_pressure"
                        << ';' << m << ".contact_detection_flag"
                        << ';' << m << ".gross_slip_count"
                        << ';' << m << ".finger_did_close"
                        << ';' << m << ".monotonic_shear_pressure_control_last_pwm"
                        << ';' << m << ".monotonic_shear_pressure_control_last_pwm_pressure"
                        << ';' << m << ".monotonic_shear_pressure_control_last_pwm_shear"
                        << ';' << m << ".monotonic_shear_pressure_control_last_pwm_slip"
                        << ';' << m << ".monotonic_shear_pressure_control_last_delta_pressure"
                        << ';' << m << ".monotonic_shear_pressure_control_last_delta_shear"
                        << ';' << m << ".target_pressure_rate_of_change"
                        << ';' << m << ".pressure_rate_of_change"
                        << ';' << m << ".unload_phase_controller_normal_force_via_pwm_last_pwm";
                };
                add_motor("mgf_ctrl_data.motor_other");
                add_motor("mgf_ctrl_data.motor_index");
                add_motor("mgf_ctrl_data.motor_thumb");

                const auto add_finger = [&](const std::string & f)
                {
                    str << ';' << f << ".fit_prox_x"
                        << ';' << f << ".fit_dist_x"
                        << ';' << f << ".fit_prox_z"
                        << ';' << f << ".fit_dist_z"
                        << ';' << f << ".fit_prox_avg"
                        << ';' << f << ".fit_dist_avg";
                    auto add_nf = [&](const std::string & n)
                    {
                        str << ';' << n << ".current"
                            << ';' << n << ".continuous_mean"
                            << ';' << n << ".current_mean_free_scaled"
                            << ';' << n << ".current_continuous_mean_free_scaled";
                    };
                    add_nf(f + ".nf_prox_l");
                    add_nf(f + ".nf_prox_r");
                    add_nf(f + ".nf_dist_joint");
                    add_nf(f + ".nf_dist_tip");
                    auto add_sf = [&](const std::string & s)
                    {
                        str << ';' << s << ".len_xy"
                            << ';' << s << ".len_z"
                            << ';' << s << ".ratio"

                            << ';' << s << ".x_current"
                            << ';' << s << ".x_continuous_mean"
                            << ';' << s << ".x_current_mean_free_scaled"
                            << ';' << s << ".x_current_continuous_mean_free_scaled"

                            << ';' << s << ".y_current"
                            << ';' << s << ".y_continuous_mean"
                            << ';' << s << ".y_current_mean_free_scaled"
                            << ';' << s << ".y_current_continuous_mean_free_scaled"

                            << ';' << s << ".z_current"
                            << ';' << s << ".z_continuous_mean"
                            << ';' << s << ".z_current_mean_free_scaled"
                            << ';' << s << ".z_current_continuous_mean_free_scaled";
                    };
                    add_sf(f + ".shear_force_dist_joint");
                    add_sf(f + ".shear_force_dist_tip");

                    str << ';' << f << ".accelerometer.shear_force_criterium"
                        << ';' << f << ".accelerometer.fft_contact_detect_energy";
                };
                add_finger("mgf_ctrl_data.finger_little");
                add_finger("mgf_ctrl_data.finger_ring");
                add_finger("mgf_ctrl_data.finger_middle");
                add_finger("mgf_ctrl_data.finger_index");
                add_finger("mgf_ctrl_data.finger_thumb");
                str << "\n";
                _mgf.csv_header = str.str();
                _mgf.csv_header.back() = '\n';
            }
        }


    }
    void SensorizedSoftFingerHandMinimalForceGraspingWidgetController::onDisconnectComponent()
    {
        ARMARX_INFO << "onDisconnectComponent";
        ARMARX_TRACE;
        std::lock_guard g{_all_mutex};
        _mgf.stop_task();
        ARMARX_INFO << "stop data streaming";
        _streaming_handler = nullptr;
    }
}
//grasp exec - cfg
namespace armarx
{
    void
    SensorizedSoftFingerHandMinimalForceGraspingWidgetController::setMGFCfg(
        const KITSensorizedSoftFingerHand::V1::NJointMinimalGraspingForceV1ControllerConfig& c)
    {
        const auto f = [](const auto s, auto t)
        {
            double max = std::max(std::abs(s) * 100.0, 5000.0);
            t->setMaximum(max);
            t->setMinimum(-max);
            t->setDecimals(8);
            t->setValue(s);
        };
        f(c.motor_other.pid_pos.p,                           _widget.doubleSpinBoxPosPo);
        f(c.motor_other.pid_pos.i,                           _widget.doubleSpinBoxPosIo);
        f(c.motor_other.pid_pos.d,                           _widget.doubleSpinBoxPosDo);
        f(c.motor_other.pid_pressure.p,                      _widget.doubleSpinBoxPressPo);
        f(c.motor_other.pid_pressure.i,                      _widget.doubleSpinBoxPressIo);
        f(c.motor_other.pid_pressure.d,                      _widget.doubleSpinBoxPressDo);
        f(c.motor_other.pid_shear.p,                         _widget.doubleSpinBoxShearPo);
        f(c.motor_other.pid_shear.i,                         _widget.doubleSpinBoxShearIo);
        f(c.motor_other.pid_shear.d,                         _widget.doubleSpinBoxShearDo);
        f(c.motor_other.max_pwm,                             _widget.doubleSpinBoxPwmMaxo);
        f(c.motor_other.pwm_open,                            _widget.doubleSpinBoxPwmOpeno);
        f(c.motor_other.pwm_freely_moving,                   _widget.doubleSpinBoxPwmFreeo);
        f(c.motor_other.min_pwm_close_to_contact,            _widget.doubleSpinBoxPwmClMino);
        f(c.motor_other.target_pressure,                     _widget.doubleSpinBoxTargPreso);
        f(c.motor_other.target_shear,                        _widget.doubleSpinBoxTargShearo);
        f(c.motor_other.free_phase_start_moving_delay_msec,  _widget.doubleSpinBoxDelayUseco);
        f(c.motor_other.gross_slip_pos_step,                 _widget.doubleSpinBoxSlipDeltaPoso);
        f(c.motor_other.gross_slip_threshold,                _widget.doubleSpinBoxSlipThresho);

        f(c.motor_index.pid_pos.p,                           _widget.doubleSpinBoxPosPi);
        f(c.motor_index.pid_pos.i,                           _widget.doubleSpinBoxPosIi);
        f(c.motor_index.pid_pos.d,                           _widget.doubleSpinBoxPosDi);
        f(c.motor_index.pid_pressure.p,                      _widget.doubleSpinBoxPressPi);
        f(c.motor_index.pid_pressure.i,                      _widget.doubleSpinBoxPressIi);
        f(c.motor_index.pid_pressure.d,                      _widget.doubleSpinBoxPressDi);
        f(c.motor_index.pid_shear.p,                         _widget.doubleSpinBoxShearPi);
        f(c.motor_index.pid_shear.i,                         _widget.doubleSpinBoxShearIi);
        f(c.motor_index.pid_shear.d,                         _widget.doubleSpinBoxShearDi);
        f(c.motor_index.max_pwm,                             _widget.doubleSpinBoxPwmMaxi);
        f(c.motor_index.pwm_open,                            _widget.doubleSpinBoxPwmOpeni);
        f(c.motor_index.pwm_freely_moving,                   _widget.doubleSpinBoxPwmFreei);
        f(c.motor_index.min_pwm_close_to_contact,            _widget.doubleSpinBoxPwmClMini);
        f(c.motor_index.target_pressure,                     _widget.doubleSpinBoxTargPresi);
        f(c.motor_index.target_shear,                        _widget.doubleSpinBoxTargSheari);
        f(c.motor_index.free_phase_start_moving_delay_msec,  _widget.doubleSpinBoxDelayUseci);
        f(c.motor_index.gross_slip_pos_step,                 _widget.doubleSpinBoxSlipDeltaPosi);
        f(c.motor_index.gross_slip_threshold,                _widget.doubleSpinBoxSlipThreshi);

        f(c.motor_thumb.pid_pos.p,                           _widget.doubleSpinBoxPosPt);
        f(c.motor_thumb.pid_pos.i,                           _widget.doubleSpinBoxPosIt);
        f(c.motor_thumb.pid_pos.d,                           _widget.doubleSpinBoxPosDt);
        f(c.motor_thumb.pid_pressure.p,                      _widget.doubleSpinBoxPressPt);
        f(c.motor_thumb.pid_pressure.i,                      _widget.doubleSpinBoxPressIt);
        f(c.motor_thumb.pid_pressure.d,                      _widget.doubleSpinBoxPressDt);
        f(c.motor_thumb.pid_shear.p,                         _widget.doubleSpinBoxShearPt);
        f(c.motor_thumb.pid_shear.i,                         _widget.doubleSpinBoxShearIt);
        f(c.motor_thumb.pid_shear.d,                         _widget.doubleSpinBoxShearDt);
        f(c.motor_thumb.max_pwm,                             _widget.doubleSpinBoxPwmMaxt);
        f(c.motor_thumb.pwm_open,                            _widget.doubleSpinBoxPwmOpent);
        f(c.motor_thumb.pwm_freely_moving,                   _widget.doubleSpinBoxPwmFreet);
        f(c.motor_thumb.min_pwm_close_to_contact,            _widget.doubleSpinBoxPwmClMint);
        f(c.motor_thumb.target_pressure,                     _widget.doubleSpinBoxTargPrest);
        f(c.motor_thumb.target_shear,                        _widget.doubleSpinBoxTargSheart);
        f(c.motor_thumb.free_phase_start_moving_delay_msec,  _widget.doubleSpinBoxDelayUsect);
        f(c.motor_thumb.gross_slip_pos_step,                 _widget.doubleSpinBoxSlipDeltaPost);
        f(c.motor_thumb.gross_slip_threshold,                _widget.doubleSpinBoxSlipThresht);

        f(c.motor_other.encoder_delta_for_movent,            _widget.doubleSpinBoxEncMoveDeltao);
        f(c.motor_index.encoder_delta_for_movent,            _widget.doubleSpinBoxEncMoveDeltai);
        f(c.motor_thumb.encoder_delta_for_movent,            _widget.doubleSpinBoxEncMoveDeltat);

        f(c.motor_other.gross_slip_pwm_step,            _widget.doubleSpinBoxSlipDeltaPwmo);
        f(c.motor_index.gross_slip_pwm_step,            _widget.doubleSpinBoxSlipDeltaPwmi);
        f(c.motor_thumb.gross_slip_pwm_step,            _widget.doubleSpinBoxSlipDeltaPwmt);

        f(c.motor_other.pid_unload.p,                         _widget.doubleSpinBoxUnloadPo);
        f(c.motor_other.pid_unload.i,                         _widget.doubleSpinBoxUnloadIo);
        f(c.motor_other.pid_unload.d,                         _widget.doubleSpinBoxUnloadDo);
        f(c.motor_index.pid_unload.p,                         _widget.doubleSpinBoxUnloadPi);
        f(c.motor_index.pid_unload.i,                         _widget.doubleSpinBoxUnloadIi);
        f(c.motor_index.pid_unload.d,                         _widget.doubleSpinBoxUnloadDi);
        f(c.motor_thumb.pid_unload.p,                         _widget.doubleSpinBoxUnloadPt);
        f(c.motor_thumb.pid_unload.i,                         _widget.doubleSpinBoxUnloadIt);
        f(c.motor_thumb.pid_unload.d,                         _widget.doubleSpinBoxUnloadDt);

        f(c.motor_other.pid_force.p,                         _widget.doubleSpinBoxForcePo);
        f(c.motor_other.pid_force.i,                         _widget.doubleSpinBoxForceIo);
        f(c.motor_other.pid_force.d,                         _widget.doubleSpinBoxForceDo);
        f(c.motor_index.pid_force.p,                         _widget.doubleSpinBoxForcePi);
        f(c.motor_index.pid_force.i,                         _widget.doubleSpinBoxForceIi);
        f(c.motor_index.pid_force.d,                         _widget.doubleSpinBoxForceDi);
        f(c.motor_thumb.pid_force.p,                         _widget.doubleSpinBoxForcePt);
        f(c.motor_thumb.pid_force.i,                         _widget.doubleSpinBoxForceIt);
        f(c.motor_thumb.pid_force.d,                         _widget.doubleSpinBoxForceDt);
        f(c.motor_other.pid_force_forward_p,            _widget.doubleSpinBoxSlipForceFwdPwmo);
        f(c.motor_index.pid_force_forward_p,            _widget.doubleSpinBoxSlipForceFwdPwmi);
        f(c.motor_thumb.pid_force_forward_p,            _widget.doubleSpinBoxSlipForceFwdPwmt);
        f(c.motor_other.pid_force_limit,            _widget.doubleSpinBoxForcePidLimito);
        f(c.motor_index.pid_force_limit,            _widget.doubleSpinBoxForcePidLimiti);
        f(c.motor_thumb.pid_force_limit,            _widget.doubleSpinBoxForcePidLimitt);

        f(c.pwm_scaling,                                     _widget.doubleSpinBoxPwmScaling);
        f(c.min_load_lift_hold_duration,                     _widget.doubleSpinBoxMinLiftHoldDur);
        f(c.place_fft_energy_threshold,                      _widget.doubleSpinBoxFFTEnergyThresh);
        f(c.place_finger_trigger_count,                      _widget.doubleSpinBoxPlaceFFTFingerCnt);

        _widget.checkBoxAlwaysExecFpgaCtrl->setChecked(c.always_execute_controllers_depending_on_fpga);
        _widget.checkBoxLLHPresRelPWM->setChecked(c.load_lift_hold_pressure_relative_pwm);
        _widget.checkBoxLLHShearRelPWM->setChecked(c.load_lift_hold_shear_relative_pwm);
        _widget.checkBoxMGFShaerXYOffset->setChecked(c.use_shear_xy_offset);
        _widget.checkBoxMGFCtrlPressOnOther->setChecked(c.control_pressure_on_other);
        _widget.spinBoxMinContactsForShearControl->setValue(c.min_contacts_for_shear_control);
    }
    KITSensorizedSoftFingerHand::V1::NJointMinimalGraspingForceV1ControllerConfig
    SensorizedSoftFingerHandMinimalForceGraspingWidgetController::getMGFCfg() const
    {
        KITSensorizedSoftFingerHand::V1::NJointMinimalGraspingForceV1ControllerConfig c;
        KITSensorizedSoftFingerHand::V1::make_default_cfg(c);
        c.hand_name = _hand_name;
        const auto f = [](auto & t, auto & s)
        {
            t = s->value();
        };
        f(c.motor_other.pid_pos.p,                           _widget.doubleSpinBoxPosPo);
        f(c.motor_other.pid_pos.i,                           _widget.doubleSpinBoxPosIo);
        f(c.motor_other.pid_pos.d,                           _widget.doubleSpinBoxPosDo);
        f(c.motor_other.pid_pressure.p,                      _widget.doubleSpinBoxPressPo);
        f(c.motor_other.pid_pressure.i,                      _widget.doubleSpinBoxPressIo);
        f(c.motor_other.pid_pressure.d,                      _widget.doubleSpinBoxPressDo);
        f(c.motor_other.pid_shear.p,                         _widget.doubleSpinBoxShearPo);
        f(c.motor_other.pid_shear.i,                         _widget.doubleSpinBoxShearIo);
        f(c.motor_other.pid_shear.d,                         _widget.doubleSpinBoxShearDo);
        f(c.motor_other.max_pwm,                             _widget.doubleSpinBoxPwmMaxo);
        f(c.motor_other.pwm_open,                            _widget.doubleSpinBoxPwmOpeno);
        f(c.motor_other.pwm_freely_moving,                   _widget.doubleSpinBoxPwmFreeo);
        f(c.motor_other.min_pwm_close_to_contact,            _widget.doubleSpinBoxPwmClMino);
        f(c.motor_other.target_pressure,                     _widget.doubleSpinBoxTargPreso);
        f(c.motor_other.target_shear,                        _widget.doubleSpinBoxTargShearo);
        f(c.motor_other.free_phase_start_moving_delay_msec,  _widget.doubleSpinBoxDelayUseco);
        f(c.motor_other.gross_slip_pos_step,                 _widget.doubleSpinBoxSlipDeltaPoso);
        f(c.motor_other.gross_slip_threshold,                _widget.doubleSpinBoxSlipThresho);

        f(c.motor_index.pid_pos.p,                           _widget.doubleSpinBoxPosPi);
        f(c.motor_index.pid_pos.i,                           _widget.doubleSpinBoxPosIi);
        f(c.motor_index.pid_pos.d,                           _widget.doubleSpinBoxPosDi);
        f(c.motor_index.pid_pressure.p,                      _widget.doubleSpinBoxPressPi);
        f(c.motor_index.pid_pressure.i,                      _widget.doubleSpinBoxPressIi);
        f(c.motor_index.pid_pressure.d,                      _widget.doubleSpinBoxPressDi);
        f(c.motor_index.pid_shear.p,                         _widget.doubleSpinBoxShearPi);
        f(c.motor_index.pid_shear.i,                         _widget.doubleSpinBoxShearIi);
        f(c.motor_index.pid_shear.d,                         _widget.doubleSpinBoxShearDi);
        f(c.motor_index.max_pwm,                             _widget.doubleSpinBoxPwmMaxi);
        f(c.motor_index.pwm_open,                            _widget.doubleSpinBoxPwmOpeni);
        f(c.motor_index.pwm_freely_moving,                   _widget.doubleSpinBoxPwmFreei);
        f(c.motor_index.min_pwm_close_to_contact,            _widget.doubleSpinBoxPwmClMini);
        f(c.motor_index.target_pressure,                     _widget.doubleSpinBoxTargPresi);
        f(c.motor_index.target_shear,                        _widget.doubleSpinBoxTargSheari);
        f(c.motor_index.free_phase_start_moving_delay_msec,  _widget.doubleSpinBoxDelayUseci);
        f(c.motor_index.gross_slip_pos_step,                 _widget.doubleSpinBoxSlipDeltaPosi);
        f(c.motor_index.gross_slip_threshold,                _widget.doubleSpinBoxSlipThreshi);

        f(c.motor_thumb.pid_pos.p,                           _widget.doubleSpinBoxPosPt);
        f(c.motor_thumb.pid_pos.i,                           _widget.doubleSpinBoxPosIt);
        f(c.motor_thumb.pid_pos.d,                           _widget.doubleSpinBoxPosDt);
        f(c.motor_thumb.pid_pressure.p,                      _widget.doubleSpinBoxPressPt);
        f(c.motor_thumb.pid_pressure.i,                      _widget.doubleSpinBoxPressIt);
        f(c.motor_thumb.pid_pressure.d,                      _widget.doubleSpinBoxPressDt);
        f(c.motor_thumb.pid_shear.p,                         _widget.doubleSpinBoxShearPt);
        f(c.motor_thumb.pid_shear.i,                         _widget.doubleSpinBoxShearIt);
        f(c.motor_thumb.pid_shear.d,                         _widget.doubleSpinBoxShearDt);
        f(c.motor_thumb.max_pwm,                             _widget.doubleSpinBoxPwmMaxt);
        f(c.motor_thumb.pwm_open,                            _widget.doubleSpinBoxPwmOpent);
        f(c.motor_thumb.pwm_freely_moving,                   _widget.doubleSpinBoxPwmFreet);
        f(c.motor_thumb.min_pwm_close_to_contact,            _widget.doubleSpinBoxPwmClMint);
        f(c.motor_thumb.target_pressure,                     _widget.doubleSpinBoxTargPrest);
        f(c.motor_thumb.target_shear,                        _widget.doubleSpinBoxTargSheart);
        f(c.motor_thumb.free_phase_start_moving_delay_msec,  _widget.doubleSpinBoxDelayUsect);
        f(c.motor_thumb.gross_slip_pos_step,                 _widget.doubleSpinBoxSlipDeltaPost);
        f(c.motor_thumb.gross_slip_threshold,                _widget.doubleSpinBoxSlipThresht);

        f(c.motor_other.encoder_delta_for_movent,            _widget.doubleSpinBoxEncMoveDeltao);
        f(c.motor_index.encoder_delta_for_movent,            _widget.doubleSpinBoxEncMoveDeltai);
        f(c.motor_thumb.encoder_delta_for_movent,            _widget.doubleSpinBoxEncMoveDeltat);

        f(c.motor_other.gross_slip_pwm_step,            _widget.doubleSpinBoxSlipDeltaPwmo);
        f(c.motor_index.gross_slip_pwm_step,            _widget.doubleSpinBoxSlipDeltaPwmi);
        f(c.motor_thumb.gross_slip_pwm_step,            _widget.doubleSpinBoxSlipDeltaPwmt);

        f(c.motor_other.pid_unload.p,                         _widget.doubleSpinBoxUnloadPo);
        f(c.motor_other.pid_unload.i,                         _widget.doubleSpinBoxUnloadIo);
        f(c.motor_other.pid_unload.d,                         _widget.doubleSpinBoxUnloadDo);
        f(c.motor_index.pid_unload.p,                         _widget.doubleSpinBoxUnloadPi);
        f(c.motor_index.pid_unload.i,                         _widget.doubleSpinBoxUnloadIi);
        f(c.motor_index.pid_unload.d,                         _widget.doubleSpinBoxUnloadDi);
        f(c.motor_thumb.pid_unload.p,                         _widget.doubleSpinBoxUnloadPt);
        f(c.motor_thumb.pid_unload.i,                         _widget.doubleSpinBoxUnloadIt);
        f(c.motor_thumb.pid_unload.d,                         _widget.doubleSpinBoxUnloadDt);

        f(c.motor_other.pid_force.p,                         _widget.doubleSpinBoxForcePo);
        f(c.motor_other.pid_force.i,                         _widget.doubleSpinBoxForceIo);
        f(c.motor_other.pid_force.d,                         _widget.doubleSpinBoxForceDo);
        f(c.motor_index.pid_force.p,                         _widget.doubleSpinBoxForcePi);
        f(c.motor_index.pid_force.i,                         _widget.doubleSpinBoxForceIi);
        f(c.motor_index.pid_force.d,                         _widget.doubleSpinBoxForceDi);
        f(c.motor_thumb.pid_force.p,                         _widget.doubleSpinBoxForcePt);
        f(c.motor_thumb.pid_force.i,                         _widget.doubleSpinBoxForceIt);
        f(c.motor_thumb.pid_force.d,                         _widget.doubleSpinBoxForceDt);
        f(c.motor_other.pid_force_forward_p,            _widget.doubleSpinBoxSlipForceFwdPwmo);
        f(c.motor_index.pid_force_forward_p,            _widget.doubleSpinBoxSlipForceFwdPwmi);
        f(c.motor_thumb.pid_force_forward_p,            _widget.doubleSpinBoxSlipForceFwdPwmt);
        f(c.motor_other.pid_force_limit,            _widget.doubleSpinBoxForcePidLimito);
        f(c.motor_index.pid_force_limit,            _widget.doubleSpinBoxForcePidLimiti);
        f(c.motor_thumb.pid_force_limit,            _widget.doubleSpinBoxForcePidLimitt);

        f(c.pwm_scaling,                                     _widget.doubleSpinBoxPwmScaling);
        f(c.min_load_lift_hold_duration,                     _widget.doubleSpinBoxMinLiftHoldDur);
        f(c.place_fft_energy_threshold,                      _widget.doubleSpinBoxFFTEnergyThresh);
        f(c.place_finger_trigger_count,                      _widget.doubleSpinBoxPlaceFFTFingerCnt);

        c.load_lift_hold_shear_relative_pwm            = _widget.checkBoxLLHShearRelPWM->isChecked();
        c.always_execute_controllers_depending_on_fpga = _widget.checkBoxAlwaysExecFpgaCtrl->isChecked();
        c.load_lift_hold_pressure_relative_pwm         = _widget.checkBoxLLHPresRelPWM->isChecked();
        c.use_shear_xy_offset                          = _widget.checkBoxMGFShaerXYOffset->isChecked();
        c.control_pressure_on_other                    = _widget.checkBoxMGFCtrlPressOnOther->isChecked();
        c.min_contacts_for_shear_control               = _widget.spinBoxMinContactsForShearControl->value();
        return c;
    }
}
//grasp exec - set poses
namespace armarx
{
    void SensorizedSoftFingerHandMinimalForceGraspingWidgetController::mgfSetGrasp()
    {
        if (_fake_mode || !_mgf.robot)
        {
            return;
        }
        synchronizeLocalClone(_mgf.robot);
        _mgf.pose_grasp = _mgf.rnh_robot_arm.getKinematicChain()->getTCP()->getPoseInRootFrame();
        _widget.labelMGFGraspSet->setText("Set # " + QString::number(++_mgf.set_cnt_pose_grasp));
    }
    void SensorizedSoftFingerHandMinimalForceGraspingWidgetController::mgfSetPre()
    {
        if (_fake_mode || !_mgf.robot)
        {
            return;
        }
        synchronizeLocalClone(_mgf.robot);
        _mgf.pose_pre = _mgf.rnh_robot_arm.getKinematicChain()->getTCP()->getPoseInRootFrame();
        _widget.labelMGFPreSet->setText("Set # " + QString::number(++_mgf.set_cnt_pose_pre));
    }
    void SensorizedSoftFingerHandMinimalForceGraspingWidgetController::mgfSetInit()
    {
        if (_fake_mode || !_mgf.robot)
        {
            return;
        }
        synchronizeLocalClone(_mgf.robot);
        _mgf.pose_init = _mgf.rnh_robot_arm.getKinematicChain()->getTCP()->getPoseInRootFrame();
        _widget.labelMGFInitSet->setText("Set # " + QString::number(++_mgf.set_cnt_pose_init));
    }
}
//grasp exec - utility
namespace armarx
{
    void SensorizedSoftFingerHandMinimalForceGraspingWidgetController::mgfCalibrateHand()
    {
        ARMARX_CHECK_EXPRESSION(!_fake_mode);
        _mgf.ctrl_mgf->activateController();
        _mgf.ctrl_mgf->overridePWM(true, -max_pwm, -max_pwm, -max_pwm);
        std::this_thread::sleep_for(std::chrono::milliseconds{1000});
        _mgf.ctrl_mgf->overridePWM(false, 0, 0, 0);
        std::this_thread::sleep_for(std::chrono::milliseconds{500});
        _mgf.ctrl_mgf->recalibrate();
        std::this_thread::sleep_for(std::chrono::milliseconds{3000});
    }
    void SensorizedSoftFingerHandMinimalForceGraspingWidgetController::mgfSendConfig()
    {
        ARMARX_CHECK_EXPRESSION(!_fake_mode);
        _mgf.ctrl_mgf->setConfig(new KITSensorizedSoftFingerHand::V1::NJointMinimalGraspingForceV1ControllerConfig(getMGFCfg()));
    }
    void SensorizedSoftFingerHandMinimalForceGraspingWidgetController::mgf0TorqueStop()
    {
        if (_fake_mode || !_mgf.robot)
        {
            return;
        }
        NameControlModeMap ncm;
        NameValueMap ntv;

        for (const auto& n : *_mgf.rnh_robot_arm.getKinematicChain())
        {
            ncm[n->getName()] = ControlMode::eVelocityControl;
            ntv[n->getName()] = 0;
        }
        getRobotUnit()->getKinematicUnit()->switchControlMode(ncm);
        getRobotUnit()->getKinematicUnit()->setJointVelocities(ntv);
    }
    void SensorizedSoftFingerHandMinimalForceGraspingWidgetController::mgf0TorqueActive()
    {
        if (_fake_mode || !_mgf.robot)
        {
            return;
        }
        NameControlModeMap ncm;
        NameValueMap ntv;

        for (const auto& n : *_mgf.rnh_robot_arm.getKinematicChain())
        {
            ncm[n->getName()] = ControlMode::eTorqueControl;
            ntv[n->getName()] = 0;
        }
        getRobotUnit()->getKinematicUnit()->switchControlMode(ncm);
        getRobotUnit()->getKinematicUnit()->setJointTorques(ntv);
    }
}
//grasp exec - job control
namespace armarx
{
    void SensorizedSoftFingerHandMinimalForceGraspingWidgetController::mgfStop()
    {
        ARMARX_CHECK_EXPRESSION(!_fake_mode);
        mgf0TorqueStop();
        _mgf.stop_task();
        if (_mgf.ctrl_wp)
        {
            _mgf.ctrl_wp->stopMovement();
            _mgf.ctrl_wp->deactivateController();
        }
        _mgf.ctrl_mgf->overridePWM(false, 0, 0, 0);
        _mgf.ctrl_mgf->deactivateController();
        mgf0TorqueStop();
    }

    void SensorizedSoftFingerHandMinimalForceGraspingWidgetController::mgfAutoCTRL()
    {
        ARMARX_CHECK_EXPRESSION(!_fake_mode);
        _mgf.stop_task();
        _mgf.reset_task(_widget, getMGFCfg());
        _mgf.next_task.close_mode   = mgf_data::task_data::close_mode_t::mgf;
        _mgf.next_task.move_init    = true;
        _mgf.next_task.move_grasp   = true;
        _mgf.next_task.close        = true;
        _mgf.next_task.wait_close   = true;
        _mgf.next_task.move_up      = true;
        _mgf.next_task.move_traj    = true;
        _mgf.next_task.move_down    = true;
        _mgf.next_task.open         = true;
        _mgf.next_task.move_retreat = true;
        _mgf.start_task(_widget, "mgfAutoCTRL");
    }
    void SensorizedSoftFingerHandMinimalForceGraspingWidgetController::mgfRampPWM()
    {
        ARMARX_CHECK_EXPRESSION(!_fake_mode);
        _mgf.stop_task();
        _mgf.reset_task(_widget, getMGFCfg());
        _mgf.next_task.ramp_pwm.enable = true;
        _mgf.start_task(_widget, "mgfRampPWM");
    }
    void SensorizedSoftFingerHandMinimalForceGraspingWidgetController::mgfAutoPWM()
    {
        ARMARX_CHECK_EXPRESSION(!_fake_mode);
        _mgf.stop_task();
        _mgf.reset_task(_widget, getMGFCfg());
        _mgf.next_task.close_mode   = mgf_data::task_data::close_mode_t::pwm;
        _mgf.next_task.move_init    = true;
        _mgf.next_task.move_grasp   = true;
        _mgf.next_task.close        = true;
        _mgf.next_task.wait_close   = true;
        _mgf.next_task.move_up      = true;
        _mgf.next_task.move_traj    = true;
        _mgf.next_task.move_down    = true;
        _mgf.next_task.open         = true;
        _mgf.next_task.move_retreat = true;
        _mgf.start_task(_widget, "mgfAutoPWM");
    }
    void SensorizedSoftFingerHandMinimalForceGraspingWidgetController::mgfMoveDown()
    {
        ARMARX_CHECK_EXPRESSION(!_fake_mode);
        _mgf.stop_task();
        _mgf.reset_task(_widget, getMGFCfg());
        _mgf.next_task.move_down = true;
        _mgf.start_task(_widget, "mgfMoveDown");
    }
    void SensorizedSoftFingerHandMinimalForceGraspingWidgetController::mgfMoveRetreat()
    {
        ARMARX_CHECK_EXPRESSION(!_fake_mode);
        _mgf.stop_task();
        _mgf.reset_task(_widget, getMGFCfg());
        _mgf.next_task.move_retreat = true;
        _mgf.start_task(_widget, "mgfMoveRetreat");
    }
    void SensorizedSoftFingerHandMinimalForceGraspingWidgetController::mgfOpenCTRL()
    {
        ARMARX_CHECK_EXPRESSION(!_fake_mode);
        _mgf.stop_task();
        _mgf.reset_task(_widget, getMGFCfg());
        _mgf.next_task.close_mode = mgf_data::task_data::close_mode_t::mgf;
        _mgf.next_task.open       = true;
        _mgf.start_task(_widget, "mgfOpenCTRL");
    }
    void SensorizedSoftFingerHandMinimalForceGraspingWidgetController::mgfOpenPWM()
    {
        ARMARX_CHECK_EXPRESSION(!_fake_mode);
        _mgf.stop_task();
        _mgf.reset_task(_widget, getMGFCfg());
        _mgf.next_task.close_mode = mgf_data::task_data::close_mode_t::pwm;
        _mgf.next_task.open       = true;
        _mgf.start_task(_widget, "mgfOpenPWM");
    }
    void SensorizedSoftFingerHandMinimalForceGraspingWidgetController::mgfMoveTraj()
    {
        ARMARX_CHECK_EXPRESSION(!_fake_mode);
        _mgf.stop_task();
        _mgf.reset_task(_widget, getMGFCfg());
        _mgf.next_task.move_traj = true;
        _mgf.start_task(_widget, "mgfMoveTraj");
    }
    void SensorizedSoftFingerHandMinimalForceGraspingWidgetController::mgfMoveUp()
    {
        ARMARX_CHECK_EXPRESSION(!_fake_mode);
        _mgf.stop_task();
        _mgf.reset_task(_widget, getMGFCfg());
        _mgf.next_task.move_up = true;
        _mgf.start_task(_widget, "mgfMoveUp");
    }
    void SensorizedSoftFingerHandMinimalForceGraspingWidgetController::mgfCloseCTRL()
    {
        ARMARX_CHECK_EXPRESSION(!_fake_mode);
        _mgf.stop_task();
        _mgf.reset_task(_widget, getMGFCfg());
        _mgf.next_task.close = true;
        _mgf.next_task.close_mode = mgf_data::task_data::close_mode_t::mgf;
        _mgf.start_task(_widget, "mgfCloseCTRL");
    }
    void SensorizedSoftFingerHandMinimalForceGraspingWidgetController::mgfClosePWM()
    {
        ARMARX_CHECK_EXPRESSION(!_fake_mode);
        _mgf.stop_task();
        _mgf.reset_task(_widget, getMGFCfg());
        _mgf.next_task.close = true;
        _mgf.next_task.close_mode = mgf_data::task_data::close_mode_t::pwm;
        _mgf.start_task(_widget, "mgfClosePWM");
    }
    void SensorizedSoftFingerHandMinimalForceGraspingWidgetController::mgfMoveToGrasp()
    {
        ARMARX_CHECK_EXPRESSION(!_fake_mode);
        ARMARX_CHECK_EXPRESSION(!_fake_mode);
        _mgf.stop_task();
        _mgf.reset_task(_widget, getMGFCfg());
        _mgf.next_task.move_grasp = true;
        _mgf.start_task(_widget, "mgfMoveToGrasp");
    }
    void SensorizedSoftFingerHandMinimalForceGraspingWidgetController::mgfMoveToInit()
    {
        ARMARX_CHECK_EXPRESSION(!_fake_mode);
        _mgf.stop_task();
        _mgf.reset_task(_widget, getMGFCfg());
        _mgf.next_task.move_init = true;
        _mgf.start_task(_widget, "mgfMoveToInit");
    }
    void SensorizedSoftFingerHandMinimalForceGraspingWidgetController::mgfCtrlReleaseFlag()
    {
        if (_fake_mode && _mgf.ctrl_mgf)
        {
            _mgf.ctrl_mgf->startPlacing();
        }
    }
}
//grasp exec - worker
namespace armarx
{
    using mgf_data = SensorizedSoftFingerHandMinimalForceGraspingWidgetController::mgf_data;

    void SensorizedSoftFingerHandMinimalForceGraspingWidgetController::mgf_data::reset_task(
        widget_t& widget,
        KITSensorizedSoftFingerHand::V1::NJointMinimalGraspingForceV1ControllerConfig mgf_cfg)
    {
        next_task = task_data{};
        next_task.mgf_cfg = mgf_cfg;

        next_task.close_sec  = widget.doubleSpinBoxMGFCloseTime->value();
        next_task.traj_top   = widget.checkBoxMGFTrajTop->isChecked();
        next_task.pose_init  = pose_init;
        next_task.pose_grasp = pose_grasp;
        next_task.pose_pre   = pose_pre;
        next_task.down_unitl_contact = widget.checkBoxMGFMoveDownUnitilContact->isChecked();
        next_task.up_pre_traj        = widget.doubleSpinBoxMGFMoveUpDistance->value();
        next_task.place_offset       = widget.doubleSpinBoxMGFPlaceOffset->value();
        next_task.contact_force      = widget.doubleSpinBoxMGFPlaceForce->value();

        next_task.cfg_trj.skipToClosestWaypoint = false;
        next_task.cfg_trj.wpCfg.maxPositionAcceleration      = 250;
        next_task.cfg_trj.wpCfg.maxOrientationAcceleration   = 0.5;
        next_task.cfg_trj.wpCfg.maxNullspaceAcceleration     = 0.5;
        next_task.cfg_trj.wpCfg.kpJointLimitAvoidance        = 0.1;
        next_task.cfg_trj.wpCfg.jointLimitAvoidanceScale     = 0.5;
        next_task.cfg_trj.wpCfg.thresholdOrientationNear     = 0.15;
        next_task.cfg_trj.wpCfg.thresholdOrientationReached  = 0.1;
        next_task.cfg_trj.wpCfg.thresholdPositionNear        = 50;
        next_task.cfg_trj.wpCfg.thresholdPositionReached     = 5;
        next_task.cfg_trj.wpCfg.maxOriVel                    = 2;
        next_task.cfg_trj.wpCfg.maxPosVel                    = 500;
        next_task.cfg_trj.wpCfg.kpOri                        = 0.5;
        next_task.cfg_trj.wpCfg.kpPos                        = 1;

        next_task.ramp_pwm.thumb   = widget.checkBoxMGFPwmRampUseT->isChecked();
        next_task.ramp_pwm.index   = widget.checkBoxMGFPwmRampUseI->isChecked();
        next_task.ramp_pwm.other   = widget.checkBoxMGFPwmRampUseO->isChecked();
        next_task.ramp_pwm.seconds = widget.doubleSpinBoxMGFRampPwmSeconds->value();
        next_task.ramp_pwm.max = widget.doubleSpinBoxMGFRampPwmEnd->value();
        next_task.ramp_pwm.initial_close   = widget.checkBoxMGFPwmRampInitialClose->isChecked();
    }

    void mgf_data::start_task(widget_t& widget, const std::string& name)
    {
        ARMARX_INFO_S << '[' << name << "] stopping old worker";
        stop_task();
        ARMARX_CHECK_NOT_EMPTY(name);
        next_task.name = name;
        worker_stop = false;
        ARMARX_INFO_S << '[' << name << "] start_task";
        worker = std::thread {[this, tdata = next_task]{ task(tdata); }};
        //setup logging
        {
            ARMARX_INFO_S << '[' << name << "] setup logging for task";
            std::lock_guard g{csv_mutex};
            if (csv_out.is_open())
            {
                csv_out.close();
            }
            const std::string fomstr = widget.lineEditMGFLogfile->text().toStdString() +
                                       "_" + name + ".csv";
            FileSystemPathBuilder pb {fomstr};
            pb.createParentDirectories();
            csv_out.open(pb.getPath());
            csv_out << csv_header;
        }
        ARMARX_INFO_S << '[' << name << "] setup logging for task ...done";
    }
    void mgf_data::stop_task()
    {
        ARMARX_INFO_S << '[' << next_task.name << "] stopping...";
        worker_stop = true;
        if (worker.joinable())
        {
            ARMARX_INFO_S << '[' << next_task.name << "] joining...";
            worker.join();
            ARMARX_INFO_S << '[' << next_task.name << "] joining...done";
        }
        //stop logging
        {
            ARMARX_INFO_S << '[' << next_task.name << "] stop logging...";
            std::lock_guard g{csv_mutex};
            if (csv_out.is_open())
            {
                csv_out.close();
            }
            ARMARX_INFO_S << '[' << next_task.name << "] stop logging...done";
        }
        ARMARX_INFO_S << '[' << next_task.name << "] stopping...done";
    }
    void mgf_data::task(task_data tdata)
    {
        ARMARX_ON_SCOPE_EXIT
        {
            if (ctrl_wp)
            {
                ARMARX_INFO_S << "[task " << tdata.name << "] stop ctrl_wp";
                ctrl_wp->stopMovement();
                ctrl_wp->deactivateController();
            }
            ARMARX_INFO_S << "[task " << tdata.name << "] stop logging";
            std::lock_guard g{csv_mutex};
            if (csv_out.is_open())
            {
                csv_out.close();
            }
            ARMARX_INFO_S << "[task " << tdata.name << "] worker stopped";
        };

        ARMARX_INFO_S << "[task " << tdata.name << "] worker started";
        if (tdata.ramp_pwm.enable)
        {
            ctrl_mgf->overridePWM(true, -max_pwm, -max_pwm, -max_pwm);
            ctrl_mgf->activateController();
            ARMARX_INFO_S << "[task " << tdata.name << "] pwm ramping";
            ARMARX_INFO_S << "[task " << tdata.name << "]     open finger";
            for (std::size_t i = 0; i < 100 && !worker_stop; ++i)
            {
                ctrl_mgf->overridePWM(true, -max_pwm, -max_pwm, -max_pwm);
                std::this_thread::sleep_for(std::chrono::milliseconds{10});
            }
            if (tdata.ramp_pwm.initial_close)
            {
                ARMARX_INFO_S << "[task " << tdata.name << "]     closing finger";
                for (std::size_t i = 0; i < 100 && !worker_stop; ++i)
                {
                    ctrl_mgf->overridePWM(true, 300, 300, 300);
                    std::this_thread::sleep_for(std::chrono::milliseconds{10});
                }
            }
            ctrl_mgf->overridePWM(true, 0, 0, 0);
            std::this_thread::sleep_for(std::chrono::milliseconds{10});
            ARMARX_INFO_S << "[task " << tdata.name << "]     start ramping";
            const std::size_t n = tdata.ramp_pwm.seconds * 100 + 1;
            for (std::size_t i = 0; i < n && !worker_stop; ++i)
            {
                const float pwm = tdata.ramp_pwm.max / n * i;
                ctrl_mgf->overridePWM(true,
                                      tdata.ramp_pwm.other ? pwm : 0.f,
                                      tdata.ramp_pwm.index ? pwm : 0.f,
                                      tdata.ramp_pwm.thumb ? pwm : 0.f);
                std::this_thread::sleep_for(std::chrono::milliseconds{10});
            }
            ARMARX_INFO_S << "[task " << tdata.name << "]     open finger";
            for (std::size_t i = 0; i < 100 && !worker_stop; ++i)
            {
                ctrl_mgf->overridePWM(true, -max_pwm, -max_pwm, -max_pwm);
                std::this_thread::sleep_for(std::chrono::milliseconds{10});
            }
            ctrl_mgf->overridePWM(true, 0, 0, 0);
            std::this_thread::sleep_for(std::chrono::milliseconds{100});
            ctrl_mgf->overridePWM(false, 0, 0, 0);
            return;
        }

        ctrl_mgf->setConfig(new KITSensorizedSoftFingerHand::V1::NJointMinimalGraspingForceV1ControllerConfig(tdata.mgf_cfg));
        if (tdata.open)
        {
            ctrl_mgf->activateController();
        }
        if (tdata.close)
        {
            ARMARX_INFO_S << "[task " << tdata.name << "] initially open the hand";
            ctrl_mgf->activateController();
            ctrl_mgf->openMGF();
            ctrl_mgf->overridePWM(false, 0, 0, 0);
            ctrl_mgf->openMGF();
        }
        if (tdata.move_init        ||
            tdata.move_grasp   ||
            tdata.move_up      ||
            tdata.move_traj    ||
            tdata.move_down    ||
            tdata.move_retreat)
        {
            ARMARX_INFO_S << "[task " << tdata.name << "] initially stop the arm";
            if (ctrl_wp)
            {
                ctrl_wp->stopMovement();
                ctrl_wp->activateController();
            }
        }

        if (!worker_stop && (tdata.move_init || tdata.move_grasp))
        {
            task_move_approach(tdata);
        }
        if (!worker_stop && tdata.close)
        {
            task_close(tdata);
        }

        if (!worker_stop && tdata.wait_close)
        {
            task_wait_for_close(tdata);
        }
        if (!worker_stop && tdata.move_up)
        {
            task_move_up(tdata);
        }
        if (!worker_stop && tdata.move_traj)
        {
            task_move_trajectory(tdata);
        }
        if (!worker_stop && tdata.move_down)
        {
            task_move_down(tdata);
        }
        if (!worker_stop && tdata.open)
        {
            task_open(tdata);
        }
        if (!worker_stop && tdata.move_retreat)
        {
            task_move_retreat(tdata);
        }
    }
    void mgf_data::task_move_approach(const task_data& tdata)
    {
        if (!ctrl_wp)
        {
            return;
        }
        ARMARX_INFO_S << "[task " << tdata.name << "]     moving";
        std::vector<Eigen::Matrix4f> wps;
        if (tdata.move_init)
        {
            ARMARX_INFO_S << "[task " << tdata.name << "]         -> moving to init";
            wps.emplace_back(tdata.pose_init);
        }
        if (tdata.move_grasp)
        {
            wps.emplace_back(tdata.pose_pre);
            wps.emplace_back(tdata.pose_grasp);
            ARMARX_INFO_S << "[task " << tdata.name << "]         -> moving to pre";
            ARMARX_INFO_S << "[task " << tdata.name << "]         -> moving to grasp";
        }
        ctrl_wp->setConfigAndWaypoints(tdata.cfg_mv_appr, wps);
        ctrl_wp->activateController();
        while (!ctrl_wp->hasReachedTarget() && !worker_stop)
        {
            ARMARX_INFO_S <<::deactivateSpam(1) << "[task " << tdata.name << "]      moving... waiting for target reached";
        }
        ctrl_wp->stopMovement();
    }
    void mgf_data::task_close(const task_data& tdata)
    {
        ARMARX_INFO_S << "[task " << tdata.name << "]     closing hand";
        ctrl_mgf->activateController();
        switch (tdata.close_mode)
        {
            case task_data::close_mode_t::pwm:
                ctrl_mgf->overridePWM(true, max_pwm, max_pwm, max_pwm);
                break;
            case task_data::close_mode_t::mgf:
                ARMARX_INFO_S << "[task " << tdata.name << "]           current phase (pre  closeMGF): " << ctrl_mgf->currentGraspPhase();
                ctrl_mgf->overridePWM(false, 0, 0, 0);
                ctrl_mgf->closeMGF();
                ARMARX_INFO_S << "          current phase (post closeMGF): " << ctrl_mgf->currentGraspPhase();
                break;
        }
    }
    void mgf_data::task_wait_for_close(const task_data& tdata)
    {
        ARMARX_INFO_S << "[task " << tdata.name << "]     wait for closing hand";
        ARMARX_INFO_S << "[task " << tdata.name << "]           current phase: " << ctrl_mgf->currentGraspPhase();
        using enum_t = KITSensorizedSoftFingerHand::V1::GraspPhase::Phase;

        switch (tdata.close_mode)
        {
            case task_data::close_mode_t::pwm:
                std::this_thread::sleep_for(std::chrono::milliseconds{0});
                break;
            case task_data::close_mode_t::mgf:
                while ((ctrl_mgf->currentGraspPhase() !=  enum_t::LOAD_LIFT_HOLD) &&  !worker_stop)
                {
                    ARMARX_INFO_S <<::deactivateSpam(1) << "[task " << tdata.name << "]      waiting for closed. current phase: " << ctrl_mgf->currentGraspPhase();
                }
                break;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds{static_cast<long>(tdata.close_sec * 1000)});
    }
    void mgf_data::task_move_up(const task_data& tdata)
    {
        if (!ctrl_wp)
        {
            return;
        }
        ARMARX_INFO_S << "[task " << tdata.name << "]     moving up";
        std::vector<Eigen::Matrix4f> wps;
        wps.emplace_back(tdata.pose_grasp);
        wps.back()(2, 3) += tdata.up_pre_traj;
        ctrl_wp->setConfigAndWaypoints(tdata.cfg_mv_up, wps);
        ctrl_wp->activateController();
        while (!ctrl_wp->hasReachedTarget() && !worker_stop)
        {
            ARMARX_INFO_S <<::deactivateSpam(1) << "[task " << tdata.name << "]      moving... waiting for target reached";
        }
        ctrl_wp->stopMovement();
    }
    void mgf_data::task_move_trajectory(const task_data& tdata)
    {
        if (!ctrl_wp)
        {
            return;
        }
        ARMARX_INFO_S << "[task " << tdata.name << "]     moving trajectory";
        std::vector<Eigen::Matrix4f> wps;
        if (tdata.traj_top)
        {
            const Eigen::Matrix4f transform = simox::math::rpy_to_mat4f(-M_PI / 2, 0, 0) *
                                              simox::math::rpy_to_mat4f(0, 0, -60.f / 180.f * M_PI);
            for (const auto& wp : traj_wps_side)
            {
                wps.emplace_back(wp * transform);
                wps.back()(1, 3) += 150;
            }
        }
        else
        {
            wps = traj_wps_side;
        }
        ctrl_wp->setConfigAndWaypoints(tdata.cfg_trj, wps);
        ctrl_wp->activateController();
        while (!ctrl_wp->hasReachedTarget() && !worker_stop)
        {
            ARMARX_INFO_S <<::deactivateSpam(1) << "[task " << tdata.name << "]      moving... waiting for target reached";
        }
        ctrl_wp->stopMovement();
    }
    void mgf_data::task_move_down(const task_data& tdata)
    {
        if (!ctrl_wp)
        {
            ctrl_mgf->startPlacing();
            return;
        }
        ARMARX_INFO_S << "[task " << tdata.name << "]     moving down";
        std::vector<Eigen::Matrix4f> wps;
        ctrl_wp->setConfigAndWaypoints(tdata.cfg_mv_down, wps);

        wps.emplace_back(tdata.pose_grasp);
        wps.back()(2, 3) += tdata.up_pre_traj;
        //move thereactivateController
        ctrl_wp->activateController();
        ctrl_wp->setConfigAndWaypoints(tdata.cfg_mv_down, wps);
        while (!ctrl_wp->hasReachedTarget() &&
               !ctrl_wp->hasReachedForceLimit() &&
               !worker_stop)
        {
            std::this_thread::sleep_for(std::chrono::milliseconds{1});
            ARMARX_INFO_S <<::deactivateSpam(1) << "[task " << tdata.name << "]          move pre down down";
        }
        if (worker_stop)
        {
            return;
        }
        wps.clear();
        wps.emplace_back(tdata.pose_grasp);

        auto cfg_mv_down_with_thresh = tdata.cfg_mv_down;
        if (tdata.down_unitl_contact)
        {
            ctrl_wp->setCurrentFTAsOffset();
            cfg_mv_down_with_thresh.forceThreshold = tdata.contact_force;
            wps.back()(2, 3) += tdata.place_offset;
        }
        ctrl_wp->setConfigAndWaypoints(cfg_mv_down_with_thresh, wps);

        using enum_t = KITSensorizedSoftFingerHand::V1::GraspPhase::Phase;

        std::this_thread::sleep_for(std::chrono::milliseconds{500});
        switch (tdata.close_mode)
        {
            case task_data::close_mode_t::pwm:
            {
                ctrl_mgf->overridePWM(true, max_pwm, max_pwm, max_pwm);
                while (!ctrl_wp->hasReachedTarget() &&
                       !(ctrl_wp->hasReachedForceLimit() && tdata.down_unitl_contact) &&
                       !worker_stop)
                {
                    ARMARX_INFO_S <<::deactivateSpam(1) << "[task " << tdata.name << "]          still moving down";
                    std::this_thread::sleep_for(std::chrono::milliseconds{1});
                }
                std::this_thread::sleep_for(std::chrono::milliseconds{1500});
                ctrl_wp->stopMovement();
            }
            break;
            case task_data::close_mode_t::mgf:
            {
                ctrl_mgf->startPlacing();
                while (!ctrl_wp->hasReachedTarget() &&
                       !(ctrl_wp->hasReachedForceLimit() && tdata.down_unitl_contact) &&
                       (ctrl_mgf->currentGraspPhase() <=  enum_t::LOAD_LIFT_HOLD) &&
                       !worker_stop)
                {
                    ARMARX_INFO_S <<::deactivateSpam(1) << "[task " << tdata.name << "]          still moving down";
                    std::this_thread::sleep_for(std::chrono::milliseconds{1});
                }
                ctrl_wp->stopMovement();
            }
            break;
        }
    }
    void mgf_data::task_open(const task_data& tdata)
    {
        ARMARX_INFO_S << "[task " << tdata.name << "]     opening";
        using enum_t = KITSensorizedSoftFingerHand::V1::GraspPhase::Phase;

        switch (tdata.close_mode)
        {
            case task_data::close_mode_t::pwm:
            {
                ctrl_mgf->activateController();
                ctrl_mgf->overridePWM(true, -max_pwm, -max_pwm, -max_pwm);
            }
            break;
            case task_data::close_mode_t::mgf:
            {
                while ((ctrl_mgf->currentGraspPhase() >  enum_t::FREELY_MOVING) && !worker_stop)
                {
                    ARMARX_INFO_S <<::deactivateSpam(1) << "[task " << tdata.name << "]      opening current phase: " << ctrl_mgf->currentGraspPhase();
                }
                ctrl_mgf->openMGF();
            }
            break;
        }
        //wait for the hand to fully open
        std::this_thread::sleep_for(std::chrono::milliseconds{750});
    }
    void mgf_data::task_move_retreat(const task_data& tdata)
    {
        if (!ctrl_wp)
        {
            return;
        }
        ARMARX_INFO_S << "[task " << tdata.name << "]    moving retreat";
        std::vector<Eigen::Matrix4f> wps;
        wps.emplace_back(tdata.pose_grasp);
        wps.emplace_back(tdata.pose_pre);
        wps.emplace_back(tdata.pose_init);
        ctrl_wp->setConfigAndWaypoints(tdata.cfg_mv_retr, wps);
        ctrl_wp->activateController();
        while (!ctrl_wp->hasReachedTarget() && !worker_stop)
        {
            ARMARX_INFO_S <<::deactivateSpam(1) << "[task " << tdata.name << "]     moving";
        }
        ctrl_wp->stopMovement();
    }
}
//fft
namespace armarx
{
    void armarx::SensorizedSoftFingerHandMinimalForceGraspingWidgetController::plotterSettingsChanged()
    {
        const std::size_t n_fft = std::pow(2, _widget.spinBoxFFTInSize->value());
        const auto zmin = _widget.doubleSpinBoxFFTValFrom->value();
        const auto zmax = _widget.doubleSpinBoxFFTValTo->value();
        const auto iterations = _widget.spinBoxFFTNumIterations->value();
        for (auto& data : _fft.finger_datas)
        {
            data.spec_data->setInterval(Qt::XAxis, QwtInterval(0, iterations));
            data.spec_data->setFrequency(_widget.doubleSpinBoxFFTFreq->value(), n_fft / 2);
            data.spec_data->setInterval(Qt::ZAxis, QwtInterval(zmin, zmax));
            data.spec_data->setIterations(iterations);

            const std::string colormap = _widget.comboBoxFFTColormap->currentText().toStdString();
            data.spec->setColorMap(new simox::qt::SimoxQwtColorMap(colormap));
            const QwtInterval zInterval = data.spec->data()->interval(Qt::ZAxis);
            // A color bar on the right axis
            QwtScaleWidget* rightAxis = data.plot->axisWidget(QwtPlot::yRight);
            rightAxis->setColorBarEnabled(true);
            rightAxis->setColorMap(zInterval, new simox::qt::SimoxQwtColorMap(colormap));
            data.plot->setAxisScale(QwtPlot::yRight, zInterval.minValue(), zInterval.maxValue());
            data.plot->enableAxis(QwtPlot::yRight);
            data.plot->setAxisScale(QwtPlot::xBottom, 0, iterations);
        }
    }

    void SensorizedSoftFingerHandMinimalForceGraspingWidgetController::fftResetMinMax()
    {
        for (auto& data : _fft.finger_datas)
        {
            data.min_fft   = +std::numeric_limits<float>::infinity();
            data.max_fft   = -std::numeric_limits<float>::infinity();
            data.min_acc_x = +std::numeric_limits<float>::infinity();
            data.max_acc_x = -std::numeric_limits<float>::infinity();
            data.min_acc_y = +std::numeric_limits<float>::infinity();
            data.max_acc_y = -std::numeric_limits<float>::infinity();
            data.min_acc_z = +std::numeric_limits<float>::infinity();
            data.max_acc_z = -std::numeric_limits<float>::infinity();
        }

    }
}

//work
namespace armarx
{
    void SensorizedSoftFingerHandMinimalForceGraspingWidgetController::timerEvent(QTimerEvent* event)
    {
        ARMARX_TRACE;
        std::lock_guard g{_all_mutex};
        ARMARX_DEBUG << "timer! " << VAROUT(_hand_name) << " " << VAROUT(_streaming_handler);
        if (!_streaming_handler && !_fake_mode)
        {
            ARMARX_TRACE;
            ARMARX_DEBUG << "skip!";
            return;
        }
        KITSensorizedSoftFingerHand::V1::NJointMinimalGraspingForceV1ControllerDebugData mgf_ctrl_data;
        bool mgf_ctrl_data_updated = false;
        if (_mgf.ctrl_mgf && !_mgf.worker_stop)
        {
            mgf_ctrl_data_updated = true;
            mgf_ctrl_data = _mgf.ctrl_mgf->getDebugData();
            ARMARX_INFO_S <<::deactivateSpam(1) << "current phase: " << mgf_ctrl_data.grasp_phase;
        }
        //read data
        std::array<std::size_t, 5> num_new_acc_values{0, 0, 0, 0, 0};
        if (_fake_mode)
        {
            ARMARX_TRACE;
            ARMARX_DEBUG << "faking data!";
            for (std::size_t fidx = 0; fidx < 5; ++fidx)
            {
                num_new_acc_values.at(fidx) = std::uniform_int_distribution<std::size_t>(0, 16)(_gen);
                auto& fdata = _fft.finger_datas.at(fidx);
                for (std::size_t i = 0; i < num_new_acc_values.at(fidx); ++i)
                {
                    fdata.insert(std::sin(fdata.end * 1 * (fidx + 1)),
                                 std::sin(fdata.end * 2 * (fidx + 2)),
                                 std::sin(fdata.end * 3 * (fidx + 3)));
                }
            }
        }
        else
        {
            std::lock_guard mgfmutex{_mgf.csv_mutex};
            //real ingest mode
            auto& data = _streaming_handler->getDataBuffer();
            ARMARX_DEBUG << "parsing " << data.size() << " updates";
            for (const auto& timestep : data)
            {
                ARMARX_TRACE;
                //const std::size_t iter_id   = timestep.iterationId;
                //accel
                {
                    const auto update_accel = [&](auto & fdata, const auto & keys, auto & curr_num_new_acc_values)
                    {
                        curr_num_new_acc_values = keys.acc_fifo_len(timestep);
                        //maybe move buffer
                        for (std::size_t i = 0; i < curr_num_new_acc_values; ++i)
                        {
                            ARMARX_TRACE;
                            fdata.insert(keys.acc_fifo.at(i).at(0)(timestep),
                                         keys.acc_fifo.at(i).at(1)(timestep),
                                         keys.acc_fifo.at(i).at(2)(timestep));
                        }
                    };
                    update_accel(_fft.finger_datas.at(0), _keys.little, num_new_acc_values.at(0));
                    update_accel(_fft.finger_datas.at(1), _keys.ring,   num_new_acc_values.at(1));
                    update_accel(_fft.finger_datas.at(2), _keys.middle, num_new_acc_values.at(2));
                    update_accel(_fft.finger_datas.at(3), _keys.index,  num_new_acc_values.at(3));
                    update_accel(_fft.finger_datas.at(4), _keys.thumb,  num_new_acc_values.at(4));
                }
                //csv_out
                if (_mgf.csv_out.is_open())
                {
                    _mgf.csv_out << timestep.iterationId << ';'
                                 << timestep.timestampUSec;
                    const auto write = [&](const auto & val)
                    {
                        _mgf.csv_out << ';' << val;
                    };
                    RobotUnitDataStreamingReceiver::VisitEntries(
                        write, timestep, _mgf.csv_fields);

                    _mgf.csv_out << ";" << mgf_ctrl_data_updated
                                 << ";" << mgf_ctrl_data.grasp_phase
                                 << ";" << mgf_ctrl_data.sensor_time_stamp_us_ice
                                 << ";" << mgf_ctrl_data.time_stamp_us_std
                                 << ";" << mgf_ctrl_data.updating_sensors
                                 << ";" << mgf_ctrl_data.resetting_controller
                                 << ";" << mgf_ctrl_data.run_controller
                                 << ";" << mgf_ctrl_data.all_shear_avg_len_xy
                                 << ";" << mgf_ctrl_data.all_shear_avg_len_z
                                 << ";" << mgf_ctrl_data.all_shear_avg_ratio
                                 << ";" << mgf_ctrl_data.last_fpga_dt
                                 << ";" << mgf_ctrl_data.finger_active_shear_avg_len_xy
                                 << ";" << mgf_ctrl_data.finger_active_shear_avg_len_z
                                 << ";" << mgf_ctrl_data.finger_active_shear_avg_ratio
                                 << ";" << mgf_ctrl_data.finger_active_shear_num
                                 << ";" << mgf_ctrl_data.active_shear_avg_len_xy
                                 << ";" << mgf_ctrl_data.active_shear_avg_len_z
                                 << ";" << mgf_ctrl_data.active_shear_avg_ratio
                                 << ";" << mgf_ctrl_data.active_shear_num
                                 << ";" << mgf_ctrl_data.used_shear_avg_ratio
                                 << ";" << mgf_ctrl_data.negative_z_shear_avg_len_xy
                                 << ";" << mgf_ctrl_data.negative_z_shear_avg_len_z
                                 << ";" << mgf_ctrl_data.negative_z_shear_avg_ratio
                                 << ";" << mgf_ctrl_data.negative_z_shear_num
                                 << ";" << mgf_ctrl_data.all_shear_avg_len_z_pressure_sensors
                                 << ";" << mgf_ctrl_data.all_shear_avg_ratio_pressure_sensors
                                 << ";" << mgf_ctrl_data.open_signal_finger_count
                                 << ";" << mgf_ctrl_data.open_signal;

                    const auto add_motor = [&](const auto & m)
                    {
                        _mgf.csv_out << ';' << m.grasp_phase
                                     << ';' << m.squish_distance
                                     << ';' << m.last_shear_pressure_position
                                     << ';' << m.pos_ctrl_target
                                     << ';' << m.normal_force_controller_position_delta
                                     << ';' << m.shear_force_controller_position_delta
                                     << ';' << m.shear_force_min_xy_len
                                     << ';' << m.shear_force_min_z_len
                                     << ';' << m.shear_force_min_ratio
                                     << ';' << m.shear_force_avg_ratio
                                     << ';' << m.shear_force_used_ratio
                                     << ';' << m.shear_force_used_ratio_num
                                     << ';' << m.slip_detection_value
                                     << ';' << m.shear_force_ratio_number_of_active_sensors
                                     << ';' << m.maximum_pressure
                                     << ';' << m.pressure_on_start_unload
                                     << ';' << m.target_shear
                                     << ';' << m.target_pressure
                                     << ';' << m.contact_detection_flag
                                     << ';' << m.gross_slip_count
                                     << ';' << m.finger_did_close
                                     << ';' << m.monotonic_shear_pressure_control_last_pwm
                                     << ';' << m.monotonic_shear_pressure_control_last_pwm_pressure
                                     << ';' << m.monotonic_shear_pressure_control_last_pwm_shear
                                     << ';' << m.monotonic_shear_pressure_control_last_pwm_slip
                                     << ';' << m.monotonic_shear_pressure_control_last_delta_pressure
                                     << ';' << m.monotonic_shear_pressure_control_last_delta_shear
                                     << ';' << m.target_pressure_rate_of_change
                                     << ';' << m.pressure_rate_of_change
                                     << ';' << m.unload_phase_controller_normal_force_via_pwm_last_pwm;
                    };
                    add_motor(mgf_ctrl_data.motor_other);
                    add_motor(mgf_ctrl_data.motor_index);
                    add_motor(mgf_ctrl_data.motor_thumb);

                    const auto add_finger = [&](const auto & f)
                    {
                        _mgf.csv_out << ';' << f.fit_prox_x
                                     << ';' << f.fit_dist_x
                                     << ';' << f.fit_prox_z
                                     << ';' << f.fit_dist_z
                                     << ';' << f.fit_prox_avg
                                     << ';' << f.fit_dist_avg;
                        auto add_nf = [&](const auto & n)
                        {
                            _mgf.csv_out << ';' << n.current
                                         << ';' << n.continuous_mean
                                         << ';' << n.current_mean_free_scaled
                                         << ';' << n.current_continuous_mean_free_scaled;
                        };
                        add_nf(f.nf_prox_l);
                        add_nf(f.nf_prox_r);
                        add_nf(f.nf_dist_joint);
                        add_nf(f.nf_dist_tip);
                        auto add_sf = [&](const auto & s)
                        {
                            _mgf.csv_out << ';' << s.len_xy
                                         << ';' << s.len_z
                                         << ';' << s.ratio

                                         << ';' << s.x_current
                                         << ';' << s.x_continuous_mean
                                         << ';' << s.x_current_mean_free_scaled
                                         << ';' << s.x_current_continuous_mean_free_scaled

                                         << ';' << s.y_current
                                         << ';' << s.y_continuous_mean
                                         << ';' << s.y_current_mean_free_scaled
                                         << ';' << s.y_current_continuous_mean_free_scaled

                                         << ';' << s.z_current
                                         << ';' << s.z_continuous_mean
                                         << ';' << s.z_current_mean_free_scaled
                                         << ';' << s.z_current_continuous_mean_free_scaled;
                        };
                        add_sf(f.shear_force_dist_joint);
                        add_sf(f.shear_force_dist_tip);

                        _mgf.csv_out << ';' << f.accelerometer.shear_force_criterium;
                        _mgf.csv_out << ';' << f.accelerometer.fft_contact_detect_energy;
                    };
                    add_finger(mgf_ctrl_data.finger_little);
                    add_finger(mgf_ctrl_data.finger_ring);
                    add_finger(mgf_ctrl_data.finger_middle);
                    add_finger(mgf_ctrl_data.finger_index);
                    add_finger(mgf_ctrl_data.finger_thumb);




                    _mgf.csv_out            << "\n";
                    mgf_ctrl_data_updated = false;
                }
            }
            data.clear();
        }
        //fft
        {
            ARMARX_TRACE;
            const std::size_t n_fft = std::pow(2, _widget.spinBoxFFTInSize->value());
            ARMARX_DEBUG << "fft width " << n_fft;
            _widget.labelFFTInputSz->setText(QString::number(n_fft));
            const std::size_t fft_every_x = _widget.spinBoxFFTEveryNth->value();
            for (std::size_t selected_finger_idx = 0; selected_finger_idx < 5; ++selected_finger_idx)
            {
                ARMARX_TRACE;
                const auto cur_num_new_acc_values = num_new_acc_values.at(selected_finger_idx);
                if (cur_num_new_acc_values)
                {
                    ARMARX_TRACE;
                    auto& finger_data = _fft.finger_datas.at(selected_finger_idx);
                    ARMARX_DEBUG << "copy data to input buffer. Finger " << selected_finger_idx;
                    for (std::size_t i = 0; i < n_fft + cur_num_new_acc_values ; ++i)
                    {
                        ARMARX_TRACE;
                        const auto& xyz = finger_data.at(-i);
                        ARMARX_TRACE;
                        const std::int64_t in_buffer_idx = static_cast<std::int64_t>(_fft.fft_in.size()) - 1 - i;
                        ARMARX_CHECK_LESS(n_fft + cur_num_new_acc_values, _fft.fft_in.size())
                                << "\n" << VAROUT(n_fft)
                                << "\n" << VAROUT(cur_num_new_acc_values)
                                << "\n" << VAROUT(_fft.fft_in.size());
                        ARMARX_CHECK_LESS(in_buffer_idx, _fft.fft_in.size());
                        ARMARX_CHECK_LESS(0, in_buffer_idx);
                        _fft.fft_in.at(in_buffer_idx) =
                            _widget.checkBoxFFTX->isChecked() * xyz(0) +
                            _widget.checkBoxFFTY->isChecked() * xyz(1) +
                            _widget.checkBoxFFTZ->isChecked() * xyz(2);
                    }

                    for (std::size_t i = 0; i < cur_num_new_acc_values; ++ i)
                    {
                        if (((finger_data.end - i) % fft_every_x) != 0)
                        {
                            continue;
                        }
                        ARMARX_TRACE;
                        ARMARX_DEBUG << "run fft " << i + 1 << " / " << cur_num_new_acc_values << "...";
                        const std::size_t idx_start_current =
                            0                        // @start
                            + _fft.fft_in.size()     // @1past
                            - n_fft                  // @      beg last  fft
                            - cur_num_new_acc_values // @      beg first fft
                            + i;                     // @      beg cur fft
                        ARMARX_DEBUG << "start idx " << idx_start_current
                                     << " last " << idx_start_current + n_fft
                                     << " (sz = " << _fft.fft_in.size() << ")";
                        if (_widget.checkBoxFFTHalfSpectrum->isChecked())
                        {
                            _fft.fft.SetFlag(_fft.fft.HalfSpectrum);
                        }
                        else
                        {
                            _fft.fft.ClearFlag(_fft.fft.HalfSpectrum);
                        }
                        if (_widget.checkBoxFFTUnscaled->isChecked())
                        {
                            _fft.fft.SetFlag(_fft.fft.Unscaled);
                        }
                        else
                        {
                            _fft.fft.ClearFlag(_fft.fft.Unscaled);
                        }

                        _fft.fft.fwd(_fft.fft_out.data(), _fft.fft_in.data() + idx_start_current, n_fft);
                        ARMARX_DEBUG << "update data buffer";

                        const auto& vs = finger_data.spec_data->update(_fft.fft_out, n_fft / 2);

                        const auto mimait = std:: minmax_element(vs.begin(), vs.end());

                        finger_data.min_fft = std::min(finger_data.min_fft, *mimait.first);
                        finger_data.max_fft = std::max(finger_data.max_fft, *mimait.second);
                        ARMARX_DEBUG << "run fft " << i + 1 << " / " << cur_num_new_acc_values << "...done!";
                    }
                    if (!_widget.checkBoxFFTPause->isChecked())
                    {
                        ARMARX_TRACE;
                        ARMARX_DEBUG << "replot";
                        finger_data.plot->replot();
                        ARMARX_DEBUG << "update nyquist bin";
                        ARMARX_CHECK_LESS(n_fft / 2, _fft.fft_out.size());
                        finger_data.labelFFTNyquistBinRe ->setText(QString::number(_fft.fft_out.at(n_fft / 2).real()));
                    }
                    finger_data.labelFFTMin->setText(QString::number(finger_data.min_fft));
                    finger_data.labelFFTMax->setText(QString::number(finger_data.max_fft));
                    finger_data.labelAccXMin->setText(QString::number(finger_data.min_acc_x));
                    finger_data.labelAccXMax->setText(QString::number(finger_data.max_acc_x));
                    finger_data.labelAccYMin->setText(QString::number(finger_data.min_acc_y));
                    finger_data.labelAccYMax->setText(QString::number(finger_data.max_acc_y));
                    finger_data.labelAccZMin->setText(QString::number(finger_data.min_acc_z));
                    finger_data.labelAccZMax->setText(QString::number(finger_data.max_acc_z));
                    ARMARX_DEBUG << "fft done";
                }
            }
        }
        ARMARX_DEBUG << "timer...done!";
    }

    void SensorizedSoftFingerHandMinimalForceGraspingWidgetController::testStaticPlotterWidget()
    {
        StringVector2fSeqDict plots_data;

        const auto series = [&]
        {
            Vector2fSeq data;
            const auto o = std::chrono::high_resolution_clock::now().time_since_epoch().count() % 1'000;
            for (auto i = o; i < o + 100; ++i)
            {
                data.emplace_back(armarx::Vector2f{i * 1.f, std::sin(i * 0.5f)});
            }
            return data;
        };
        plots_data["a"] = series();
        plots_data["b"] = series();
        _static_plotter->addPlot(getName(), plots_data);
    }
}
