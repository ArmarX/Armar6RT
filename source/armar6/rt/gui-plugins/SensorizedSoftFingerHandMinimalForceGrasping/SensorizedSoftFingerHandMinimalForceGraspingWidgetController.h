/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_rt::gui-plugins::SensorizedSoftFingerHandMinimalForceGraspingWidgetController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <random>
#include <thread>
#include <fstream>

#include <unsupported/Eigen/FFT>

#include <qwt_plot.h>

#include <SimoxQtUtility/qwt/SimoxQwtSpectrogramData.h>
#include <SimoxUtility/json.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/libraries/SimpleConfigDialog/SimpleConfigDialog.h>

#include <ArmarXGui/interface/StaticPlotterInterface.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotUnitComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotStateComponentPlugin.h>
#include <RobotAPI/interface/units/RobotUnit/NJointCartesianWaypointController.h>

#include <armar6/rt/interface/units/KITSensorizedSoftFingerHand/V1/NJointMinimalGraspingForceV1Controller.h>

#include <armar6/rt/gui-plugins/SensorizedSoftFingerHandMinimalForceGrasping/ui_SensorizedSoftFingerHandMinimalForceGraspingWidget.h>

namespace armarx
{
    class SpectrogramData;

    /**
    \page armar6_rt-GuiPlugins-SensorizedSoftFingerHandMinimalForceGrasping SensorizedSoftFingerHandMinimalForceGrasping
    \brief The SensorizedSoftFingerHandMinimalForceGrasping allows visualizing ...

    \image html SensorizedSoftFingerHandMinimalForceGrasping.png
    The user can

    API Documentation \ref SensorizedSoftFingerHandMinimalForceGraspingWidgetController

    \see SensorizedSoftFingerHandMinimalForceGraspingGuiPlugin
    */


    /**
     * \class SensorizedSoftFingerHandMinimalForceGraspingGuiPlugin
     * \ingroup ArmarXGuiPlugins
     * \brief SensorizedSoftFingerHandMinimalForceGraspingGuiPlugin brief description
     *
     * Detailed description
     */

    /**
     * \class SensorizedSoftFingerHandMinimalForceGraspingWidgetController
     * \brief SensorizedSoftFingerHandMinimalForceGraspingWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        SensorizedSoftFingerHandMinimalForceGraspingWidgetController:
        public armarx::ArmarXComponentWidgetControllerTemplate < SensorizedSoftFingerHandMinimalForceGraspingWidgetController >,
        public virtual RobotStateComponentPluginUser,
        public virtual RobotUnitComponentPluginUser
    {
        Q_OBJECT
    public:
        /// Controller Constructor
        explicit SensorizedSoftFingerHandMinimalForceGraspingWidgetController();
        virtual ~SensorizedSoftFingerHandMinimalForceGraspingWidgetController();

        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;
        QPointer<QDialog> getConfigDialog(QWidget* parent) override;
        void configured() override;
    private:
        void setupNames(const std::string& hand_name,
                        const std::string& robot_unit_name,
                        const std::string& rsc_name,
                        const std::string& static_plotter_name);
        void setMGFCfg(const KITSensorizedSoftFingerHand::V1::NJointMinimalGraspingForceV1ControllerConfig& c);
        KITSensorizedSoftFingerHand::V1::NJointMinimalGraspingForceV1ControllerConfig getMGFCfg() const;
    public:

        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "Grasping.SensorizedSoftFingerHand.MinimalForceGrasping";
        }

        void onInitComponent()       override {}
        void onConnectComponent()    override;
        void onDisconnectComponent() override;
        void onExitComponent()       override {}

    protected:
        void timerEvent(QTimerEvent* event) override;


    private slots:
        void mgfStop();

        void mgfAutoCTRL();
        void mgfAutoPWM();
        void mgfRampPWM();
        void mgfCtrlReleaseFlag();

        void mgfMoveToInit();
        void mgfMoveToGrasp();
        void mgfCloseCTRL();
        void mgfClosePWM();
        void mgfMoveUp();
        void mgfMoveTraj();
        void mgfMoveDown();
        void mgfOpenCTRL();
        void mgfOpenPWM();
        void mgfMoveRetreat();

        void mgf0TorqueStop();
        void mgf0TorqueActive();

        void mgfSetGrasp();
        void mgfSetPre();
        void mgfSetInit();
        void mgfCalibrateHand();
        void mgfSendConfig();

        void testStaticPlotterWidget();
        void fftResetMinMax();
        void plotterSettingsChanged();

    private:
        using widget_t =  Ui::SensorizedSoftFingerHandMinimalForceGraspingWidget;
        QPointer<SimpleConfigDialog>        _dialog;
        std::string                         _hand_name;

        mutable std::recursive_mutex        _all_mutex;
        widget_t                            _widget;
        RobotUnitDataStreamingReceiverPtr   _streaming_handler;

        std::mt19937                        _gen;
        bool                                _fake_mode = false;

        struct fft_data
        {
            struct finger_data
            {
                std::vector<Eigen::Vector3f> vals;
                std::size_t end = 0;

                std::int64_t index(std::int64_t i) const
                {
                    std::int64_t r = (end + i) % vals.size();
                    while (r < 0)
                    {
                        r += vals.size();
                    }
                    return r;
                }

                Eigen::Vector3f& at(std::int64_t i)
                {
                    return vals.at(index(i));
                }

                void insert(float x, float y, float z)
                {
                    at(0) << x, y, z;
                    ++end;
                    min_acc_x = std::min(min_acc_x, x);
                    max_acc_x = std::max(max_acc_x, x);

                    min_acc_y = std::min(min_acc_y, y);
                    max_acc_y = std::max(max_acc_y, y);

                    min_acc_z = std::min(min_acc_z, z);
                    max_acc_z = std::max(max_acc_z, z);
                }

                QwtPlot*            plot      = nullptr;
                QwtPlotSpectrogram* spec      = nullptr;
                simox::qt::SimoxQwtSpectrogramData* spec_data = nullptr;
                QLabel* labelFFTNyquistBinRe  = nullptr;
                QLabel* labelFFTMin           = nullptr;
                QLabel* labelFFTMax           = nullptr;
                QLabel* labelAccXMin          = nullptr;
                QLabel* labelAccXMax          = nullptr;
                QLabel* labelAccYMin          = nullptr;
                QLabel* labelAccYMax          = nullptr;
                QLabel* labelAccZMin          = nullptr;
                QLabel* labelAccZMax          = nullptr;
                float min_fft                 = +std::numeric_limits<float>::infinity();
                float max_fft                 = -std::numeric_limits<float>::infinity();
                float min_acc_x               = +std::numeric_limits<float>::infinity();
                float max_acc_x               = -std::numeric_limits<float>::infinity();
                float min_acc_y               = +std::numeric_limits<float>::infinity();
                float max_acc_y               = -std::numeric_limits<float>::infinity();
                float min_acc_z               = +std::numeric_limits<float>::infinity();
                float max_acc_z               = -std::numeric_limits<float>::infinity();
            };
            std::array<finger_data, 5> finger_datas;
            std::vector<float> fft_in;
            std::vector<std::complex<float>> fft_out;
            Eigen::FFT<float> fft;
        };
        fft_data _fft;

        struct keys
        {
            struct finger
            {
                std::array<std::array<RobotUnitDataStreamingReceiver::DataEntryReader<Ice::Short>, 3>, 16> acc_fifo;
                RobotUnitDataStreamingReceiver::DataEntryReader<Ice::Long> acc_fifo_len;
            };
            finger little;
            finger ring;
            finger middle;
            finger index;
            finger thumb;
        };
        keys _keys;

        //minimal grasping force
    public:
        struct mgf_data
        {
            using GraspControllerPrx = KITSensorizedSoftFingerHand::V1::NJointMinimalGraspingForceV1ControllerInterfacePrx;
            VirtualRobot::RobotPtr                          robot;
            NJointCartesianWaypointControllerInterfacePrx   ctrl_wp;
            GraspControllerPrx                              ctrl_mgf;
            RobotNameHelper::RobotArm                       rnh_robot_arm;

            Eigen::Matrix4f  pose_init = simox::json::posquat2eigen4f(R"(
                                                                      {
                                                                          "qw": -0.1024223268032074,
                                                                          "qx": 0.7071946263313293,
                                                                          "qy": 0.698394238948822,
                                                                          "qz": -0.04038770869374275,
                                                                          "x": 656.0859985351563,
                                                                          "y": 659.9922485351563,
                                                                          "z": 1072.3370361328125
                                                                      }
                                                                  )");
            Eigen::Matrix4f  pose_pre = simox::json::posquat2eigen4f(R"(
                                                                     {
                                                                         "qw": -0.06676588207483292,
                                                                         "qx": 0.6669273376464844,
                                                                         "qy": 0.7370484471321106,
                                                                         "qz": -0.08665984123945236,
                                                                         "x": 435.029296875,
                                                                         "y": 811.6380615234375,
                                                                         "z": 1020.8592529296875
                                                                     }
                                                                 )");
            Eigen::Matrix4f  pose_grasp = simox::json::posquat2eigen4f(R"(
                                                                       {
                                                                           "qw": -0.04655272513628006,
                                                                           "qx": 0.6558828949928284,
                                                                           "qy": 0.7445601224899292,
                                                                           "qz": -0.11524268239736557,
                                                                           "x": 356.443115234375,
                                                                           "y": 867.9481201171875,
                                                                           "z": 993.3532104492188
                                                                       }
                                                                   )");
            std::size_t      set_cnt_pose_init  = 0;
            std::size_t      set_cnt_pose_pre   = 0;
            std::size_t      set_cnt_pose_grasp = 0;

            struct task_data
            {
                float close_sec = 3;
                std::string name;
                struct ramp_pwm_data
                {
                    bool enable = false;
                    bool thumb  = false;
                    bool index  = false;
                    bool other  = false;
                    double seconds = 0;
                    double max = 0;
                    bool initial_close = false;
                };
                ramp_pwm_data ramp_pwm;


                KITSensorizedSoftFingerHand::V1::NJointMinimalGraspingForceV1ControllerConfig mgf_cfg;
                enum class close_mode_t
                {
                    pwm,
                    mgf
                };
                bool             traj_top = false;
                Eigen::Matrix4f  pose_init ;
                Eigen::Matrix4f  pose_pre  ;
                Eigen::Matrix4f  pose_grasp;
                bool             down_unitl_contact;
                close_mode_t     close_mode   = close_mode_t::pwm;
                double           up_pre_traj  = 5;
                bool             move_init    = false;
                bool             move_grasp   = false;
                bool             close        = false;
                bool             wait_close   = false;
                bool             move_up      = false;
                bool             move_traj    = false;
                bool             move_down    = false;
                bool             open         = false;
                bool             move_retreat = false;

                float            contact_force = 25;

                float            place_offset = -25;

                NJointCartesianWaypointControllerRuntimeConfig cfg_trj;
                NJointCartesianWaypointControllerRuntimeConfig cfg_mv_up;
                NJointCartesianWaypointControllerRuntimeConfig cfg_mv_appr;
                NJointCartesianWaypointControllerRuntimeConfig cfg_mv_down;
                NJointCartesianWaypointControllerRuntimeConfig cfg_mv_retr;
            };
            void reset_task(widget_t& widget,
                            KITSensorizedSoftFingerHand::V1::NJointMinimalGraspingForceV1ControllerConfig cfg);
            void start_task(widget_t& widget, const std::string& name);
            void stop_task();

            void task(task_data tdata);
            void task_move_approach(const task_data& tdata);
            void task_close(const task_data& tdata);
            void task_wait_for_close(const task_data& tdata);
            void task_move_up(const task_data& tdata);
            void task_move_trajectory(const task_data& tdata);
            void task_move_down(const task_data& tdata);
            void task_open(const task_data& tdata);
            void task_move_retreat(const task_data& tdata);

            task_data                                      next_task;
            std::thread                                    worker;
            std::atomic_bool                               worker_stop = false;

            std::mutex                                     csv_mutex;
            std::string                                    csv_header;
            std::vector<RobotUnitDataStreaming::DataEntry> csv_fields;
            std::ofstream                                  csv_out;
        };
    private:
        mgf_data _mgf;

        std::string               _static_plotter_name;
        StaticPlotterInterfacePrx _static_plotter;
    };
}
