/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * \package    armar6_rt::gui-plugins::FingerVisionSoftHandGraspingWidgetController
 * \author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * \date       2021
 * \copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <ArmarXCore/core/util/FileSystemPathBuilder.h>

#include <armar6/rt/libraries/KITFingerVisionSoftHandV1NJointControllers/GraspOnDetection.h>

#include "FingerVisionSoftHandGraspingWidgetController.h"

//boilerplate config
namespace armarx
{
    void FingerVisionSoftHandGraspingWidgetController::setupNames(const std::string& hand_name,
            const std::string& robot_unit_name,
            const std::string& rsc_name)
    {
        ARMARX_TRACE;
        std::lock_guard g{_all_mutex};
        _hand_name                 = hand_name;
        if (robot_unit_name.empty() || _hand_name == "FAKE")
        {
            _fake_mode = true;
            _hand_name = "FAKE";
            getRobotUnitComponentPlugin().deactivate();
            getRobotStateComponentPlugin().deactivate();
        }
        else
        {
            getRobotUnitComponentPlugin().setRobotUnitName(robot_unit_name);
            if (rsc_name.empty())
            {
                getRobotStateComponentPlugin().deactivate();
            }
            else
            {
                getRobotStateComponentPlugin().setRobotStateComponentName(rsc_name);
            }
        }
        ARMARX_INFO << "params "
                    << VAROUT(_hand_name) << " "
                    << VAROUT(robot_unit_name) << " "
                    << VAROUT(rsc_name);
    }

    void FingerVisionSoftHandGraspingWidgetController::loadSettings(QSettings* settings)
    {
        ARMARX_INFO << "loadSettings";
        setupNames(settings->value("hand_name", "RightHand").toString().toStdString(),
                   settings->value("ru", "Armar6Unit").toString().toStdString(),
                   settings->value("rsc", "Armar6StateComponent").toString().toStdString());
    }

    void FingerVisionSoftHandGraspingWidgetController::saveSettings(QSettings* settings)
    {
        ARMARX_INFO << "saveSettings";
        ARMARX_TRACE;
        std::lock_guard g{_all_mutex};
        settings->setValue("ru", QString::fromStdString(getRobotUnitComponentPlugin().getRobotUnitName()));
        settings->setValue("hand_name", QString::fromStdString(_hand_name));
        settings->setValue("rsc", QString::fromStdString(getRobotStateComponentPlugin().getRobotStateComponentName()));
    }

    QPointer<QDialog> FingerVisionSoftHandGraspingWidgetController::getConfigDialog(QWidget* parent)
    {
        ARMARX_INFO << "getConfigDialog";
        ARMARX_TRACE;
        std::lock_guard g{_all_mutex};
        if (!_dialog)
        {
            _dialog = new SimpleConfigDialog(parent);
            _dialog->addProxyFinder<RobotUnitInterfacePrx>("ru", "Robot Unit", "*Unit");
            _dialog->addProxyFinder<RobotStateComponentInterfacePrx>("rsc", "Robot State Component", "*Component");
            _dialog->addLineEdit("hand_name", "Hand Name (or 'FAKE')", "RightHand");
        }
        return qobject_cast<SimpleConfigDialog*>(_dialog);
    }

    void FingerVisionSoftHandGraspingWidgetController::configured()
    {
        ARMARX_INFO << "configured";
        setupNames(_dialog->get("hand_name"),
                   _dialog->get("ru"),
                   _dialog->get("rsc"));
    }
}
//cycle
namespace armarx
{
    FingerVisionSoftHandGraspingWidgetController::FingerVisionSoftHandGraspingWidgetController() :
        _gen{std::random_device{}()}
    {
        using T = FingerVisionSoftHandGraspingWidgetController;
        ARMARX_INFO << "ctor...";
        _widget.setupUi(getWidget());
        ARMARX_INFO << "setting up cnn";
        {
            const auto setup_data = [&](auto & data, auto limg, auto lpop, auto lfpga)
            {
                data.labelFPGA  = lfpga;
                data.labelPop   = lpop;
                data.labelImage = limg;

                data.img = QImage(88, 72, QImage::Format_Mono);
            };
            setup_data(_cnn.finger_datas.at(0),
                       _widget.labelImageLittle,
                       _widget.labelPopCountLittle,
                       _widget.labelFPGACountLittle);

            setup_data(_cnn.finger_datas.at(1),
                       _widget.labelImageRing,
                       _widget.labelPopCountRing,
                       _widget.labelFPGACountRing);

            setup_data(_cnn.finger_datas.at(2),
                       _widget.labelImageMiddle,
                       _widget.labelPopCountMiddle,
                       _widget.labelFPGACountMiddle);

            setup_data(_cnn.finger_datas.at(3),
                       _widget.labelImageIndex,
                       _widget.labelPopCountIndex,
                       _widget.labelFPGACountIndex);

            setup_data(_cnn.finger_datas.at(4),
                       _widget.labelImageThumb,
                       _widget.labelPopCountThumb,
                       _widget.labelFPGACountThumb);
        }
        ARMARX_INFO << "connect slots";
        {
            connect(_widget.pushButtonGODE1LoadPreset,        &QPushButton::clicked, this, &T::godE1LoadPreset);
            connect(_widget.pushButtonGODE1Init,              &QPushButton::clicked, this, &T::godE1Init);
            connect(_widget.pushButtonGODE1Start,             &QPushButton::clicked, this, &T::godE1Start);
            connect(_widget.pushButtonGODE1Stop,              &QPushButton::clicked, this, &T::godStop);

            connect(_widget.pushButtonGODE2LoadPreset,        &QPushButton::clicked, this, &T::godE2LoadPreset);
            connect(_widget.pushButtonGODE2Init,              &QPushButton::clicked, this, &T::godE2Init);
            connect(_widget.pushButtonGODE2Start,             &QPushButton::clicked, this, &T::godE2Start);
            connect(_widget.pushButtonGODE2Stop,              &QPushButton::clicked, this, &T::godStop);

            connect(_widget.pushButtonGODE3LoadPreset,        &QPushButton::clicked, this, &T::godE3LoadPreset);
            connect(_widget.pushButtonGODE3Init,              &QPushButton::clicked, this, &T::godE3Init);
            connect(_widget.pushButtonGODE3Start,             &QPushButton::clicked, this, &T::godE3Start);
            connect(_widget.pushButtonGODE3Stop,              &QPushButton::clicked, this, &T::godStop);

            connect(_widget.pushButtonGODE2Activate0Torque,   &QPushButton::clicked, this, &T::godActivate0Torque);
            connect(_widget.pushButtonGODE2Deactivate0Torque, &QPushButton::clicked, this, &T::godDeactivate0Torque);

            connect(_widget.pushButtonGODE3Activate0Torque,   &QPushButton::clicked, this, &T::godActivate0Torque);
            connect(_widget.pushButtonGODE3Deactivate0Torque, &QPushButton::clicked, this, &T::godDeactivate0Torque);

            connect(_widget.pushButtonGODSendConfig,          &QPushButton::clicked, this, &T::godSendConfig);
            connect(_widget.pushButtonGODE2SetPose,           &QPushButton::clicked, this, &T::godE2SetPose);
            connect(_widget.pushButtonGODE3SetInit,           &QPushButton::clicked, this, &T::godE3SetInit);
            connect(_widget.pushButtonGODE3SetGoal,           &QPushButton::clicked, this, &T::godE3SetGoal);
        }
        ARMARX_INFO << "starting timer";
        startTimer(10);
        ARMARX_INFO << "ctor...done!";
        godE1LoadPreset();
    }

    FingerVisionSoftHandGraspingWidgetController::~FingerVisionSoftHandGraspingWidgetController()
    {}

    void FingerVisionSoftHandGraspingWidgetController::onConnectComponent()
    {
        ARMARX_INFO << "onConnectComponent";
        ARMARX_TRACE;
        std::lock_guard g{_all_mutex};
        ARMARX_INFO << VAROUT(_hand_name);
        if (_fake_mode)
        {
            return;
        }
        ARMARX_TRACE;
        ARMARX_INFO << "get robot model";
        {
            if (getRobotStateComponent())
            {
                _god.robot = addRobot("god robot", VirtualRobot::RobotIO::eStructure);
                _god.rnh_robot_arm = getRobotNameHelper().getRobotArm("Right", _god.robot);
            }
        }
        ARMARX_INFO << "setup data streaming";
        {
            ARMARX_CHECK_NULL(_streaming_handler);
            RobotUnitDataStreaming::Config cfg;
            if (_god.robot)
            {
                for (const auto& n : *_god.rnh_robot_arm.getKinematicChain())
                {
                    cfg.loggingNames.emplace_back("sens." + n->getName() + ".position");
                }
            }
            cfg.loggingNames.emplace_back("sens." + _hand_name);

            _streaming_handler = getRobotUnitComponentPlugin().startDataSatreming(cfg);
            //get idx
            ARMARX_TRACE;
            //dump entries
            ARMARX_INFO << _streaming_handler->getDataDescriptionString();

            ARMARX_TRACE;
            _streaming_handler->getDataEntryReader(
                _keys.cnn_data_finger_number,
                "sens." + _hand_name + ".cnn_data_finger_number");
            ARMARX_TRACE;
            _streaming_handler->getDataEntryReader(
                _keys.frame_counter,
                "sens." + _hand_name + ".frame_counter");
            for (std::size_t i = 0; i < _keys.cnn_data.size(); ++i)
            {
                ARMARX_TRACE;
                _streaming_handler->getDataEntryReader(
                    _keys.cnn_data.at(i),
                    "sens." + _hand_name + ".cnn_data.element_" + std::to_string(i));
            }
        }
        ARMARX_INFO << "init controllers";
        {
            ARMARX_INFO << "init wp controller";
            if (_god.robot)
            {
                NJointCartesianWaypointControllerConfigPtr cfg_wp = new NJointCartesianWaypointControllerConfig;
                cfg_wp->rns                = _god.rnh_robot_arm.getArm().getKinematicChain();
                cfg_wp->ftName             = "FT R";
                cfg_wp->referenceFrameName = "root";
                getRobotUnitComponentPlugin().createNJointController(
                    _god.ctrl_wp,
                    "NJointCartesianWaypointController",
                    "FingerVisionSoftHandGraspingWidget_wp",
                    cfg_wp
                );
            }
            ARMARX_INFO << "init GOD controller";
            {
                namespace GOD = KITFingerVisionSoftHand::V1;
                GOD::NJointGraspOnDetectionControllerConfigPtr cfg_god =
                    new GOD::NJointGraspOnDetectionControllerConfig;
                *cfg_god = getGODCfg();
                getRobotUnitComponentPlugin().createNJointController(
                    _god.ctrl_god,
                    "KITFingerVisionSoftHandV1_GraspOnDetection_NJointController",
                    "FingerVisionSoftHandGraspingWidget_god",
                    cfg_god
                );
            }
            //csv
            {
                std::stringstream str;
                str << "iteration;timestep";
                for (const auto& [k, v] : _streaming_handler->getDataDescription().entries)
                {
                    str << ';' << k;
                    _god.csv_fields.emplace_back(v);
                }
                str << ";detected\n";
                _god.csv_header = str.str();
                _god.csv_header.back();
            }
        }
    }
    void FingerVisionSoftHandGraspingWidgetController::onDisconnectComponent()
    {
        ARMARX_INFO << "onDisconnectComponent";
        ARMARX_TRACE;
        std::lock_guard g{_all_mutex};
        ARMARX_INFO << "stop data streaming";
        _streaming_handler = nullptr;
    }
}
//grasp exec - cfg
namespace armarx
{
    void
    FingerVisionSoftHandGraspingWidgetController::setGODCfg(
        const KITFingerVisionSoftHand::V1::NJointGraspOnDetectionControllerConfig& c)
    {
        const auto f = [](const auto s, auto t)
        {
            double max = std::max(std::abs(s) * 100.0, 10000.0);
            t->setMaximum(max);
            t->setMinimum(-max);
            t->setDecimals(8);
            t->setValue(s);
        };
        f(c.object_id,                      _widget.doubleSpinBoxObjectId);
        f(c.detection_threshold_per_finger, _widget.doubleSpinBoxGODGrspCfDetThreshFinger);
        f(c.detection_threshold_total,      _widget.doubleSpinBoxGODGrspCfDetThreshTotal);
        f(c.finger_count_threahold,         _widget.doubleSpinBoxGODGrspCfgMinFCnt);

        f(c.motor_other.pos_pid.p,          _widget.doubleSpinBoxGODGrspCfgPosPO);
        f(c.motor_other.pos_pid.i,          _widget.doubleSpinBoxGODGrspCfgPosIO);
        f(c.motor_other.pos_pid.d,          _widget.doubleSpinBoxGODGrspCfgPosDO);
        f(c.motor_other.pos_close,          _widget.doubleSpinBoxGODGrspCfgPosCloseO);
        f(c.motor_other.pos_open,           _widget.doubleSpinBoxGODGrspCfgPosPreO);
        f(c.motor_other.pwm_max,            _widget.doubleSpinBoxGODGrspCfgPwmMaxO);
        f(c.motor_other.pwm_open,           _widget.doubleSpinBoxGODGrspCfgPwmOpenO);

        f(c.motor_index.pos_pid.p,          _widget.doubleSpinBoxGODGrspCfgPosPI);
        f(c.motor_index.pos_pid.i,          _widget.doubleSpinBoxGODGrspCfgPosII);
        f(c.motor_index.pos_pid.d,          _widget.doubleSpinBoxGODGrspCfgPosDI);
        f(c.motor_index.pos_close,          _widget.doubleSpinBoxGODGrspCfgPosCloseI);
        f(c.motor_index.pos_open,           _widget.doubleSpinBoxGODGrspCfgPosPreI);
        f(c.motor_index.pwm_max,            _widget.doubleSpinBoxGODGrspCfgPwmMaxI);
        f(c.motor_index.pwm_open,           _widget.doubleSpinBoxGODGrspCfgPwmOpenI);

        f(c.motor_thumb.pos_pid.p,          _widget.doubleSpinBoxGODGrspCfgPosPT);
        f(c.motor_thumb.pos_pid.i,          _widget.doubleSpinBoxGODGrspCfgPosIT);
        f(c.motor_thumb.pos_pid.d,          _widget.doubleSpinBoxGODGrspCfgPosDT);
        f(c.motor_thumb.pos_close,          _widget.doubleSpinBoxGODGrspCfgPosCloseT);
        f(c.motor_thumb.pos_open,           _widget.doubleSpinBoxGODGrspCfgPosPreT);
        f(c.motor_thumb.pwm_max,            _widget.doubleSpinBoxGODGrspCfgPwmMaxT);
        f(c.motor_thumb.pwm_open,           _widget.doubleSpinBoxGODGrspCfgPwmOpenT);

        _widget.checkBoxGODGrspCfgFullForceClose->setChecked(c.close_full_force);
        _widget.checkBoxGODGrspCfgDetectOnL->setChecked(c.detect_on_little);
        _widget.checkBoxGODGrspCfgDetectOnR->setChecked(c.detect_on_ring);
        _widget.checkBoxGODGrspCfgDetectOnM->setChecked(c.detect_on_middle);
        _widget.checkBoxGODGrspCfgDetectOnI->setChecked(c.detect_on_index);
        _widget.checkBoxGODGrspCfgDetectOnT->setChecked(c.detect_on_thumb);
    }
    KITFingerVisionSoftHand::V1::NJointGraspOnDetectionControllerConfig
    FingerVisionSoftHandGraspingWidgetController::getGODCfg() const
    {
        auto c = KITFingerVisionSoftHand::V1::GraspOnDetection_NJointController::make_default_cfg();
        c.hand_name = _hand_name;
        const auto f = [](auto & t, auto & s)
        {
            t = s->value();
        };
        f(c.object_id,                      _widget.doubleSpinBoxObjectId);
        f(c.detection_threshold_per_finger, _widget.doubleSpinBoxGODGrspCfDetThreshFinger);
        f(c.detection_threshold_total,      _widget.doubleSpinBoxGODGrspCfDetThreshTotal);
        f(c.finger_count_threahold,         _widget.doubleSpinBoxGODGrspCfgMinFCnt);

        f(c.motor_other.pos_pid.p,          _widget.doubleSpinBoxGODGrspCfgPosPO);
        f(c.motor_other.pos_pid.i,          _widget.doubleSpinBoxGODGrspCfgPosIO);
        f(c.motor_other.pos_pid.d,          _widget.doubleSpinBoxGODGrspCfgPosDO);
        f(c.motor_other.pos_close,          _widget.doubleSpinBoxGODGrspCfgPosCloseO);
        f(c.motor_other.pos_open,           _widget.doubleSpinBoxGODGrspCfgPosPreO);
        f(c.motor_other.pwm_max,            _widget.doubleSpinBoxGODGrspCfgPwmMaxO);
        f(c.motor_other.pwm_open,           _widget.doubleSpinBoxGODGrspCfgPwmOpenO);

        f(c.motor_index.pos_pid.p,          _widget.doubleSpinBoxGODGrspCfgPosPI);
        f(c.motor_index.pos_pid.i,          _widget.doubleSpinBoxGODGrspCfgPosII);
        f(c.motor_index.pos_pid.d,          _widget.doubleSpinBoxGODGrspCfgPosDI);
        f(c.motor_index.pos_close,          _widget.doubleSpinBoxGODGrspCfgPosCloseI);
        f(c.motor_index.pos_open,           _widget.doubleSpinBoxGODGrspCfgPosPreI);
        f(c.motor_index.pwm_max,            _widget.doubleSpinBoxGODGrspCfgPwmMaxI);
        f(c.motor_index.pwm_open,           _widget.doubleSpinBoxGODGrspCfgPwmOpenI);

        f(c.motor_thumb.pos_pid.p,          _widget.doubleSpinBoxGODGrspCfgPosPT);
        f(c.motor_thumb.pos_pid.i,          _widget.doubleSpinBoxGODGrspCfgPosIT);
        f(c.motor_thumb.pos_pid.d,          _widget.doubleSpinBoxGODGrspCfgPosDT);
        f(c.motor_thumb.pos_close,          _widget.doubleSpinBoxGODGrspCfgPosCloseT);
        f(c.motor_thumb.pos_open,           _widget.doubleSpinBoxGODGrspCfgPosPreT);
        f(c.motor_thumb.pwm_max,            _widget.doubleSpinBoxGODGrspCfgPwmMaxT);
        f(c.motor_thumb.pwm_open,           _widget.doubleSpinBoxGODGrspCfgPwmOpenT);

        c.close_full_force                = _widget.checkBoxGODGrspCfgFullForceClose->isChecked();
        c.detect_on_little                = _widget.checkBoxGODGrspCfgDetectOnL->isChecked();
        c.detect_on_ring                  = _widget.checkBoxGODGrspCfgDetectOnR->isChecked();
        c.detect_on_middle                = _widget.checkBoxGODGrspCfgDetectOnM->isChecked();
        c.detect_on_index                 = _widget.checkBoxGODGrspCfgDetectOnI->isChecked();
        c.detect_on_thumb                 = _widget.checkBoxGODGrspCfgDetectOnT->isChecked();
        return c;
    }
}
//grasp exec - set poses
namespace armarx
{
    void FingerVisionSoftHandGraspingWidgetController::godE3SetGoal()
    {
        if (_fake_mode || !_god.robot)
        {
            return;
        }
        synchronizeLocalClone(_god.robot);
        _god.e3_end_pose = _god.rnh_robot_arm.getKinematicChain()->getTCP()->getPoseInRootFrame();
        _widget.labelGODE3SetGoalPose->setText(QString::number(++_god.set_cnt_pose_e3_goal));
    }
    void FingerVisionSoftHandGraspingWidgetController::godE3SetInit()
    {
        if (_fake_mode || !_god.robot)
        {
            return;
        }
        synchronizeLocalClone(_god.robot);
        _god.e3_start_pose = _god.rnh_robot_arm.getKinematicChain()->getTCP()->getPoseInRootFrame();
        _widget.labelGODE3SetInitPose->setText(QString::number(++_god.set_cnt_pose_e3_start));
    }
    void FingerVisionSoftHandGraspingWidgetController::godE2SetPose()
    {
        if (_fake_mode || !_god.robot)
        {
            return;
        }
        synchronizeLocalClone(_god.robot);
        _god.e2_start_pose = _god.rnh_robot_arm.getKinematicChain()->getTCP()->getPoseInRootFrame();
        _widget.labelGODE2SetPose->setText(QString::number(++_god.set_cnt_pose_e2_start));
    }
}
//grasp exec - utility
namespace armarx
{
    void FingerVisionSoftHandGraspingWidgetController::godActivate0Torque()
    {
        if (_fake_mode || !_god.robot)
        {
            return;
        }
        NameControlModeMap ncm;
        NameValueMap ntv;

        for (const auto& n : *_god.rnh_robot_arm.getKinematicChain())
        {
            ncm[n->getName()] = ControlMode::eTorqueControl;
            ntv[n->getName()] = 0;
        }
        getRobotUnit()->getKinematicUnit()->switchControlMode(ncm);
        getRobotUnit()->getKinematicUnit()->setJointTorques(ntv);
    }
    void FingerVisionSoftHandGraspingWidgetController::godDeactivate0Torque()
    {
        if (_fake_mode || !_god.robot)
        {
            return;
        }
        NameControlModeMap ncm;
        NameValueMap ntv;

        for (const auto& n : *_god.rnh_robot_arm.getKinematicChain())
        {
            ncm[n->getName()] = ControlMode::eVelocityControl;
            ntv[n->getName()] = 0;
        }
        getRobotUnit()->getKinematicUnit()->switchControlMode(ncm);
        getRobotUnit()->getKinematicUnit()->setJointVelocities(ntv);
    }
    void FingerVisionSoftHandGraspingWidgetController::godSendConfig()
    {
        ARMARX_CHECK_EXPRESSION(!_fake_mode);
        _god.ctrl_god->configure(new KITFingerVisionSoftHand::V1::NJointGraspOnDetectionControllerConfig(getGODCfg()));
    }

    void FingerVisionSoftHandGraspingWidgetController::godE3LoadPreset()
    {
        ARMARX_CHECK_EXPRESSION(!_fake_mode);
        auto c = KITFingerVisionSoftHand::V1::GraspOnDetection_NJointController::make_default_cfg();
        c.motor_index.pos_pid.p = 2.0f;
        c.motor_index.pos_open  = 0.2;
        c.motor_index.pos_close = 1;

        c.motor_other.pos_open  = 0.2f;
        c.motor_other.pos_close = 1;

        c.motor_thumb.pos_open  = 0.2;
        c.motor_thumb.pos_close = 1;

        c.object_id = 1;
        setGODCfg(c);
    }
    void FingerVisionSoftHandGraspingWidgetController::godE2LoadPreset()
    {
        ARMARX_CHECK_EXPRESSION(!_fake_mode);
        auto c = KITFingerVisionSoftHand::V1::GraspOnDetection_NJointController::make_default_cfg();
        c.motor_index.pos_pid.p = 2.0f;
        c.motor_index.pos_open  = 0.2;
        c.motor_index.pos_close = 0.6f;

        c.motor_other.pos_open  = 0.f;
        c.motor_other.pos_close = 0.f;

        c.motor_thumb.pos_open  = 0.2;
        c.motor_thumb.pos_close = 0.4f;

        c.object_id = 2;

        c.detection_threshold_per_finger = 750.f;
        c.detection_threshold_total = 2700.f;
        c.finger_count_threahold = 2.f;



        setGODCfg(c);
    }
    void FingerVisionSoftHandGraspingWidgetController::godE1LoadPreset()
    {
        ARMARX_CHECK_EXPRESSION(!_fake_mode);
        auto c = KITFingerVisionSoftHand::V1::GraspOnDetection_NJointController::make_default_cfg();

        c.motor_index.pos_pid.p = 2.0f;
        c.motor_index.pos_open  = 0.48f;
        c.motor_index.pos_close = 0.63f;

        c.motor_other.pos_open  = 0.f;
        c.motor_other.pos_close = 0.f;

        c.motor_thumb.pos_open  = 0.34f;
        c.motor_thumb.pos_close = 0.51f;

        c.object_id = 0;

        setGODCfg(c);
    }
}
//grasp exec - job control
namespace armarx
{
    void FingerVisionSoftHandGraspingWidgetController::godStop()
    {
        ARMARX_CHECK_EXPRESSION(!_fake_mode);
        godDeactivate0Torque();
        _god.stop_task();
        if (_god.ctrl_wp)
        {
            _god.ctrl_wp->stopMovement();
            _god.ctrl_wp->deactivateController();
        }
        _god.ctrl_god->deactivateController();
        godDeactivate0Torque();
    }
    void FingerVisionSoftHandGraspingWidgetController::godE3Start()
    {
        ARMARX_CHECK_EXPRESSION(!_fake_mode);
        _god.stop_task();
        _god.reset_task(_widget, getGODCfg(), this);
        _god.next_task.experiment = 3;
        _god.next_task.init = false;
        _god.start_task(_widget, "godE3Start");
    }
    void FingerVisionSoftHandGraspingWidgetController::godE2Start()
    {
        ARMARX_CHECK_EXPRESSION(!_fake_mode);
        _god.stop_task();
        _god.reset_task(_widget, getGODCfg(), this);
        _god.next_task.experiment = 2;
        _god.next_task.init = false;
        _god.start_task(_widget, "godE2Start");
    }
    void FingerVisionSoftHandGraspingWidgetController::godE1Start()
    {
        ARMARX_CHECK_EXPRESSION(!_fake_mode);
        _god.stop_task();
        _god.reset_task(_widget, getGODCfg(), this);
        _god.next_task.experiment = 1;
        _god.next_task.init = false;
        _god.start_task(_widget, "godE1Start");
    }

    void FingerVisionSoftHandGraspingWidgetController::godE3Init()
    {
        ARMARX_CHECK_EXPRESSION(!_fake_mode);
        _god.stop_task();
        _god.reset_task(_widget, getGODCfg(), this);
        _god.next_task.experiment = 3;
        _god.next_task.init = true;
        _god.start_task(_widget, "godE3Init");
    }
    void FingerVisionSoftHandGraspingWidgetController::godE2Init()
    {
        ARMARX_CHECK_EXPRESSION(!_fake_mode);
        _god.stop_task();
        _god.reset_task(_widget, getGODCfg(), this);
        _god.next_task.experiment = 2;
        _god.next_task.init = true;
        _god.start_task(_widget, "godE2Init");
    }
    void FingerVisionSoftHandGraspingWidgetController::godE1Init()
    {
        ARMARX_CHECK_EXPRESSION(!_fake_mode);
        _god.stop_task();
        _god.reset_task(_widget, getGODCfg(), this);
        _god.next_task.experiment = 1;
        _god.next_task.init = true;
        _god.start_task(_widget, "godE1Init");
    }
}
//grasp exec - worker
namespace armarx
{
    using god_data = FingerVisionSoftHandGraspingWidgetController::god_data;

    void god_data::reset_task(widget_t& widget,
                              KITFingerVisionSoftHand::V1::NJointGraspOnDetectionControllerConfig cfg,
                              FingerVisionSoftHandGraspingWidgetController* parent)
    {
        next_task = task_data{};
        next_task.god_cfg = cfg;

        next_task.e2.start_pose = e2_start_pose;
        next_task.e2.end_pose   = e2_start_pose;
        Eigen::Matrix4f translate = Eigen::Matrix4f::Identity();
        translate(1, 3) += widget.doubleSpinBoxGODE2Distance->value();
        next_task.e2.end_pose   = e2_start_pose * translate;

        next_task.e3.start_pose = e3_start_pose;
        next_task.e3.end_pose   = e3_end_pose;

        next_task.parent = parent;
    }
    void god_data::start_task(widget_t& widget, const std::string& name)
    {
        ARMARX_INFO_S << '[' << name << "] stopping old worker";
        stop_task();
        ARMARX_CHECK_NOT_EMPTY(name);
        next_task.name = name;
        worker_stop = false;
        ARMARX_INFO_S << '[' << name << "] start_task";
        worker = std::thread {[this, tdata = next_task]{ task(tdata); }};
        //setup logging
        {
            ARMARX_INFO_S << '[' << name << "] setup logging for task";
            std::lock_guard g{csv_mutex};
            if (csv_out.is_open())
            {
                csv_out.close();
            }
            const std::string fomstr = widget.lineEditGODLogfile->text().toStdString() +
                                       "_" + name + ".csv";
            FileSystemPathBuilder pb {fomstr};
            pb.createParentDirectories();
            csv_out.open(pb.getPath());
            csv_out << csv_header;
        }
        ARMARX_INFO_S << '[' << name << "] setup logging for task ...done";
    }
    void god_data::stop_task()
    {
        ARMARX_INFO_S << '[' << next_task.name << "] stopping...";
        worker_stop = true;
        if (worker.joinable())
        {
            ARMARX_INFO_S << '[' << next_task.name << "] joining...";
            worker.join();
            ARMARX_INFO_S << '[' << next_task.name << "] joining...done";
        }
        //stop logging
        {
            ARMARX_INFO_S << '[' << next_task.name << "] stop logging...";
            std::lock_guard g{csv_mutex};
            if (csv_out.is_open())
            {
                csv_out.close();
            }
            ARMARX_INFO_S << '[' << next_task.name << "] stop logging...done";
        }
        ARMARX_INFO_S << '[' << next_task.name << "] stopping...done";
    }
    void god_data::task(task_data tdata)
    {
        ARMARX_ON_SCOPE_EXIT
        {
            if (ctrl_wp)
            {
                ARMARX_INFO_S << "[task " << tdata.name << "] stop ctrl_wp";
                ctrl_wp->stopMovement();
                ctrl_wp->deactivateController();
            }
            ARMARX_INFO_S << "[task " << tdata.name << "] worker stopped";
        };
        if (ctrl_wp)
        {
            ARMARX_INFO_S << "[task " << tdata.name << "] stop ctrl_wp";
            ctrl_wp->stopMovement();
            ctrl_wp->activateController();
        }
        namespace GOD = KITFingerVisionSoftHand::V1;
        ctrl_god->configure(new KITFingerVisionSoftHand::V1::NJointGraspOnDetectionControllerConfig(tdata.god_cfg));
        ctrl_god->reset();
        ctrl_god->activateController();
        switch (tdata.experiment)
        {
            case 1 :
                task_e1(tdata);
                break;
            case 2 :
                task_e2(tdata);
                break;
            case 3 :
                task_e3(tdata);
                break;
        };
    }

    void god_data::task_e1(const task_data& tdata)
    {
        if (tdata.init)
        {
        }
        else
        {
            ctrl_god->start();
        }
    }
    void god_data::task_e2(const task_data& tdata)
    {
        if (tdata.init)
        {
            if (!ctrl_wp)
            {
                return;
            }
            ARMARX_INFO_S << "[task " << tdata.name << "]     moving";
            std::vector<Eigen::Matrix4f> wps;
            wps.emplace_back(tdata.e2.start_pose);
            NJointCartesianWaypointControllerRuntimeConfig cfg_wp;
            ctrl_wp->setConfigAndWaypoints(cfg_wp, wps);
            ctrl_wp->activateController();
            while (!ctrl_wp->hasReachedTarget() && !worker_stop)
            {
                ARMARX_INFO_S <<::deactivateSpam(1) << "[task " << tdata.name << "]      moving... waiting for target reached";
            }
            ctrl_wp->stopMovement();
            ctrl_god->setAutofire(false);
            ctrl_god->start();
        }
        else
        {
            ctrl_god->setAutofire(false);
            ctrl_god->start();
            if (ctrl_wp)
            {
                ARMARX_INFO_S << "[task " << tdata.name << "]     moving";
                std::vector<Eigen::Matrix4f> wps;
                wps.emplace_back(tdata.e2.start_pose);
                wps.emplace_back(tdata.e2.end_pose);
                NJointCartesianWaypointControllerRuntimeConfig cfg_wp;
                cfg_wp.wpCfg.maxPositionAcceleration = 300;
                cfg_wp.wpCfg.maxPosVel = 30;
                ctrl_wp->setConfigAndWaypoints(cfg_wp, wps);
                ctrl_wp->activateController();
                while (!ctrl_wp->hasReachedTarget() && !worker_stop && !ctrl_god->detected())
                {
                    ARMARX_INFO_S <<::deactivateSpam(1) << "[task " << tdata.name << "]      moving... waiting for target reached";
                }
                ctrl_wp->stopMovement();
            }
            ctrl_god->setAutofire(true);
            ctrl_god->start();
            if (ctrl_wp)
            {
                std::this_thread::sleep_for(std::chrono::milliseconds{2000});
                tdata.parent->synchronizeLocalClone(tdata.parent->_god.robot);
                std::vector<Eigen::Matrix4f> wps;
                wps.emplace_back(tdata.parent->_god.rnh_robot_arm.getKinematicChain()->getTCP()->getPoseInRootFrame());
                wps.back()(2, 3) += 250;
                NJointCartesianWaypointControllerRuntimeConfig cfg_wp;
                cfg_wp.wpCfg.maxPositionAcceleration = 300;
                cfg_wp.wpCfg.maxPosVel = 30;
                ctrl_wp->setConfigAndWaypoints(cfg_wp, wps);
                ctrl_wp->activateController();
                while (!ctrl_wp->hasReachedTarget() && !worker_stop)
                {
                    ARMARX_INFO_S <<::deactivateSpam(1) << "[task " << tdata.name << "]      moving... waiting for target reached";
                }
                ctrl_wp->stopMovement();
            }

        }
    }
    void god_data::task_e3(const task_data& tdata)
    {
        if (tdata.init)
        {
            if (!ctrl_wp)
            {
                return;
            }
            ARMARX_INFO_S << "[task " << tdata.name << "]     moving";
            std::vector<Eigen::Matrix4f> wps;
            wps.emplace_back(tdata.e3.start_pose);
            NJointCartesianWaypointControllerRuntimeConfig cfg_wp;
            ctrl_wp->setConfigAndWaypoints(cfg_wp, wps);
            ctrl_wp->activateController();
            while (!ctrl_wp->hasReachedTarget() && !worker_stop)
            {
                ARMARX_INFO_S <<::deactivateSpam(1) << "[task " << tdata.name << "]      moving... waiting for target reached";
            }
            ctrl_wp->stopMovement();
        }
        else
        {
            if (ctrl_wp)
            {
                ARMARX_INFO_S << "[task " << tdata.name << "]     moving";
                std::vector<Eigen::Matrix4f> wps;
                wps.emplace_back(tdata.e3.start_pose);
                wps.emplace_back(tdata.e3.end_pose);
                NJointCartesianWaypointControllerRuntimeConfig cfg_wp;
                ctrl_wp->setConfigAndWaypoints(cfg_wp, wps);
                ctrl_wp->activateController();
                while (!ctrl_wp->hasReachedTarget() && !worker_stop)
                {
                    ARMARX_INFO_S <<::deactivateSpam(1) << "[task " << tdata.name << "]      moving... waiting for target reached";
                }
                ctrl_wp->stopMovement();
            }
            ctrl_god->setAutofire(true);
            ctrl_god->start();
        }
    }
}
//timer
namespace armarx
{
    void FingerVisionSoftHandGraspingWidgetController::timerEvent(QTimerEvent* event)
    {
        ARMARX_TRACE;
        ARMARX_DEBUG << "timer! " << VAROUT(_hand_name) << " " << VAROUT(_streaming_handler);
        std::lock_guard g{_all_mutex};
        if (!_streaming_handler && _hand_name != "FAKE")
        {
            ARMARX_TRACE;
            ARMARX_DEBUG << "skip!";
            return;
        }

        std::array updated_finger = {0, 0, 0, 0, 0};
        //read data
        if (_hand_name == "FAKE")
        {
            ARMARX_TRACE;
            ARMARX_DEBUG << "faking data!";
            const auto fidx = std::uniform_int_distribution<char> {0, 4}(_gen);
            updated_finger.at(fidx) = true;
            auto& fdata = _cnn.finger_datas.at(fidx);
            std::uniform_int_distribution<std::size_t> dbin{0, fdata.vals.uints.size() - 1};
            std::uniform_int_distribution<std::uint32_t> dv{0, std::numeric_limits<std::uint32_t>::max()};
            for (std::size_t i = 0; i < 100; ++i)
            {
                fdata.vals.uints.at(dbin(_gen)) = dv(_gen);
            }
        }
        else
        {
            //real ingest mode
            auto& data = _streaming_handler->getDataBuffer();
            ARMARX_DEBUG << "parsing " << data.size() << " updates";
            std::size_t updated_fingers = 0;
            for (int didx = static_cast<int>(data.size()) - 1; didx >= 0 && updated_fingers < 5; --didx)
            {
                ARMARX_TRACE;
                const auto& timestep = data.at(didx);
                const auto fidx = _keys.cnn_data_finger_number(timestep);
                if (updated_finger.at(fidx))
                {
                    continue;
                }
                updated_finger.at(fidx) = true;
                ++updated_fingers;
                auto& fdata = _cnn.finger_datas.at(fidx);
                fdata.fpga_count = _keys.frame_counter(timestep);
                for (std::size_t i = 0; i < _keys.cnn_data.size(); ++i)
                {
                    fdata.vals.uints.at(i) = _keys.cnn_data.at(i)(timestep);
                }

                //csv_out
                if (_god.csv_out.is_open())
                {
                    _god.csv_out << timestep.iterationId << ';'
                                 << timestep.timestampUSec;
                    const auto write = [&](const auto & val)
                    {
                        _god.csv_out << ';' << val;
                    };
                    RobotUnitDataStreamingReceiver::VisitEntries(
                        write, timestep, _god.csv_fields);

                    _god.csv_out << ";" << _god.ctrl_god->detected() << "\n";
                }
            }
            data.clear();
        }
        //update images / gui
        {
            ARMARX_TRACE;
            for (std::size_t fidx = 0; fidx < 5; ++fidx)
            {
                if (updated_finger.at(fidx))
                {
                    continue;
                }
                ARMARX_DEBUG << "update finger " << fidx;
                auto& fdata = _cnn.finger_datas.at(fidx);

                for (int y = 0; y < fdata.img.height(); ++y)
                {
                    for (int x = 0; x < fdata.img.width(); ++x)
                    {
                        fdata.img.setPixel(x, y, fdata.vals.bitset.test(x + y * fdata.img.width()));
                    }
                }
                fdata.labelImage->setPixmap(QPixmap::fromImage(fdata.img));
                fdata.labelFPGA ->setText(QString::number(fdata.fpga_count));
                fdata.labelPop  ->setText(QString::number(fdata.vals.bitset.count()));
            }
        }
        ARMARX_DEBUG << "timer...done!";
    }
}
