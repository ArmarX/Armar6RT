﻿/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_rt::gui-plugins::FingerVisionSoftHandGraspingWidgetController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <random>
#include <bitset>
#include <array>
#include <thread>
#include <fstream>

#include <QImage>

#include <SimoxUtility/json.h>

#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>
#include <ArmarXGui/libraries/SimpleConfigDialog/SimpleConfigDialog.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotUnitComponentPlugin.h>
#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotStateComponentPlugin.h>
#include <RobotAPI/interface/units/RobotUnit/NJointCartesianWaypointController.h>

#include <armar6/rt/units/Armar6Unit/Devices/KITFingerVisionSoftHand/V1/utility/Identities.h>
#include <armar6/rt/interface/units/KITFingerVisionSoftHand/V1/NJointGraspOnDetectionController.h>

#include <armar6/rt/gui-plugins/FingerVisionSoftHandGrasping/ui_FingerVisionSoftHandGraspingWidget.h>

namespace armarx
{
    /**
    \page armar6_rt-GuiPlugins-FingerVisionSoftHandGrasping FingerVisionSoftHandGrasping
    \brief The FingerVisionSoftHandGrasping allows visualizing ...

    \image html FingerVisionSoftHandGrasping.png
    The user can

    API Documentation \ref FingerVisionSoftHandGraspingWidgetController

    \see FingerVisionSoftHandGraspingGuiPlugin
    */


    /**
     * \class FingerVisionSoftHandGraspingGuiPlugin
     * \ingroup ArmarXGuiPlugins
     * \brief FingerVisionSoftHandGraspingGuiPlugin brief description
     *
     * Detailed description
     */

    /**
     * \class FingerVisionSoftHandGraspingWidgetController
     * \brief FingerVisionSoftHandGraspingWidgetController brief one line description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT
        FingerVisionSoftHandGraspingWidgetController:
        public armarx::ArmarXComponentWidgetControllerTemplate < FingerVisionSoftHandGraspingWidgetController >,
        public virtual RobotStateComponentPluginUser,
        public virtual RobotUnitComponentPluginUser
    {
        Q_OBJECT
    public:
        explicit FingerVisionSoftHandGraspingWidgetController();
        virtual ~FingerVisionSoftHandGraspingWidgetController();

        void loadSettings(QSettings* settings) override;
        void saveSettings(QSettings* settings) override;
        QPointer<QDialog> getConfigDialog(QWidget* parent) override;
        void configured() override;
    private:
        void setupNames(const std::string& hand_name,
                        const std::string& robot_unit_name,
                        const std::string& rsc_name);
        void setGODCfg(const KITFingerVisionSoftHand::V1::NJointGraspOnDetectionControllerConfig& c);
        KITFingerVisionSoftHand::V1::NJointGraspOnDetectionControllerConfig getGODCfg() const;

    public:
        /**
         * Returns the Widget name displayed in the ArmarXGui to create an
         * instance of this class.
         */
        static QString GetWidgetName()
        {
            return "Grasping.FingerVisionSoftHand.Grasping";
        }

        void onInitComponent()       override {}
        void onConnectComponent()    override;
        void onDisconnectComponent() override;
        void onExitComponent()       override {}

    protected:
        void timerEvent(QTimerEvent* event) override;

    private slots:
        void godStop();

        void godE3Start();
        void godE2Start();
        void godE1Start();

        void godE3Init();
        void godE2Init();
        void godE1Init();

        void godE3LoadPreset();
        void godE2LoadPreset();
        void godE1LoadPreset();

        void godE3SetGoal();
        void godE3SetInit();
        void godE2SetPose();

        void godDeactivate0Torque();
        void godActivate0Torque();
        void godSendConfig();

    private:
        using widget_t =  Ui::FingerVisionSoftHandGraspingWidget;
        QPointer<SimpleConfigDialog>      _dialog;
        std::string                       _hand_name;

        mutable std::recursive_mutex      _all_mutex;
        widget_t                          _widget;
        RobotUnitDataStreamingReceiverPtr _streaming_handler;

        std::mt19937                      _gen;
        bool                              _fake_mode = false;

        struct cnn_data
        {
            struct finger_data
            {
                template<std::size_t N>
                union strange_bits
                {
                    strange_bits() : bitset() {};
                    ~strange_bits() {};

                    std::bitset<N * 32>          bitset;
                    std::array<std::uint32_t, N> uints;
                };

                strange_bits<198> vals;
                std::uint32_t     fpga_count = 0;

                QLabel* labelImage = nullptr;
                QLabel* labelFPGA  = nullptr;
                QLabel* labelPop   = nullptr;
                QImage  img;
            };
            KITFingerVisionSoftHand::V1::identity::FingerArray<finger_data> finger_datas;
        };
        cnn_data _cnn;

        struct keys
        {
            std::array<RobotUnitDataStreamingReceiver::DataEntryReader<Ice::Long>, 198> cnn_data;
            RobotUnitDataStreamingReceiver::DataEntryReader<Ice::Long> cnn_data_finger_number;
            RobotUnitDataStreamingReceiver::DataEntryReader<Ice::Long> frame_counter;
        };
        keys _keys;

        //grasp on detect
    public:
        struct god_data
        {
            using NJointGODControllerPrx = KITFingerVisionSoftHand::V1::NJointGraspOnDetectionControllerInterfacePrx;
            VirtualRobot::RobotPtr                          robot;
            NJointCartesianWaypointControllerInterfacePrx   ctrl_wp;
            NJointGODControllerPrx                          ctrl_god;
            RobotNameHelper::RobotArm                       rnh_robot_arm;

            std::size_t set_cnt_pose_e3_start = 0;
            Eigen::Matrix4f e3_start_pose  = simox::json::posquat2eigen4f(R"(
                                              {
                                                  "qw": 0.6778368949890137,
                                                  "qx": -0.6916049122810364,
                                                  "qy": -0.04508979246020317,
                                                  "qz": -0.24533002078533173,
                                                  "x": 619.2015380859375,
                                                  "y": 710.0147094726563,
                                                  "z": 1025.9853515625
                                              }
                                          )");
            std::size_t set_cnt_pose_e3_goal = 0;
            Eigen::Matrix4f e3_end_pose    = simox::json::posquat2eigen4f(R"(
                                              {
                                                  "qw": 0.6778368949890137,
                                                  "qx": -0.6916049122810364,
                                                  "qy": -0.04508979246020317,
                                                  "qz": -0.24533002078533173,
                                                  "x": 619.2015380859375,
                                                  "y": 710.0147094726563,
                                                  "z": 1025.9853515625
                                              }
                                          )");
            std::size_t set_cnt_pose_e2_start = 0;
            Eigen::Matrix4f e2_start_pose = simox::json::posquat2eigen4f(R"(
                                                                         {
                                                                             "qw": -0.2325868457555771,
                                                                             "qx": 0.6695533394813538,
                                                                             "qy": 0.5662460327148438,
                                                                             "qz": 0.4206749498844147,
                                                                             "x": 853.0943603515625,
                                                                             "y": 662.9944458007813,
                                                                             "z": 1027.8096923828125
                                                                         }
                                          )");

            struct task_data
            {
                FingerVisionSoftHandGraspingWidgetController* parent = nullptr;
                std::string name;
                KITFingerVisionSoftHand::V1::NJointGraspOnDetectionControllerConfig god_cfg;
                struct start_stop
                {
                    Eigen::Matrix4f start_pose;
                    Eigen::Matrix4f end_pose;
                };
                start_stop e2;
                start_stop e3;

                bool init = false;
                std::size_t experiment = 0;
            };
            void reset_task(widget_t& widget,
                            KITFingerVisionSoftHand::V1::NJointGraspOnDetectionControllerConfig cfg,
                            FingerVisionSoftHandGraspingWidgetController* parent);
            void start_task(widget_t& widget, const std::string& name);
            void stop_task();

            void task(task_data tdata);
            void task_e1(const task_data& tdata);
            void task_e2(const task_data& tdata);
            void task_e3(const task_data& tdata);

            task_data                                      next_task;
            std::thread                                    worker;
            std::atomic_bool                               worker_stop = true;

            std::mutex                                     csv_mutex;
            std::string                                    csv_header;
            std::vector<RobotUnitDataStreaming::DataEntry> csv_fields;
            std::ofstream                                  csv_out;
        };

    public:
        god_data _god;
    };
}
