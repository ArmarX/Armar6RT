/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_rt::ArmarXObjects::SensorizedSoftFingerHandDerivedValueCalculator
 * @author     [Author Name] ( [Author Email] )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <atomic>
#include <thread>

#include <SimoxUtility/meta/enum/adapt_enum.h>

#include <ArmarXCore/util/CPPUtility/ConfigIntrospection/create_macro.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/application/properties/PluginAll.h>
#include <ArmarXCore/libraries/ArmarXCoreComponentPlugins/DebugObserverComponentPlugin.h>

#include <ArmarXGui/libraries/ArmarXGuiComponentPlugins/RemoteGuiComponentPlugin.h>

#include <RobotAPI/libraries/RobotAPIComponentPlugins/RobotUnitComponentPlugin.h>
#include <RobotAPI/libraries/RobotUnitDataStreamingReceiver/RobotUnitDataStreamingReceiver.h>

namespace armarx::sensofihav1
{
    enum finger_encoder
    {
        little_dist,
        little_prox,
        ring_dist,
        ring_prox,
        middle_dist,
        middle_prox,
        index_dist,
        index_prox,
        thumb_dist,
        thumb_prox
    };
}

namespace simox::meta
{
    template<>
    const simox::meta::EnumNames<armarx::sensofihav1::finger_encoder>
    enum_names<armarx::sensofihav1::finger_encoder>
    {
        { armarx::sensofihav1::finger_encoder::little_dist, "little_dist"},
        { armarx::sensofihav1::finger_encoder::little_prox, "little_prox"},
        { armarx::sensofihav1::finger_encoder::ring_dist, "ring_dist"  },
        { armarx::sensofihav1::finger_encoder::ring_prox, "ring_prox"  },
        { armarx::sensofihav1::finger_encoder::middle_dist, "middle_dist"},
        { armarx::sensofihav1::finger_encoder::middle_prox, "middle_prox"},
        { armarx::sensofihav1::finger_encoder::index_dist, "index_dist" },
        { armarx::sensofihav1::finger_encoder::index_prox, "index_prox" },
        { armarx::sensofihav1::finger_encoder::thumb_dist, "thumb_dist" },
        { armarx::sensofihav1::finger_encoder::thumb_prox, "thumb_prox" }
    };
}

ARMARX_CONFIG_STRUCT_DEFINE_ADAPT_CONFIGURE(
    armarx::sensofihav1, record_joint_enc,
    (::armarx::sensofihav1::finger_encoder, finger, AX_DEFAULT(::armarx::sensofihav1::finger_encoder::thumb_prox)),
    (int, iterationts_to_average, AX_DEFAULT(1), AX_MIN(1), AX_MAX(10000))
);
ARMARX_CONFIG_STRUCT_DEFINE_ADAPT_CONFIGURE(
    armarx::sensofihav1, gui_cfg,
    (record_joint_enc, record_joint_enc_cfg)
);
namespace armarx
{
    /**
     * @defgroup Component-SensorizedSoftFingerHandDerivedValueCalculator SensorizedSoftFingerHandDerivedValueCalculator
     * @ingroup armar6_rt-Components
     * A description of the component SensorizedSoftFingerHandDerivedValueCalculator.
     *
     * @class SensorizedSoftFingerHandDerivedValueCalculator
     * @ingroup Component-SensorizedSoftFingerHandDerivedValueCalculator
     * @brief Brief description of class SensorizedSoftFingerHandDerivedValueCalculator.
     *
     * Detailed description of class SensorizedSoftFingerHandDerivedValueCalculator.
     */
    class SensorizedSoftFingerHandDerivedValueCalculator :
        public virtual RobotUnitComponentPluginUser,
        public virtual DebugObserverComponentPluginUser,
        public virtual RemoteGuiComponentPluginUser,
        public virtual Component
    {
    public:

        /// @see armarx::ManagedIceObject::getDefaultName()
        std::string getDefaultName() const override;


    protected:
        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;

        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;
    private:
        void reset();
        void calculate(const std::deque<RobotUnitDataStreaming::TimeStep>& data);
    private:
        void start();
        void stop();


        RobotUnitDataStreaming::DataStreamingDescription keys;


        std::string _sensor_prefix = "sens.RightHand*";

        mutable std::recursive_mutex      _mutex;
        std::atomic_bool                  _stop_thread{false};
        std::thread                       _thread;

        struct remote_gui
        {
            RemoteGui::WidgetPtr buildGui();
            void processGui(RemoteGui::TabProxy& prx);

            using config_t = armarx::sensofihav1::gui_cfg;
            WriteBufferedTripleBuffer<config_t>     cfg_buf;
            std::atomic_bool                        button_pressed_avg_joint_encoder = false;
        };

        remote_gui                      _remote_gui;

        struct process_data
        {
            struct encoder_data
            {
                std::string prefix;
                std::size_t iter_start_timestamp = 0;
                std::size_t iter_start = 0;
                std::size_t iter_num   = 0;
                float avg_x = 0;
                float avg_y = 0;
                float avg_z = 0;
                RobotUnitDataStreaming::DataEntry key_x;
                RobotUnitDataStreaming::DataEntry key_y;
                RobotUnitDataStreaming::DataEntry key_z;
            };
            std::map<sensofihav1::finger_encoder, encoder_data> encoders;
        };
        process_data _process_data;
    };
}
