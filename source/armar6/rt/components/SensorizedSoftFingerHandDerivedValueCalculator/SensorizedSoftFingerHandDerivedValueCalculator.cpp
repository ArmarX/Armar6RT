/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_rt::ArmarXObjects::SensorizedSoftFingerHandDerivedValueCalculator
 * @author     [Author Name] ( [Author Email] )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "SensorizedSoftFingerHandDerivedValueCalculator.h"

#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>
#include <ArmarXGui/libraries/RemoteGui/ConfigIntrospection.h>


namespace armarx
{

    ARMARX_DECOUPLED_REGISTER_COMPONENT(SensorizedSoftFingerHandDerivedValueCalculator);


    void
    SensorizedSoftFingerHandDerivedValueCalculator::reset()
    {
        ARMARX_INFO << "calling reset";
        //clear local structures
        //just print all possible name
        ARMARX_INFO << "receiving data for " << keys.entries.size() << " entries:";
        for (const auto& [name, _] : keys.entries)
        {
            ARMARX_INFO << name;
        }
        {
            _process_data.encoders.clear();
            auto init = [&](auto enu, const std::string& f, const std::string& suf)
            {
                auto& e = _process_data.encoders[enu];
                e.prefix = "sens.RightHand.finger." + f + ".raw_joint_encoder_" + suf;
                e.key_x = keys.entries.at(e.prefix + ".element_0");
                e.key_y = keys.entries.at(e.prefix + ".element_1");
                e.key_z = keys.entries.at(e.prefix + ".element_2");
            };
            init(armarx::sensofihav1::finger_encoder::little_dist, "little", "dist");
            init(armarx::sensofihav1::finger_encoder::little_prox, "little", "prox");
            init(armarx::sensofihav1::finger_encoder::ring_dist, "ring", "dist");
            init(armarx::sensofihav1::finger_encoder::ring_prox, "ring", "prox");
            init(armarx::sensofihav1::finger_encoder::middle_dist, "middle", "dist");
            init(armarx::sensofihav1::finger_encoder::middle_prox, "middle", "prox");
            init(armarx::sensofihav1::finger_encoder::index_dist, "index", "dist");
            init(armarx::sensofihav1::finger_encoder::index_prox, "index", "prox");
            init(armarx::sensofihav1::finger_encoder::thumb_dist, "thumb", "dist");
            init(armarx::sensofihav1::finger_encoder::thumb_prox, "thumb", "prox");
        }
    }
    void
    SensorizedSoftFingerHandDerivedValueCalculator::calculate(
        const std::deque<RobotUnitDataStreaming::TimeStep>& data)
    {
        ARMARX_INFO << "calling calculate with " << data.size() << " timesteps\n";
        //get key (can be saved in a variable when reset is called)

        const std::size_t iter_0_id = data.front().iterationId;
        const std::size_t iter_0_time = data.front().timestampUSec;

        const auto& gui_cfg = _remote_gui.cfg_buf.getUpToDateReadBuffer();

        if (_remote_gui.button_pressed_avg_joint_encoder.exchange(false))
        {
            auto& e = _process_data.encoders[gui_cfg.record_joint_enc_cfg.finger];
            e.iter_start_timestamp = iter_0_time;
            e.iter_start = iter_0_id;
            e.iter_num = gui_cfg.record_joint_enc_cfg.iterationts_to_average;
            e.avg_x = 0;
            e.avg_y = 0;
            e.avg_z = 0;
        }
        for (const auto& timestep : data)
        {
            const std::size_t iter_id = timestep.iterationId;
            for (auto& [_, value] : _process_data.encoders)
            {
                const auto stop_at = value.iter_start + value.iter_num;
                if (iter_id > stop_at)
                {
                    break;
                }
                if (iter_id == stop_at)
                {
                    ARMARX_IMPORTANT << value.prefix << " @ " << value.iter_start_timestamp
                                     << " -> " << value.avg_x << "\t" << value.avg_y << "\t"
                                     << value.avg_z;
                    break;
                }
                using RUDSR = RobotUnitDataStreamingReceiver;
                value.avg_x += RUDSR::GetAs<float>(timestep, value.key_x) / value.iter_num;
                value.avg_y += RUDSR::GetAs<float>(timestep, value.key_y) / value.iter_num;
                value.avg_z += RUDSR::GetAs<float>(timestep, value.key_z) / value.iter_num;
            }
        }

        const std::string name = "sens.RightHand.finger.little.joint_encoder_prox.x";
        auto key = keys.entries.at(name);

        //lambda for visitor pattern
        const auto lambda = [&](auto value) // (auto type -> auto determine type of var);
        { setDebugObserverDatafield(name, value); };

        //iterate over timeframes
        for (const auto& timestep : data)
        {

            RobotUnitDataStreamingReceiver::VisitEntry(lambda, timestep, key);
            setDebugObserverDatafield(name + "_2",
                                      RobotUnitDataStreamingReceiver::GetAs<float>(timestep, key));
        }
    }
} // namespace armarx

namespace armarx
{

    armarx::PropertyDefinitionsPtr
    SensorizedSoftFingerHandDerivedValueCalculator::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr defs =
            new ComponentPropertyDefinitions(getConfigIdentifier());

        defs->optional(_sensor_prefix, "SensorValuePrefix", "Prefix of all sensor values");
        return defs;
    }


    std::string
    SensorizedSoftFingerHandDerivedValueCalculator::getDefaultName() const
    {
        return "SensorizedSoftFingerHandDerivedValueCalculator";
    }

    void
    SensorizedSoftFingerHandDerivedValueCalculator::onInitComponent()
    {
    }

    void
    SensorizedSoftFingerHandDerivedValueCalculator::onConnectComponent()
    {
        createOrUpdateRemoteGuiTab(_remote_gui.buildGui(),
                                   [this](RemoteGui::TabProxy& prx)
                                   { _remote_gui.processGui(prx); });
        start();
    }

    void
    SensorizedSoftFingerHandDerivedValueCalculator::onDisconnectComponent()
    {
        stop();
    }

    void
    SensorizedSoftFingerHandDerivedValueCalculator::onExitComponent()
    {
        stop();
    }
    void
    SensorizedSoftFingerHandDerivedValueCalculator::stop()
    {
        std::lock_guard g{_mutex};
        _stop_thread = true;
        if (_thread.joinable())
        {
            _thread.join();
        }
    }
    void
    SensorizedSoftFingerHandDerivedValueCalculator::start()
    {
        std::lock_guard g{_mutex};
        stop();

        RobotUnitDataStreaming::Config cfg;
        cfg.loggingNames.emplace_back(_sensor_prefix);

        RobotUnitDataStreamingReceiverPtr streaming_handler =
            make_shared<RobotUnitDataStreamingReceiver>(this, getRobotUnit(), cfg);


        keys = streaming_handler->getDataDescription();

        const auto job = [handler = streaming_handler, this]
        {
            setDebugObserverBatchModeEnabled(true);
            reset();
            while (!_stop_thread)
            {
                auto& data = handler->getDataBuffer();
                if (!data.empty())
                {
                    calculate(data);
                }
                sendDebugObserverBatch();
                data.clear();
            }
        };
        _stop_thread = false;
        _thread = std::thread{job};
    }
} // namespace armarx
//gui
namespace armarx
{
    void
    SensorizedSoftFingerHandDerivedValueCalculator::remote_gui::processGui(RemoteGui::TabProxy& prx)
    {
        ARMARX_TRACE;
        prx.receiveUpdates();
        {
            auto& buf = cfg_buf.getWriteBuffer();
            prx.getValue(buf, "config");
            cfg_buf.commitWrite();
        }
        if (prx.getButtonClicked("record_joint_enc"))
        {
            button_pressed_avg_joint_encoder = true;
        }
        //update
        prx.sendUpdates();
    }

    RemoteGui::WidgetPtr
    SensorizedSoftFingerHandDerivedValueCalculator::remote_gui::buildGui()
    {
        ARMARX_TRACE;
        return RemoteGui::makeVBoxLayout()
            .addChild(RemoteGui::makeButton("record_joint_enc"))
            .addChild(RemoteGui::MakeGuiConfig("config", cfg_buf.getWriteBuffer()));
    }
} // namespace armarx
