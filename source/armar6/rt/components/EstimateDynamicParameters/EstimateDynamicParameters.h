/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_rt::ArmarXObjects::EstimateDynamicParameters
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <Eigen/Core>

#include <ArmarXCore/core/Component.h>

#include <VirtualRobot/Nodes/RobotNode.h>

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/Tools/Gravity.h>
#include <nlopt.hpp>
#include <IceUtil/UUID.h>
#include <ArmarXGui/interface/StaticPlotterInterface.h>
#include <RobotAPI/components/units/RobotUnit/util/EigenForwardDeclarations.h>

namespace armarx
{

    // csv header example:
    // timestamp ms,FT L.fx,FT L.fy,FT L.fz,FT L.tx,FT L.ty,FT L.tz,FT L.adc1,FT L.adc2,FT L.adc3,FT L.adc4,FT L.adc5,FT L.adc6,FT R.fx,FT R.fy,FT R.fz,FT R.tx,FT R.ty,FT R.tz,FT R.adc1,FT R.adc2,FT R.adc3,FT R.adc4,FT R.adc5,FT R.adc6,ArmL1_Cla1.position,ArmL1_Cla1.velocity,ArmL1_Cla1.torque,ArmL1_Cla1.torqueTicks,ArmL2_Sho1.position,ArmL2_Sho1.velocity,ArmL2_Sho1.torque,ArmL2_Sho1.torqueTicks,ArmL3_Sho2.position,ArmL3_Sho2.velocity,ArmL3_Sho2.torque,ArmL3_Sho2.torqueTicks,ArmL4_Sho3.position,ArmL4_Sho3.velocity,ArmL4_Sho3.torque,ArmL4_Sho3.torqueTicks,ArmL5_Elb1.position,ArmL5_Elb1.velocity,ArmL5_Elb1.torque,ArmL5_Elb1.torqueTicks,ArmL6_Elb2.position,ArmL6_Elb2.velocity,ArmL6_Elb2.torque,ArmL6_Elb2.torqueTicks,ArmL7_Wri1.position,ArmL7_Wri1.velocity,ArmL7_Wri1.torque,ArmL7_Wri1.torqueTicks,ArmL8_Wri2.position,ArmL8_Wri2.velocity,ArmL8_Wri2.torque,ArmL8_Wri2.torqueTicks,ArmR1_Cla1.position,ArmR1_Cla1.velocity,ArmR1_Cla1.torque,ArmR1_Cla1.torqueTicks,ArmR2_Sho1.position,ArmR2_Sho1.velocity,ArmR2_Sho1.torque,ArmR2_Sho1.torqueTicks,ArmR3_Sho2.position,ArmR3_Sho2.velocity,ArmR3_Sho2.torque,ArmR3_Sho2.torqueTicks,ArmR4_Sho3.position,ArmR4_Sho3.velocity,ArmR4_Sho3.torque,ArmR4_Sho3.torqueTicks,ArmR5_Elb1.position,ArmR5_Elb1.velocity,ArmR5_Elb1.torque,ArmR5_Elb1.torqueTicks,ArmR6_Elb2.position,ArmR6_Elb2.velocity,ArmR6_Elb2.torque,ArmR6_Elb2.torqueTicks,ArmR7_Wri1.position,ArmR7_Wri1.velocity,ArmR7_Wri1.torque,ArmR7_Wri1.torqueTicks,ArmR8_Wri2.position,ArmR8_Wri2.velocity,ArmR8_Wri2.torque,ArmR8_Wri2.torqueTicks
    // several hundred examples should suffice for dynamic calibration


    /**
     * @class EstimateDynamicParametersPropertyDefinitions
     * @brief
     */
    class EstimateDynamicParametersPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        EstimateDynamicParametersPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("CSVFile", "Path to the CSV File with the sensor recordings");
            defineOptionalProperty<std::string>("RobotFilename", "armar6_rt/robotmodel/Armar6-SH/Armar6-SH.xml", "Path to the Robot file that should be used");
            defineOptionalProperty<std::string>("BusXMLPath", "armar6_rt/HardwareConfig/Armar6Bus.xml", "Path to the Robot Bus XML file that should be used");
            defineOptionalProperty<std::string>("PackageNames", "armar6_rt", "Comma seperated list of packages to retrieve the datapaths from.");
            defineOptionalProperty<int>("LineSkipOptimization", 10, "Process only every n-th data entry during optimization");
            defineOptionalProperty<int>("LineSkipReading", 10, "Read only every n-th line from the csv file");
            defineOptionalProperty<int>("MaxNumThreads", -1, "Number of threads for parallel optimization. -1 for number of cores of cpu");
            defineOptionalProperty<std::string> ("Joints", "ArmR2_Sho1, ArmR3_Sho2, ArmR4_Sho3, ArmR5_Elb1, ArmR6_Elb2, ArmR7_Wri1, ArmR8_Wri2, Hand R Col", "Comma seperated list of joints that should be optimized");
            defineOptionalProperty<std::string> ("JointNameInfix", "ArmR", "Infix for joints that should be updated on the robot model");

            defineOptionalProperty<float>("MaxOptimizationTime", 60 * 3, "Timeout for the optimization in seconds");
            defineOptionalProperty<float>("GaussianFilterRadius", 0.01f, "Apply a gaussian filter with a radius in seconds");
        }
    };

    std::string NlOptResultToString(nlopt::result result)
    {
        switch (result)
        {
            case nlopt::FAILURE:
                return "FAILURE"; /* generic failure code */
            case nlopt::INVALID_ARGS:
                return "INVALID_ARGS";
            case nlopt::OUT_OF_MEMORY:
                return "OUT_OF_MEMORY";
            case nlopt::ROUNDOFF_LIMITED:
                return "ROUNDOFF_LIMITED";
            case nlopt::FORCED_STOP:
                return "FORCED_STOP";
            case nlopt::SUCCESS:
                return "SUCCESS"; /* generic success code */
            case nlopt::STOPVAL_REACHED:
                return "STOPVAL_REACHED";
            case nlopt::FTOL_REACHED:
                return "FTOL_REACHED";
            case nlopt::XTOL_REACHED:
                return "XTOL_REACHED";
            case nlopt::MAXEVAL_REACHED:
                return "MAXEVAL_REACHED";
            case nlopt::MAXTIME_REACHED:
                return "MAXTIME_REACHED";
        }
        return "UNKNOWN";
    }


    /**
     * @defgroup Component-EstimateDynamicParameters EstimateDynamicParameters
     * @ingroup armar6_rt-Components
     * A description of the component EstimateDynamicParameters.
     *
     * @class EstimateDynamicParameters
     * @ingroup Component-EstimateDynamicParameters
     * @brief Brief description of class EstimateDynamicParameters.
     *
     * Detailed description of class EstimateDynamicParameters.
     */
    class EstimateDynamicParameters :
        virtual public armarx::Component
    {
    public:

        struct FTSensorInfo
        {
            VirtualRobot::SensorPtr sensorNode;
            double mass;
            double ticksToVoltFactor = 0.0;
            Eigen::MatrixXf calibrationMatrix;
            Eigen::Vector3f comLocationLocal;
        };

        struct JointInfo
        {
            VirtualRobot::RobotNodePtr bodyNode;
            Eigen::VectorXd CoM;
            double mass = 0;
            double torqueTickFactor = 0;
            double torqueTickOffset = 0;
            bool optimize = false;
            bool onlyBody = false;
        };

        struct JointSensorValues
        {
            float jointValue = 0.0f;
            float torque = 0.0f;
            float torqueTicks = 0.0f;
        };

        using JointInfoMap = std::map<std::string, JointInfo>;
        using FTSensorInfoMap = std::map<std::string, FTSensorInfo>;

        class CSVHelper
        {
        public:
            CSVHelper(VirtualRobot::RobotPtr robot);
            void addHeader(const std::string& line, const Ice::StringSeq& jointsToUse, const std::string& infix);
            void addLine(const std::string& line);
            void parseBusXML(const std::string& busXMLPath);
            Ice::DoubleSeq toInitialConfiguration();
            Ice::DoubleSeq toConfiguration();
            static std::string JointInfoToString(const JointInfoMap& jointData);
            std::string configurationToString(const Ice::DoubleSeq& configuration);
            void fromConfiguration(const Ice::DoubleSeq& configuration,  JointInfoMap& jointData);
            JointInfoMap applyConfigurationToRobot(const Ice::DoubleSeq& configuration);
            const std::vector<std::map<std::string, JointSensorValues> >& getJointSensorData() const;
            const std::vector<std::map<std::string, JointSensorValues> >& getJointSensorTestData() const;

            std::pair<Ice::DoubleSeq, Ice::DoubleSeq> getBounds() const;
            const JointInfoMap& getJointData() const;

            void applyGaussianFilterToTorque(float filterRadius, StaticPlotterInterfacePrx plotter = NULL);

            const std::vector<std::map<std::string, Eigen::Vector6i> >& getFtSensorData() const;
            void addBodies(const Ice::StringSeq& bodies);
            const std::map<std::string, double>& getLossScaling() const;


            bool getOptimizeMass() const;
            void setOptimizeMass(bool value);

        protected:
            VirtualRobot::RobotPtr robot;
            std::map<std::string, size_t> nameToIndexMapping, ftSensorNameToIndexMapping;
            JointInfoMap jointData;
            std::vector<std::map<std::string, JointSensorValues>> jointSensorData, jointSensorTestData;
            std::vector<std::map<std::string, Eigen::Vector6i>> ftSensorData;
            FTSensorInfoMap ftInfoData;
            std::map<std::string, double> lossScaling;
            bool optimizeMass = false;
        };
        using CSVHelperPtr = std::shared_ptr<CSVHelper>;

        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "EstimateDynamicParameters";
        }

        void optimize();
        void optimizeFT(CSVHelperPtr csv);

        static std::pair<double, Eigen::Vector3f> CalculateMass(VirtualRobot::SensorPtr sensorNode);
    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;


        void setOptimizationBounds(nlopt::opt& optimizer, const Ice::DoubleSeq& intialConfig) const;
        static double objectiveFunctionWrapperStatic(const std::vector<double>& configuration, std::vector<double>& grad, void* data);
        double objectiveFunction(const std::vector<double>& configuration, std::vector<double>& grad, bool verbose = false);
        Eigen::VectorXd assessJointParametrization(const JointInfoMap& jointData, bool verbose = false, bool useTestData = false, const std::string& csvPath = "");
        Eigen::VectorXd assessJointFTParametrization(const FTSensorInfoMap& ftInfoData, bool verbose = false, bool writeCSVFile = false);


        struct RobotData
        {
            VirtualRobot::RobotPtr robot;
            VirtualRobot::GravityPtr gravity;
        };
        std::vector<RobotData> robotDataStructs;
        CSVHelperPtr csv;

        Vector2fSeq lossProgress;
        StaticPlotterInterfacePrx plotter;
    };
}
