/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armar6_rt::ArmarXObjects::EstimateDynamicParameters
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "EstimateDynamicParameters.h"

#include <fstream>
#include <memory>

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/join.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/shared_ptr.hpp>

#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/XML/RobotIO.h>

#include <ArmarXCore/core/ArmarXManager.h>
#include <ArmarXCore/core/ArmarXObjectScheduler.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>
#include <ArmarXGui/interface/StaticPlotterInterface.h>
#include <RobotAPI/libraries/core/Trajectory.h>

#include <omp.h>


using boost::dynamic_pointer_cast;
using std::dynamic_pointer_cast;


namespace armarx
{

    ARMARX_DECOUPLED_REGISTER_COMPONENT(EstimateDynamicParameters);


    void
    EstimateDynamicParameters::optimizeFT(CSVHelperPtr csv)
    {
    }


    void
    EstimateDynamicParameters::optimize()
    {
        lossProgress.clear();

        std::vector<double> configuration = csv->toInitialConfiguration();
        Eigen::VectorXd initialConfiguration =
            Eigen::Map<Eigen::VectorXd>(configuration.data(), configuration.size());

        ARMARX_INFO << "average difference per joint: "
                    << assessJointParametrization(
                           csv->getJointData(), true, false, "/tmp/diffBefore.csv");

        std::vector<nlopt::algorithm> nloptAlgorithms = {
            //        nlopt::GN_MLSL_LDS,
            //                                                     nlopt::GN_MLSL, nlopt::GN_ORIG_DIRECT, nlopt::GN_ESCH, nlopt::GN_ISRES,
            nlopt::LN_COBYLA,
            nlopt::LN_SBPLX, //nlopt::LN_NEWUOA_BOUND,
            nlopt::LN_NELDERMEAD,
            nlopt::LN_AUGLAG,
            nlopt::LN_AUGLAG_EQ,
            nlopt::LN_BOBYQA,
            nlopt::LN_PRAXIS};
        int run = 1;
        for (auto nloptAlgorithm : nloptAlgorithms)
        {
            double objectiveValue = std::nan("");

            std::vector<double> configuration = csv->toConfiguration();
            try
            {
                ARMARX_INFO << "Dimensionality: " << configuration.size() << "\n"
                            << VAROUT(configuration);
                ARMARX_INFO << "Initial values: " << csv->configurationToString(configuration);
                // Initialize optimization
                nlopt::opt optimizer(nloptAlgorithm, configuration.size());
                ARMARX_IMPORTANT << "Starting optimization run " << run++ << " using algorithm "
                                 << optimizer.get_algorithm_name();
                optimizer.set_min_objective(
                    EstimateDynamicParameters::objectiveFunctionWrapperStatic, this);
                optimizer.set_ftol_abs(-0.000001);
                optimizer.set_ftol_rel(-0.000001);
                optimizer.set_xtol_abs(-0.00001);
                optimizer.set_xtol_rel(-0.00001);
                optimizer.set_stopval(0.00015);
                optimizer.set_maxtime(getProperty<float>("MaxOptimizationTime").getValue());


                auto boundsPair = csv->getBounds();
                optimizer.set_lower_bounds(boundsPair.first);
                optimizer.set_upper_bounds(boundsPair.second);
                //        setOptimizationBounds(optimizer, configuration);

                // Run optimization

                nlopt::result resultCode = optimizer.optimize(configuration, objectiveValue);
                ARMARX_INFO << "Optimization finished with code " << NlOptResultToString(resultCode)
                            << ". " << std::endl;
            }
            catch (const nlopt::roundoff_limited&)
            {
                ARMARX_INFO << "Optimization finished by throwing nlopt::roundoff_limited (the "
                               "result should be usable)."
                            << std::endl;
            }
            catch (const std::invalid_argument& e)
            {
                ARMARX_ERROR << " nlopt failed with: " << e.what();
            }
            ARMARX_INFO << "Final loss: " << objectiveValue
                        << " Final configuration: " << csv->configurationToString(configuration);
            JointInfoMap jointData = csv->applyConfigurationToRobot(configuration);
            ARMARX_INFO << "average difference per joint: "
                        << assessJointParametrization(jointData, true, false, "/tmp/diffAfter.csv");
            ARMARX_INFO << "average difference per joint on TESTDATA: "
                        << assessJointParametrization(jointData, false, true, "/tmp/diffAfter.csv");
            //        Eigen::VectorXd configVec = Eigen::Map<Eigen::VectorXd>(configuration.data(), configuration.size());
            //        Eigen::VectorXd diff = configVec - initialConfiguration;
            //        ARMARX_INFO << "difference between before and after " << configVec.rows() << " vs " << initialConfiguration.rows() << ":\n" << (diff);
            csv->setOptimizeMass(!csv->getOptimizeMass());
        }
    }


    void
    EstimateDynamicParameters::onInitComponent()
    {
        srand(IceUtil::Time::now().toMicroSeconds());
        auto packages = Split(getProperty<std::string>("PackageNames"), ",", true, true);
        for (auto& p : packages)
        {
            CMakePackageFinder finder(p);
            ARMARX_CHECK_EXPRESSION(finder.packageFound()) << p;
            ArmarXDataPath::addDataPaths(finder.getDataDir());
        }
        std::string filename;
        ArmarXDataPath::getAbsolutePath(getProperty<std::string>("RobotFilename").getValue(),
                                        filename);
        auto robot = VirtualRobot::RobotIO::loadRobot(filename);


        if (!robot)
        {
            ARMARX_ERROR << " ERROR while creating robot";
            return;
        }

        if (getProperty<int>("MaxNumThreads").getValue() > 0)
        {
            omp_set_num_threads(getProperty<int>("MaxNumThreads").getValue());
        }

        int numThreads = omp_get_max_threads();
        ARMARX_INFO << "Creating " << numThreads << " robot copies for parallel optimization";
        robotDataStructs.resize(numThreads);
        int i = 0;
        for (auto& data : robotDataStructs)
        {
            data.robot = robot->clone(std::to_string(i));
            data.robot->setUpdateVisualization(false);
            data.robot->setUpdateCollisionModel(false);
            data.robot->setThreadsafe(false);
            auto robotJointSet = data.robot->getRobotNodeSet("RobotJoints");
            auto robotBodySet = data.robot->getRobotNodeSet("RobotCol");
            ARMARX_CHECK_EXPRESSION(robotJointSet);
            ARMARX_CHECK_EXPRESSION(robotBodySet);
            data.gravity.reset(new VirtualRobot::Gravity(data.robot, robotJointSet, robotBodySet));
        }
    }


    void
    EstimateDynamicParameters::onConnectComponent()
    {
        plotter = getTopic<StaticPlotterInterfacePrx>("StaticPlotter");
        std::string filename;
        ArmarXDataPath::getAbsolutePath(getProperty<std::string>("CSVFile").getValue(), filename);

        std::ifstream infile(filename);
        ARMARX_CHECK_EXPRESSION(infile.is_open()) << filename;


        std::string line;
        int i = 0;
        int lineSkip = getProperty<int>("LineSkipReading");
        std::getline(infile, line);
        csv.reset(new CSVHelper(robotDataStructs.at(0).robot));
        std::string infix = getProperty<std::string>("JointNameInfix");
        auto joints = Split(getProperty<std::string>("Joints"), ",", true, true);
        csv->addHeader(line, joints, infix);
        for (auto& j : joints)
        {
            if (!csv->getJointData().count(j))
            {
                csv->addBodies({j});
            }
        }
        //    Ice::StringSeq additionalBodies = {"Hand R Col"};
        //    csv->addBodies(additionalBodies);
        std::string busXMLFilename;
        ArmarXDataPath::getAbsolutePath(getProperty<std::string>("BusXMLPath").getValue(),
                                        busXMLFilename);
        csv->parseBusXML(busXMLFilename);
        while (std::getline(infile, line))
        {
            if (i % lineSkip == 0)
            {
                csv->addLine(line);
            }
            if (i % 10000 == 0)
            {
                ARMARX_INFO << "Reading data .... " << i << " lines";
            }
            i++;
        }

        ARMARX_INFO << "Loaded " << csv->getJointSensorData().size() << " samples";

        float filterRadius = getProperty<float>("GaussianFilterRadius").getValue();
        if (filterRadius > 0)
        {
            csv->applyGaussianFilterToTorque(filterRadius, plotter);
        }
        optimize();
        plotter->addPlot("loss", {{"loss", lossProgress}});
        ::exit(0);
    }


    void
    EstimateDynamicParameters::onDisconnectComponent()
    {
    }


    void
    EstimateDynamicParameters::onExitComponent()
    {
    }

    armarx::PropertyDefinitionsPtr
    EstimateDynamicParameters::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(
            new EstimateDynamicParametersPropertyDefinitions(getConfigIdentifier()));
    }

    double
    EstimateDynamicParameters::objectiveFunctionWrapperStatic(
        const std::vector<double>& configuration,
        std::vector<double>& grad,
        void* data)
    {
        return static_cast<EstimateDynamicParameters*>(data)->objectiveFunction(
            configuration, grad, false);
    }

    double
    EstimateDynamicParameters::objectiveFunction(const std::vector<double>& configuration,
                                                 std::vector<double>& grad,
                                                 bool verbose)
    {
        ARMARX_CHECK_EXPRESSION(grad.empty());
        //    TIMING_START(objectiveFunction);
        double loss = 0.0;
        auto jointData = csv->applyConfigurationToRobot(configuration);
        //    JointInfoMap jointData;
        //    //    ARMARX_INFO << VAROUT(configuration);
        //    csv->fromConfiguration(configuration, jointData);
        //    for (auto& joint : jointData)
        //    {
        //        auto& info = joint.second;
        //        info.bodyNode->setCoMLocal(info.CoM.cast<float>());
        //        info.bodyNode->setMass(info.mass);
        //        //        ARMARX_INFO << info.bodyNode->getName() << " mass: " << info.bodyNode->getMass() << " com: " << info.bodyNode->getCoMLocal();
        //    }
        Eigen::VectorXd lossPerJoint = assessJointParametrization(jointData, verbose);
        loss = lossPerJoint.mean();
        //    TIMING_END(objectiveFunction);
        ARMARX_INFO << deactivateSpam(5)
                    << "Current Loss: " << loss /*<< " config: \n" << configuration*/;
        lossProgress.push_back({(float)lossProgress.size(), (float)loss});
        return loss;
    }

    Eigen::VectorXd
    EstimateDynamicParameters::assessJointParametrization(const JointInfoMap& jointData,
                                                          bool verbose,
                                                          bool useTestData,
                                                          const std::string& csvPath)
    {
        std::shared_ptr<std::ofstream> f;
        if (!csvPath.empty())
        {
            f.reset(new std::ofstream(csvPath));
        }

        int optimizedJoints = 0;
        for (auto& joint : jointData)
        {
            if (joint.second.optimize && !joint.second.onlyBody)
            {
                optimizedJoints++;
            }
        }
        std::vector<Eigen::VectorXd> diffPerJoint = std::vector<Eigen::VectorXd>(
            robotDataStructs.size(), Eigen::VectorXd::Zero(optimizedJoints));

        int lineSkip = getProperty<int>("LineSkipOptimization");

        std::atomic<size_t> i;
        i = 0;
        std::atomic<int> processedEntries;
        processedEntries = 0;
        auto& sensorData = useTestData ? csv->getJointSensorTestData() : csv->getJointSensorData();
        size_t stepSize = sensorData.size() / 10;

        Ice::FloatSeq timestamps;
        StringFloatSeqDict plotsData;
        //    for (auto& sensorDataMap  : sensorData)

        auto executeOpt = [&]
        {
            //        ARMARX_INFO << "Current line " << i;
            auto& sensorDataMap = sensorData.at(i);
            if (i % lineSkip != 0)
            {
                return;
            }
            if (this->getObjectScheduler()->isTerminationRequested())
            {
                throw LocalException("Shutdown requested");
            }
            timestamps.push_back(i);
            int thread_num = omp_get_thread_num();
            std::map<std::string, float> jointValues;
            for (auto& sensorData : sensorDataMap)
            {
                jointValues[sensorData.first] = sensorData.second.jointValue;
            }
            robotDataStructs.at(thread_num).robot->setJointValues(jointValues);
            //        TIMING_START(Gravity);
            auto robotJointSet =
                robotDataStructs.at(thread_num).robot->getRobotNodeSet("RobotJoints");
            auto robotBodySet = robotDataStructs.at(thread_num).robot->getRobotNodeSet("RobotCol");
            //        float massSum = robotBodySet->getMass();
            ARMARX_CHECK_EXPRESSION(robotJointSet);
            ARMARX_CHECK_EXPRESSION(robotBodySet);
            robotDataStructs.at(thread_num)
                .gravity.reset(new VirtualRobot::Gravity(
                    robotDataStructs.at(thread_num).robot, robotJointSet, robotBodySet));
            std::map<std::string, float> gravityTorques =
                robotDataStructs.at(thread_num).gravity->computeGravityTorque();
            //        TIMING_END(Gravity);
            int jointIndex = 0;
            Ice::StringSeq csv;
            csv.push_back(std::to_string(processedEntries));
            for (auto& sensorData : sensorDataMap)
            {
                auto jointInfo = jointData.at(sensorData.first);
                if (jointInfo.optimize)
                {
                    double measuredTorque =
                        (sensorData.second.torqueTicks + jointInfo.torqueTickOffset) *
                        jointInfo.torqueTickFactor;
                    double torqueDiff =
                        std::abs(measuredTorque + gravityTorques.at(sensorData.first));

                    csv.push_back(std::to_string(measuredTorque));
                    csv.push_back(std::to_string(-gravityTorques.at(sensorData.first)));
                    csv.push_back(std::to_string(torqueDiff));
                    std::stringstream s;
                    s << "#" << i << ": "
                      << sensorData.first
                      //              << "  ticks: " << sensorData.second.torqueTicks << VAROUT(jointInfo.torqueTickOffset) << VAROUT(jointInfo.torqueTickFactor) << " - "
                      << " " << VAROUT(measuredTorque)
                      << " gravity: " << (-gravityTorques.at(sensorData.first)) << " "
                      << VAROUT(torqueDiff);
                    if (i % stepSize == 0)
                    {
                        if (verbose)
                        {
                            ARMARX_INFO << s.str();
                        }
                        else
                        {
                            //                    ARMARX_INFO << deactivateSpam(5, sensorData.first + std::to_string(i)) << s.str();
                        }
                    }
                    if (verbose)
                    {
                        plotsData[sensorData.first].push_back(torqueDiff);
                    }
                    auto it = this->csv->getLossScaling().find(sensorData.first);
                    if (it != this->csv->getLossScaling().end())
                    {
                        torqueDiff *= it->second;
                    }
                    //                torqueDiff /= massSum / 100;
                    diffPerJoint.at(thread_num)(jointIndex) += torqueDiff * torqueDiff;
                    jointIndex++;
                }
            }
            if (f)
            {
                std::string s = boost::algorithm::join(csv, ",");
                //            ARMARX_INFO << "Writing line " << processedEntries;
                *f << s << std::endl;
            }
            if (i % stepSize == 0)
            {
                if (verbose)
                {
                    ARMARX_INFO << "\n";
                }
                else
                {
                    //                ARMARX_INFO << deactivateSpam(5, std::to_string(i)) << "\n";
                }
            }
            processedEntries++;
        };


        if (true) //f || getProperty<int>("MaxNumThreads").getValue() <= 1)
        {
            for (i = 0; i < sensorData.size(); i++)
            {
                executeOpt();
            }
        }
        else
        {
#pragma omp parallel for
            for (i = 0; i < sensorData.size(); i++)
            {
                executeOpt();
            }
        }
        Eigen::VectorXd diffPerJointSum = Eigen::VectorXd::Zero(optimizedJoints);
        for (auto& elem : diffPerJoint)
        {
            diffPerJointSum += elem;
        }
        if (verbose)
        {
            plotter->addPlotWithTimestampVector(
                std::to_string(diffPerJointSum.mean() / processedEntries), timestamps, plotsData);
        }
        return diffPerJointSum / processedEntries;
    }

    Eigen::VectorXd
    EstimateDynamicParameters::assessJointFTParametrization(const FTSensorInfoMap& ftInfoData,
                                                            bool verbose,
                                                            bool writeCSVFile)
    {
        std::shared_ptr<std::ofstream> f;
        if (writeCSVFile)
        {
            f.reset(new std::ofstream("/tmp/diff.csv"));
        }

        Eigen::VectorXd diffPerJoint = Eigen::VectorXd::Zero(ftInfoData.size());

        int lineSkip = getProperty<int>("LineSkipOptimization");

        size_t i = 0;
        int processedEntries = 0;
        int stepSize = csv->getJointSensorData().size() / 10;
#pragma omp parallel for
        for (i = 0; i < csv->getJointSensorData().size(); i++)
        //    for (auto& sensorDataMap  : csv->getJointSensorData())
        {
            auto& ftSensorDataMap = csv->getFtSensorData().at(i);
            if (i % lineSkip != 0)
            {
                i++;
                continue;
            }
            if (this->getObjectScheduler()->isTerminationRequested())
            {
                throw LocalException("Shutdown requested");
            }
            int thread_num = omp_get_thread_num();
            std::map<std::string, float> jointValues;
            for (auto& sensorData : csv->getJointSensorData().at(i))
            {
                jointValues[sensorData.first] = sensorData.second.jointValue;
            }
            robotDataStructs.at(thread_num).robot->setJointValues(jointValues);


            //        TIMING_START(Gravity);

            //        TIMING_END(Gravity);
            int sensorIndex = 0;
            Ice::StringSeq csv;
            csv.push_back(std::to_string(i));
            for (auto& sensorData : ftSensorDataMap)
            {
                auto ftSensorInfo = ftInfoData.at(sensorData.first);
                auto parent = ftSensorInfo.sensorNode->getParent();
                Eigen::Vector3f gravityVec(0, 0, 9.81f * ftSensorInfo.mass);

                Eigen::Vector6f measuredFT =
                    ftSensorInfo.calibrationMatrix *
                    (sensorData.second.cast<float>() * ftSensorInfo.ticksToVoltFactor);

                Eigen::Matrix4f parentGlobalPose = parent->getGlobalPose();
                Eigen::Matrix3f globalParentOrientation = parentGlobalPose.block<3, 3>(0, 0);
                Eigen::Matrix4f sensorPose = ftSensorInfo.sensorNode->getGlobalPose();
                Eigen::Vector2f sensorPosition = sensorPose.block<2, 1>(0, 3);
                Eigen::Vector2f comPositionGlobal =
                    (globalParentOrientation * ftSensorInfo.comLocationLocal.cast<float>() +
                     parentGlobalPose.block<3, 1>(0, 3))
                        .block<2, 1>(0, 0);


                //    Eigen::Vector3f forceGlobal = changeDirectionFrameToGlobal(sensorValue.force, parent);
                Eigen::Vector3f forceGlobal = globalParentOrientation * measuredFT.head(3);
                Eigen::Vector3f torqueGlobal = globalParentOrientation * measuredFT.tail(3);
                Eigen::Vector6f measuredFTGlobal;
                measuredFTGlobal.head(3) = forceGlobal;
                measuredFTGlobal.tail(3) = torqueGlobal;

                float wrench = (comPositionGlobal - sensorPosition).norm();
                Eigen::Vector3f gravityTorque = wrench * 0.001f * gravityVec;
                Eigen::Vector6f gravity6d;
                gravity6d.head(3) = gravityVec;
                gravity6d.tail(3) = gravityTorque;
                double torqueDiff = (measuredFTGlobal - gravity6d).norm();
                //            csv.push_back(std::to_string(measuredTorque));
                //            csv.push_back(std::to_string(gravityTorques.at(sensorData.first)));
                //            std::stringstream s;
                //            s << "#" << i << ": " <<  sensorData.first
                //              //              << "  ticks: " << sensorData.second.torqueTicks << VAROUT(jointInfo.torqueTickOffset) << VAROUT(jointInfo.torqueTickFactor) << " - "
                //              << " " << VAROUT(measuredTorque) << " gravity: " << gravityTorques.at(sensorData.first) << VAROUT(torqueDiff);
                //            if (i % stepSize == 0)
                //            {
                //                if (verbose)
                //                {
                //                    ARMARX_INFO << s.str();
                //                }
                //                else
                //                {
                //                    //                    ARMARX_INFO << deactivateSpam(5, sensorData.first + std::to_string(i)) << s.str();
                //                }

                //            }

                diffPerJoint(sensorIndex) += torqueDiff * torqueDiff;
                sensorIndex++;
            }
            if (f)
            {
                std::string s = boost::algorithm::join(csv, ",");
                *f << s << std::endl;
            }
            if (i % stepSize == 0)
            {
                if (verbose)
                {
                    ARMARX_INFO << "\n";
                }
                else
                {
                    //                ARMARX_INFO << deactivateSpam(5, std::to_string(i)) << "\n";
                }
            }
            i++;
            processedEntries++;
        }

        return diffPerJoint / processedEntries;
    }

    void
    EstimateDynamicParameters::setOptimizationBounds(nlopt::opt& optimizer,
                                                     const Ice::DoubleSeq& intialConfig) const
    {
        Ice::DoubleSeq lowerBounds, upperBounds;
        for (auto& val : intialConfig)
        {
            if (val > 0)
            {
                lowerBounds.push_back(val * 0.8);
                upperBounds.push_back(val * 1.2);
            }
            else
            {
                upperBounds.push_back(val * 0.8);
                lowerBounds.push_back(val * 1.2);
            }
        }
        optimizer.set_lower_bounds(lowerBounds);
        optimizer.set_upper_bounds(upperBounds);
    }


    EstimateDynamicParameters::CSVHelper::CSVHelper(VirtualRobot::RobotPtr robot) : robot(robot)
    {
        lossScaling["ArmL5_Elb1"] = 2;
        lossScaling["ArmL6_Elb2"] = 4;
        lossScaling["ArmL7_Wri1"] = 10;
        lossScaling["ArmL8_Wri2"] = 10;
        lossScaling["ArmR5_Elb1"] = 2;
        lossScaling["ArmR6_Elb2"] = 4;
        lossScaling["ArmR7_Wri1"] = 10;
        lossScaling["ArmR8_Wri2"] = 10;
    }

    void
    EstimateDynamicParameters::CSVHelper::addHeader(const std::string& line,
                                                    const Ice::StringSeq& jointsToUse,
                                                    const std::string& infix)
    {
        auto columnNames = armarx::Split(line, ",", true, false);
        size_t i = 0;
        while (i < columnNames.size())
        {
            auto col = columnNames.at(i);
            auto pos = col.find(".position");
            auto pos2 = col.find(infix);
            if (pos != std::string::npos && (infix.empty() || pos2 != std::string::npos))
            {
                auto jointName = col.substr(0, pos);
                //            if (jointsToUse.empty() || std::find(jointsToUse.begin(), jointsToUse.end(), jointName) != jointsToUse.end())
                {


                    ARMARX_INFO_S << "Found joint: " << jointName;
                    nameToIndexMapping[jointName] = i;
                    JointInfo info;

                    for (auto& child : robot->getRobotNode(jointName)->getChildren())
                    {
                        if (child->getMass() > 0)
                        {
                            info.mass = child->getMass();
                            info.CoM = child->getCoMLocal().cast<double>();
                            info.bodyNode = dynamic_pointer_cast<VirtualRobot::RobotNode>(child);
                            info.optimize =
                                jointsToUse.empty() ||
                                std::find(jointsToUse.begin(), jointsToUse.end(), jointName) !=
                                    jointsToUse.end();
                            break;
                        }
                    }
                    if (info.bodyNode)
                    {
                        jointData[jointName] = info;
                        ARMARX_INFO_S << info.bodyNode->getName()
                                      << " mass: " << info.bodyNode->getMass()
                                      << " com: " << info.CoM;
                        //                info.node->print();
                    }
                    else
                    {
                        throw LocalException("No child with mass found for ") << jointName;
                    }
                }
            }
            else
            {
                auto pos = col.find(".adc1");
                if (pos != std::string::npos)
                {
                    auto sensorName = col.substr(0, pos);
                    ftSensorNameToIndexMapping[sensorName] = i;
                }
            }
            i++;
        }
    }

    void
    EstimateDynamicParameters::CSVHelper::addLine(const std::string& line)
    {
        auto columns = armarx::Split(line, ", ", true, false);
        std::map<std::string, JointSensorValues> newData;
        for (auto& pair : nameToIndexMapping)
        {
            JointSensorValues info;
            info.jointValue = armarx::toFloat(columns.at(pair.second));
            info.torque = armarx::toFloat(columns.at(pair.second + 2));
            info.torqueTicks = armarx::toFloat(columns.at(pair.second + 3));
            newData[pair.first] = info;
        }
        if ((float)(rand() % 1000) / 1000.0f > 0.1f)
        {
            jointSensorData.push_back(newData);
        }
        else
        {
            jointSensorTestData.push_back(newData);
        }


        //    std::map<std::string, Eigen::Vector6i> newFtData;
        //    for (auto& pair : nameToIndexMapping)
        //    {
        //        Eigen::Vector6i ftAdc;
        //        int i = pair.second;
        //        ftAdc << std::atoi(columns.at(i + 0).c_str()),
        //              std::atoi(columns.at(i + 1).c_str()),
        //              std::atoi(columns.at(i + 2).c_str()),
        //              std::atoi(columns.at(i + 3).c_str()),
        //              std::atoi(columns.at(i + 4).c_str()),
        //              std::atoi(columns.at(i + 5).c_str());
        //        newFtData[pair.first] = ftAdc;
        //    }
        //    ftSensorData.push_back(newFtData);
    }

    std::pair<double, Eigen::Vector3f>
    EstimateDynamicParameters::CalculateMass(VirtualRobot::SensorPtr sensorNode)
    {
        auto parent = sensorNode->getParent();
        double massSum = 0;
        Eigen::Vector3f averageCoM;
        std::set<VirtualRobot::SceneObjectPtr> allChildren;
        std::vector<std::string> childrenNames;
        auto children = parent->getChildren();
        while (!children.empty())
        {
            auto child = children.back();
            if (!allChildren.count(child))
            {
                allChildren.insert(child);
                auto newChildren = child->getChildren();
                children.pop_back();
                children.insert(children.end(), newChildren.begin(), newChildren.end());
            }
        }
        for (auto& child : allChildren)
        {
            if (dynamic_pointer_cast<VirtualRobot::RobotNode>(child))
            {
                childrenNames.push_back(child->getName());
            }
        }
        ARMARX_DEBUG_S << "CoM nodes: " << childrenNames;
        VirtualRobot::RobotNodeSetPtr handNodeSet =
            VirtualRobot::RobotNodeSet::createRobotNodeSet(sensorNode->getRobotNode()->getRobot(),
                                                           "handBodiesSet" + sensorNode->getName(),
                                                           childrenNames,
                                                           parent->getName());
        massSum = handNodeSet->getMass();

        Eigen::Vector3f comLocationLocal =
            parent->toLocalCoordinateSystemVec(handNodeSet->getCoM());
        ARMARX_INFO_S << VAROUT(massSum) << VAROUT(comLocationLocal);
        return std::make_pair(massSum, comLocationLocal);
    }

    void
    EstimateDynamicParameters::CSVHelper::parseBusXML(const std::string& busXMLPath)
    {

        auto reader = RapidXmlReader::FromFile(busXMLPath);
        for (auto& node : reader->getRoot().nodes("Joint"))
        {
            auto name = node.attribute_value("name");
            auto it = jointData.find(name);
            if (it != jointData.end())
            {
                auto torqueNode = node.first_node("SensorBoard")
                                      .first_node("ConversionParameters")
                                      .first_node("actualTorque");
                it->second.torqueTickFactor = torqueNode.attribute_as_float("factor");
                it->second.torqueTickOffset = torqueNode.attribute_as_float("offset");
                ARMARX_INFO_S << name << " Offset " << it->second.torqueTickOffset
                              << " factor : " << it->second.torqueTickFactor;
            }
        }
        return;
        auto ticksToVoltFactor = reader->getRoot()
                                     .first_node("DefaultConfiguration")
                                     .first_node("FTSensorDefaultConversionParameters")
                                     .first_node("voltage")
                                     .value_as_float();
        for (auto& node : reader->getRoot().nodes("ForceTorqueSensor"))
        {
            auto name = node.attribute_value("name");
            auto sensor = robot->getSensor(name);
            if (sensor)
            {
                std::tie(ftInfoData[name].mass, ftInfoData[name].comLocationLocal) =
                    CalculateMass(sensor);
                ftInfoData[name].sensorNode = sensor;
                ftInfoData[name].ticksToVoltFactor = ticksToVoltFactor;

                std::string FTCalibrationMatrixString =
                    node.first_node("FTCalibrationMatrix").value();
                boost::trim_if(FTCalibrationMatrixString, boost::is_any_of("\t \n"));
                Ice::StringSeq FTCalibrationMatrixValueStrings;
                boost::algorithm::split(FTCalibrationMatrixValueStrings,
                                        FTCalibrationMatrixString,
                                        boost::is_any_of("\t \n"),
                                        boost::token_compress_on);
                ARMARX_CHECK_EQUAL(FTCalibrationMatrixValueStrings.size(), 36);
                ftInfoData[name].calibrationMatrix.resize(6, 6);
                int i = 0;
                for (auto val : FTCalibrationMatrixValueStrings)
                {
                    ftInfoData[name].calibrationMatrix(i / 6, i % 6) = atof(val.c_str());
                    i++;
                }
            }
        }
    }

    Ice::DoubleSeq
    EstimateDynamicParameters::CSVHelper::toInitialConfiguration()
    {
        Ice::DoubleSeq config;
        for (auto& joint : jointData)
        {
            if (!joint.second.optimize)
            {
                continue;
            }
            config.push_back(joint.second.CoM(0));
            config.push_back(joint.second.CoM(1));
            config.push_back(joint.second.CoM(2));
            if (optimizeMass)
            {
                config.push_back(joint.second.mass);
            }
            else
            {
                config.push_back(joint.second.torqueTickFactor);
                config.push_back(joint.second.torqueTickOffset);
            }
        }
        return config;
    }

    Ice::DoubleSeq
    EstimateDynamicParameters::CSVHelper::toConfiguration()
    {
        Ice::DoubleSeq config;
        for (auto& joint : jointData)
        {
            if (!joint.second.optimize)
            {
                continue;
            }
            config.push_back(joint.second.bodyNode->getCoMLocal()(0));
            config.push_back(joint.second.bodyNode->getCoMLocal()(1));
            config.push_back(joint.second.bodyNode->getCoMLocal()(2));
            if (optimizeMass)
            {
                config.push_back(joint.second.bodyNode->getMass());
            }
            else
            {
                config.push_back(joint.second.torqueTickFactor);
                config.push_back(joint.second.torqueTickOffset);
            }
        }
        return config;
    }

    std::string
    EstimateDynamicParameters::CSVHelper::JointInfoToString(
        const EstimateDynamicParameters::JointInfoMap& jointData)
    {
        std::stringstream s;
        for (auto& joint : jointData)
        {
            if (!joint.second.optimize)
            {
                continue;
            }
            s << joint.first << " : \n";
            s << "Center of Mass : \n" << joint.second.CoM << "\n";
            s << "Mass : " << joint.second.mass << " kg\n";
            s << "Torque Tick Factor : " << joint.second.torqueTickFactor << "\n";
            s << "Torque Tick Offset : " << joint.second.torqueTickOffset << "\n\n";
        }
        return s.str();
    }

    std::string
    EstimateDynamicParameters::CSVHelper::configurationToString(const Ice::DoubleSeq& configuration)
    {
        JointInfoMap jointData;
        fromConfiguration(configuration, jointData);
        return JointInfoToString(jointData);
    }

    std::pair<Ice::DoubleSeq, Ice::DoubleSeq>
    EstimateDynamicParameters::CSVHelper::getBounds() const
    {
        Ice::DoubleSeq lowerBounds, upperBounds;
        double lowerBoundFactor = 0.2;
        double upperBoundFactor = 2.0;
        double comOffset = 60.0;
        for (auto& joint : jointData)
        {
            if (!joint.second.optimize)
            {
                continue;
            }
            lowerBounds.push_back(joint.second.CoM(0) - comOffset);
            lowerBounds.push_back(joint.second.CoM(1) - comOffset);
            lowerBounds.push_back(joint.second.CoM(2) - comOffset);

            if (optimizeMass)
            {
                lowerBounds.push_back(joint.second.mass * lowerBoundFactor);
            }
            else
            {
                lowerBounds.push_back(joint.second.torqueTickFactor > 0
                                          ? joint.second.torqueTickFactor * lowerBoundFactor
                                          : joint.second.torqueTickFactor * upperBoundFactor);
                lowerBounds.push_back(joint.second.torqueTickOffset > 0
                                          ? joint.second.torqueTickOffset * lowerBoundFactor
                                          : joint.second.torqueTickOffset * upperBoundFactor);
            }


            upperBounds.push_back(joint.second.CoM(0) + comOffset);
            upperBounds.push_back(joint.second.CoM(1) + comOffset);
            upperBounds.push_back(joint.second.CoM(2) + comOffset);

            if (optimizeMass)
            {
                upperBounds.push_back(joint.second.mass * upperBoundFactor);
            }
            else
            {
                upperBounds.push_back(joint.second.torqueTickFactor > 0
                                          ? joint.second.torqueTickFactor * upperBoundFactor
                                          : joint.second.torqueTickFactor * lowerBoundFactor);
                upperBounds.push_back(joint.second.torqueTickOffset > 0
                                          ? joint.second.torqueTickOffset * upperBoundFactor
                                          : joint.second.torqueTickOffset * lowerBoundFactor);
            }
        }
        return std::make_pair(lowerBounds, upperBounds);
    }

    const EstimateDynamicParameters::JointInfoMap&
    EstimateDynamicParameters::CSVHelper::getJointData() const
    {
        return jointData;
    }

    void
    EstimateDynamicParameters::CSVHelper::applyGaussianFilterToTorque(
        float filterRadius,
        StaticPlotterInterfacePrx plotter)
    {
        TrajectoryPtr traj(new Trajectory());
        TrajectoryPtr trajTicks(new Trajectory());

        double t = 0;
        double timestep = 0.001;
        Ice::DoubleSeq pos;
        StringVector2fSeqDict ticks;
        for (auto& data : jointSensorData)
        {
            int d = 0;
            for (auto& pair : data)
            {
                trajTicks->setPositionEntry(t, d, pair.second.torqueTicks);
                ticks[pair.first].push_back(Vector2f{(float)t, pair.second.torqueTicks});
                traj->setPositionEntry(t, d, pair.second.torque);
                d++;
            }
            if (int(t * 1000) % 10000 == 0)
            {
                ARMARX_INFO_S << "Reading data .... " << t << " ";
            }
            t += timestep;
        }
        ARMARX_INFO_S << "Applying filter...this might take a while";
        TrajectoryPtr trajTicksOrig = TrajectoryPtr::dynamicCast(trajTicks->clone());
        trajTicks->gaussianFilter(filterRadius);
        traj->gaussianFilter(filterRadius);
        ARMARX_INFO_S << trajTicksOrig->getState(0) << " vs. " << trajTicks->getState(0);
        ARMARX_INFO_S << trajTicksOrig->getState(1) << " vs. " << trajTicks->getState(1);

        t = 0;
        for (auto& data : jointSensorData)
        {
            int d = 0;
            for (auto& pair : data)
            {
                pair.second.torque = traj->getState(t, d, 0);
                pair.second.torqueTicks = trajTicks->getState(t, d, 0);
                ticks[pair.first + "_filtered"].push_back(
                    Vector2f{(float)t, pair.second.torqueTicks});
                d++;
            }
            if (int(t * 1000) % 10000 == 0)
            {
                ARMARX_INFO_S << "Reading data .... " << t << " ";
            }
            t += timestep;
        }

        if (plotter)
        {
            plotter->addPlot("torqueTicks", ticks);
        }
    }

    const std::vector<std::map<std::string, Eigen::Vector6i>>&
    EstimateDynamicParameters::CSVHelper::getFtSensorData() const
    {
        return ftSensorData;
    }

    void
    EstimateDynamicParameters::CSVHelper::addBodies(const Ice::StringSeq& bodies)
    {
        for (auto& body : bodies)
        {
            auto node = robot->getRobotNode(body);
            ARMARX_CHECK_EXPRESSION(node) << body;
            ARMARX_CHECK_EXPRESSION(node->getMass() > 0) << body;
            JointInfo info;
            info.mass = node->getMass();
            info.CoM = node->getCoMLocal().cast<double>();
            info.bodyNode = dynamic_pointer_cast<VirtualRobot::RobotNode>(node);
            info.optimize = true;
            info.onlyBody = true;
            jointData[body] = info;
        }
    }

    const std::map<std::string, double>&
    EstimateDynamicParameters::CSVHelper::getLossScaling() const
    {
        return lossScaling;
    }

    bool
    EstimateDynamicParameters::CSVHelper::getOptimizeMass() const
    {
        return optimizeMass;
    }

    void
    EstimateDynamicParameters::CSVHelper::setOptimizeMass(bool value)
    {
        optimizeMass = value;
    }

    void
    EstimateDynamicParameters::CSVHelper::fromConfiguration(const Ice::DoubleSeq& configuration,
                                                            JointInfoMap& jointData)
    {
        int i = 0;
        if (jointData.empty())
        {
            jointData = this->jointData;
        }
        for (auto& joint : jointData)
        {
            if (!joint.second.optimize)
            {
                continue;
            }
            joint.second.CoM(0) = configuration.at(i++);
            joint.second.CoM(1) = configuration.at(i++);
            joint.second.CoM(2) = configuration.at(i++);

            if (optimizeMass)
            {
                joint.second.mass = configuration.at(i++);
            }
            else
            {
                joint.second.torqueTickFactor = configuration.at(i++);
                joint.second.torqueTickOffset = configuration.at(i++);
            }
        }
    }

    EstimateDynamicParameters::JointInfoMap
    EstimateDynamicParameters::CSVHelper::applyConfigurationToRobot(
        const Ice::DoubleSeq& configuration)
    {
        //    JointInfoMap jointData;
        //    ARMARX_INFO << VAROUT(configuration);
        fromConfiguration(configuration, jointData);
        for (auto& joint : jointData)
        {
            auto& info = joint.second;
            info.bodyNode->setCoMLocal(info.CoM.cast<float>());
            info.bodyNode->setMass(info.mass);
            //        ARMARX_INFO << info.bodyNode->getName() << " mass: " << info.bodyNode->getMass() << " com: " << info.bodyNode->getCoMLocal();
        }
        return jointData;
    }

    const std::vector<std::map<std::string, EstimateDynamicParameters::JointSensorValues>>&
    EstimateDynamicParameters::CSVHelper::getJointSensorData() const
    {
        return jointSensorData;
    }

    const std::vector<std::map<std::string, EstimateDynamicParameters::JointSensorValues>>&
    EstimateDynamicParameters::CSVHelper::getJointSensorTestData() const
    {
        return jointSensorTestData;
    }
} // namespace armarx
